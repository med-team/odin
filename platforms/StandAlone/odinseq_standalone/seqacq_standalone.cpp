#include "seqacq_standalone.h"

SeqAcqStandAlone::SeqAcqStandAlone(const SeqAcqStandAlone& sas) {
  set_label(sas.get_label());
}

bool SeqAcqStandAlone::prep_driver(kSpaceCoord& recoindex, double sweepwidth,unsigned int nAcqPoints, double acqcenter, int freqchannel) {
  Log<SeqStandAlone> odinlog(this,"prep_driver");

  adc_curve.label=get_label().c_str();
  adc_curve.channel=rec_plotchan;
  adc_curve.spikes=true;

  double acqdur=secureDivision(double(nAcqPoints),sweepwidth);
  double dt=secureInv(sweepwidth);
  ODINLOG(odinlog,normalDebug) << "acqdur/dt/nAcqPoints=" << acqdur << "/" << dt << "/" << nAcqPoints << STD_endl;

  adc_curve.resize(nAcqPoints);
  for(unsigned int i=0; i<nAcqPoints; i++) {
    adc_curve.x[i]=(double(i)+0.5)*dt;
    adc_curve.y[i]=1.0;
  }


  // mark end of acquisition
  endacq_mark.label=get_label().c_str();
  endacq_mark.marklabel=markLabel[endacq_marker];
  endacq_mark.marker=endacq_marker;
  endacq_mark.marker_x=double(nAcqPoints)*dt;
  ODINLOG(odinlog,normalDebug) << "endacq_mark.marker_x=" << endacq_mark.marker_x << STD_endl;

  // ADC with no marker at the the center
  adc_curve_nomark=adc_curve;

  ODINLOG(odinlog,normalDebug) << "acqcenter/acqdur=" << acqcenter << "/" << acqdur << STD_endl;
  if(acqcenter>=0.0 && acqcenter<=acqdur) {
    adc_curve.marker=acquisition_marker;
    adc_curve.marklabel=markLabel[acquisition_marker];
    adc_curve.marker_x=acqcenter;
  }

  if(dump2console) {
    STD_cout << adc_curve << STD_endl;
    STD_cout << adc_curve_nomark << STD_endl;
    STD_cout << endacq_mark << STD_endl;
  }

  return true;
}


void SeqAcqStandAlone::event(eventContext& context, double start) const {
  Log<SeqStandAlone> odinlog(this,"event");

  // plot center and end marker
  append_curve2plot(start,&adc_curve,current_rf_rec_freq,current_rf_rec_phase);
  append_curve2plot(start,&endacq_mark);
}
