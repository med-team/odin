#include "reco.h"

#include <tjutils/tjlog.h>
#include <tjutils/tjtest.h>


static const char* recoFlagTrue="X";
static const char* recoFlagFalse="-";


void kSpaceCoord::reset2defaults() {
  number=0;
  reps=1;

  adcSize=0;
  channels=1;
  preDiscard=0;
  postDiscard=0;
  concat=1;
  oversampling=1.0;
  relcenter=0.5;
  readoutIndex=-1;
  trajIndex=-1;
  weightIndex=-1;
  dtIndex=0;

  for(int i=0; i<n_recoIndexDims; i++) index[i]=0;

  flags=0;
  flags=recoLastInChunkBit|flags;
}



bool kSpaceCoord::operator == (const kSpaceCoord& ri) const {
  for(int i=0; i<n_recoIndexDims; i++) {
    if(index[i]!=ri.index[i]) return false;
  }
  if(ri.adcSize!=adcSize) return false;
  if(ri.channels!=channels) return false;
  if(ri.preDiscard!=preDiscard) return false;
  if(ri.postDiscard!=postDiscard) return false;
  if(ri.concat!=concat) return false;
  if(ri.oversampling!=oversampling) return false;
  if(ri.relcenter!=relcenter) return false;
  if(ri.readoutIndex!=readoutIndex) return false;
  if(ri.trajIndex!=trajIndex) return false;
  if(ri.weightIndex!=weightIndex) return false;
  if(ri.dtIndex!=dtIndex) return false;
  if(ri.flags!=flags) return false;
  return true; // all equal
}



bool kSpaceCoord::operator < (const kSpaceCoord& ri) const {
  for(int i=0; i<n_recoIndexDims; i++) {
    if(index[i]!=ri.index[i]) return (index[i]<ri.index[i]);
  }
  if(adcSize!=ri.adcSize) return (adcSize<ri.adcSize);
  if(channels!=ri.channels) return (channels<ri.channels);
  if(preDiscard!=ri.preDiscard) return (preDiscard<ri.preDiscard);
  if(postDiscard!=ri.postDiscard) return (postDiscard<ri.postDiscard);
  if(concat!=ri.concat) return (concat<ri.concat);
  if(oversampling!=ri.oversampling) return (oversampling<ri.oversampling);
  if(relcenter!=ri.relcenter) return (relcenter<ri.relcenter);
  if(readoutIndex!=ri.readoutIndex) return (readoutIndex<ri.readoutIndex);
  if(trajIndex!=ri.trajIndex) return (trajIndex<ri.trajIndex);
  if(weightIndex!=ri.weightIndex) return (weightIndex<ri.weightIndex);
  if(dtIndex!=ri.dtIndex) return (dtIndex<ri.dtIndex);
  if(flags!=ri.flags) return (flags<ri.flags);
  return false; // all equal
}


STD_string kSpaceCoord::print_header(const unsigned short* numof_cache) {
  STD_string result;
  result+="number,";
  result+="reps,";
  result+="adcSize,";
  result+="channels,";
  result+="preDiscard,";
  result+="postDiscard,";
  result+="concat,";
  result+="oversampling,";
  result+="relcenter,";
  result+="readoutIndex,";
  result+="trajIndex,";
  result+="weightIndex,";
  result+="dtIndex,";
  for(int i=0; i<n_recoIndexDims; i++) {
    if(numof_cache[i]>1) result+=STD_string(recoDimLabel[i])+",";
  }
  result+="lastinchunk,";
  result+="reflect";

  return result;
}


STD_string kSpaceCoord::printcoord(const unsigned short* numof_cache) const {
  STD_string result;
  STD_string tab(",");
  result+=itos(number)+tab;
  result+=itos(reps)+tab;
  result+=itos(adcSize)+tab;
  result+=itos(channels)+tab;
  result+=itos(preDiscard)+tab;
  result+=itos(postDiscard)+tab;
  result+=itos(concat)+tab;
  result+=ftos(oversampling)+tab;
  result+=ftos(relcenter)+tab;
  result+=itos(readoutIndex)+tab;
  result+=itos(trajIndex)+tab;
  result+=itos(weightIndex)+tab;
  result+=itos(dtIndex)+tab;
  for(int i=0; i<n_recoIndexDims; i++) {
    if(numof_cache[i]>1) result+=index2string(index[i], recoDim(i))+tab;
  }
  if(flags&recoLastInChunkBit) result+=recoFlagTrue+tab; else result+=recoFlagFalse+tab;
  if(flags&recoReflectBit)     result+=recoFlagTrue;     else result+=recoFlagFalse;

  return result;
}

int kSpaceCoord::string2index(const STD_string& str, recoDim dim) {
  int result=0;
  bool numeric_encoding=true; // Fallback: Convert string to int

  // Special treatment of template dimension which has characters to index different template types (for extensibility)
  if(dim==templtype) {
    if(str.size()) {
      char templchar=str[0];
      if(templchar>='A' && templchar<='Z') {
        for(int itempl=0; itempl<n_templateTypes; itempl++) {
          if(templchar==templateTypeChar[itempl]) {
            result=itempl;
            numeric_encoding=false;
          }
        }
      }
    }
  }

  // Special treatment of navigator dimension which has characters to index different template types (for extensibility)
   if(dim==navigator) {
    if(str.size()) {
      char navchar=str[0];
      if(navchar>='a' && navchar<='z') {
        for(int inav=0; inav<n_navigatorTypes; inav++) {
          if(navchar==navigatorTypeChar[inav]) {
            result=inav;
            numeric_encoding=false;
          }
        }
      }
    }
  }

  if(numeric_encoding) result=atoi(str.c_str());
  return result;
}


STD_string kSpaceCoord::index2string(int index, recoDim dim, int numof) {
  STD_string result;
  bool numeric_encoding=true;
  if(dim==templtype) { // Special treatment of template dimension which has characters to index different template types (for extensibility)
    if(index>=0 && index<n_templateTypes) {
      result=STD_string(1,templateTypeChar[index]);
      numeric_encoding=false;
    }
  }
  if(dim==navigator) { // Special treatment of navigator dimension which has characters to index different template types (for extensibility)
    if(index>=0 && index<n_navigatorTypes) {
      result=STD_string(1,navigatorTypeChar[index]);
      numeric_encoding=false;
    }
  }
  if(numeric_encoding) {
    result=itos(index,STD_max(0,numof-1));
  }
  return result;
}


bool kSpaceCoord::parsecoord(const STD_string& str) {
  Log<Para> odinlog("kSpaceCoord","parsecoord");
  reset2defaults();

  svector toks(tokens(str,','));

  int ntoks=toks.size();
  if(ntoks<max_parsepos) {
    ODINLOG(odinlog,errorLog) << "Not enough tokens in line" << STD_endl;
    return false;
  }

  if(parsepos_number>=0) number=atoi(toks[parsepos_number].c_str());
  if(parsepos_reps>=0) reps=atoi(toks[parsepos_reps].c_str());
  if(parsepos_adcSize>=0) adcSize=atoi(toks[parsepos_adcSize].c_str());
  if(parsepos_channels>=0) channels=atoi(toks[parsepos_channels].c_str());
  if(parsepos_preDiscard>=0) preDiscard=atoi(toks[parsepos_preDiscard].c_str());
  if(parsepos_postDiscard>=0) postDiscard=atoi(toks[parsepos_postDiscard].c_str());
  if(parsepos_concat>=0) concat=atoi(toks[parsepos_concat].c_str());
  if(parsepos_oversampling>=0) oversampling=atof(toks[parsepos_oversampling].c_str());
  if(parsepos_relcenter>=0) relcenter=atof(toks[parsepos_relcenter].c_str());
  if(parsepos_readoutIndex>=0) readoutIndex=atoi(toks[parsepos_readoutIndex].c_str());
  if(parsepos_trajIndex>=0) trajIndex=atoi(toks[parsepos_trajIndex].c_str());
  if(parsepos_weightIndex>=0) weightIndex=atoi(toks[parsepos_weightIndex].c_str());
  if(parsepos_dtIndex>=0) dtIndex=atoi(toks[parsepos_dtIndex].c_str());
  for(int i=0; i<n_recoIndexDims; i++) {
    if(parsepos_index[i]>=0) {
      STD_string indexstr(toks[parsepos_index[i]]);
      index[i]=string2index(indexstr, recoDim(i));
    }
  }

  if(parsepos_lastinchunk>=0) if(toks[parsepos_lastinchunk]==recoFlagFalse) flags=flags&(recoLastInChunkBit^recoAllBits);
  if(parsepos_reflect>=0)     if(toks[parsepos_reflect]    ==recoFlagTrue)  flags=flags|recoReflectBit;

  return true;
}


int findval(const svector& strvec, const STD_string& val) {
  for(unsigned int i=0;i<strvec.size();i++) {
    if(strvec[i]==val) return i;
  }
  return -1;
}


void kSpaceCoord::assign_parsepos(const STD_string& header) {
  Log<Para> odinlog("kSpaceCoord","assign_parsepos");

  svector toks(tokens(header,','));

  max_parsepos=STD_max(max_parsepos,parsepos_number=findval(toks,"number"));
  max_parsepos=STD_max(max_parsepos,parsepos_reps=findval(toks,"reps"));
  max_parsepos=STD_max(max_parsepos,parsepos_adcSize=findval(toks,"adcSize"));
  max_parsepos=STD_max(max_parsepos,parsepos_channels=findval(toks,"channels"));
  max_parsepos=STD_max(max_parsepos,parsepos_preDiscard=findval(toks,"preDiscard"));
  max_parsepos=STD_max(max_parsepos,parsepos_postDiscard=findval(toks,"postDiscard"));
  max_parsepos=STD_max(max_parsepos,parsepos_concat=findval(toks,"concat"));
  max_parsepos=STD_max(max_parsepos,parsepos_oversampling=findval(toks,"oversampling"));
  max_parsepos=STD_max(max_parsepos,parsepos_relcenter=findval(toks,"relcenter"));
  max_parsepos=STD_max(max_parsepos,parsepos_readoutIndex=findval(toks,"readoutIndex"));
  max_parsepos=STD_max(max_parsepos,parsepos_trajIndex=findval(toks,"trajIndex"));
  max_parsepos=STD_max(max_parsepos,parsepos_weightIndex=findval(toks,"weightIndex"));
  max_parsepos=STD_max(max_parsepos,parsepos_dtIndex=findval(toks,"dtIndex"));
  for(int i=0; i<n_recoIndexDims; i++) max_parsepos=STD_max(max_parsepos,parsepos_index[i]=findval(toks,recoDimLabel[i]));
  max_parsepos=STD_max(max_parsepos,parsepos_lastinchunk=findval(toks,"lastinchunk"));
  max_parsepos=STD_max(max_parsepos,parsepos_reflect=findval(toks,"reflect"));

  ODINLOG(odinlog,normalDebug) << "max_parsepos=" << max_parsepos << STD_endl;
}



int kSpaceCoord::parsepos_number;
int kSpaceCoord::parsepos_reps;
int kSpaceCoord::parsepos_adcSize;
int kSpaceCoord::parsepos_channels;
int kSpaceCoord::parsepos_preDiscard;
int kSpaceCoord::parsepos_postDiscard;
int kSpaceCoord::parsepos_concat;
int kSpaceCoord::parsepos_oversampling;
int kSpaceCoord::parsepos_relcenter;
int kSpaceCoord::parsepos_readoutIndex;
int kSpaceCoord::parsepos_trajIndex;
int kSpaceCoord::parsepos_weightIndex;
int kSpaceCoord::parsepos_dtIndex;
int kSpaceCoord::parsepos_index[n_recoIndexDims];
int kSpaceCoord::parsepos_lastinchunk;
int kSpaceCoord::parsepos_reflect;

int kSpaceCoord::max_parsepos;



////////////////////////////////////////////////////////////////////////////


LDRkSpaceCoords::LDRkSpaceCoords() : state(coords_in_list) {
  Log<Para> odinlog(this,"LDRkSpaceCoords()");
  LDRkSpaceCoords::clear();
}


void LDRkSpaceCoords::clear() {
  Log<Para> odinlog(this,"clear");
  if(state==has_vec_alloc) {
    for(unsigned int i=0; i<size(); i++) {
      delete vec_cache[i];
    }
  }
  vec_cache.clear();
  for(int j=0; j<n_recoIndexDims; j++) numof_cache[j]=1;
  coordlist.clear();
  state=coords_in_list;
}


LDRkSpaceCoords& LDRkSpaceCoords::append_coord(const kSpaceCoord& coord) {
  state=coords_in_list;
  coord.number=coordlist.size();
  coordlist.push_back(coord);
  return *this;
}


void LDRkSpaceCoords::create_vec_cache() const {
  if(state==has_vec_cache || state==has_vec_alloc) return;

  for(int j=0; j<n_recoIndexDims; j++) numof_cache[j]=1;

  // move k-space coords from list to vector
  int adcindices_size=coordlist.size();
  vec_cache.resize(adcindices_size);
  int counter=0;
  for(STD_list<kSpaceCoord>::iterator it=coordlist.begin(); it!=coordlist.end(); ++it) {
    kSpaceCoord& coord=(*it);
    vec_cache[counter]=&coord;
    for(int j=0; j<n_recoIndexDims; j++) numof_cache[j]=STD_max(numof_cache[j],(unsigned short)(coord.index[j]+1));
    counter++;
  }
  state=has_vec_cache;
}


bool LDRkSpaceCoords::parsevalstring(const STD_string& parstring, const LDRserBase*) {
  Log<Para> odinlog(this,"parsevalstring");
  ODINLOG(odinlog,normalDebug) << "tokenizing ..." << STD_endl;
  svector toks(tokens(parstring));
  ODINLOG(odinlog,normalDebug) << "toks.size()=" << toks.size() << STD_endl;
  if(!toks.size()) return true;
  kSpaceCoord::assign_parsepos(toks[0]);
  unsigned int n_lines=toks.size()-1;
  ODINLOG(odinlog,normalDebug) << "parsing " << n_lines << " lines" << STD_endl;
  clear();
  vec_cache.resize(n_lines);
  bool result=true;
  ODINLOG(odinlog,normalDebug) << "parsing sinle tokens ..." << STD_endl;
  for(unsigned int i=0; i<n_lines; i++) {
    vec_cache[i]=new kSpaceCoord;
    if(!vec_cache[i]->parsecoord(toks[i+1])) result=false;
    for(int j=0; j<n_recoIndexDims; j++) numof_cache[j]=STD_max(numof_cache[j],(unsigned short)(vec_cache[i]->index[j]+1));
  }
  state=has_vec_alloc;
  ODINLOG(odinlog,normalDebug) << "size()=" << size() << STD_endl;
  return result;
}


STD_string LDRkSpaceCoords::printvalstring(const LDRserBase*) const {
  create_vec_cache();
  STD_string result(kSpaceCoord::print_header(numof_cache)+"\n");
  for(unsigned int i=0; i<size(); i++) {
    result+=(*this)[i].printcoord(numof_cache);
    result+="\n";
  }
  return result;
}


STD_ostream& LDRkSpaceCoords::print2stream(STD_ostream& os, const LDRserBase& serializer) const {
  create_vec_cache();
  os << kSpaceCoord::print_header(numof_cache) << "\n";
  unsigned int ncoords=size();
  for(unsigned int i=0; i<ncoords; i++) {
    os << (*this)[i].printcoord(numof_cache);
    if(i<(ncoords-1)) os << "\n";
  }
  return os;
}


//////////////////////////////////////////////////////////////////

LDRrecoValList::LDRrecoValList(const STD_string& ldrlabel) {
  Log<Para> odinlog(ldrlabel.c_str(), "LDRrecoValList(label)");
  set_label(ldrlabel);
}

LDRrecoValList& LDRrecoValList::operator = (const LDRrecoValList& jdrvl) {
  RecoValList::operator = (jdrvl);
  LDRbase::operator = (jdrvl);
  return *this;
}

LDRrecoValList& LDRrecoValList::operator = (const RecoValList& rvl) {
  STD_string label_backup(get_label());
  RecoValList::operator = (rvl);
  set_label(label_backup);
  return *this;
}


bool LDRrecoValList::parsevalstring (const STD_string& parstring, const LDRserBase*) {
  Log<Para> odinlog(this,"parsevalstring");
  STD_string sting4vallist(rmblock(parstring,"(",")",true,true));
  return ValList<int>::parsevallist(sting4vallist);
}


STD_string LDRrecoValList::printvalstring(const LDRserBase*) const {
  return "("+itos(size())+")\n"+tokenstring(tokens(ValList<int>::printvallist())); // nice formatting
}


STD_ostream& LDRrecoValList::print2stream(STD_ostream& os, const LDRserBase& serializer) const {
  os << "(" << itos(size()) << ")\n";
  ValList<int>::print2stream(os);
  return os;
}

//////////////////////////////////////////////////////////////////

void RecoPars::common_init() {
  Log<Para> odinlog(this,"common_init");
  cache_is_up2date=false;
  ReadoutDstSize.resize(MAX_NUMOF_READOUT_SHAPES);
}

RecoPars::RecoPars(const STD_string& label) : LDRblock(label) {
  Log<Para> odinlog(this,"RecoPars(label)");

  common_init();

  if(little_endian_byte_order()) LittleEndian=true;
  else LittleEndian=false;
  ODINLOG(odinlog,normalDebug) << "LittleEndian=" << bool(LittleEndian) << STD_endl;

  append_all_members();
}



RecoPars::RecoPars(const RecoPars& sr) {
  common_init();
  RecoPars::operator = (sr);
}


RecoPars& RecoPars::operator = (const RecoPars& sr) {
  LDRblock::operator = (sr);
  append_all_members();

  // copy only visible members of LDRblock
  copy_ldr_vals(sr);
  return (*this);
}


RecoPars& RecoPars::set_DimValues(recoDim dim, const dvector& vals) {
  Log<Para> odinlog(this,"set_DimValues");
  if(dim<n_recoIndexDims) DimValues[dim]=vals;
  ODINLOG(odinlog,normalDebug) << "DimValues[" << recoDimLabel[dim] << "]=" << DimValues[dim].printbody() << STD_endl;
  return *this;
}



int RecoPars::append_readout_shape(const fvector& shape, unsigned int dstsize) {
  for(unsigned int i=0; i<MAX_NUMOF_READOUT_SHAPES; i++) {
    if(!(ReadoutShape[i].length())) {
      ReadoutShape[i]=shape;
      ReadoutDstSize[i]=dstsize;
      return i;
    }
    if((const STD_vector<float>&)ReadoutShape[i]==(const STD_vector<float>&)shape && ReadoutDstSize[i]==int(dstsize)) { // cast is required for VxWorks
      return i;
    }
  }
  return -1;
}


int RecoPars::append_kspace_traj(const farray& kspace_traj) {
  for(unsigned int i=0; i<MAX_NUMOF_KSPACE_TRAJS; i++) {
    if(!(kSpaceTraj[i].length())) {
      kSpaceTraj[i]=kspace_traj;
      return i;
    }
    if((const STD_vector<float>&)kSpaceTraj[i]==(const STD_vector<float>&)kspace_traj) { // cast is required for VxWorks
      return i;
    }
  }

  return -1;
}


int RecoPars::append_adc_weight_vec(const cvector& weightvec) {
  for(unsigned int i=0; i<MAX_NUMOF_ADC_WEIGHTING_VECTORS; i++) {
    if(!(AdcWeightVector[i].length())) {
      AdcWeightVector[i]=weightvec;
      return i;
    }
    if((const STD_vector<STD_complex>&)AdcWeightVector[i]==(const STD_vector<STD_complex>&)weightvec) { // cast is required for VxWorks
      return i;
    }
  }
  return -1;
}


int RecoPars::append_dwell_time(double dt) {
  unsigned int n=DwellTime.length();
  unsigned int i;
  for(i=0; i<n; i++) {
    if(fabs(DwellTime[i]-dt)<1.0e-6) return i;
  }
  dvector dt_copy(DwellTime);
  DwellTime.resize(n+1);
  for(i=0; i<n; i++) {
    DwellTime[i]=dt_copy[i];
  }
  DwellTime[n]=dt;
  return n;
}



void RecoPars::get_ReadoutShape(unsigned int i, fvector& shape, unsigned int& dstsize) const {
  if(i>=MAX_NUMOF_READOUT_SHAPES) i=0;
  shape=ReadoutShape[i];
  dstsize=ReadoutDstSize[i];
}

unsigned int RecoPars::numof_kSpaceTraj() const {
  unsigned int result=0;
  for(unsigned int i=0; i<MAX_NUMOF_KSPACE_TRAJS; i++) {
    if(kSpaceTraj[i].length()) result++;
  }
  return result;
}


const farray& RecoPars::get_kSpaceTraj(unsigned int i) const {
  if(i>=MAX_NUMOF_KSPACE_TRAJS) i=0;
  return kSpaceTraj[i];
}

const cvector& RecoPars::get_AdcWeightVector(unsigned int i) const {
  if(i>=MAX_NUMOF_ADC_WEIGHTING_VECTORS) i=0;
  return AdcWeightVector[i];
}


double RecoPars::get_DwellTime(unsigned int i) const {
  if(i<DwellTime.length()) return DwellTime[i];
  return 0.0;
}


int RecoPars::get_NumOfAdcChunks() const {
  Log<Para> odinlog(this,"get_NumOfAdcChunks");
  int result=0;

  for(unsigned int i=0; i<kSpaceCoords.size(); i++) {
    const kSpaceCoord& kcoord=kSpaceCoords[i];
    if(kcoord.flags&recoLastInChunkBit) result+=kcoord.reps;
  }
  return result;
}


LONGEST_INT RecoPars::get_TotalNumOfSamples(bool discard) const {
  LONGEST_INT result=0;
  for(unsigned int i=0; i<kSpaceCoords.size(); i++) {
    const kSpaceCoord& kcoord=kSpaceCoords[i];
    unsigned long adcsize=kcoord.adcSize;
    if(discard) adcsize-=(kcoord.preDiscard+kcoord.postDiscard);
    result+=adcsize*(unsigned long)kcoord.channels*(unsigned long)kcoord.reps;
  }
  return result;
}


const kSpaceCoord& RecoPars::get_kSpaceCoord(unsigned int i) const {
  if(!cache_is_up2date) create_cache();
  return kSpaceCoords[kSpaceOrdering_cache[i]];
}


void RecoPars::create_cache() const {
  kSpaceOrdering_cache=kSpaceOrdering.get_values_flat();
  cache_is_up2date=true;
}


void RecoPars::reset() {
  int i;
  for(i=0; i<MAX_NUMOF_READOUT_SHAPES; i++) ReadoutShape[i].resize(0);
  for(i=0; i<MAX_NUMOF_KSPACE_TRAJS; i++)      kSpaceTraj[i].resize(0);
  for(i=0; i<MAX_NUMOF_ADC_WEIGHTING_VECTORS; i++) AdcWeightVector[i].resize(0);
  for(i=0; i<n_recoIndexDims; i++) DimValues[i].resize(0);
  DwellTime.resize(0);
  Recipe=""; // might not be set/reset by sequence
  PreProc3D=""; // might not be set/reset by sequence
  PostProc3D=""; // might not be set/reset by sequence
  CmdLineOpts=""; // might not be set/reset by sequence
  kSpaceCoords.clear();
  kSpaceOrdering.clear();
  kSpaceOrdering_cache.clear();
  cache_is_up2date=false;
}


void RecoPars::append_all_members() {
  Log<Para> odinlog(this,"append_all_members");
  int i;
  clear();
  append_member(prot,PROTOCOL_BLOCK_LABEL);
  append_member(DataFormat,"DataFormat");
  append_member(LittleEndian,"LittleEndian");
  append_member(RawFile,"RawFile");
  append_member(RawHeaderSize,"RawHeaderSize");
  append_member(RelativeOffset,"RelativeOffset");
  append_member(ImageProc,"ImageProc");
  append_member(ChannelScaling,"ChannelScaling");
  append_member(DwellTime,"DwellTime");


  for(i=0; i<MAX_NUMOF_READOUT_SHAPES; i++)   append_member(ReadoutShape[i],"ReadoutShape"+itos(i));
  append_member(ReadoutDstSize,"ReadoutDstSize");
  for(i=0; i<MAX_NUMOF_KSPACE_TRAJS; i++)      append_member(kSpaceTraj[i],"kSpaceTraj"+itos(i));
  for(i=0; i<MAX_NUMOF_ADC_WEIGHTING_VECTORS; i++) append_member(AdcWeightVector[i],"AdcWeightVector"+itos(i));
  for(i=0; i<n_recoIndexDims; i++) append_member(DimValues[i],"DimValues_"+STD_string(recoDimLabel[i]));

  append_member(Recipe,"Recipe");
  append_member(PreProc3D,"PreProc3D");
  append_member(PostProc3D,"PostProc3D");
  append_member(CmdLineOpts,"CmdLineOpts");

  append_member(kSpaceCoords,"kSpaceCoords");
  append_member(kSpaceOrdering,"kSpaceOrdering");
}

//////////////////////////////////////////////////////////////////


CoilSensitivity::CoilSensitivity(const STD_string& label) : LDRblock(label) {
  append_all_members();
}

CoilSensitivity::CoilSensitivity(const CoilSensitivity& cs) {
  CoilSensitivity::operator = (cs);
}

CoilSensitivity& CoilSensitivity::operator = (const CoilSensitivity& cs) {
  LDRblock::operator = (cs);
  append_all_members();

  // copy only visible members of LDRblock
  copy_ldr_vals(cs);
  return *this;
}

CoilSensitivity& CoilSensitivity::set_sensitivity_map(const carray& sens_map, float FOVx, float FOVy, float FOVz) {
  if(sens_map.dim()==4) {
    SensitivityMap=sens_map;
    FOV[0]=FOVx;
    FOV[1]=FOVy;
    FOV[2]=FOVz;
  }
  return *this;
}

STD_complex CoilSensitivity::get_sensitivity_value(unsigned int channel, float x, float y, float z) const {
  Log<Para> odinlog(this,"get_sensitivity_value");

  STD_complex result(0);

  ndim nn=SensitivityMap.get_extent();
  int nx=nn[3];
  int ny=nn[2];
  int nz=nn[1];
  int nchans=nn[0];

  if(int(channel) >= nchans) return result;

  float xDelta=secureDivision(FOV[0],nx);
  float yDelta=secureDivision(FOV[1],ny);
  float zDelta=secureDivision(FOV[2],nz);

  // indices of the grid square which contain the desired point
  float fx=(x+0.5*FOV[0])/xDelta;
  float fy=(y+0.5*FOV[1])/yDelta;
  float fz=(z+0.5*FOV[2])/zDelta;
  int ixlow=int(floor(fx-0.5)); int ixupp=int(floor(fx+0.5));
  int iylow=int(floor(fy-0.5)); int iyupp=int(floor(fy+0.5));
  int izlow=int(floor(fz-0.5)); int izupp=int(floor(fz+0.5));
  if(nx<=1) ixlow=ixupp=0;
  if(ny<=1) iylow=iyupp=0;
  if(nz<=1) izlow=izupp=0;

  if(ixlow==-1) ixlow=0;
  if(iylow==-1) iylow=0;
  if(izlow==-1) izlow=0;
  if(ixupp==nx) ixupp=nx-1;
  if(iyupp==ny) iyupp=ny-1;
  if(izupp==nz) izupp=nz-1;
  if(ixlow<0) return result;
  if(iylow<0) return result;
  if(izlow<0) return result;
  if(ixupp>=nx) return result;
  if(iyupp>=ny) return result;
  if(izupp>=nz) return result;
  ODINLOG(odinlog,normalDebug) << "ixlow/iylow/izlow(" << x << "," << y << "," << z << ")=" << ixlow << "/" << iylow << "/" << izlow << STD_endl;
  ODINLOG(odinlog,normalDebug) << "ixupp/iyupp/izupp(" << x << "," << y << "," << z << ")=" << ixupp << "/" << iyupp << "/" << izupp << STD_endl;

  float xlow=-0.5*FOV[0]+(float(ixlow)+0.5)*xDelta;
  float ylow=-0.5*FOV[1]+(float(iylow)+0.5)*yDelta;
  float zlow=-0.5*FOV[2]+(float(izlow)+0.5)*zDelta;

  // relative positions within the grid square
  float sx=(x-xlow)/xDelta; if(sx<0.0) sx=0.0; if(sx>1.0) sx=1.0;
  float sy=(y-ylow)/yDelta; if(sy<0.0) sy=0.0; if(sy>1.0) sy=1.0;
  float sz=(z-zlow)/zDelta; if(sz<0.0) sz=0.0; if(sz>1.0) sz=1.0;
  ODINLOG(odinlog,normalDebug) << "sx/sy/sz(" << x << "," << y << "," << z << ")=" << sx << "/" << sy << "/" << sz << STD_endl;

  result=STD_complex((1.0-sz)*(1.0-sy)*(1.0-sx))*SensitivityMap(channel,izlow,iylow,ixlow)
        +STD_complex((1.0-sz)*(1.0-sy)*(    sx))*SensitivityMap(channel,izlow,iylow,ixupp)
        +STD_complex((1.0-sz)*(    sy)*(1.0-sx))*SensitivityMap(channel,izlow,iyupp,ixlow)
        +STD_complex((1.0-sz)*(    sy)*(    sx))*SensitivityMap(channel,izlow,iyupp,ixupp)
        +STD_complex((    sz)*(1.0-sy)*(1.0-sx))*SensitivityMap(channel,izupp,iylow,ixlow)
        +STD_complex((    sz)*(1.0-sy)*(    sx))*SensitivityMap(channel,izupp,iylow,ixupp)
        +STD_complex((    sz)*(    sy)*(1.0-sx))*SensitivityMap(channel,izupp,iyupp,ixlow)
        +STD_complex((    sz)*(    sy)*(    sx))*SensitivityMap(channel,izupp,iyupp,ixupp);

  return result;
}



void CoilSensitivity::append_all_members() {
  clear();
  SensitivityMap.redim(1,1,1,1);
  SensitivityMap.set_filemode(compressed);
  append_member(FOV,"FOV");
  append_member(SensitivityMap,"SensitivityMap");
}

////////////////////////////////////////////////


#ifndef NO_UNIT_TEST
class CoilSensitivityTest : public UnitTest {

 public:
  CoilSensitivityTest() : UnitTest("CoilSensitivity") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");
    unsigned int i;
    int testsize=10;
    float testfov=200.0;

    CoilSensitivity sens;
    carray testmap(1,1,testsize,testsize);

    ndim nn=testmap.get_extent();
    ndim ii;
    float center=0.5*float(testsize-1);
    for(i=0; i<nn.total(); i++) {
      ii=nn.index2extent(i);
      float x=float(int(ii[3])-center);
      float y=float(int(ii[2])-center);
      float radius2=x*x+y*y;
      testmap(ii)=STD_complex(radius2);
    }
    float mean1=secureDivision(cabs(testmap.sum()),testmap.total());


    sens.set_sensitivity_map(testmap,testfov,testfov,testfov);

    int testsize2=100;

    carray testmap2(1,1,testsize2,testsize2);

    nn=testmap2.get_extent();
    for(i=0; i<nn.total(); i++) {
      ii=nn.index2extent(i);
      float x=(float(ii[3])/float(testsize2-1)-0.5)*testfov;
      float y=(float(ii[2])/float(testsize2-1)-0.5)*testfov;
      testmap2(ii)=sens.get_sensitivity_value(0, x, y, 0.25*testfov);
    }
    float mean2=secureDivision(cabs(testmap2.sum()),testmap2.total());

    if(fabs(mean1-mean2)>1.0) {
      ODINLOG(odinlog,errorLog) <<  "mean1=" << mean1 << STD_endl;
      ODINLOG(odinlog,errorLog) <<  "mean2=" << mean2 << STD_endl;
      return false;
    }

    return true;
  }

};

void alloc_CoilSensitivityTest() {new CoilSensitivityTest();} // create test instance
#endif
