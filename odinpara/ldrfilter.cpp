#include "ldrfilter.h"
#include "ldrnumbers.h"


class Gauss : public LDRfunctionPlugIn {

  LDRdouble filterwidth;

 public:
  Gauss() : LDRfunctionPlugIn("Gauss")  {

    filterwidth=0.36169; filterwidth.set_minmaxval(0.1,1.0);

    append_member(filterwidth,"FilterWidth");
  }

 private:

  float calculate_filter (float rel_kradius) const {
    float factor = (-1) * log (0.5) * secureInv( (filterwidth * filterwidth)) ;
    if(rel_kradius<0.0) rel_kradius=0.0;
    return exp(-(rel_kradius*rel_kradius)*factor);
  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new Gauss;}
};


///////////////////////////////////////////////////////////



class NoFilter : public LDRfunctionPlugIn {

 public:

  NoFilter() : LDRfunctionPlugIn("NoFilter") {}

  float calculate_filter (float rel_kradius) const {
    if(rel_kradius>1.0) return 0.0;
    return 1.0;
  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new NoFilter;}
};

///////////////////////////////////////////////////////////

class Triangle : public LDRfunctionPlugIn {

 public:

  Triangle() : LDRfunctionPlugIn("Triangle") { }

  float calculate_filter (float rel_kradius) const {
    if(rel_kradius<0.0) rel_kradius=0.0;
    if(rel_kradius>1.0) rel_kradius=1.0;
    return 1.0-rel_kradius;
  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new Triangle;}
};

///////////////////////////////////////////////////////////

class Hann : public LDRfunctionPlugIn {

 public:

  Hann() : LDRfunctionPlugIn("Hann") { }

  float calculate_filter (float rel_kradius) const {
    if(rel_kradius<0.0) rel_kradius=0.0;
    if(rel_kradius>1.0) rel_kradius=1.0;
    return 0.5*(1.0+cos(rel_kradius*PII));
  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new Hann;}
};


///////////////////////////////////////////////////////////

class Hamming : public LDRfunctionPlugIn {

 public:

  Hamming() : LDRfunctionPlugIn("Hamming") { }

  float calculate_filter (float rel_kradius) const {
    if(rel_kradius<0.0) rel_kradius=0.0;
    if(rel_kradius>1.0) rel_kradius=1.0;
	 return (0.53836+0.46164*cos(rel_kradius*PII));
  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new Hamming;}
};

///////////////////////////////////////////////////////////

class CosSq : public LDRfunctionPlugIn {

 public:

  CosSq() : LDRfunctionPlugIn("CosSq") { }

 private:
  float calculate_filter (float rel_kradius) const {
    if(rel_kradius<0.0) rel_kradius=0.0;
    if(rel_kradius>1.0) rel_kradius=1.0;

    double value=pow(cos(rel_kradius*PII/2.0),2.0);
    return (value);
  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new CosSq;}
};

///////////////////////////////////////////////////////////

class Blackman : public LDRfunctionPlugIn {

 public:

  Blackman() : LDRfunctionPlugIn("Blackman") { }

  float calculate_filter (float rel_kradius) const {
    if(rel_kradius<0.0) rel_kradius=0.0;
    if(rel_kradius>1.0) rel_kradius=1.0;
    return 0.42+0.5*cos(rel_kradius*PII)+0.08*cos(2.0*rel_kradius*PII);
  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new Blackman;}
};

///////////////////////////////////////////////////////////

class BlackmanNuttall : public LDRfunctionPlugIn {

 public:

  BlackmanNuttall() : LDRfunctionPlugIn("BlackmanNuttall") {  }


 private:

  float calculate_filter (float rel_kradius) const {

    if(rel_kradius<0.0) rel_kradius=0.0;
    if(rel_kradius>1.0) rel_kradius=1.0;

    float step = PII*0.5*rel_kradius;
    float a0 = 0.3635819;
    float a1 = 0.4891775;
    float a2 = 0.1365995;
    float a3 = 0.0106411;
    double value=a0+a1*cos(2.0*step)+a2*cos(4.0*step)+a3*cos(6.0*step);
    return (value);
  }
  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new BlackmanNuttall;}
};


///////////////////////////////////////////////////////////

class Exp : public LDRfunctionPlugIn {

 LDRdouble filterwidth;

 public:
  Exp() : LDRfunctionPlugIn("Exp")  {

  }

 private:

  float calculate_filter (float rel_kradius) const {

    return exp(-rel_kradius);
  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new Exp;}
};


///////////////////////////////////////////////////////////

/*
class Fermi : public LDRfunctionTemplate<Fermi> {

 public:

  Fermi() : LDRfunctionTemplate<Fermi>("Fermi") { }

  float calculate (float knorm, float kernel) const {
    float dummy=(knorm*kernel*2.0-8.0);
    return 1.0/(exp(dummy)+1.0);
  }
};
*/

///////////////////////////////////////////////////////////


void LDRfilter::init_static() {

  (new Gauss)->register_function(filterFunc, funcMode(0));
  (new NoFilter)->register_function(filterFunc, funcMode(0));
  (new Triangle)->register_function(filterFunc, funcMode(0));
  (new Hann)->register_function(filterFunc, funcMode(0));
  (new Hamming)->register_function(filterFunc, funcMode(0));
  (new CosSq)->register_function(filterFunc, funcMode(0));
  (new Blackman)->register_function(filterFunc, funcMode(0));
  (new BlackmanNuttall)->register_function(filterFunc, funcMode(0));
  (new Exp)->register_function(filterFunc, funcMode(0));
//  (new Fermi)->register_function(filterFunc, funcMode(0));

}

void LDRfilter::destroy_static() {
  // pulgins will be deleted by LDRfunction
}

EMPTY_TEMPL_LIST bool StaticHandler<LDRfilter>::staticdone=false;

