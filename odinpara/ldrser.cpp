#include "ldrser.h"
#include "ldrbase.h"
#include "ldrblock.h"
#include "ldrnumbers.h" // for UnitTest
#include "ldrtypes.h" // for UnitTest
#include <tjutils/tjtest.h>


STD_string LDRserJDX::get_prefix(const LDRbase& ldr) const {
  STD_string result;
  if(ldr.get_typeInfo()=="LDRblock") {
    result="##TITLE="+ldr.get_label()+"\n";
    result+="##JCAMPDX=4.24\n";
    result+="##DATATYPE=Parameter Values\n";
  } else {
    result="##";
    if(ldr.get_jdx_props().userdef_parameter) result+="$";
    result+=ldr.get_label();
    result+="=";
  }
  return result;
}

STD_string LDRserJDX::get_postfix(const LDRbase& ldr) const {
  if(ldr.get_typeInfo()=="LDRblock") {
    return "##END=\n";
  }
  return "\n";
}


STD_string LDRserJDX::extract_valstring(const STD_string& parstring) const {
  Log<LDRcomp> odinlog("LDRserJDX","extract_valstring");

  STD_string result;

  ODINLOG(odinlog,normalDebug) << "parstring: >" << parstring << "<" << STD_endl;

  STD_string from_hashes=extract(parstring,"##",""); // from first hash to the end
  ODINLOG(odinlog,normalDebug) << "from_hashes: >" << from_hashes << "<" << STD_endl;
  if(from_hashes=="") return result;

  STD_string to_hashes;
  if(noccur(from_hashes,"\n##")>0) {
    to_hashes=extract(from_hashes,"","\n##"); // to the next double hash
  } else {
    to_hashes=from_hashes;
  }
  ODINLOG(odinlog,normalDebug) << "to_hashes: >" << to_hashes << "<" << STD_endl;


  result=extract(to_hashes,"=",""); // everything after equal sign

  ODINLOG(odinlog,normalDebug) << "result: >" << result << "<" << STD_endl;

  return result;
}


STD_string LDRserJDX::remove_comments(const STD_string& parstring) const {
  STD_string result;

  result=rmblock(parstring, "\n$$", "\n", true, false); // Remove single-line comments
  result=rmblock(result, "$$", "\n", true, false); // Remove remaining end-line comments

  return result;
}


STD_string LDRserJDX::escape_characters(const STD_string& parstring) const {
/*
  Log<LDRcomp> odinlog("LDRserJDX","escape_characters");
  ODINLOG(odinlog,normalDebug) << "parstring: >" << parstring << "<" << STD_endl;

  STD_string result;
  if (parstring.length()>=2 && parstring[(unsigned int)0]=='<' && parstring[parstring.length()-1]=='>') {
    result="\""+parstring+"\"";
  } else {
    result=parstring;
  }
  ODINLOG(odinlog,normalDebug) << "result: >" << result << "<" << STD_endl;
  return result;
*/
  return parstring;
}

STD_string LDRserJDX::deescape_characters(const STD_string& parstring) const {
/*
  STD_string result;
  STD_string extracted=extract(parstring,"\"<",">\"",true);
  if(extracted!="") {
    result="<"+extracted+">";
  } else {
    result=parstring;
  }
  return result;
*/
  return parstring;
}


STD_string LDRserJDX::print_string(const STD_string& str) const {
  Log<LDRcomp> odinlog("LDRserJDX","print_string");
  STD_string result;
  if(get_jdx_compatmode()==bruker) {
    ndim nn(1);
    int l=str.length()*_BRUKER_MODE_STRING_CAP_FACTOR_;
    if(!l) l=_BRUKER_MODE_STRING_CAP_START_;
    if(l<_BRUKER_MODE_STRING_MIN_SIZE_) l=_BRUKER_MODE_STRING_MIN_SIZE_;
    nn[0]=l;
    result+=STD_string(nn)+"\n";
  }

  if(get_jdx_compatmode()==bruker) {
    result+="<"+str+">";
  } else {
    if(str.length()>=2 && str[(unsigned int)0]=='<' && str[str.length()-1]=='>') {
      result+="<"+str+">"; // add extra delimiters in non-bruker mode if the string itself contains <> at the beginning/end
    } else {
      result+=str;
    }
  }
  ODINLOG(odinlog,normalDebug) << "returning >" << result << "<" << STD_endl;
  return result;
}


STD_string LDRserJDX::parse_string(const STD_string& parstring) const {
  Log<LDRcomp> odinlog("LDRserJDX","parse_string");
  STD_string result;
  ODINLOG(odinlog,normalDebug) << "str: >" << parstring << "<" << STD_endl;
  STD_string without_size_field;
  if(get_jdx_compatmode()==bruker) without_size_field=extract(parstring,"\n","");
  else without_size_field=parstring;
  ODINLOG(odinlog,normalDebug) << "without_size_field: >" << without_size_field << "<" << STD_endl;
  STD_string shrinked=shrink(without_size_field);
  ODINLOG(odinlog,normalDebug) << "shrinked: >" << shrinked << "<" << STD_endl;
  if (shrinked.length()>=2 && shrinked[(unsigned int)0]=='<' && shrinked[shrinked.length()-1]=='>') result=extract(without_size_field,"<",">",true);
  else result=without_size_field;
  return result;
}


STD_string LDRserJDX::get_blocklabel(const STD_string& parstring) const {
  STD_string result;
  STD_string parlabel=extract(parstring,"##","=");
  if(parlabel=="TITLE") {
    result=extract(parstring,"##TITLE=","\n");
  }
  return result;
}


STD_string LDRserJDX::get_blockbody(const STD_string& parstring, bool including_delimiters) const {
  STD_string body=extract(parstring,"##TITLE=","\n##END=",true); // Extract body of parameter block, regarding nested structure (blocks within blocks)
  if(including_delimiters) return "##TITLE="+body+"\n##END=";
  return body;
}


STD_string LDRserJDX::get_parlabel(const STD_string& parstring) const {
  STD_string parlabel(extract(parstring,"##","="));
//  if(parlabel=="END") return ""; // break while loop in calling code
  if (parlabel[0]=='$') {
    parlabel+="=";
    parlabel=extract(parlabel,"$","=");
  }
  if(parlabel=="TITLE") {
    parlabel=extract(parstring,"##TITLE=","\n");
  }
  return parlabel;
}

void LDRserJDX::remove_next_ldr(STD_string& parstring) const {

  if(noccur(parstring,"##")>1) {
    parstring=rmblock(parstring,"##","##",true,false,false,false);
  } else {
    parstring="";
  }

}


///////////////////////////////////////////////////////////////////////

STD_string LDRserXML::str_between_delimiters(const STD_string& parstring, STD_string& startdelim, STD_string& enddelim) const {
  Log<LDRcomp> odinlog("LDRserXML","str_between_delimiters");

  STD_string result;

  STD_string parlabel=get_parlabel(parstring);
  ODINLOG(odinlog,normalDebug) << "parlabel: >" << parlabel << "<" << STD_endl;

  STD_string argsstr=extract(parstring,"<"+parlabel,">");
  ODINLOG(odinlog,normalDebug) << "argsstr: >" << argsstr << "<" << STD_endl;

  startdelim="<"+parlabel+argsstr+">";
  enddelim="</"+parlabel+">";
  ODINLOG(odinlog,normalDebug) << "startdelim-enddelim: " << startdelim << "-" << enddelim << STD_endl;

  result=extract(parstring,startdelim,enddelim,true);

  return result;
}


STD_string LDRserXML::create_well_formed_tag(const STD_string& ldrlabel) {
  STD_string result(ldrlabel);

  for(unsigned int i=0; i<result.length(); i++) {
    char c=result[i];
    if(i==0 && !(c=='_' || isalpha(c)) ) c='_'; // tags must start with a letter or underscore
    if(!( isalnum(c) || c=='-' || c=='_' || c=='-' )) c='_'; // tags can contain letters, digits, hyphens, underscores, and periods
    result[i]=c;
  }
  if(tolowerstr(result).find("xml")==0) result="_"+result; // tags cannot start with the letters xml

  return result;
}


STD_string LDRserXML::get_prefix(const LDRbase& ldr) const {
  STD_string result;
  result="<"+create_well_formed_tag(ldr.get_label())+">";
  if(ldr.get_typeInfo()=="LDRblock") result+="\n";
  return result;
}

STD_string LDRserXML::get_postfix(const LDRbase& ldr) const {
  STD_string result;
//  if(ldr.get_typeInfo()!="LDRblock") result+="\n";
  result+="</"+create_well_formed_tag(ldr.get_label())+">\n";
  return result;
}


STD_string LDRserXML::extract_valstring(const STD_string& parstring) const {
  Log<LDRcomp> odinlog("LDRserXML","extract_valstring");
  STD_string result;

  STD_string startdelim;
  STD_string enddelim;
  result=str_between_delimiters(parstring,startdelim,enddelim);

  ODINLOG(odinlog,normalDebug) << "result: >" << result << "<" << STD_endl;

  return result;
}


STD_string LDRserXML::remove_comments(const STD_string& parstring) const {
  STD_string result;

  result=rmblock(parstring, "<!--", "-->", true, true, true, true);
  result=rmblock(result, "<?", "?>", true, true, true, true);

  return result;
}

STD_string LDRserXML::escape_characters(const STD_string& parstring) const {
  STD_string result;
  result=replaceStr(result,"&","&amp;");
  result=replaceStr(parstring,"\"","&quot;");
  result=replaceStr(result,"<","&lt;");
  result=replaceStr(result,">","&gt;");
  return result;
}

STD_string LDRserXML::deescape_characters(const STD_string& parstring) const {
  STD_string result;
  result=replaceStr(parstring,"&quot;","\"");
  result=replaceStr(result,"&lt;","<");
  result=replaceStr(result,"&gt;",">");
  result=replaceStr(result,"&amp;","&");
  return result;
}

STD_string LDRserXML::parse_string(const STD_string& parstring) const {
  STD_string result;
  if (parstring.length()>=2 && parstring[(unsigned int)0]=='\'' && parstring[parstring.length()-1]=='\'') result=extract(parstring,"'","'",true);
  else result=parstring;
  return result;
}

STD_string LDRserXML::get_blocklabel(const STD_string& parstring) const {
  Log<LDRcomp> odinlog("LDRserXML","get_blocklabel");
  STD_string result;

  STD_string blockbody=get_blockbody(parstring, false);
  ODINLOG(odinlog,normalDebug) << "blockbody: >" << blockbody << "<" << STD_endl;

  if(extract(blockbody,"<",">")!="") { // any tags inside?
    result=get_parlabel(parstring);
  }

  ODINLOG(odinlog,normalDebug) << "result: >" << result << "<" << STD_endl;

  return result;
}


STD_string LDRserXML::get_blockbody(const STD_string& parstring, bool including_delimiters) const {
  Log<LDRcomp> odinlog("LDRserXML","get_blockbody");
  STD_string result;

  STD_string startdelim;
  STD_string enddelim;
  result=str_between_delimiters(parstring,startdelim,enddelim);

  if(including_delimiters) result=startdelim+result+enddelim;

  return result;
}


STD_string LDRserXML::get_parlabel(const STD_string& parstring) const {
  Log<LDRcomp> odinlog("LDRserXML","get_parlabel");
  STD_string result;
  STD_string label_with_args=extract(parstring,"<",">",true);
  svector toks=tokens(label_with_args); // strip arguments
  if(toks.size()) result=toks[0];
  ODINLOG(odinlog,normalDebug) << "result: >" << result << "<" << STD_endl;
  return result;
}


void LDRserXML::remove_next_ldr(STD_string& parstring) const {
  Log<LDRcomp> odinlog("LDRserXML","remove_next_ldr");

  STD_string startdelim;
  STD_string enddelim;
  STD_string valstr=str_between_delimiters(parstring,startdelim,enddelim);

  ODINLOG(odinlog,normalDebug) << "startdelim-enddelim: " << startdelim << "-" << enddelim << STD_endl;

  parstring=rmblock(parstring,startdelim,enddelim,true,true,false,true);
}



///////////////////////////////////////////////////////////////////////




#ifndef NO_UNIT_TEST


class LDRserXMLTest : public UnitTest {

 public:
  LDRserXMLTest()
    : UnitTest("LDRserXML") {}


  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    LDRserXML xmlser;

    LDRint malformed_at_first_char(1,"1malformed");
    STD_string expected="<_malformed>1</_malformed>\n";
    STD_string printed=malformed_at_first_char.print(xmlser);
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "malformed_at_first_char failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }

    LDRint malformed_tag(1,"a:B*c< D >e-f");
    expected="<a_B_c__D__e-f>1</a_B_c__D__e-f>\n";
    printed=malformed_tag.print(xmlser);
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "malformed_tag failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }

    LDRint malformed_contains_xml_at_beginning(1,"xmlabc");
    expected="<_xmlabc>1</_xmlabc>\n";
    printed=malformed_contains_xml_at_beginning.print(xmlser);
    if(printed!=expected) {
      ODINLOG(odinlog,errorLog) << "malformed_contains_xml_at_beginning failed: got >" << printed << "<, but expected >" << expected << "<" << STD_endl;
      return false;
    }

    return true;
  }

};



template <class SerializerType>
class LDRserBlockTest : public UnitTest {

 public:
  LDRserBlockTest()
    : UnitTest("LDRblock("+SerializerType().get_default_file_prefix()+")") {}


  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    SerializerType ser;


    // Testing nested blocks

    LDRint ldrint11(11,"ldrint11");
    LDRint ldrint12(12,"ldrint12");
    LDRint ldrint2(2,"ldrint2");

    const char* teststr1="strtest";
    const char* teststr2="<screwmeup>";
    LDRstring ldrstr1(teststr1,"ldrstr1");
    LDRstring ldrstr2(teststr2,"ldrstr2");

    LDRfloat* pldrfloat=new LDRfloat(1.23,"pldrfloat");

    LDRblock block1("block1");
    LDRblock block2("block2");
 
    block2.append(ldrint2);
    block1.append(ldrstr1);
    block1.append(ldrstr2);
    block1.append(ldrint11);
    block1.append(block2);
    block1.append(*pldrfloat);
    block1.append(ldrint12);

    STD_string tmpfname=tempfile()+"."+ser.get_default_file_prefix();
    if(block1.write(tmpfname,ser)<0) {
      ODINLOG(odinlog,errorLog) << "block1.write(" << tmpfname << ") failed" << STD_endl;
      return false;
    }

    ldrint11=0;
    ldrint12=0;
    ldrint2=0;
    ldrstr1="";
    ldrstr2="";
    delete pldrfloat; // remove from block

    if(block1.load(tmpfname,ser)<0) {
      ODINLOG(odinlog,errorLog) << "block1.load(" << tmpfname << ") failed" << STD_endl;
      return false;
    }

    if(ldrint11!=11 || ldrint12!=12 || ldrint2!=2 || ldrstr1!=teststr1 || ldrstr2!=teststr2) {
      ODINLOG(odinlog,errorLog) << "block1 after load(" << tmpfname << "):" << STD_endl << block1.print(ser) << STD_endl;
      return false;
    }

    return true;
  }

};



void alloc_LDRserTest() {
  new LDRserXMLTest;
  new LDRserBlockTest<LDRserJDX>;
  new LDRserBlockTest<LDRserXML>;
}
#endif

