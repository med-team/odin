#include "seqacqepi.h"
#include "seqgradchanparallel.h"
#include "seqgradvec.h"
#include "seqveciter.h"


SeqEpiDriverDefault::SeqEpiDriverDefault() {
  SeqAcqInterface::set_marshall(&adc);
  SeqFreqChanInterface::set_marshall(&adc);
  templtype=no_template;
}

SeqEpiDriverDefault::SeqEpiDriverDefault(const SeqEpiDriverDefault& sedi)
 : SeqEpiDriver(sedi) {
  SeqAcqInterface::set_marshall(&adc);
  SeqFreqChanInterface::set_marshall(&adc);

  adc=sedi.adc;
  acqdelay_begin=sedi.acqdelay_begin;
  acqdelay_middle=sedi.acqdelay_middle;
  acqdelay_end=sedi.acqdelay_end;
  posread=sedi.posread;
  negread=sedi.negread;
  phaseblip1st=sedi.phaseblip1st;
  phaseblip2nd=sedi.phaseblip2nd;
  phasezero1st=sedi.phasezero1st;
  phasezero2nd=sedi.phasezero2nd;
  phasezero_lastblip=sedi.phasezero_lastblip;
  gradkernel=sedi.gradkernel;
  lastgradkernel=sedi.lastgradkernel;
  oneadckernel=sedi.oneadckernel;
  adckernel=sedi.adckernel;
  lastadckernel=sedi.lastadckernel;
  kernel=sedi.kernel;
  lastkernel=sedi.lastkernel;
  loop=sedi.loop;

  gradint2center_read=sedi.gradint2center_read;
  gradint2center_phase=sedi.gradint2center_phase;
  centerindex_phase=sedi.centerindex_phase;
  readshape=sedi.readshape;

  templtype=sedi.templtype;
  echopairs=sedi.echopairs;
  lastecho=sedi.lastecho;

  build_seq();
}


void SeqEpiDriverDefault::init_driver(const STD_string& object_label,double sweepwidth,
            float kread_min,  float kread_max,  unsigned readntps,
            float kphase_min, float kphase_max, unsigned phasentps,int startindex_phase,
            bool ramp_sampling,rampType rampmode, float ramp_steepness,
            const STD_string& nucleus, const dvector& phaselist, const dvector& freqlist, unsigned int echo_pairs) {
  set_label(object_label);

  Log<Seq> odinlog(this,"init_driver");

  echopairs=echo_pairs;

  double read_integral=kread_max-kread_min;

  double readgraddur=secureDivision(double(readntps),sweepwidth);
  float readstrength=secureDivision(read_integral,readgraddur);

//  double rastertime=systemInfo->get_rastertime(gradObj);
//  if(rastertime>0.0) {
//    int nraster=(int)secureDivision(readgraddur,rastertime);
//    if(rastertime*nraster!=readgraddur) nraster++; // always increase length of constant part
//    readgraddur=nraster*rastertime;
//  }

  ODINLOG(odinlog,normalDebug) << "readntps/sweepwidth=" << readntps << "/" << sweepwidth << STD_endl;
  ODINLOG(odinlog,normalDebug) << "kread_max/kread_min=" << kread_max << "/" << kread_min << STD_endl;
  ODINLOG(odinlog,normalDebug) << "readgraddur/readstrength=" << readgraddur << "/" << readstrength << STD_endl;

  adc.set_sweepwidth(sweepwidth,1.0); // To obtain correct pre/post delays

  double adcpredelay=get_acquisition_start();
  double adcpostdelay=adc.get_duration()-adc.get_acquisition_duration()-adcpredelay;
  double minadcdelay=STD_max(adcpredelay,adcpostdelay); // The min delay at either sides of the ADC kernel, i.e. prior to posread ADC and after negread
  ODINLOG(odinlog,normalDebug) << "adcpredelay/adcpostdelay/minadcdelay=" << adcpredelay << "/" << adcpostdelay << "/" << minadcdelay << STD_endl;

  double rastime=systemInfo->get_rastertime(gradObj); // Sampling rate for ramps

  posread=SeqGradTrapez(object_label+"_posread",readDirection, readstrength,readgraddur,rastime,rampmode,minadcdelay,ramp_steepness);
  negread=SeqGradTrapez(object_label+"_negread",readDirection,-readstrength,readgraddur,rastime,rampmode,minadcdelay,ramp_steepness);
  ODINLOG(odinlog,normalDebug) << "posread/negread.get_duration()=" << posread.get_duration() << "/" << negread.get_duration() << STD_endl;

  double rampondur= posread.get_onramp_duration();
  double rampoffdur=posread.get_offramp_duration();
  double rampdur=0.5*(rampondur+rampoffdur);
  ODINLOG(odinlog,normalDebug) << "rampondur/rampoffdur/rampdur=" << rampondur << "/" << rampoffdur << "/" << rampdur << STD_endl;

  // defaults for non-ramp-sampling
  double adc2adc_duration=2.0*rampdur; // Duration from the end of acquisition of one ADC to the acquisition start of the next
  unsigned int nAcqPoints=readntps;
  float addint4deph=posread.get_onramp_integral(); // Extra dephaser for ramps

  unsigned int ramp_npoints=0;
  float  rel_gradstartval=1.0;

  ODINLOG(odinlog,normalDebug) << "ramp_sampling=" << ramp_sampling << STD_endl;
  if(ramp_sampling) {

    // Shorten constant part of read gradient according to available ramp integral
    double  onrampintegral_acq=posread.get_onramp_integral(minadcdelay,rampondur);
    double offrampintegral_acq=posread.get_offramp_integral(0.0,rampoffdur-minadcdelay);
    double rampintegral_acq=onrampintegral_acq+offrampintegral_acq;
    ODINLOG(odinlog,normalDebug) << "onrampintegral_acq/offrampintegral_acq/rampintegral_acq=" << onrampintegral_acq << "/" << offrampintegral_acq << "/" << rampintegral_acq << STD_endl;

    // calculate the shortening of the readout gradient as multiples of the dwell time
    double subtract_duration=secureDivision(rampintegral_acq, readstrength);
    unsigned int subtract_npoints=(unsigned int)(subtract_duration*sweepwidth+0.5);
    double subtract_duration_grid=secureDivision(subtract_npoints,sweepwidth);
    ODINLOG(odinlog,normalDebug) << "subtract_duration/subtract_npoints/subtract_duration_grid=" << subtract_duration << "/" << subtract_npoints << "/" << subtract_duration_grid << STD_endl;

    double newconstdur=posread.get_constgrad_duration()-subtract_duration_grid;
    ODINLOG(odinlog,normalDebug) << "newconstdur=" << newconstdur << STD_endl;
    posread.set_constgrad_duration(newconstdur);
    negread.set_constgrad_duration(newconstdur);
    ODINLOG(odinlog,normalDebug) << "posread/negread.get_duration()=" << posread.get_duration() << "/" << negread.get_duration() << STD_endl;

    // Number of extra points we can stuff into the ramps
    double effective_rampdur=rampdur-minadcdelay;
    ramp_npoints=(unsigned int)(effective_rampdur*sweepwidth); // round down so that the duration of the ADC kernel will not exceed the duration of the gradient kernel
    ODINLOG(odinlog,normalDebug) << "effective_rampdur/ramp_npoints=" << effective_rampdur << "/" << ramp_npoints << STD_endl;

    // New number of samples in ADC
    nAcqPoints=nAcqPoints-subtract_npoints+2*ramp_npoints;


    // Shortest possible delay between ADCs
    adc2adc_duration=2.0*minadcdelay;
    ODINLOG(odinlog,normalDebug) << "adc2adc_duration(rampsampling)=" << adc2adc_duration << STD_endl;

    // The relative gradient strength the ramp starts with
    rel_gradstartval=secureDivision(minadcdelay,rampdur);
    ODINLOG(odinlog,normalDebug) << "rel_gradstartval=" << rel_gradstartval << STD_endl;

    // Additional dephasing integral to account for the part of the ramp without sampling
    addint4deph=0.5*minadcdelay*readstrength*rel_gradstartval;
  }

  adc=SeqAcq(object_label+"_adc",nAcqPoints,sweepwidth,1.0,nucleus,phaselist,freqlist);
  ODINLOG(odinlog,normalDebug) << "adc.get_duration()=" << adc.get_duration() << STD_endl;


  // Construct readout shape for reco
  readshape.resize(nAcqPoints);
  readshape=1.0;
  unsigned int i;
  for(i=0; i<ramp_npoints; i++) {
    float rel_rampval=rel_gradstartval+secureDivision(double(i),double(ramp_npoints))*(1.0-rel_gradstartval);
    readshape[i]=rel_rampval;
    readshape[nAcqPoints-1-i]=rel_rampval;
  }

  // The dephasing integrals
  gradint2center_read=-kread_min+addint4deph;
  gradint2center_phase=-kphase_min;

  // Timing delays between ADCs
  double total_acqdelay=posread.get_gradduration()+negread.get_gradduration()-2.0*adc.get_duration();
  if(total_acqdelay<0.0) {
    ODINLOG(odinlog,warningLog) << "Timing mismatch: negative total_acqdelay=" << total_acqdelay << STD_endl;
    total_acqdelay=0.0;
  }

  double middle_acqdelay=0.5*total_acqdelay;
  ODINLOG(odinlog,normalDebug) << "total_acqdelay/middle_acqdelay=" << total_acqdelay << "/" << middle_acqdelay << STD_endl;

  double begin_delaydur=0.5*(adcpostdelay-adcpredelay+middle_acqdelay);
  ODINLOG(odinlog,normalDebug) << "begin_delaydur=" << begin_delaydur << STD_endl;
  if(begin_delaydur<0.0) {
    ODINLOG(odinlog,warningLog) << "Timing mismatch: negative begin_delaydur=" << begin_delaydur << STD_endl;
    begin_delaydur=0.0;
  }
  double end_delaydur  =middle_acqdelay-begin_delaydur; // the remaining time
  ODINLOG(odinlog,normalDebug) << "end_delaydur=" << end_delaydur << STD_endl;
  if(end_delaydur<0.0) {
    ODINLOG(odinlog,warningLog) << "Timing mismatch: negative end_delaydur=" << end_delaydur << STD_endl;
    end_delaydur=0.0;
  }

  ODINLOG(odinlog,normalDebug) << "begin_delaydur/end_delaydur=" << begin_delaydur << "/" << end_delaydur << STD_endl;

  // Try to shift ADC within possible range
  double max_possible_shift=STD_min(begin_delaydur,end_delaydur);
  double sysgradshift=systemInfo->get_grad_shift_delay();
  if(fabs(sysgradshift)>max_possible_shift) {
    ODINLOG(odinlog,warningLog) << "sysgradshift=" << sysgradshift << " exceeds max_possible_shift=" << max_possible_shift << STD_endl;
    if(sysgradshift<0.0) sysgradshift=-max_possible_shift;
    else                 sysgradshift= max_possible_shift;
  }

  double acqdelay_shifted_begin=begin_delaydur+sysgradshift;
  double acqdelay_shifted_end  =end_delaydur  -sysgradshift;

  acqdelay_begin=SeqDelay(object_label+"_acqdelay_begin",acqdelay_shifted_begin);
  acqdelay_end=SeqDelay(object_label+"_acqdelay_end",acqdelay_shifted_end);

  acqdelay_middle=SeqDelay(object_label+"_acqdelay_middle",middle_acqdelay);


  float blipint=secureDivision(kphase_max-kphase_min,float(phasentps));

  if(phasentps<=1) blipint=0.0; // We do not need blips in this case

  ODINLOG(odinlog,normalDebug) << "blipint=" << blipint << STD_endl;

  centerindex_phase=int(secureDivision(-kphase_min,kphase_max-kphase_min)*float(phasentps));

  if(echopairs>0) centerindex_phase*=(2*echopairs);

  // The actual phase encoding blips
  phaseblip1st=SeqGradTrapez(object_label+"_phaseblip1st",blipint,phaseDirection,0.0,rastime,rampmode,rampdur);
  phaseblip2nd=SeqGradTrapez(object_label+"_phaseblip2nd",blipint,phaseDirection,0.0,rastime,rampmode,rampdur);
  if(phaseblip1st.get_gradduration()!=2.0*rampdur) {
    ODINLOG(odinlog,warningLog) << "Timing mismatch: phaseblip1st(" << phaseblip1st.get_gradduration() << ") != 2.0*rampdur(" << 2.0*rampdur << ")" << STD_endl;
  }
  phaseblip2nd.exclude_offramp_from_timing(true);
  if(phaseblip2nd.get_gradduration()!=2.0*rampdur) {
    ODINLOG(odinlog,warningLog) << "Timing mismatch: phaseblip2nd(" << phaseblip2nd.get_gradduration() << ") != 2.0*rampdur(" << 2.0*rampdur << ")" << STD_endl;
  }

  // The padding delays
  double phasezerodur1=posread.get_constgrad_duration()+phaseblip2nd.get_offramp_duration();
  double phasezerodur2=posread.get_constgrad_duration();
  double phasezerodur_lastblip=posread.get_gradduration()+negread.get_onramp_duration()+negread.get_constgrad_duration();
  phasezero1st=SeqGradDelay(object_label+"_phasezero1st",phaseDirection,phasezerodur1);
  phasezero2nd=SeqGradDelay(object_label+"_phasezero2nd",phaseDirection,phasezerodur2);
  phasezero_lastblip=SeqGradDelay(object_label+"_phasezero_lastblip",phaseDirection,phasezerodur_lastblip);


  lastecho=bool(phasentps%2);
  int n_kernels=phasentps/2;
  ODINLOG(odinlog,normalDebug) << "n_kernels/startindex_phase/phasentps/lastecho=" << n_kernels << "/" << startindex_phase << "/" << phasentps  << "/" << lastecho << STD_endl;


  gradkernel.set_label(object_label+"_gradkernel");
  lastgradkernel.set_label(object_label+"_lastgradkernel");
  oneadckernel.set_label(object_label+"_oneadckernel");
  adckernel.set_label(object_label+"_adckernel");
  lastadckernel.set_label(object_label+"_lastadckernel");
  kernel.set_label(object_label+"_kernel");
  lastkernel.set_label(object_label+"_lastkernel");

  loop.set_label(object_label+"_loop");
  loop.set_times(n_kernels);

  build_seq();
}


unsigned int SeqEpiDriverDefault::get_numof_gradechoes() const {
  Log<Seq> odinlog(this,"get_numof_gradechoes");
  unsigned int looptimes=loop.get_times();
  unsigned int result=2*looptimes+int(lastecho);
  if(echopairs>0) result*=2*echopairs;
  ODINLOG(odinlog,normalDebug) << "looptimes/lastecho/echopairs/result=" << looptimes << "/" << lastecho << "/" << echopairs << "/" << result << STD_endl;
  return result;
}


SeqAcqInterface& SeqEpiDriverDefault::set_template_type(templateType type) {
  templtype=type;

  adc.set_template_type(type);

  if(type==phasecorr_template) {
    phaseblip1st.set_strength(0.0);
    phaseblip2nd.set_strength(0.0);
    gradint2center_phase=0.0;
  }

  build_seq();

  return *this;
}


fvector SeqEpiDriverDefault::get_gradintegral() const {
  fvector result(3);
  result=0;
  result += loop.get_times()*kernel.get_gradintegral();
  if(lastecho) result += lastkernel.get_gradintegral();
  return result;
}


double SeqEpiDriverDefault::get_acquisition_center() const {
  return (double(centerindex_phase)+0.5)*posread.get_gradduration();
}

double SeqEpiDriverDefault::get_acquisition_start() const {
  return adc.get_acquisition_start();
}


void SeqEpiDriverDefault::build_seq() {
  Log<Seq> odinlog(this,"build_seq");

  gradkernel.clear();
  lastgradkernel.clear();
  oneadckernel.clear();
  adckernel.clear();
  lastadckernel.clear();
  lastkernel.clear();

  oneadckernel = acqdelay_begin + adc + acqdelay_middle + adc + acqdelay_end;

  ODINLOG(odinlog,normalDebug) << "echopairs=" << echopairs << STD_endl;

  if(echopairs>0) {

    int nkernels=2*echopairs;

    for(int i=0; i<nkernels; i++) {

      if(i==(nkernels-1) || i==(nkernels/2-1)) {

        gradkernel += ( posread + negread ) /
                      ( phasezero_lastblip + phaseblip2nd );

      } else {
        gradkernel += ( posread + negread );

      }

      adckernel  += oneadckernel;
    }

    if(lastecho) {
      for(int i=0; i<echopairs; i++) {
        lastgradkernel += ( posread + negread );  // We do not need phaseblip here
        lastadckernel  += oneadckernel;
      }
    }


  } else {

    gradkernel += ( posread + negread ) /
                 ( phasezero1st + phaseblip1st + phasezero2nd + phaseblip2nd );
    adckernel  += oneadckernel;

    if(lastecho) {
      lastgradkernel += posread;  // We do not need phaseblip here
      lastadckernel  += acqdelay_begin + adc;
    }

  }


  ODINLOG(odinlog,normalDebug) << "gradkernel.get_duration()=" << gradkernel.get_duration() << STD_endl;
  ODINLOG(odinlog,normalDebug) << "adckernel.get_duration()=" << adckernel.get_duration() << STD_endl;

  kernel = gradkernel / adckernel;

  if(lastecho) lastkernel = lastgradkernel / lastadckernel;

  SeqObjList::clear();

  loop.set_body(kernel);
  (*this) += loop;

  if(lastecho) (*this) += lastkernel;

}


/////////////////////////////////////////////////////////////////////////////////////////

class SeqAcqEPIDephVec : public SeqGradVector {
 public:
  SeqAcqEPIDephVec() : SeqGradVector() {}
  SeqAcqEPIDephVec& operator = (const SeqGradVector& sgv) {SeqGradVector::operator = (sgv); return *this;}
  bool is_acq_vector() const {return true;} // overloaded function of SeqVector to tell that dephaser alters reco indices
};


struct SeqAcqEPIdephObjs {
  SeqGradTrapez readdephgrad;
  SeqGradTrapez readrephgrad;
  SeqGradTrapez phasedephgrad;
  SeqGradTrapez phaserephgrad;
  SeqAcqEPIDephVec phasesegdephgrad;
  SeqAcqEPIDephVec phasesegrephgrad;
};



void SeqAcqEPI::common_init() {
  readsize_os_cache=0;
  os_factor_cache=1.0;
  phasesize_cache=0;
  segments_cache=1;
  reduction_cache=1;
  echo_pairs_cache=0;
  blipint_cache=0.0;
  templtype_cache=no_template;
  ramptype_cache=linear;

  dephobjs=new SeqAcqEPIdephObjs;
}


SeqAcqEPI::~SeqAcqEPI() {
  delete dephobjs;
}



SeqAcqEPI::SeqAcqEPI(const STD_string& object_label, double sweepwidth,
                     unsigned int read_size, float FOVread,
                     unsigned int phase_size, float FOVphase,
                     unsigned int shots, unsigned int reduction, float os_factor, const STD_string& nucleus,
                     const dvector& phaselist, const dvector& freqlist,
                     rampType rampmode, bool ramp_sampling, float ramp_steepness,
                     float fourier_factor, unsigned int echo_pairs, bool invert_partial_fourier)
  : SeqObjBase(object_label), driver(object_label) {

  Log<Seq> odinlog(this,"SeqAcqEPI(...)");

  common_init();

  // Manage oversampling simply by increasing the number of points
  // and the sweep width by the given factor,
  // the drivers will see no oversampling
  readsize_os_cache=(unsigned int)(os_factor*read_size+0.5);
  os_factor_cache=os_factor;

  segments_cache=shots;
  if(!segments_cache || segments_cache>phase_size) segments_cache=1;

  reduction_cache=reduction;
  if(!reduction_cache || reduction_cache>phase_size) reduction_cache=1;

  unsigned int phase_increment=segments_cache*reduction_cache;

  phasesize_cache=(unsigned int)(phase_size/phase_increment)*phase_increment; // phase_size must be multiple of (segments * reduction factor)


  echo_pairs_cache=echo_pairs;

  float gamma=systemInfo->get_gamma(nucleus);

  float resread=secureDivision( FOVread , read_size );
  ODINLOG(odinlog,normalDebug) << "FOVread/read_size/resread=" << FOVread << "/" << read_size << "/" << resread << STD_endl;
  float resphase=secureDivision( FOVphase , phasesize_cache );
  ODINLOG(odinlog,normalDebug) << "FOVphase/phasesize_cache/resphase=" << FOVphase << "/" << phasesize_cache << "/" << resphase << STD_endl;

  float k_indep_read=secureDivision( 2.0*PII , gamma * resread  );
  float k_indep_phase=secureDivision( 2.0*PII , gamma * resphase );
  ODINLOG(odinlog,normalDebug) << "k_indep_read/k_indep_phase=" << k_indep_read << "/" << k_indep_phase << STD_endl;

  float k_indep_read_min=-0.5*k_indep_read;
  float k_indep_read_max=0.5*k_indep_read;


  float kspace_part=1.0-fourier_factor;
  if(kspace_part<0.0) kspace_part=0.0;
  if(kspace_part>1.0) kspace_part=1.0;
  float k_indep_phase_min;
  float k_indep_phase_max;
  if (invert_partial_fourier) {
    k_indep_phase_min=-0.5*k_indep_phase;
    k_indep_phase_max=0.5*kspace_part*k_indep_phase;
  } else {
    k_indep_phase_min=-0.5*kspace_part*k_indep_phase;
    k_indep_phase_max=0.5*k_indep_phase;
  }

  int phasenpts=(unsigned int)(float(phasesize_cache)*(0.5*kspace_part+0.5));
  int startindex_phase=phasesize_cache-phasenpts;

  // Achieve segmentation/GRAPPA while keeping FOV
  unsigned int driver_phasenpts=int(secureDivision(phasenpts,phase_increment)+0.5);
  unsigned int driver_startindex=int(secureDivision(startindex_phase,phase_increment)+0.5);

  blipint_cache=secureDivision(k_indep_phase_max-k_indep_phase_min, driver_phasenpts); // remember blip integral for dephasing segmented EPI

  ODINLOG(odinlog,normalDebug) << "phase_increment/driver_phasenpts/driver_startindex=" << phase_increment << "/" << driver_phasenpts << "/" << driver_startindex << STD_endl;


  // Use ADC of driver to adjust sweepwidth and use it in init_driver()
  // do this here so we do not have to do it separately in each driver
  // Thereby, add overampling at this point
  driver->set_sweepwidth(os_factor*sweepwidth,1.0);

  float maxgrad=systemInfo->get_max_grad();
  float expected_gradstrength=secureDivision(2.0*PII*secureDivision(driver->get_sweepwidth(),os_factor), gamma*FOVread);
  ODINLOG(odinlog,normalDebug) << "expected_gradstrength/maxgrad=" << expected_gradstrength << "/" << maxgrad << STD_endl;
  if(expected_gradstrength>maxgrad) {
    double strength_downscale=0.99*secureDivision(maxgrad, expected_gradstrength); // 1% Buffer
    ODINLOG(odinlog,normalDebug) << "strength_downscale/sweepwidth=" << strength_downscale << "/" << sweepwidth << STD_endl;
    sweepwidth*=strength_downscale;
    ODINLOG(odinlog,warningLog) << "Gradient strength (" << expected_gradstrength << ") exceeds maximum (" << maxgrad << "), scaling sweepwidth down (factor=" << strength_downscale << ") to " << sweepwidth  << ODIN_FREQ_UNIT << STD_endl;
    driver->set_sweepwidth(os_factor*sweepwidth,1.0); // let driver adjust sweepwidth
  }

  for(int itry=0; itry<10; itry++) { // several attempts possible to get a valid setup, but avois infinite loop

    ODINLOG(odinlog,normalDebug) << "[" << itry << "]sweepwidth/os_factor/driver->get_sweepwidth()=" << sweepwidth << "/" << os_factor << "/" << driver->get_sweepwidth() << STD_endl;

    // Initialize driver for only one segment (and/or GRAPPA interleave), different segments will be encoded by dephasing gradient
    driver->init_driver(object_label,driver->get_sweepwidth(),
              k_indep_read_min, k_indep_read_max,readsize_os_cache,
              k_indep_phase_min, k_indep_phase_max,
              driver_phasenpts, driver_startindex,
              ramp_sampling,rampmode,ramp_steepness,
              nucleus,phaselist,freqlist,echo_pairs);


    double gradfreq=secureInv(2.0*driver->get_echoduration());
    ODINLOG(odinlog,normalDebug) << "gradfreq[" << itry << "]=" << gradfreq << STD_endl;

    double low,upp;
    if(systemInfo->allowed_grad_freq(gradfreq, low, upp)) {
      break;
    } else {
      double downscale=STD_max(0.5,1.0-secureDivision(2.0*fabs(upp-low),gradfreq));
      ODINLOG(odinlog,normalDebug) << "downscale/sweepwidth=" << downscale << "/" << sweepwidth << STD_endl;
      sweepwidth*=downscale;
      ODINLOG(odinlog,warningLog) << "Gradient switching frequency (" << gradfreq  << ODIN_FREQ_UNIT << ") not allowed, scaling sweepwidth down (factor=" << downscale << ") to " << sweepwidth  << ODIN_FREQ_UNIT << STD_endl;
      driver->set_sweepwidth(os_factor*sweepwidth,1.0); // let driver adjust sweepwidth
    }
  }

  // Initialize dephasers/rephasers so that consecutive calls to get_dephgrad do not have to alter them
  create_deph_and_reph();

}


void SeqAcqEPI::create_deph_and_reph() {
  Log<Seq> odinlog(this,"create_deph_and_reph");

  float centint_read= driver->get_gradintegral2center_read();
  float centint_phase=driver->get_gradintegral2center_phase();
  fvector gradint_driver=driver->get_gradintegral();
  ODINLOG(odinlog,normalDebug) << "centint_read/centint_phase/gradint_driver=" << centint_read << "/" << centint_phase << "/" << gradint_driver.printbody() << STD_endl;

  float integral_deph_read  =-centint_read;
  float integral_deph_phase =-centint_phase;

  float integral_reph_read= -(gradint_driver[readDirection]- centint_read);
  float integral_reph_phase=-(gradint_driver[phaseDirection]-centint_phase);

  float maxintegral=STD_max( STD_max(fabs(integral_deph_read),fabs(integral_deph_phase)), STD_max(fabs(integral_reph_read),fabs(integral_reph_phase)) );

  float dephdur=secureDivision(maxintegral,fabs(driver->get_strength()));

  ODINLOG(odinlog,normalDebug) << "integral_deph_read/integral_deph_phase/maxintegral/dephdur=" << integral_deph_read << "/" << integral_deph_phase << "/" << maxintegral << "/" << dephdur << STD_endl;


  float rampdt=driver->get_ramp_rastertime();

  STD_string object_label(get_label());

  // Initialize objects with the same integral to obtain synchronous switch points
  dephobjs->readdephgrad= SeqGradTrapez(object_label+"_readdephgrad", maxintegral,readDirection, dephdur,rampdt,ramptype_cache);
  dephobjs->readrephgrad= SeqGradTrapez(object_label+"_readrephgrad", maxintegral,readDirection, dephdur,rampdt,ramptype_cache);
  dephobjs->phasedephgrad=SeqGradTrapez(object_label+"_phasedephgrad",maxintegral,phaseDirection,dephdur,rampdt,ramptype_cache);
  dephobjs->phaserephgrad=SeqGradTrapez(object_label+"_phaserephgrad",maxintegral,phaseDirection,dephdur,rampdt,ramptype_cache);

  // Adjust amplitude
  dephobjs->readdephgrad.set_integral(integral_deph_read);
  dephobjs->readrephgrad.set_integral(integral_reph_read);
  dephobjs->phasedephgrad.set_integral(integral_deph_phase);
  dephobjs->phaserephgrad.set_integral(integral_reph_phase);

  unsigned int phase_increment=segments_cache*reduction_cache;

  if(phase_increment>1) { // Create vector even if there is no phase encoding because it used in loop

    // use duration of on-ramp and constant part of read gradient as duration of constant part of the gradient vector
    double phasevecdur=dephobjs->readdephgrad.get_constgrad_duration()+dephobjs->readdephgrad.get_onramp_duration();

    fvector deph_strengthvec(phase_increment);
    fvector reph_strengthvec(phase_increment);
    for(unsigned int i=0; i<phase_increment; i++) {
      float phasestep=secureDivision(i,phase_increment)*blipint_cache;
      deph_strengthvec[i]=-centint_phase                             +phasestep;
      reph_strengthvec[i]=+centint_phase-gradint_driver[phaseDirection]-phasestep;
    }

    if(phasevecdur) {
      deph_strengthvec/=phasevecdur;
      reph_strengthvec/=phasevecdur;
    }

    float deph_maxstrength=deph_strengthvec.normalize();
    float reph_maxstrength=reph_strengthvec.normalize();
    ODINLOG(odinlog,normalDebug) << "deph_maxstrength/deph_strengthvec=" << deph_maxstrength << "/" << deph_strengthvec.printbody() << STD_endl;
    ODINLOG(odinlog,normalDebug) << "reph_maxstrength/reph_strengthvec=" << reph_maxstrength << "/" << reph_strengthvec.printbody() << STD_endl;

    dephobjs->phasesegdephgrad=SeqGradVector(object_label+"_phasesegdephgrad", phaseDirection, deph_maxstrength, deph_strengthvec, phasevecdur);
    dephobjs->phasesegrephgrad=SeqGradVector(object_label+"_phasesegrephgrad", phaseDirection, reph_maxstrength, reph_strengthvec, phasevecdur);

    ODINLOG(odinlog,normalDebug) << "phasesegdephgrad.get_numof_iterations()=" << dephobjs->phasesegdephgrad.get_numof_iterations() << STD_endl;

    // achieve GRAPPA encoding by interleavedSegmented reordering
    if(reduction_cache>1) {

      dephobjs->phasesegdephgrad.set_reorder_scheme(interleavedSegmented, reduction_cache);
      dephobjs->phasesegrephgrad.set_reorder_scheme(interleavedSegmented, reduction_cache);

      ODINLOG(odinlog,normalDebug) << "reduction_cache=" << reduction_cache << STD_endl;
      ODINLOG(odinlog,normalDebug) << "phasesegdephgrad.get_numof_iterations()/reorder.get_numof_iterations()=" << dephobjs->phasesegdephgrad.get_numof_iterations() << "/" << dephobjs->phasesegdephgrad.get_reorder_vector().get_numof_iterations() << STD_endl;
    }

  }

}



const SeqVector* SeqAcqEPI::get_dephgrad(SeqGradChanParallel& dephobj, bool rephase) const {
  Log<Seq> odinlog(this,"get_dephgrad");

  const SeqVector* result=0; 

  if(dephobjs->phasedephgrad.get_strength()!=0.0) {
    if(segments_cache>1 || reduction_cache>1) {
      if(rephase) {
        dephobj += ((dephobjs->readrephgrad)/(dephobjs->phasesegrephgrad));
        result=&(dephobjs->phasesegrephgrad);
      } else {
        dephobj += ((dephobjs->readdephgrad)/(dephobjs->phasesegdephgrad));
        result=&(dephobjs->phasesegdephgrad);
      }
    } else {
      if(rephase) dephobj += ((dephobjs->readrephgrad)/(dephobjs->phaserephgrad));
      else        dephobj += ((dephobjs->readdephgrad)/(dephobjs->phasedephgrad));
    }
  } else {
    if(rephase) dephobj += dephobjs->readrephgrad;
    else        dephobj += dephobjs->readdephgrad;
  }

  ODINLOG(odinlog,normalDebug) << "result/dephobj.get_gradintegral()=" << (void*)result << "/" << dephobj.get_gradintegral().printbody() << STD_endl;

  return result;
}


SeqAcqInterface& SeqAcqEPI::set_template_type(templateType type) {
  templtype_cache=type;
  driver->set_template_type(type);
  create_deph_and_reph();
  return *this;
}


SeqAcqEPI::SeqAcqEPI(const SeqAcqEPI& sae) : driver(sae.get_label()) {
  common_init();
  SeqAcqEPI::operator = (sae);
}


SeqAcqEPI::SeqAcqEPI(const STD_string& object_label)
 : SeqObjBase(object_label), driver(object_label) {
  common_init();
}



RecoValList SeqAcqEPI::get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const {
  Log<Seq> odinlog(this,"get_recovallist");

  int nTEs=2*echo_pairs_cache;
  if(nTEs<=0) nTEs=1;

  int nechoes=driver->get_numof_gradechoes();
  int startindex_phase=int(phasesize_cache)-nechoes/nTEs*segments_cache*reduction_cache; // recalculate with actual number of grad echoes

  ODINLOG(odinlog,normalDebug) << "nechoes/phasesize/startindex_phase=" << nechoes << "/" << phasesize_cache << "/" << startindex_phase << STD_endl;

  unsigned int padded_zeroes=0;
  kSpaceCoord kcoord_base(driver->get_kcoord_template(padded_zeroes));
  kcoord_base.oversampling=os_factor_cache;

  RecoValList result;

  int isegment=dephobjs->phasesegdephgrad.get_current_index();
  if(templtype_cache==no_template) {
    ODINLOG(odinlog,normalDebug) << "isegment=" << isegment << STD_endl;
  }

  for(int iecho=0; iecho<nechoes; iecho++) {
    kSpaceCoord kcoord(kcoord_base);

    kcoord.index[echo]=iecho;

    int iphase=startindex_phase + iecho/nTEs*segments_cache*reduction_cache + isegment;
    if(templtype_cache==phasecorr_template) iphase=0; // PE indices will be ignored in reco

    kcoord.index[line]=iphase;
    ODINLOG(odinlog,normalDebug) << "segments/isegment/line/reps[" << iecho << "]=" << segments_cache << "/" << isegment << "/" << kcoord.index[line] << "/" << kcoord.reps << STD_endl;

    bool reflect=(iecho%2);
    if(reflect) kcoord.flags=kcoord.flags|recoReflectBit;
    else        kcoord.flags=kcoord.flags&(recoReflectBit^recoAllBits);

    // Tag last ADC in echo train
    if(iecho==(nechoes-1)) {
      kcoord.flags=kcoord.flags|recoLastInChunkBit;
      kcoord.adcSize+=padded_zeroes;
      kcoord.postDiscard+=padded_zeroes;
    }

    kcoord.reps=reptimes;

    if(echo_pairs_cache>0) {
      kcoord.index[te]=iecho%nTEs;
    }

    coords.append_coord(kcoord);
    RecoValList rvl;
    rvl.set_value(kcoord.number);
    result.add_sublist(rvl);
  }

  return result;
}


SeqAcqInterface& SeqAcqEPI::set_sweepwidth(double sw, float os_factor) {
  Log<Seq> odinlog(this,"set_sweepwidth");
  ODINLOG(odinlog,warningLog) << "Ignoring request to change sweepwidth after construction" << STD_endl;
  return *this;
}


bool SeqAcqEPI::prep() {
  Log<Seq> odinlog(this,"prep");


  double gradfreq=secureInv(2.0*driver->get_echoduration());
  double low,upp;
  if(!systemInfo->allowed_grad_freq(gradfreq, low, upp)) {
    ODINLOG(odinlog,normalDebug) << "gradient frequency " << gradfreq << ODIN_FREQ_UNIT << " not allowed" << STD_endl;
    return false;
  }

  fvector gread=driver->get_readout_shape();
  unsigned int greadsize=gread.size();
  if( greadsize != readsize_os_cache ) {  // assume ramp sampling
    driver->set_readout_shape(gread,readsize_os_cache);
  }

  if(echo_pairs_cache>0) { // Only store TEs in a multi-TE EPI scan
    int nTEs=2*echo_pairs_cache;
    dvector TEs(nTEs);
    double echodur=driver->get_echoduration();
    TEs.fill_linear(0.5*echodur, (0.5+double(nTEs-1))*echodur);
    recoInfo->set_DimValues(te,TEs);
  }

  ODINLOG(odinlog,normalDebug) << "templtype_cache=" << templtype_cache << STD_endl;

  if(templtype_cache==no_template) { // Only store echo time stamps for non-template EPI as they are the basis for field-map based distortion corrections
    unsigned int nechoes=get_numof_gradechoes();
    double echodur=driver->get_echoduration();
    ODINLOG(odinlog,normalDebug) << "nechoes/echodur=" << nechoes << "/" << echodur << STD_endl;
    if(nechoes && echodur>0.0) { // discard empty EPIs
      dvector echotimestamps(nechoes);
      echotimestamps.fill_linear(0.0, double(nechoes-1)*echodur);
      ODINLOG(odinlog,normalDebug) << "echotimestamps=" << echotimestamps.printbody() << STD_endl;
      recoInfo->set_DimValues(echo,echotimestamps);
    }
  }

  return true;
}


SeqAcqEPI& SeqAcqEPI::operator = (const SeqAcqEPI& sae) {
  SeqObjBase::operator = (sae);

  readsize_os_cache=sae.readsize_os_cache;
  os_factor_cache=sae.os_factor_cache;
  phasesize_cache=sae.phasesize_cache;
  segments_cache=sae.segments_cache;
  reduction_cache=sae.reduction_cache;
  echo_pairs_cache=sae.echo_pairs_cache;
  blipint_cache=sae.blipint_cache;
  templtype_cache=sae.templtype_cache;
  ramptype_cache=sae.ramptype_cache;

  driver=sae.driver;

  create_deph_and_reph(); // initialize dephobjs

  return *this;
}
