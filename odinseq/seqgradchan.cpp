#include "seqgradchan.h"
#include "seqparallel.h"

#include <tjutils/tjfeedback.h>
#include <tjutils/tjlist_code.h>




SeqGradChan::SeqGradChan(const STD_string& object_label,direction gradchannel,
                   float gradstrength, double gradduration)
  : SeqDur(object_label), graddriver(object_label) {
  channel=gradchannel;
  SeqGradChan::set_strength(gradstrength); // perform strength checking, do NOT call virtual function in constructor
  set_duration(gradduration);
}


SeqGradChan::SeqGradChan(const STD_string& object_label)
: SeqDur(object_label), graddriver(object_label) {
  SeqGradChan::set_strength(0.0); // perform strength checking, do NOT call virtual function in constructor
  channel=readDirection;
}

SeqGradChan::SeqGradChan(const SeqGradChan& sgc) {
  SeqGradChan::operator = (sgc);
}


SeqGradChan& SeqGradChan::operator = (const SeqGradChan& sgc) {
  SeqDur::operator = (sgc);
  graddriver=sgc.graddriver;
  gradrotmatrix=sgc.gradrotmatrix;
  channel=sgc.channel;
  strength=sgc.strength;
  return *this;
}



SeqGradInterface& SeqGradChan::set_strength(float gradstrength) {
  Log<Seq> odinlog(this,"set_strength");

  ODINLOG(odinlog,normalDebug) << "gradstrength(pre)=" << gradstrength << STD_endl;
  gradstrength=graddriver->check_strength(gradstrength);
  ODINLOG(odinlog,normalDebug) << "gradstrength(post)=" << gradstrength << STD_endl;

  float maxgrad=systemInfo->get_max_grad();
  ODINLOG(odinlog,normalDebug) << "maxgrad=" << maxgrad << STD_endl;

  if(gradstrength>maxgrad) {
    float oldstrength=gradstrength;
    gradstrength=maxgrad;
    ODINLOG(odinlog,warningLog) << "Gradient strength (" << oldstrength << ") exceeds maximum, setting to " << gradstrength << STD_endl;
  }
  strength=gradstrength;
  return *this;
}

SeqGradInterface& SeqGradChan::invert_strength() {
  strength=-strength;
  return *this;
}

float SeqGradChan::get_strength() const {
  return strength;
}


direction SeqGradChan::get_channel() const {return channel;}

SeqGradInterface& SeqGradChan::set_gradrotmatrix(const RotMatrix& matrix) {
  Log<Seq> odinlog(this,"set_gradrotmatrix");
  for(unsigned int i=0;i<n_directions;i++) {
    for(unsigned int j=0;j<n_directions;j++) {
      gradrotmatrix[j][i]=matrix[j][i];
      if(gradrotmatrix[j][i]>1.0) {
        gradrotmatrix[j][i]=1.0;
        ODINLOG(odinlog,warningLog) << "exceeded 1.0 in gradrotmatrix[" << j << "][" << i << "], setting to 1.0" << STD_endl;
      }
      if(gradrotmatrix[j][i]<-1.0) {
        gradrotmatrix[j][i]=-1.0;
        ODINLOG(odinlog,warningLog) << "exceeded -1.0 in gradrotmatrix[" << j << "][" << i << "], setting to -1.0" << STD_endl;
      }

    }
  }

  return *this;
}



STD_string SeqGradChan::get_grdpart_rot(direction chan) const {
  Log<Seq> odinlog(this,"get_grdpart_rot");
  STD_string result;
  float matrixfactor=get_grdfactor(chan);

  ODINLOG(odinlog,normalDebug) << "matrixfactor[" << int(chan) << "][" << int(get_channel()) << "]=" << matrixfactor << STD_endl;
  if(fabs(matrixfactor)>_GRADROTMATRIX_LIMIT_) {
    result+=get_grdpart(matrixfactor);
  }
  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return result;
}

RotMatrix SeqGradChan::get_total_rotmat() const {
  RotMatrix result;
  const SeqRotMatrixVector* rotvec=SeqObjList::current_gradrotmatrixvec.get_handled();
  if(rotvec) result=rotvec->get_current_matrix();
  result=result*gradrotmatrix; // Multiplying the matrix coming from the vector with the local matrix 
  return result;
}

float SeqGradChan::get_grdfactor(direction chan) const {
   return get_total_rotmat()[chan][get_channel()];
}


fvector SeqGradChan::get_grdfactors_norot() const {
  fvector result(n_directions);
  for(int i=0; i<n_directions; i++) {
    result[i]=gradrotmatrix[i][get_channel()];
    if(fabs(result[i])<_GRADROTMATRIX_LIMIT_) result[i]=0.0;
  }
  return result;
}


fvector SeqGradChan::get_gradintegral() const {
  dvector result_norot(n_directions);
  result_norot[get_channel()]=get_integral();
  return dvector2fvector(get_total_rotmat()*result_norot);
}


STD_string SeqGradChan::get_properties() const {
  STD_string chanstring="read";
  if(get_channel()==phaseDirection) chanstring="phase";
  if(get_channel()==sliceDirection) chanstring="slice";
  return "Strength="+ftos(get_strength())+", Channel="+chanstring;
}


unsigned int SeqGradChan::event(eventContext& context) const {
  Log<Seq> odinlog(this,"event");
  double startelapsed=context.elapsed;
  SeqTreeObj::event(context); // printEvent if necessary
  if(context.action==seqRun) {
    graddriver->event(context,startelapsed);
  }
  context.elapsed=startelapsed+get_gradduration();
  context.increase_progmeter();
  return 1;
}

// Template instantiations
template class ListItem<SeqGradChan>;
template class List<SeqGradChan,SeqGradChan*, SeqGradChan&>;

