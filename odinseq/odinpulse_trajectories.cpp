#include "odinpulse.h"

#include <tjutils/tjnumeric.h>


/////////////////////////////////////////////////////////////////////////
// 1D trajectories:

class Const : public LDRfunctionPlugIn {

  LDRdouble lower;
  LDRdouble upper;

 public:

  Const() : LDRfunctionPlugIn("Const") {
    lower=0.0; lower.set_minmaxval(0.0,1.0);
    upper=1.0; upper.set_minmaxval(0.0,1.0);
    append_member(lower,"lowerBoundary");
    append_member(upper,"upperBoundary");
    set_description("A trajectory with a linear stepping in k-space (for slice-selective pulses) or in the time domain (for frequency-selective pulses)."
                    "With the parameters lowerBoundary and upperBoundary, a subarea of the pulse can be specified.");
  }

  const kspace_coord& calculate_traj(float s) const {

    double l=lower;
    double u=upper;
    if( l <0.0 ) l=0.0;
    if( l >1.0 ) l=1.0;
    if( u <0.0 ) u=0.0;
    if( u >1.0 ) u=1.0;

    double offset=l;
    double slew=u-l;
    

    coord_retval.traj_s=offset+slew*s;
    coord_retval.kz = 2.0 * coord_retval.traj_s - 1.0;
    coord_retval.Gz = 2.0 * slew;
    coord_retval.denscomp =  1.0;
    return coord_retval;
  }

  const traj_info& get_traj_properties() const {

    double l=lower;
    double u=upper;
    if( l <0.0 ) l=0.0;
    if( l >1.0 ) l=1.0;
    if( u <0.0 ) u=0.0;
    if( u >1.0 ) u=1.0;

    traj_info_retval.rel_center=secureDivision((0.5-l),(u-l));
    if(traj_info_retval.rel_center<0.0) traj_info_retval.rel_center=0.0;
    if(traj_info_retval.rel_center>1.0) traj_info_retval.rel_center=1.0;
    return traj_info_retval;
  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new Const;}
};

/////////////////////////////////////////////////////////////////////////

class Sinus : public LDRfunctionPlugIn {

  LDRint cycles;
  LDRfilter spectfilter;

 public:

  Sinus() : LDRfunctionPlugIn("Sinus"), spectfilter("spectfilter")  {
    cycles=8; cycles.set_minmaxval(1,20);
    append_member(cycles,"NumPulses");
    spectfilter.set_function(0);
    append_member(spectfilter,"SpectralFilter");
    set_description("This is a trajectory with a sinus-shaped gradient waveform. The NumPulses\n"
       "parameter specifies the number of times the trajectory passes the k-space origin. This trajectory may be used for spectral-spatial\n"
       "selective pulses.");
  }

  const kspace_coord& calculate_traj(float s) const {
    coord_retval.traj_s=s;
    float phi = PII * float(cycles) * (s - 1.0);
    coord_retval.kz = -cos (phi);
    coord_retval.Gz = PII * cycles * sin (phi);
    coord_retval.denscomp = fabs(coord_retval.Gz) * spectfilter.calculate (fabs(s-0.5)*2.0); // Quick hack: Apply filter in spectral dimension via denscomp
    return coord_retval;
  }

  const traj_info& get_traj_properties() const {
    traj_info_retval.rel_center=1.0-1.0/(float(cycles)*2.0);
    return traj_info_retval;
  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new Sinus;}
};



/////////////////////////////////////////////////////////////////////////
// 2D trajectories:

// Base class for all Archimedian spirals, c.f. Block and Frahm, JMRI 21:657-668(2005)
class ArchimedianSpiral : public LDRfunctionPlugIn {

  LDRint cycles;

  virtual void calc_theta(float& theta, float& dtheta, float s) const = 0;

 public:
  ArchimedianSpiral(const STD_string& label) : LDRfunctionPlugIn(label) {
    cycles=16; cycles.set_minmaxval(1,64);
    append_member(cycles,"NumCycles");
  }

  const kspace_coord& calculate_traj(float s) const {
    coord_retval.traj_s=s;

    float theta, dtheta;
    calc_theta(theta, dtheta, s);

    float phi = -2.0 * PII * (float)cycles * theta;
    float dphi = -2.0 * PII * (float)cycles * dtheta;

    coord_retval.kx = theta * cos (phi);
    coord_retval.ky = theta * sin (phi);
    coord_retval.Gx = dtheta * cos (phi) - theta * sin (phi) * dphi;
    coord_retval.Gy = dtheta * sin (phi) + theta * cos (phi) * dphi;

    coord_retval.denscomp = fabs(phi*dphi); //radius / sqrt (phi * phi + 1.0);

    return coord_retval;
  }

  const traj_info& get_traj_properties() const {
    traj_info_retval.rel_center=1.0;
    traj_info_retval.max_kspace_step=secureInv(2.0*cycles);
    return traj_info_retval;
  }

};


///////////////////////////////////////////////////////////////////////////



class ConstSpiral : public ArchimedianSpiral {

 public:
  ConstSpiral() : ArchimedianSpiral("ConstSpiral") {
    set_description("An Archimedian spiral where the radius increases linearly with time.");
  }

  // implementing virtual function of ArchimedianSpiral
  void calc_theta(float& theta, float& dtheta, float s) const {
    // Spiral is out->in
    theta=1.0-s;
    dtheta=-1.0;
  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new ConstSpiral;}
};

/////////////////////////////////////////////////////////////////////////

class WrapSpiral : public ArchimedianSpiral {

  LDRfloat switchpoint;

 public:
  WrapSpiral() : ArchimedianSpiral("WrapSpiral") {
    switchpoint=0.5; switchpoint.set_minmaxval(0.0,1.0);
    append_member(switchpoint,_TRAJ_OPTIMIZE_PARLABEL_);
    set_description("An Archimedian spiral.\n"
       "In the inner part of k-space the radius increases linerly with time,\n"
       "while in the outer part the distance between adjacent sampling points along the\n"
       "trajectory in k-space is kept constant.\n"
       "The " _TRAJ_OPTIMIZE_PARLABEL_ " parameter determines the relative point in time (between\n"
       "0.0 and 1.0) where the switching between these two modes occurs.");
  }

  // implementing virtual function of ArchimedianSpiral
  void calc_theta(float& theta, float& dtheta, float s) const {
    if (s < switchpoint) { // const sampling density
      theta = sqrt (1.0 - 2.0 * s / (switchpoint + 1.0));
      dtheta = -1.0 / ((switchpoint + 1.0) * theta);

    } else { // const spiral

      if(switchpoint>=1.0) { // take radius/dradius of const spiral
        theta = 1.0-s;
        dtheta = -1.0;
      } else {
        float denominator=sqrt (1.0 - switchpoint*switchpoint);
        theta = secureDivision(1.0-s, denominator);
        dtheta = secureDivision(-1.0, denominator);
      }
    }
  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new WrapSpiral;}

};

/////////////////////////////////////////////////////////////////////////

class BoernertSpiral : public ArchimedianSpiral {

  LDRfloat alpha;

 public:
  BoernertSpiral() : ArchimedianSpiral("BoernertSpiral") {
    alpha=0.5; alpha.set_minmaxval(0.0,1.0);
    append_member(alpha,_TRAJ_OPTIMIZE_PARLABEL_);
    set_description("An Archimedian spiral as described in Boernert et al, MAGMA 9:29-41(1999).");
  }

  // implementing virtual function of ArchimedianSpiral
  void calc_theta(float& theta, float& dtheta, float s) const {

    float f=1.0-s;
    float df=-1.0;

    float g=sqrt(alpha+(1.0-alpha)*(1.0-s));
    float dg=secureDivision(alpha-1.0, 2.0*g);

    theta=secureDivision(f,g);
    dtheta=secureDivision(df*g-f*dg,g*g);

  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new BoernertSpiral;}

};


/////////////////////////////////////////////////////////////////////////

class SegmentedRotation : public LDRfunctionPlugIn {

  LDRtrajectory Trajectory;
  LDRint Segment;
  LDRint Nsegments;
  mutable kspace_coord tds; // mutable -> member function calculate_traj defined as const
  mutable traj_info ti;
  RotMatrix rot_matrix;
  float deltaphi;
  mutable dvector vec;
  mutable dvector rot_vec;

 public:
  SegmentedRotation() : LDRfunctionPlugIn("SegmentedRotation") {

     
    Nsegments=8; Nsegments.set_minmaxval(1,30);
    Segment=1;  Segment.set_minmaxval(1,30);
    vec.resize(3);       
    rot_vec.resize(3);

    append_member(Trajectory,"Trajectory");
    append_member(Nsegments,"NumSegments");
    append_member(Segment,"CurrSegment");

    set_description("This is a segmented trajectory, which can be used to rotate the other 2D-trajectories.");
    
    Trajectory.set_function_mode(twoDeeMode);
  }

  void init_trajectory(OdinPulse* pls=0) {

    if (Nsegments < 1) Nsegments = 1; 
    if (Segment >= Nsegments) Segment = int (Nsegments);
    if (Segment < 1) Segment = 1;
    Trajectory.init_trajectory(pls);
    float phi= float(Segment-1) * 2.0 * PII / float(Nsegments);
    rot_matrix.set_inplane_rotation(phi);
  }

  const kspace_coord& calculate_traj(float s) const {

    tds=Trajectory.calculate(s);
    vec[0]=tds.kx;
    vec[1]=tds.ky;    
    vec[2]=0.0;
    
    rot_vec=rot_matrix*vec;
    tds.kx=rot_vec[0];
    tds.ky=rot_vec[1];
    tds.kz=0.0;
    
    // calculate gradient vector
    vec[0]=tds.Gx;
    vec[1]=tds.Gy;    
    vec[2]=0.0;
    
    rot_vec=rot_matrix*vec;
    tds.Gx=rot_vec[0];
    tds.Gy=rot_vec[1];
    tds.Gz=0.0;
    

    return tds;

  }



  const traj_info& get_traj_properties() const {
    
    ti=Trajectory.get_traj_info();
    ti.max_kspace_step/=float(Nsegments);
    return ti;
  }

  // implementing virtual function of LDRfunctionPlugIn
  LDRfunctionPlugIn* clone() const {return new SegmentedRotation;}
};




/////////////////////////////////////////////////////////////////////////
// allocating plugins

void LDRtrajectory::init_static() {

  (new Const)->register_function(trajFunc,zeroDeeMode).register_function(trajFunc,oneDeeMode);

  (new Sinus)->register_function(trajFunc,oneDeeMode);

  (new ConstSpiral)->register_function(trajFunc,twoDeeMode);
  (new WrapSpiral)->register_function(trajFunc,twoDeeMode);
  (new BoernertSpiral)->register_function(trajFunc,twoDeeMode);
  (new SegmentedRotation)->register_function(trajFunc,twoDeeMode);
}

void LDRtrajectory::destroy_static() {
  // pulgins will be deleted by LDRfunction
}

EMPTY_TEMPL_LIST bool StaticHandler<LDRtrajectory>::staticdone=false;
