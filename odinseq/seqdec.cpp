#include "seqdec.h"

#include <tjutils/tjfeedback.h>

SeqDecoupling::SeqDecoupling(const STD_string& object_label,const STD_string& nucleus,float decpower,const dvector& freqlist,
       const STD_string decprog,float decpulsduration) : SeqObjList(object_label), SeqFreqChan(object_label,nucleus,freqlist),
       decdriver(object_label) {

  set_decpower(decpower);
  set_program(decprog);
  set_pulsduration(decpulsduration);

}

SeqDecoupling::SeqDecoupling(const SeqDecoupling& sd) {
  SeqDecoupling::operator = (sd);
}

SeqDecoupling::SeqDecoupling(const STD_string& object_label)
  : SeqObjList(object_label), SeqFreqChan(object_label), decdriver(object_label) {

  set_decpower(120.0);
  set_program("");
  set_pulsduration(0.0);
}


bool SeqDecoupling::prep() {
  if(!SeqFreqChan::prep()) return false;
  return decdriver->prep_driver(SeqObjList::get_duration(),get_channel(),get_decpower(),get_program(),get_pulsduration());
}


const SeqVector& SeqDecoupling::get_freqlist_vector() const {
  SeqSimultanVector* result=new  SeqSimultanVector(STD_string(get_label())+"_instancevec");
  result->set_temporary();

  for(constinstiter it=get_const_inst_begin(); it!=get_const_inst_end(); ++it) (*result)+=(**it);

  return *result;
}


STD_string SeqDecoupling::get_program(programContext& context) const {
  STD_string result;
  result+=decdriver->get_preprogram(context,get_iteratorcommand(freqObj));
  result+=SeqObjList::get_program(context);
  result+=decdriver->get_postprogram(context);
  return result;
}


SeqValList SeqDecoupling::get_freqvallist(freqlistAction action) const {
  Log<Seq> odinlog(this,"get_freqvallist");
  SeqValList result(get_label());

  double newfreq=SeqFreqChan::get_frequency();

  if(action==calcDecList) {
    result.set_value(newfreq);
    ODINLOG(odinlog,normalDebug) << "freq=" << newfreq << STD_endl;
  }

  return result;
}


void SeqDecoupling::clear_container() {
  SeqObjList::clear_container();
  clear_instances();
}

double SeqDecoupling::get_duration() const {
  double result=0.0;
  result+=decdriver->get_preduration();
  result+=SeqObjList::get_duration();
  result+=decdriver->get_postduration();
  return result;
}


unsigned int SeqDecoupling::event(eventContext& context) const {
  Log<Seq> odinlog(this,"event");
  unsigned int result=0;

  double start=context.elapsed+decdriver->get_preduration();

  if(context.action==seqRun) {
    SeqFreqChan::pre_event(context,start);
    decdriver->event(context,start);
    result+=SeqObjList::event(context);
    SeqFreqChan::post_event(context,start+SeqObjList::get_duration());
  }
  context.increase_progmeter();
  result++;
  return result;
}



SeqDecoupling& SeqDecoupling::operator () (const SeqObjBase& so) {
  SeqDecoupling& dec=set_embed_body(so);
  return dec;
}


STD_string SeqDecoupling::get_program() const {return decprogram;}

void SeqDecoupling::set_program(const STD_string& p) {
  decprogram=p;
}


double SeqDecoupling::get_pulsduration() const {return pulsduration;}

void SeqDecoupling::set_pulsduration(float d) {
  pulsduration=d;
}



SeqDecoupling& SeqDecoupling::operator = (const SeqDecoupling& sd) {
  SeqObjList::operator = (sd);
  SeqFreqChan::operator = (sd);
  decdriver=sd.decdriver;
  set_program(sd.get_program());
  set_decpower(sd.get_decpower());
  set_pulsduration(sd.get_pulsduration());
  return *this;
}

int SeqDecoupling::set_body(const SeqObjBase& so) {
  clear();
  (*this)+=(so);
  return 0;
}



