#include "seqgradconst.h"


SeqGradConst::SeqGradConst(const STD_string& object_label,direction gradchannel,
                            float gradstrength, double gradduration)
                  : SeqGradChan(object_label,gradchannel,gradstrength,gradduration) {}


SeqGradConst::SeqGradConst(const SeqGradConst& sgc) {
  SeqGradConst::operator = (sgc);
}


SeqGradConst::SeqGradConst(const STD_string& object_label)
                  : SeqGradChan(object_label) {}


STD_string SeqGradConst::get_grdpart(float matrixfactor) const {
  return graddriver->get_const_program(get_strength(),matrixfactor);
}


SeqGradConst& SeqGradConst::operator = (const SeqGradConst& sgc) {
  SeqGradChan::operator = (sgc);
  return *this;
}

SeqGradChan& SeqGradConst::get_subchan(double starttime, double endtime) const {
  SeqGradConst* sgc= new SeqGradConst(STD_string(get_label())+"_("+ftos(starttime)+"-"+ftos(endtime)+")",get_channel(),get_strength(),endtime-starttime);
  sgc->set_temporary();
  return *sgc;
}


bool SeqGradConst::prep() {
  Log<Seq> odinlog(this,"prep");
  if(!SeqGradChan::prep()) return false;
  double dur=get_gradduration();
  float str=get_strength();
  float possible_maxgrad=dur*systemInfo->get_max_slew_rate();
  if(fabs(str)>possible_maxgrad) {
    ODINLOG(odinlog,errorLog) << "Duration=" << dur << " too short to ramp up to strength=" << str << STD_endl;
    return false;
  }
  return graddriver->prep_const(str,get_grdfactors_norot(),dur);
}





///////////////////////////////////////////////////////////////////////////////////////////



SeqGradDelay::SeqGradDelay(const STD_string& object_label,direction gradchannel, double gradduration)
                  : SeqGradChan(object_label,gradchannel,0.0,gradduration) {
}


SeqGradDelay::SeqGradDelay(const SeqGradDelay& sgd) {
  SeqGradDelay::operator = (sgd);
}


SeqGradDelay::SeqGradDelay(const STD_string& object_label)
                  : SeqGradChan(object_label) {
}


STD_string SeqGradDelay::get_grdpart(float matrixfactor) const {
  return graddriver->get_delay_program(get_strength(),matrixfactor);
}


SeqGradDelay& SeqGradDelay::operator = (const SeqGradDelay& sgd) {
  SeqGradChan::operator = (sgd);
  return *this;
}

SeqGradChan& SeqGradDelay::get_subchan(double starttime, double endtime) const {
  SeqGradDelay* sgd= new SeqGradDelay(STD_string(get_label())+"_("+ftos(starttime)+"-"+ftos(endtime)+")",get_channel(),endtime-starttime);
  sgd->set_temporary();
  return *sgd;
}



