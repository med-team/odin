#include "seqgradtrapez.h"

SeqGradTrapezDefault::SeqGradTrapezDefault() {
  const_dur=0.0;
  exclude_offramp=false;
}


SeqGradTrapezDefault::SeqGradTrapezDefault(const SeqGradTrapezDefault& sgtd)
  : SeqGradTrapezDriver(sgtd), SeqGradChan(sgtd) {
  graddriver->set_label(STD_string(sgtd.get_label()));
  onramp_cache=sgtd.onramp_cache;
  offramp_cache=sgtd.offramp_cache;
  const_dur=sgtd.const_dur;
  exclude_offramp=sgtd.exclude_offramp;
}


bool SeqGradTrapezDefault::update_driver(direction channel, double onrampdur, double constdur, double offrampdur, float strength, double timestep, rampType type, bool exclude_offramp_from_timing) {
  Log<Seq> odinlog(this,"update_driver");

  STD_string objlabel(get_label());

  set_duration(onrampdur+constdur+offrampdur); // Store redundant info, e.g. for correct display in sequence tree

  double mingraddur=0.0;
  if(constdur<mingraddur) {
    ODINLOG(odinlog,warningLog) << "increasing gradient duration " << constdur << ODIN_TIME_UNIT << " to " << mingraddur << ODIN_TIME_UNIT << STD_endl;
    constdur=mingraddur;
  }

  onramp_cache= SeqGradRamp(objlabel+"_onramp_cache" ,channel,onrampdur, 0.0,strength,timestep,type,false);
  offramp_cache=SeqGradRamp(objlabel+"_offramp_cache",channel,offrampdur,strength,0.0,timestep,type,true);
  const_dur=constdur;
  exclude_offramp=exclude_offramp_from_timing;
  return true;
}


SeqGradChanList SeqGradTrapezDefault::get_driverchanlist() {
  SeqGradChanList result(get_label());
  result+=(*this);
  return result;
}


bool SeqGradTrapezDefault::prep() {
  if(!SeqGradChan::prep()) return false;
  graddriver->set_label(get_label());
  return graddriver->prep_trapez(get_strength(),get_grdfactors_norot(),
                                 onramp_cache.get_gradduration(),  onramp_cache.get_wave(),
                                 const_dur,
                                 offramp_cache.get_gradduration(), offramp_cache.get_wave());
}

STD_string SeqGradTrapezDefault::get_properties() const {
  return SeqGradChan::get_properties()+", up/const/down="+ftos(onramp_cache.get_gradduration())+"/"+ftos(const_dur)+"/"+ftos(offramp_cache.get_gradduration());
}

SeqGradInterface& SeqGradTrapezDefault::set_strength(float gradstrength) {
  SeqGradChan::set_strength(gradstrength);
  onramp_cache.set_strength(gradstrength);
  offramp_cache.set_strength(gradstrength);
  return *this;
}

SeqGradInterface& SeqGradTrapezDefault::invert_strength() {
  SeqGradChan::invert_strength();
  onramp_cache.invert_strength();
  offramp_cache.invert_strength();
  return *this;
}


float SeqGradTrapezDefault::get_integral() const {
  return onramp_cache.get_gradintegral().sum() + get_strength()*const_dur + offramp_cache.get_gradintegral().sum();
}


double SeqGradTrapezDefault::get_gradduration() const {
  double result=onramp_cache.get_gradduration()+const_dur;
  if(!exclude_offramp) result+=offramp_cache.get_gradduration();
  return result;
}

SeqGradInterface& SeqGradTrapezDefault::set_gradrotmatrix(const RotMatrix& matrix) {
  SeqGradChan::set_gradrotmatrix(matrix);
  onramp_cache.set_gradrotmatrix(matrix);
  offramp_cache.set_gradrotmatrix(matrix);
  return *this;
}

SeqGradChan& SeqGradTrapezDefault::get_subchan(double starttime, double endtime) const {
  SeqGradTrapezDefault* sgti= new SeqGradTrapezDefault(*this);
  sgti->set_temporary();
  return *sgti;
}

///////////////////////////////////////////////////////////////


void SeqGradTrapez::common_init() {
  rampMode=linear;
  dt=0.0;
  steepnessfactor=1.0;
  exclude_offramp_timing=false;
  trapezchannel=readDirection;
  onrampdur=0.0;
  constdur=0.0;
  offrampdur=0.0;
  trapezstrength=0.0;
}


SeqGradTrapez::SeqGradTrapez(const STD_string& object_label,
                          direction gradchannel,float gradstrength,
                        double constgradduration, double timestep,rampType type,
                        double minrampduration, float steepness)
               : SeqGradChanList(object_label), trapezdriver(object_label) {
  Log<Seq> odinlog(this,"SeqGradTrapez");

  ODINLOG(odinlog,normalDebug) << "gradstrength/constgradduration=" << gradstrength << "/" << constgradduration << STD_endl;

  common_init();

  rampMode=type;
  dt=timestep;
  steepnessfactor=steepness;
  trapezchannel=gradchannel;

  constdur=constgradduration;
  trapezstrength=gradstrength;

  check_platform();

  float rampintegral;
  get_ramps(get_label(), rampintegral, onrampdur, offrampdur, trapezstrength, dt, rampMode, steepnessfactor, minrampduration);

  update_driver();
  build_seq();
}



SeqGradTrapez::SeqGradTrapez(const STD_string& object_label,
              float gradintegral, direction gradchannel, double constgradduration,
               double timestep,rampType type, double minrampduration, float steepness) :
               SeqGradChanList(object_label), trapezdriver(object_label) {
  Log<Seq> odinlog(this,"SeqGradTrapez");

  common_init();

  rampMode=type;
  dt=timestep;
  steepnessfactor=steepness;
  trapezchannel=gradchannel;

  if(constgradduration<=0.0) { // ramp-only gradient
    constdur=0.0;
    float sign=secureDivision(gradintegral,fabs(gradintegral));
    trapezstrength=sign*sqrt(fabs(gradintegral)*systemInfo->get_max_slew_rate()); // Assume linear ramps
  } else {
    constdur=constgradduration;
    trapezstrength=secureDivision(gradintegral,constgradduration);
  }

  check_platform();

  float rampintegral;
  get_ramps(get_label(), rampintegral, onrampdur, offrampdur, trapezstrength, dt, rampMode, steepnessfactor, minrampduration);

  // Fine-tune gradient integral
  float oldintegral=rampintegral+trapezstrength*constdur;
  trapezstrength*=secureDivision( gradintegral, oldintegral );

  update_driver();
  build_seq();
}



SeqGradTrapez::SeqGradTrapez(const STD_string& object_label,float gradintegral,
               float gradstrength, direction gradchannel,
               double timestep, rampType type, double minrampduration, float steepness) :
               SeqGradChanList(object_label), trapezdriver(object_label) {
  Log<Seq> odinlog(this,"SeqGradTrapez");

  common_init();

  rampMode=type;
  dt=timestep;
  steepnessfactor=steepness;
  trapezchannel=gradchannel;


  check_platform();

  // Take note of sign of integral, apply later
  float sign=secureDivision(gradintegral,fabs(gradintegral));

   // Use magnitude
  gradstrength=fabs(gradstrength);
  gradintegral=fabs(gradintegral);

  float rampintegral;
  get_ramps(get_label(), rampintegral, onrampdur, offrampdur, gradstrength, dt, rampMode, steepnessfactor, minrampduration);

  if(rampintegral<0.0) { // Consistency check: should be positive
    ODINLOG(odinlog,warningLog) << "Polarity mismatch: rampintegral=" << rampintegral << STD_endl;
  }

  if(rampintegral>gradintegral) { // Use ramp-only gradient
    constdur=0.0;
    trapezstrength=secureDivision(gradintegral,rampintegral)*gradstrength; // scale down ramp for correct integral
  } else {
    
    constdur=secureDivision(gradintegral-rampintegral,gradstrength); // Use const part for remainder
    trapezstrength=gradstrength;

    double rastertime=systemInfo->get_rastertime(gradObj);
    if(rastertime>0.0) {
      
      int nraster=(int)secureDivision(constdur,rastertime);
      if(rastertime*nraster!=constdur) nraster++; // always increase length of constant part
    
      constdur=nraster*rastertime;
    
      float rastered_integral=constdur*gradstrength+rampintegral;

      float scalefactor=secureDivision(gradintegral,rastered_integral); // scale down amplitude
      if(scalefactor>1.0) {
        ODINLOG(odinlog,warningLog) << "scalefactor=" << scalefactor << ", setting to 1" << STD_endl;
      }
    
      trapezstrength*=scalefactor;
    }
  }

  // Apply sign
  trapezstrength=sign*trapezstrength;

  update_driver();
  build_seq();
}



SeqGradTrapez::SeqGradTrapez(const SeqGradTrapez& sgt) {
  common_init();
  SeqGradTrapez::operator = (sgt);
}



SeqGradTrapez::SeqGradTrapez(const STD_string& object_label) : SeqGradChanList(object_label), trapezdriver(object_label)  {
  common_init();
}


SeqGradTrapez& SeqGradTrapez::set_constgrad_duration(double duration) {
  constdur=duration;
  update_driver();
  return *this;
}


SeqGradTrapez& SeqGradTrapez::set_integral(float gradintegral) {
  float newstrength=secureDivision( gradintegral, get_integral() )*trapezstrength;
  trapezstrength=newstrength;

  update_driver();
  return *this;
}


float SeqGradTrapez::get_integral() const {
  return get_onramp_integral()+get_constgrad_integral()+get_offramp_integral();
}




unsigned int SeqGradTrapez::get_onramp_npts() const {
  return (unsigned int)( secureDivision(onrampdur, dt)+0.5);
}

unsigned int SeqGradTrapez::get_const_npts() const {
  return (unsigned int)( secureDivision(constdur, dt)+0.5);
}

unsigned int SeqGradTrapez::get_offramp_npts() const {
  return (unsigned int)( secureDivision(offrampdur, dt)+0.5);
}


unsigned int SeqGradTrapez::get_npts() const {
  return  ( get_onramp_npts()+ get_const_npts() + get_offramp_npts() );
}

fvector SeqGradTrapez::get_trapezshape() const {
  fvector result(get_npts());
  fvector onramp(get_onramp());
  fvector offramp(get_offramp());

  unsigned int index=0;
  unsigned int i;
  for(i=0; i<onramp.size(); i++) result[index+i]=trapezstrength*onramp[i];
  index+=onramp.size();
  for(i=0; i<get_const_npts(); i++) result[index+i]=trapezstrength;
  index+=get_const_npts();
  for(i=0; i<offramp.size(); i++) result[index+i]=trapezstrength*offramp[i];

  return result;
}


SeqGradTrapez& SeqGradTrapez::operator = (const SeqGradTrapez& sgt) {
  SeqGradChanList::operator = (sgt);
  trapezdriver=sgt.trapezdriver;
  rampMode=sgt.rampMode;
  steepnessfactor=sgt.steepnessfactor;
  dt=sgt.dt;
  exclude_offramp_timing=sgt.exclude_offramp_timing;

  trapezchannel=sgt.trapezchannel;
  onrampdur=sgt.onrampdur;
  constdur=sgt.constdur;
  offrampdur=sgt.offrampdur;
  trapezstrength=sgt.trapezstrength;

  clear();
  build_seq();
  return *this;
}

SeqGradTrapez& SeqGradTrapez::exclude_offramp_from_timing(bool flag) {
  exclude_offramp_timing=flag;
  update_driver();
  return *this;
}


void SeqGradTrapez::check_platform() {
  Log<Seq> odinlog(this,"check_platform");

  ODINLOG(odinlog,normalDebug) << "checking if dt is valid on current platform" << STD_endl;
  double min_dt=systemInfo->get_min_grad_rastertime();
  if(dt<systemInfo->get_min_grad_rastertime()) {
    dt=min_dt;
  }

  ODINLOG(odinlog,normalDebug) << "checking if ramptype is valid on current platform" << STD_endl;
  if(!trapezdriver->check_ramptype(rampMode)) {
    ODINLOG(odinlog,errorLog) << "rampMode not supported on this platform" << STD_endl;
  }

}



void SeqGradTrapez::get_ramps(const STD_string& label, float& rampintegral, double& rampondur, double& rampoffdur, float strength, double dwelltime, rampType ramptype, float steepness, double mindur) {
  Log<Seq> odinlog(label.c_str(),"get_ramps");

  ODINLOG(odinlog,normalDebug) << "steepness/dwelltime=" << steepness << "/" << dwelltime << STD_endl;

  if( (steepness<=0.0) || (steepness>1.0) ) {
    ODINLOG(odinlog,warningLog) << "Steepness out of range, setting to 1.0" << STD_endl;
    steepness=1.0;
  }

  SeqGradRamp onramp4calc (label+"_onramp4calc", readDirection,0.0,strength,dwelltime,ramptype,steepness,false);
  SeqGradRamp offramp4calc(label+"_offramp4calc",readDirection,strength,0.0,dwelltime,ramptype,steepness,true);

  ODINLOG(odinlog,normalDebug) << "on/off/mindur=" << onramp4calc.get_duration() << "/" << offramp4calc.get_duration() << "/" << mindur << STD_endl;

  if(onramp4calc.get_duration()<mindur)  onramp4calc. set_ramp(mindur,0.0,strength,dwelltime,ramptype,false);
  if(offramp4calc.get_duration()<mindur) offramp4calc.set_ramp(mindur,strength,0.0,dwelltime,ramptype,true);

  rampondur= onramp4calc. get_gradduration();
  rampoffdur=offramp4calc.get_gradduration();
  ODINLOG(odinlog,normalDebug) << "rampondur/rampoffdur=" << rampondur << "/" << rampoffdur << STD_endl;

  rampintegral=onramp4calc.get_integral(0.0,rampondur)+offramp4calc.get_integral(0.0,rampoffdur);

}


void SeqGradTrapez::update_driver() {
  Log<Seq> odinlog(this,"update_driver");

  ODINLOG(odinlog,normalDebug) << "setting label" << STD_endl;
  trapezdriver->set_label(STD_string(get_label()));

  ODINLOG(odinlog,normalDebug) << "updating driver: onrampdur/constdur/offrampdur=" << onrampdur << "/" << constdur << "/" << offrampdur << STD_endl;
  trapezdriver->update_driver(trapezchannel,onrampdur,constdur,offrampdur,trapezstrength,dt,rampMode,exclude_offramp_timing);
}




void SeqGradTrapez::build_seq() {
  Log<Seq> odinlog(this,"build_seq");
  clear();

  SeqGradChanList sgcl_driver(trapezdriver->get_driverchanlist());

  (*this)+=sgcl_driver;
}

//////////////////////////////////////////////////////////


SeqGradTrapezParallel::SeqGradTrapezParallel(const STD_string& object_label,
               float gradintegral_read, float gradintegral_phase, float gradintegral_slice,
               float maxgradstrength, double timestep,
               rampType type, double minrampduration) : SeqGradChanParallel(object_label) {

  Log<Seq> odinlog(this,"build_seq");

  float maxint=maxof3(fabs(gradintegral_read),fabs(gradintegral_phase),fabs(gradintegral_slice));
  ODINLOG(odinlog,normalDebug) << "maxint=" << maxint << STD_endl;


  // first, initialize sub-pulses with max integral:
  readgrad =SeqGradTrapez(object_label+"_readgrad", maxint,maxgradstrength,readDirection, timestep,type,minrampduration);
  phasegrad=SeqGradTrapez(object_label+"_phasegrad",maxint,maxgradstrength,phaseDirection,timestep,type,minrampduration);
  slicegrad=SeqGradTrapez(object_label+"_slicegrad",maxint,maxgradstrength,sliceDirection,timestep,type,minrampduration);
  ODINLOG(odinlog,normalDebug) << "readgrad.get_gradintegral()=" << readgrad.get_gradintegral().printbody() << STD_endl;

  // second, adjust strength
  readgrad. set_strength(secureDivision(gradintegral_read, maxint)*readgrad.get_strength());
  phasegrad.set_strength(secureDivision(gradintegral_phase,maxint)*phasegrad.get_strength());
  slicegrad.set_strength(secureDivision(gradintegral_slice,maxint)*slicegrad.get_strength());
  ODINLOG(odinlog,normalDebug) << "readgrad.get_gradintegral()=" << readgrad.get_gradintegral().printbody() << STD_endl;

  build_seq();
}

SeqGradTrapezParallel::SeqGradTrapezParallel(const SeqGradTrapezParallel& sgtp) : SeqGradChanParallel(sgtp) {
  SeqGradTrapezParallel::operator = (sgtp);
}

SeqGradTrapezParallel::SeqGradTrapezParallel(const STD_string& object_label) : SeqGradChanParallel(object_label) {
}

SeqGradTrapezParallel& SeqGradTrapezParallel::operator = (const SeqGradTrapezParallel& sgtp) {
  SeqGradChanParallel::operator = (sgtp);
  readgrad =sgtp.readgrad;
  phasegrad=sgtp.phasegrad;
  slicegrad=sgtp.slicegrad;
  build_seq();
  return *this;
}

void SeqGradTrapezParallel::build_seq() {
  clear();
  (*this)+=readgrad/phasegrad/slicegrad;
}

