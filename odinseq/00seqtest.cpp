#include "seqall.h"

#ifdef ODIN_DEBUG

int seqtestfunc() {

  SeqGradConst constgrad;
  SeqGradConstPulse constgradpulse;
  SeqPulsar pulsar;
  SeqObjList list;
  SeqDelay delay;
  SeqObjLoop loop;
  SeqDecoupling dec;
  SeqGradEcho grech;
  SeqAcqEPI epi;
  SeqAcqSpiral spiral;
  SeqDiffWeight dw;
  SeqSat sat;
  SeqFieldMap fmap;


  // Testing + operator
  constgrad + constgrad;
  constgrad + constgradpulse;
  constgrad + pulsar;
  constgrad + list;
  constgrad + delay;
  constgrad + loop;
  constgrad + dec;

  constgradpulse + constgrad;
  constgradpulse + constgradpulse;
  constgradpulse + pulsar;
  constgradpulse + list;
  constgradpulse + delay;
  constgradpulse + loop;
  constgradpulse + dec;

  pulsar + constgrad;
  pulsar + constgradpulse;
  pulsar + pulsar;
  pulsar + list;
  pulsar + delay;
  pulsar + loop;
  pulsar + dec;

  list + constgrad;
  list + constgradpulse;
  list + pulsar;
  list + list;
  list + delay;
  list + loop;
  list + dec;

  delay + constgrad;
  delay + constgradpulse;
  delay + pulsar;
  delay + list;
  delay + delay;
  delay + loop;
  delay + dec;

  loop + constgrad;
  loop + constgradpulse;
  loop + pulsar;
  loop + list;
  loop + delay;
  loop + loop;
  loop + dec;

  dec + constgrad;
  dec + constgradpulse;
  dec + pulsar;
  dec + list;
  dec + delay;
  dec + loop;
  dec + dec;

  // Testing / operator
  constgrad / constgrad;
  constgrad / constgradpulse;
  constgrad / list;
  constgrad / delay;
  constgrad / loop;
  constgrad / dec;

  constgradpulse / constgrad;
  constgradpulse / constgradpulse;
  constgradpulse / list;
  constgradpulse / delay;
  constgradpulse / loop;
  constgradpulse / dec;


  list / constgrad;
  list / constgradpulse;

  delay / constgrad;
  delay / constgradpulse;

  loop / constgrad;
  loop / constgradpulse;

  dec / constgrad;
  dec / constgradpulse;


  
  // Testing += operator
  list += constgrad;
  list += constgradpulse;
  list += pulsar;
  list += list;
  list += delay;
  list += loop;
  list += dec;

  constgradpulse += constgrad;
  constgradpulse += constgradpulse;
  


  // Testing = operator
  list = constgrad;
  list = constgradpulse;
  list = pulsar;
  list = list;
  list = delay;
  list = loop;
  list = dec;

  // testing loop construction, this causes internal compiler error in GCC4.9 (https://gcc.gnu.org/bugzilla/show_bug.cgi?id=60871)
  loop( list )[pulsar];

  return 0;
}


#endif

