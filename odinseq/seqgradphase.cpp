#include "seqgradphase.h"

void SeqGradPhaseEnc::init_encoding(unsigned int nsteps, encodingScheme scheme, reorderScheme reorder, unsigned int nsegments, unsigned int reduction, unsigned int acl_bands, float partial_fourier) {
  Log<Seq> odinlog(this,"init_encoding");

  if(partial_fourier<0.0) partial_fourier=0.0;
  if(partial_fourier>1.0) partial_fourier=1.0;
  unsigned int startindex=(unsigned int)(partial_fourier*0.5*nsteps+0.5);

  if( partial_fourier && (scheme==centerOutEncoding || scheme==centerInEncoding ) ) {
    ODINLOG(odinlog,warningLog) << "center in/out encoding and partial Fourier: Not implemented" << STD_endl;
  }

  // some plausibility checks
  if(reduction>nsteps) reduction=nsteps;  // / order is important
  if(reduction<1) reduction=1;            // \ here because nsteps might be 0
  unsigned int ngaps=nsteps/reduction;
  if(acl_bands>ngaps) acl_bands=ngaps;

  // The indices covering the central part with auto-calibration lines
  unsigned int acl_start= (ngaps-acl_bands)/2 * reduction;
  unsigned int acl_end=acl_start+acl_bands*reduction;
  ODINLOG(odinlog,normalDebug) << "acl_start/acl_end=" << acl_start << "/" << acl_end << STD_endl;

  // decrease startindex so that we cover all of the ACL area
  if(reduction>1 && startindex>acl_start) startindex=acl_start;


  unsigned int ncovered=nsteps-startindex; // the number of encoding steps actually covered by partial Fourier
  unsigned int n_scanned=ncovered/reduction;
  if(ncovered%reduction) n_scanned++; // we can stuff in one more

  unsigned int vecsize=n_scanned+acl_bands*(reduction-1); // the number of lines actually scanned

  ODINLOG(odinlog,normalDebug) << "nsteps/startindex/reduction/ngaps/acl_bands/vecsize=" << nsteps << "/" << startindex << "/" << reduction << "/" << ngaps << "/" << acl_bands << "/" << vecsize << STD_endl;

  fvector petrims(vecsize);

  ivector recoindexivec(vecsize);

  float stepsize=secureDivision(2.0,nsteps);

  unsigned int ivec=0;
  for(unsigned int istep=0; istep<nsteps; istep++) {
    bool scan_line=false;
    if(!(istep%reduction)) {
      scan_line=true;
    }
    if(!scan_line && istep>=acl_start && istep<acl_end) {
      scan_line=true;
    }
    if(istep<startindex) {
      scan_line=false;
    }
    ODINLOG(odinlog,normalDebug) << "scan_line(" << istep << "/" << ivec << ")=" << scan_line << STD_endl;
    if(scan_line) {
      if(ivec<vecsize) {
        petrims[ivec]=-1.0+(0.5+float(istep))*stepsize;
        recoindexivec[ivec]=istep;
      }
      ivec++;
    }
  }

  // Quick hack for half-Fourier acquisition: Always acquire center k-space line
  if(reduction==1 && partial_fourier==1.0) petrims-=petrims.minvalue();

  set_trims(petrims);
  SeqGradVectorPulse::vectorgrad.set_indexvec(recoindexivec); // even if we have no recoindexivec, reset it to zero size

  set_encoding_scheme(scheme);
  set_reorder_scheme(reorder,nsegments);
}



SeqGradPhaseEnc::SeqGradPhaseEnc(const STD_string& object_label, unsigned int nsteps,
                                 float fov, direction gradchannel, float gradstrength,
                                 encodingScheme scheme, reorderScheme reorder,
                                 unsigned int nsegments, unsigned int reduction, unsigned int acl_bands,
                                 float partial_fourier, const STD_string& nucleus)
    : SeqGradVectorPulse(object_label,gradchannel,gradstrength,fvector(nsteps),0.0) {
  Log<Seq> odinlog(this,"SeqGradPhaseEnc(gradstrength)");

  init_encoding(nsteps, scheme, reorder, nsegments, reduction, acl_bands, partial_fourier);

  float gamma=systemInfo->get_gamma(nucleus);
  float resolution=secureDivision(fov,nsteps);


  float integral=secureDivision( PII , ( gamma * resolution ) );
  float sr=systemInfo->get_max_slew_rate();
  float Gmax_sr=sqrt(integral * sr);
  if(fabs(gradstrength)>Gmax_sr) {
    gradstrength=secureDivision(gradstrength,fabs(gradstrength));  //preserve sign
    gradstrength*=Gmax_sr;
    SeqGradVectorPulse::set_strength(gradstrength);
    ODINLOG(odinlog,warningLog) << "Reducing strength of SeqGradPhaseEnc in order satisfy integral" << STD_endl;
  }

  double dur=secureDivision( integral , gradstrength );
  set_constduration(dur);
}


SeqGradPhaseEnc::SeqGradPhaseEnc(const STD_string& object_label, unsigned int nsteps,
                  float fov, float gradduration, direction gradchannel,
                  encodingScheme scheme, reorderScheme reorder,
                  unsigned int nsegments, unsigned int reduction, unsigned int acl_bands,
                  float partial_fourier, const STD_string& nucleus)
    : SeqGradVectorPulse(object_label,gradchannel,0.0,fvector(nsteps),gradduration) {
  Log<Seq> odinlog(this,"SeqGradPhaseEnc(fov)");

  init_encoding(nsteps, scheme, reorder, nsegments, reduction, acl_bands, partial_fourier);

  float gamma=systemInfo->get_gamma(nucleus);
  float resolution=secureDivision(fov,nsteps);

  float integral=secureDivision( PII , ( gamma * resolution ) );

  float gradstrength=secureDivision( integral , gradduration );
  SeqGradVectorPulse::set_strength(gradstrength);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////

void SeqGradPhaseEncFlowComp::calc_flowcomp_pe(float& negfact, float& tc, float Gpos, float M0, float t0, float slewrate) {
  Log<Seq> odinlog("SeqGradPhaseEncFlowComp","calc_flowcomp_pe");

  ODINLOG(odinlog,normalDebug) << "Gpos/M0/t0/slewrate=" << Gpos << "/" << M0 << "/" << t0 << "/" << slewrate << STD_endl;

  // ramp time
  float tr=secureDivision(Gpos,slewrate);
  ODINLOG(odinlog,normalDebug) << "tr=" << tr << STD_endl;


  // constant part of trapez ending at tc  with ramp time tr and starting at t0 was
  // calculated with maxima:
//     solve ( Gp*    ( integrate(t*(t-t0      )/tr ,t, t0,       t0+tr)      + integrate(t, t, t0+tr,      t0+tc     ) + integrate(t*(t0+tr+tc    -t)/tr, t, t0+tc,      t0+tr+tc    ) ) - (Gp-M0/tc)*( integrate(t*(t-t0-tr-tc)/tr ,t, t0+tr+tc, t0+tc+2*tr) + integrate(t, t, t0+tc+2*tr, t0+tr+2*tc) + integrate(t*(t0+2*(tr+tc)-t)/tr, t, t0+tr+2*tc, t0+2*(tr+tc)) ), tc );


  float arg= 9.0*M0*M0 + (12.0*Gpos*tr + 16.0*Gpos*t0)*M0 + 4.0*Gpos*Gpos*tr*tr;
  ODINLOG(odinlog,normalDebug) << "arg=" << arg << STD_endl;

  float tc1=0.0;
  float tc2=0.0;
  if(arg>=0) {
    tc1=secureDivision(-sqrt(arg) - 3.0*M0 + 2.0*Gpos*tr, 4.0*Gpos);
    tc2=secureDivision(+sqrt(arg) + 3.0*M0 - 2.0*Gpos*tr, 4.0*Gpos);
    ODINLOG(odinlog,normalDebug) << "tc1/tc2=" << tc1 << "/" << tc2 << STD_endl;
  } else {
    ODINLOG(odinlog,errorLog) << "Cannot solve equation for flow compensation" << STD_endl;
  }


  tc=STD_max(tc1,tc2);
  negfact=secureDivision(Gpos-secureDivision(M0,tc), Gpos);
  ODINLOG(odinlog,normalDebug) << "negfact=" << negfact << STD_endl;

}



SeqGradPhaseEncFlowComp::SeqGradPhaseEncFlowComp(const STD_string& object_label, double t0, unsigned int nsteps,
                  float fov, direction gradchannel, float gradstrength,
                  encodingScheme scheme, reorderScheme reorder,
                  unsigned int nsegments, unsigned int reduction, unsigned int acl_bands,
                  float partial_fourier, const STD_string& nucleus)
 : SeqGradChanList(object_label), simvec(object_label+"_simvec") {

  // Template for gradient trims
  SeqGradPhaseEnc petmp(object_label, nsteps, fov, gradchannel, gradstrength, scheme, reorder, nsegments, reduction, acl_bands, partial_fourier, nucleus);

  // Calculate flow-compensated bipolar gradients
  float negfact, tc;
  calc_flowcomp_pe(negfact, tc, petmp.get_strength(), petmp.get_strength()*petmp.get_constduration(), t0, systemInfo->get_max_slew_rate());


  pos=SeqGradVectorPulse(object_label+"pos", gradchannel, petmp.get_strength(), petmp.get_trims(), tc);
  neg=SeqGradVectorPulse(object_label+"neg", gradchannel, petmp.get_strength(), -negfact*petmp.get_trims(), tc);

  simvec.set_indexvec(((const SeqVector&)petmp).get_indexvec());  // Preserve k-space indices

  build_seq();
}


SeqGradPhaseEncFlowComp& SeqGradPhaseEncFlowComp::operator = (const SeqGradPhaseEncFlowComp& sgpefc) {
  SeqGradChanList::operator = (sgpefc);
  pos=sgpefc.pos;
  neg=sgpefc.neg;
  simvec=sgpefc.simvec;

  build_seq();
  return *this;
}


void SeqGradPhaseEncFlowComp::build_seq() {
  SeqGradChanList::clear();
  simvec.clear();

  simvec+=pos;
  simvec+=neg;

  (*this)+=pos;
  (*this)+=neg;

}


