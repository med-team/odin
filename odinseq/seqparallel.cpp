#include "seqparallel.h"
#include "seqdelay.h"
#include "seqgradchan.h"
#include "seqgradchanparallel.h"

#include <tjutils/tjhandler_code.h>

SeqParallel::SeqParallel(const STD_string& object_label) : SeqObjBase(object_label), pardriver(object_label) {
}

SeqParallel::SeqParallel(const SeqParallel& sgp) : pardriver(sgp.get_label())  {
  SeqParallel::operator = (sgp);
}

SeqParallel& SeqParallel::operator = (const SeqParallel& sgp) {
  SeqObjBase::operator = (sgp);
  pardriver=sgp.pardriver;
  pulsptr=sgp.pulsptr;
  gradptr=sgp.gradptr;
  return *this;
}


SeqParallel& SeqParallel::operator /= (SeqGradChan& sgc) {
  SeqGradChanParallel* sgcp=new SeqGradChanParallel(sgc.get_label());
  sgcp->set_temporary();
  (*sgcp)+=sgc;
  set_gradptr((SeqGradObjInterface*)sgcp);
  return *this;
}


SeqParallel& SeqParallel::operator /= (SeqGradChanList& sgcl) {
  SeqGradChanParallel* sgcp=new SeqGradChanParallel(sgcl.get_label());
  sgcp->set_temporary();
  (*sgcp)+=sgcl;
  set_gradptr((SeqGradObjInterface*)sgcp);
  return *this;
}

SeqParallel& SeqParallel::operator /= (SeqGradChanParallel& sgcp) {
  set_gradptr((SeqGradObjInterface*)&sgcp);
  return *this;
}

SeqParallel& SeqParallel::operator /= (SeqGradObjInterface& sgoa) {
  set_gradptr((SeqGradObjInterface*)&sgoa);
  return *this;
}

SeqParallel& SeqParallel::operator /= (const SeqObjBase& soa) {
  SeqObjList* sol=new SeqObjList(soa.get_label());
  sol->set_temporary();
  (*sol)+=soa;
  set_pulsptr(sol);
  return *this;
}


double SeqParallel::get_duration() const {
  Log<Seq> odinlog(this,"SeqParallel::get_duration()");

  const SeqObjBase* soa=get_pulsptr();
  const SeqGradObjInterface* sgoa=get_const_gradptr();

  double result=0.0;
  if(soa) result=soa->get_duration();
  double graddur=0.0;
  if(sgoa) graddur=sgoa->get_gradduration();
  if(graddur>result) result=graddur;

  double dur_driver=pardriver->get_duration(soa,sgoa);
  if(dur_driver>result) result=dur_driver;

  return result;
}



double SeqParallel::get_gradduration() const {
  Log<Seq> odinlog(this,"SeqParallel::get_gradduration()");
  double result=0.0;

  const SeqGradObjInterface* sgoa=get_const_gradptr();
  if(sgoa) {
    result=sgoa->get_gradduration();
    ODINLOG(odinlog,normalDebug) << "gradduration=" << result << STD_endl;
  } else ODINLOG(odinlog,normalDebug) << "no gradobject defined" << STD_endl;

  return result;
}


double SeqParallel::get_pulprogduration() const {
  return pardriver->get_predelay(get_pulsptr(),get_const_gradptr());
}


SeqGradInterface& SeqParallel::set_strength(float gradstrength) {
  SeqGradObjInterface* sgoa=get_gradptr();
  if(sgoa) sgoa->set_strength(gradstrength);
  return *this;
}


SeqGradInterface& SeqParallel::invert_strength() {
  SeqGradObjInterface* sgoa=get_gradptr();
  if(sgoa) sgoa->invert_strength();
  return *this;
}


float SeqParallel::get_strength() const {
  float result=0.0;
  const SeqGradObjInterface* sgoa=get_const_gradptr();
  if(sgoa) result=sgoa->get_strength();
  return result;
}


SeqGradInterface& SeqParallel::set_gradrotmatrix(const RotMatrix& matrix) {
  SeqGradObjInterface* sgoa=get_gradptr();
  if(sgoa) sgoa->set_gradrotmatrix(matrix);
  return *this;
}


fvector SeqParallel::get_gradintegral() const {
  fvector result(3);
  const SeqGradObjInterface* sgoa=get_const_gradptr();
  if(sgoa) result=sgoa->get_gradintegral();
  return result;
}


void SeqParallel::query(queryContext& context) const {
  SeqTreeObj::query(context); // default

  context.parentnode=this;
  const SeqObjBase* soa=get_pulsptr();

  context.treelevel++;
  if(soa) soa->query(context);

  if(context.action!=count_acqs) {
    context.parentnode=this; // reset to this because it might changed by last query
    const SeqGradObjInterface* sgoa=get_const_gradptr();
    if(sgoa) sgoa->query(context);
  }
  context.treelevel--;
}


RecoValList SeqParallel::get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const {
  RecoValList result(get_label());
  const SeqObjBase* soa=get_pulsptr();
  if(soa) result=soa->get_recovallist(reptimes,coords);
  return result;
}


STD_string SeqParallel::get_properties() const {
  STD_string result;
  if(get_pulsptr()) result+="RF";
  else result+="-";
  result+="/";
  if(get_const_gradptr()) result+="Grad";
  else result+="-";
  return result;
}


unsigned int SeqParallel::event(eventContext& context) const {
  Log<Seq> odinlog(this,"SeqParallel::event");

  unsigned int result=0;

  double startelapsed=context.elapsed;

  ODINLOG(odinlog,normalDebug) << "startelapsed=" << startelapsed << STD_endl;

  const SeqObjBase* soa=get_pulsptr();
  const SeqGradObjInterface* sgoa=get_const_gradptr();

  double starteleapsed_pre=startelapsed+pardriver->get_predelay(soa,sgoa);

  bool flush_cache=context.noflush;
  context.noflush=true;

  context.elapsed=starteleapsed_pre;
  if(sgoa) result+=sgoa->event(context);
  if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately

  context.elapsed=starteleapsed_pre;
  if(soa) result+=soa->event(context);
  if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately

  context.noflush=flush_cache;

  context.elapsed=startelapsed+get_duration();

  return result;
}




SeqValList SeqParallel::get_freqvallist(freqlistAction action) const {
  const SeqObjBase* soa=get_pulsptr();
  if(soa) return soa->get_freqvallist(action);
  else return SeqValList();
}


SeqValList SeqParallel::get_delayvallist() const {
  const SeqObjBase* soa=get_pulsptr();
  if(soa) return soa->get_delayvallist();
  else return SeqValList();
}


double SeqParallel::get_rf_energy() const {
  double result=0.0;
  const SeqObjBase* soa=get_pulsptr();
  if(soa) result=soa->get_rf_energy();
  return result;
}


void SeqParallel::clear() {
  pulsptr.clear_handledobj();
  gradptr.clear_handledobj();
  const_gradptr.clear_handledobj();
}


SeqParallel& SeqParallel::set_pulsptr(const SeqObjBase* pptr) {
  pulsptr.set_handled(pptr);
  return *this;
}


const SeqObjBase* SeqParallel::get_pulsptr() const {return pulsptr.get_handled();}


SeqParallel& SeqParallel::set_gradptr(SeqGradObjInterface* gptr) {
  gradptr.set_handled(gptr);
  return *this;
}


SeqParallel& SeqParallel::set_gradptr(const SeqGradObjInterface* gptr) {
  const_gradptr.set_handled(gptr);
  return *this;
}


SeqParallel& SeqParallel::clear_gradptr() {
  gradptr.clear_handledobj();
  const_gradptr.clear_handledobj();
  return *this;
}


SeqGradObjInterface* SeqParallel::get_gradptr() const {return gradptr.get_handled();}


const SeqGradObjInterface* SeqParallel::get_const_gradptr() const {
  if(gradptr.get_handled()) return gradptr.get_handled();
  else return const_gradptr.get_handled();
}





// Template instantiations
template class Handled<SeqGradObjInterface*>;
template class Handler<SeqGradObjInterface*>;
template class Handled<const SeqGradObjInterface*>;
template class Handler<const SeqGradObjInterface*>;
template class Handled<const SeqObjBase*>;
template class Handler<const SeqObjBase*>;


