#include "seqgradobj.h"
#include "seqparallel.h"

SeqGradObjInterface::SeqGradObjInterface(const STD_string& object_label)
     : SeqTreeObj() {
  set_label(object_label);
}


SeqGradObjInterface::SeqGradObjInterface(const SeqGradObjInterface& sgoa) {
  SeqGradObjInterface::operator = (sgoa);
}

SeqGradObjInterface& SeqGradObjInterface::operator = (const SeqGradObjInterface& sgoa) {
  SeqTreeObj::operator = (sgoa);
  return *this;
}


double SeqGradObjInterface::get_duration() const {
  Log<Seq> odinlog(this,"SeqGradObjInterface::get_duration()");
  SeqParallel dummy;
  dummy.set_gradptr(this);    // will use SeqParallel to calculate duration
  ODINLOG(odinlog,normalDebug) << "duration=" << dummy.get_duration() << STD_endl;
  return dummy.get_duration();
}


double SeqGradObjInterface::get_pulprogduration() const {
  SeqParallel dummy;
  return dummy.SeqParallel::get_pulprogduration();
}


