#include "seqclass.h"
#include "seqplatform.h"
#include "seqvec.h"


#include <tjutils/tjlog_code.h>
#include <tjutils/tjhandler_code.h>

const char* Seq::get_compName() {return "Seq";}
LOGGROUNDWORK(Seq)


///////////////////////////////////////////////////////////////////////////


SeqClass::SeqClass() : systemInfo(*systemInfo_ptr) {
  Log<Seq> odinlog("SeqClass","SeqClass");
  set_label("unnamedSeqClass");
  // Do NOT use the MessageHandlers in here
  if(allseqobjs) allseqobjs->push_back(this);
}

SeqClass::~SeqClass() {
  Log<Seq> odinlog(this,"~SeqClass");
  if(allseqobjs) allseqobjs->remove(this);
  if(tmpseqobjs) tmpseqobjs->remove(this);
  if(seqobjs2prep) seqobjs2prep->remove(this);
  if(seqobjs2clear) seqobjs2clear->remove(this);
}


SeqClass& SeqClass::operator = (const SeqClass& sc) {
  Labeled::operator = (sc);
  return *this;
}


void SeqClass::marshall_error() const {
  Log<Seq> odinlog(this, "marshall_error");
  ODINLOG(odinlog,errorLog) << "Marshalling error: No sub-object available" << STD_endl;
}


bool SeqClass::prep_all() {
  Log<Seq> odinlog("SeqClass","prep_all");

  bool result=true;
  STD_list< SeqClass* >::iterator it;

  // two stage iteration through the list because prep() may modify the 'allseqobjs' list
  seqobjs2prep->clear();
  for(it=allseqobjs->begin(); it!=allseqobjs->end(); ++it) {
    (*it)->prepped=false;
    seqobjs2prep->push_back(*it);
  }

  while(seqobjs2prep->begin()!=seqobjs2prep->end()) {
    ODINLOG(odinlog,normalDebug) << "seqobjs2prep->size()=" << seqobjs2prep->size() << STD_endl;
    SeqClass* scs=*(seqobjs2prep->begin());
    bool do_prep=true;
    if(scs->prepped) do_prep=false;
    if(do_prep) {
      ODINLOG(odinlog,normalDebug) << "prepping " << scs->get_label() << STD_endl;
      if(!(scs->prep())) {
        result=false;
        ODINLOG(odinlog,errorLog) << scs->get_label() << "->prep() failed" << STD_endl;
      }
      scs->prepped=true;
    }
    seqobjs2prep->remove(scs);
  }

  return result;
}


void SeqClass::init_static() {
  Log<Seq> odinlog("SeqClass","init_static");

  allseqobjs.init("allseqobjs");
  tmpseqobjs.init("tmpseqobjs");
  seqobjs2prep.init("seqobjs2prep");
  seqobjs2clear.init("seqobjs2clear");
  ODINLOG(odinlog,normalDebug) << "seqobjs done" << STD_endl;

  geometryInfo.init("geometryInfo");
  ODINLOG(odinlog,normalDebug) << "geometryInfo done" << STD_endl;

  studyInfo.init("studyInfo");
  ODINLOG(odinlog,normalDebug) << "studyInfo done" << STD_endl;

  recoInfo.init("recoInfo");
  ODINLOG(odinlog,normalDebug) << "recoInfo done" << STD_endl;


  systemInfo_ptr=new SystemInterface(); // create systemInfo_platform
  ODINLOG(odinlog,normalDebug) << "systemInfo_ptr done" << STD_endl;

  SeqPlatformProxy(); // create platform instances
}


void SeqClass::destroy_static() {
  Log<Seq> odinlog("SeqClass","destroy_static");


  delete systemInfo_ptr;
  recoInfo.destroy();
  geometryInfo.destroy();
  studyInfo.destroy();

  seqobjs2clear.destroy();
  seqobjs2prep.destroy();
  tmpseqobjs.destroy();
  allseqobjs.destroy();

}

SeqClass& SeqClass::set_temporary() {
  if(tmpseqobjs) tmpseqobjs->push_back(this);
  return *this;
}

void SeqClass::clear_temporary() {
  Log<Seq> odinlog("SeqClass","clear_temporary");

  if(!tmpseqobjs) return;

  STD_list<SeqClass *> garbage;
  STD_list<SeqClass *>::iterator it;


  for(it=tmpseqobjs->begin(); it!=tmpseqobjs->end(); ++it) {
    ODINLOG(odinlog,normalDebug) << "tmpobj >" <<  (*it)->get_label() << "<" << STD_endl;
    garbage.push_back(*it);
  }

  tmpseqobjs->erase(tmpseqobjs->begin(),tmpseqobjs->end());

  for(it=garbage.begin(); it!=garbage.end(); ++it) {
    ODINLOG(odinlog,normalDebug) << "deleting >" <<  (*it)->get_label() << "<(" << (void*)(*it) << ")" << STD_endl;
    allseqobjs->remove(*it);
    delete (*it);
  }
}

void SeqClass::clear_containers() {
  Log<Seq> odinlog("SeqClass","clear_containers");
  STD_list< SeqClass*>::iterator it;

  // two stage iteration through the list because clear_container() may modify the 'allseqobjs' list
  seqobjs2clear->clear();
  for(it=allseqobjs->begin(); it!=allseqobjs->end(); ++it) seqobjs2clear->push_back(*it);


  while(seqobjs2clear->begin()!=seqobjs2clear->end()) {
    SeqClass* scs=*(seqobjs2clear->begin());
    ODINLOG(odinlog,normalDebug) << "clearing >" <<  scs->get_label() << "<" << STD_endl;
    scs->clear_container();
    ODINLOG(odinlog,normalDebug) << "removing >" <<  scs->get_label() << "<" << STD_endl;
    seqobjs2clear->remove(scs);
  }

}

void SeqClass::clear_objlists() {
  if(allseqobjs) allseqobjs->clear();
  if(tmpseqobjs) tmpseqobjs->clear();
  if(seqobjs2prep) seqobjs2prep->clear();
  if(seqobjs2clear) seqobjs2clear->clear();
}

SystemInterface* SeqClass::systemInfo_ptr;

template class SingletonHandler<Geometry,false>;
SingletonHandler<Geometry,false> SeqClass::geometryInfo;

template class SingletonHandler<Study,false>;
SingletonHandler<Study,false> SeqClass::studyInfo;

template class SingletonHandler<RecoPars,false>;
SingletonHandler<RecoPars,false> SeqClass::recoInfo;

template class SingletonHandler<SeqClass::SeqClassList,false>;

SingletonHandler<SeqClass::SeqClassList,false> SeqClass::allseqobjs;
SingletonHandler<SeqClass::SeqClassList,false> SeqClass::tmpseqobjs;

SingletonHandler<SeqClass::SeqClassList,false> SeqClass::seqobjs2prep;
SingletonHandler<SeqClass::SeqClassList,false> SeqClass::seqobjs2clear;

EMPTY_TEMPL_LIST bool StaticHandler<SeqClass>::staticdone=false;


SeqVector& SeqClass::get_dummyvec() {
  if(!dummyvec) dummyvec=new SeqVector("dummyvec");
  return *dummyvec;
}
SeqVector* SeqClass::dummyvec=0;
