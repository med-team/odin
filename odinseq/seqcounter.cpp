#include "seqcounter.h"



SeqCounter::SeqCounter(const STD_string& object_label) : counterdriver(object_label) {
  disable_counter();
  set_label(object_label);
}


SeqCounter::SeqCounter(const SeqCounter& sc) {
  disable_counter();
  SeqCounter::operator = (sc);
}


SeqCounter& SeqCounter::operator = (const SeqCounter& sc) {
  SeqTreeObj::operator = (sc);

  counterdriver=sc.counterdriver;
  counterdriver->outdate_cache();

  clear_vectorlist();
  for(veciter=sc.get_vecbegin(); veciter!=sc.get_vecend(); ++veciter) {
    add_vector(**veciter);
  }

  return *this;
}


void SeqCounter::set_vechandler_for_all() const {
  Log<Seq> odinlog(this,"set_vechandler_for_all");
  for(veciter=get_vecbegin(); veciter!=get_vecend(); ++veciter) {
    ODINLOG(odinlog,normalDebug) << "veciter=" << (*veciter)->get_label() << STD_endl;
    (*veciter)->set_vechandler(this);
  }
  return;
}


bool SeqCounter::prep_veciterations() const {
  Log<Seq> odinlog(this,"prep_veciterations");
  for(veciter=get_vecbegin(); veciter!=get_vecend(); ++veciter) {
    ODINLOG(odinlog,normalDebug) << "calling prep_iteration of vector " << (*veciter)->get_label() << STD_endl;
    if(!(*veciter)->prep_iteration()) return false;
    ODINLOG(odinlog,normalDebug) << "prep done" << STD_endl;
  }
  return true;
}


void SeqCounter::init_counter(unsigned int start) const {
  set_vechandler_for_all();
  int niter=get_times();
  if(start && niter>0) counter=start%niter;
  else                 counter=0;
}


void SeqCounter::add_vector(const SeqVector& seqvector) {
  Log<Seq> odinlog(this,"add_vector");

  if( get_times() && (int(seqvector.get_numof_iterations())!=get_times()) ) {
    ODINLOG(odinlog,errorLog) << "size mismatch: this=" << get_times() << ", " << seqvector.get_label() << "=" << seqvector.get_numof_iterations() << STD_endl;
  } else {
    vectors.append(seqvector);
    seqvector.set_vechandler(this);
    seqvector.nr_cache_up2date=false;
    ODINLOG(odinlog,normalDebug) << "added vector >" << seqvector.get_label() << "<" << STD_endl;
  }
  counterdriver->outdate_cache();
}



int SeqCounter::get_times() const {
  Log<Seq> odinlog(this,"get_times");
  if(n_vectors()) {
    ODINLOG(odinlog,normalDebug) << "vectorsize(" << (*(get_vecbegin()))->get_label() << ")=" << (*(get_vecbegin()))->get_numof_iterations() << STD_endl;
    return (*(get_vecbegin()))->get_numof_iterations();
  }
  return 0;
}


bool  SeqCounter::prep() {
  if(!SeqTreeObj::prep()) return false;
  if(!counterdriver->prep_driver()) return false;
  return true;
}


void SeqCounter::clear_container() {
  clear_vectorlist();
  counterdriver->outdate_cache();
}

