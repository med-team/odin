#include "seqloop.h"
#include "seqdelay.h"


SeqObjLoop::SeqObjLoop(const STD_string& object_label)
  : SeqCounter(object_label), SeqObjList(object_label), times(0), numof_acq_cache(0), is_toplevel_reploop(false) {
}

SeqObjLoop::SeqObjLoop(const SeqObjLoop& sl) : numof_acq_cache(0), is_toplevel_reploop(false) {
  SeqObjLoop::operator = (sl);
}


SeqObjLoop& SeqObjLoop::operator = (const SeqObjLoop& sl) {
  SeqObjList::operator = ((const SeqObjList&)sl); // cast is necessary to avoid calling SeqObjList::operator = (const SeqObjLoop&)
  SeqCounter::operator = (sl);
  times=sl.times;

  return *this;
}


double SeqObjLoop::get_preduration() const {
  counterdriver->update_driver(this,this,&vectors);
  return counterdriver->get_preduration();
}


double SeqObjLoop::get_postduration() const {
  counterdriver->update_driver(this,this,&vectors);
  return counterdriver->get_postduration();
}


float SeqObjLoop::get_single_duration() const {
  counterdriver->update_driver(this,this,&vectors); // required for correct preduration/postduration_inloop
  return counterdriver->get_preduration_inloop()+SeqObjList::get_duration()+counterdriver->get_postduration_inloop();
}



STD_string SeqObjLoop::get_program(programContext& context) const {
  Log<Seq> odinlog(this,"SeqObjLoop::get_program");
  STD_string result;

  if(!get_times()) return result;

  counterdriver->outdate_cache();
  counterdriver->update_driver(this,this,&vectors);



  if(unroll_program(context)) {
    ODINLOG(odinlog,normalDebug) << "unrolling loop in program"  << STD_endl;

    // retrieve kernel with counter set to 1st iteration so that can be used later to avoid multiple get_program() calls
    init_counter();
    prep_veciterations();
    STD_string loopkernel=SeqObjList::get_program(context);

    // do we actually have to create a program?
    if(!counterdriver->create_program(context,loopkernel)) {
      return result;
    }

    result+=counterdriver->get_program_head_unrolled(context, 0);
    result+=loopkernel;

    // retrieve remaining loop kernels
    increment_counter();
    for(; get_counter()<get_times(); increment_counter()) {
      prep_veciterations();
      ODINLOG(odinlog,normalDebug) << "retrieving loopkernel for iteration #" << get_counter() << STD_endl;
      result+=counterdriver->get_program_head_unrolled(context, get_counter());
      result+=SeqObjList::get_program(context);
    }
    disable_counter();
    prep_veciterations(); // Reset to default iteration if outside loop

  } else { // unroll_program
    ODINLOG(odinlog,normalDebug) << "loop command in program"  << STD_endl;

    context.nestlevel++;
    context.neststatus=true;

    disable_counter(); // retrieve kernel with counter disabled so that can be used later with loop commands
    ODINLOG(odinlog,normalDebug) << "retrieving initial loopkernel"  << STD_endl;
    STD_string loopkernel=SeqObjList::get_program(context);

    // do we actually have to create a program?
    if(!counterdriver->create_program(context,loopkernel)) {
      context.nestlevel--;
      context.neststatus=false;
      return result;
    }

    result+=counterdriver->get_program_head(context,loopkernel,get_times());
    result+=loopkernel;
    context.nestlevel--;
    result+=counterdriver->get_program_tail(context,loopkernel,get_times());
    context.neststatus=false;
  }

  return result;
}


double SeqObjLoop::get_duration() const {
  Log<Seq> odinlog(this,"get_duration");
  double result=0.0;

  counterdriver->update_driver(this,this,&vectors);
  result+=counterdriver->get_preduration();
  result+=counterdriver->get_postduration();
  double preduration_inloop =counterdriver->get_preduration_inloop();
  double postduration_inloop=counterdriver->get_postduration_inloop();

  if(!is_repetition_loop(true)) {
    ODINLOG(odinlog,normalDebug) << "is vector loop" << STD_endl;

    for(init_counter(); get_counter()<get_times(); increment_counter()) {
      result+=preduration_inloop;
      result+=get_single_duration();
      result+=postduration_inloop;
    }
    disable_counter();

  } else {
    ODINLOG(odinlog,normalDebug) << "is repetition loop" << STD_endl;
    result=double(get_times())*get_single_duration();
  }
  return result;
}

SeqObjLoop& SeqObjLoop::set_times(unsigned int t) {
  for(institer it=get_inst_begin();it!=get_inst_end();++it) {
    (*it)->set_times(t);
  }
  times=t;
  return *this;
}

int SeqObjLoop::get_times() const {
  Log<Seq> odinlog(this,"get_times");
  if(n_vectors()) {
    return SeqCounter::get_times();
  } else {
    ODINLOG(odinlog,normalDebug) << "times=" << times << STD_endl;
    return times;
  }
}


unsigned int SeqObjLoop::get_numof_acq() const {
  if(numof_acq_cache) return numof_acq_cache;

  queryContext context;
  context.action=count_acqs;

  unsigned int result=0;
  if(is_obj_repetition_loop()) {
    SeqObjList::query(context);
    result=context.numof_acqs*get_times();
  } else {
    for(init_counter(); get_counter()<get_times(); increment_counter()) {
      SeqObjList::query(context);
      result+=context.numof_acqs;
    }
    disable_counter();
  }

  numof_acq_cache=result;

  return result;
}


void SeqObjLoop::query(queryContext& context) const {
  Log<Seq> odinlog(this,"query");

  if(context.action==tag_toplevel_reploop) {
    if(is_repetition_loop() && get_times()>1 && context.repetitions_prot==get_times()) {
      if(get_numof_acq()) is_toplevel_reploop=true; // do not tag dummy loops
    }
    return; // do not query sub-branch
  }

  SeqObjList::query(context); // default

  if(context.action==count_acqs) context.numof_acqs=get_numof_acq();

}



bool SeqObjLoop::contains_acq_iter() const {
  queryContext qc;
  qc.action=check_acq_iter;
  SeqObjList::query(qc);
  return qc.check_acq_iter_result;
}

  
  
RecoValList SeqObjLoop::get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const {
  Log<Seq> odinlog(this,"get_recovallist");

  RecoValList result(get_label());

  if(!contains_acq_iter() && is_acq_repetition_loop()) {

    unsigned int totaltimes=reptimes*get_times();
    ODINLOG(odinlog,normalDebug) << "repetition loop reptimes/get_times()/totaltimes=" << reptimes << "/" << get_times() << "/" << totaltimes << STD_endl;

    for(constiter it=get_const_begin();it!=get_const_end();++it) {
      result.add_sublist((*it)->get_recovallist(totaltimes,coords));
    }
    result.multiply_repetitions(get_times());

  } else {

    ODINLOG(odinlog,normalDebug) << "explicit loop reptimes/get_times()=" << reptimes << "/" << get_times() << STD_endl;

    RecoValList* thisiter;
    for(init_counter(); get_counter()<get_times(); increment_counter()) {
      thisiter=new RecoValList;
      for(constiter it=get_const_begin();it!=get_const_end();++it) {
        ODINLOG(odinlog,normalDebug) << "counter/obj=" << get_counter() << "/" << (*it)->get_label() << STD_endl;
        thisiter->add_sublist((*it)->get_recovallist(reptimes,coords));
      }
      result.add_sublist(*thisiter);
      delete thisiter;
    }
    disable_counter();

  }
  return result;
}



SeqObjLoop& SeqObjLoop::operator () (const SeqObjBase& embeddedBody) {
  Log<Seq> odinlog(this,"operator () (const SeqObjBase&)");
  ODINLOG(odinlog,normalDebug) << " label=" << get_label() << STD_endl;
  SeqObjLoop& loop=set_embed_body(embeddedBody);
  ODINLOG(odinlog,normalDebug) << " loop.label=" << loop.get_label() << STD_endl;
  return loop;
}


unsigned int SeqObjLoop::event(eventContext& context) const {
  Log<Seq> odinlog(this,"event");

  unsigned int result=0;

  looplevel++;

  counterdriver->update_driver(this,this,&vectors);
  double predur=counterdriver->get_preduration();
  ODINLOG(odinlog,normalDebug) << "predur=" << predur << STD_endl;
  if(predur) {
    SeqDelay sd("predelay",predur);
    result+=sd.event(context);
    if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately
  }

  int niterations=get_times();
  ODINLOG(odinlog,normalDebug) << "niterations=" << niterations << STD_endl;

  // within fSEQCheck it is sufficient to call the loop kernel for only one iteration:
  if( context.action==seqRun && context.seqcheck && is_repetition_loop(true) ) {
    niterations=1;
  }

  // to count events, is is sufficient to call the loop kernel for only one iteration:
  unsigned count_factor=1;
  if(context.action==countEvents && is_obj_repetition_loop()) {
    niterations=1;
    count_factor=get_times();
  }

  unsigned int n_events=0;
  for(init_counter(); get_counter()<niterations; increment_counter()) {
    ODINLOG(odinlog,normalDebug) << "counter=" << get_counter() << STD_endl;

    int repcounter=-1;
    if(is_toplevel_reploop) repcounter=get_counter();

    counterdriver->pre_vecprepevent(context);
    if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately

    prep_veciterations();

    counterdriver->post_vecprepevent(context,repcounter);
    if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately


    ODINLOG(odinlog,normalDebug) << "calling SeqObjList::event" << STD_endl;
    n_events+=SeqObjList::event(context);
    ODINLOG(odinlog,normalDebug) << "calling SeqObjList::event done" << STD_endl;
    if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately


    double postdur_inloop=counterdriver->get_postduration_inloop();
    if(postdur_inloop) {
      SeqDelay sd("postdelay_inloop",postdur_inloop);
      n_events+=sd.event(context);
      if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately
    }
  }
  disable_counter();
  prep_veciterations(); // Reset to default iteration if outside loop

  result+=n_events*count_factor;

  looplevel--;

  double postdur=counterdriver->get_postduration();
  ODINLOG(odinlog,normalDebug) << "postdur=" << postdur << STD_endl;
  if(postdur) {
    SeqDelay sd("postdelay",postdur);
    result+=sd.event(context);
    if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately
  }

  return result;
}




SeqValList SeqObjLoop::get_freqvallist(freqlistAction action) const {
  Log<Seq> odinlog(this,"get_freqvallist");
  SeqValList result(get_label()+STD_string("freqlist"));

  if(is_repetition_loop()) {

    for(constiter it=get_const_begin();it!=get_const_end();++it) {
      result.add_sublist((*it)->get_freqvallist(action));
    }
    result.multiply_repetitions(get_times());

  } else {

    SeqValList* thisiter;
    for(init_counter(); get_counter()<get_times(); increment_counter()) {
      thisiter=new SeqValList;
      for(constiter it=get_const_begin();it!=get_const_end();++it) {
        thisiter->add_sublist((*it)->get_freqvallist(action));
      }
      result.add_sublist(*thisiter);
      delete thisiter;
    }
    disable_counter();


  }
  return result;
}


SeqValList SeqObjLoop::get_delayvallist() const {
  Log<Seq> odinlog(this,"get_delayvallist");
  SeqValList result(get_label()+STD_string("delaylist"));

  if(is_repetition_loop()) {

    for(constiter it=get_const_begin();it!=get_const_end();++it) {
      result.add_sublist((*it)->get_delayvallist());
    }
    result.multiply_repetitions(get_times());

  } else {

    SeqValList* thisiter;
    for(init_counter(); get_counter()<get_times(); increment_counter()) {
      thisiter=new SeqValList;
      for(constiter it=get_const_begin();it!=get_const_end();++it) {
        thisiter->add_sublist((*it)->get_delayvallist());
      }
      result.add_sublist(*thisiter);
      delete thisiter;
    }
    disable_counter();

  }
  return result;
}

double SeqObjLoop::get_rf_energy() const {
  double result=0.0;
  if(is_repetition_loop(true)) result=SeqObjList::get_rf_energy()*get_times();
  else {
    for(init_counter(); get_counter()<get_times(); increment_counter()) {
      prep_veciterations(); // necessary for correct prepping of amplitude of idea pulses
      result+=SeqObjList::get_rf_energy();
    }
    disable_counter();
    prep_veciterations(); // Reset to default iteration if outside loop

  }
  return result;
}





SeqObjLoop& SeqObjLoop::operator [] (const SeqVector& seqvector) {
  add_vector(seqvector);
  counterdriver->outdate_cache();
  return *this;
}

SeqObjLoop& SeqObjLoop::operator [] (unsigned int t) {
  set_times(t);
  counterdriver->outdate_cache();
  return *this;
}

bool SeqObjLoop::is_repetition_loop(bool only_qualvectors) const {
  Log<Seq> odinlog(this,"is_repetition_loop");

  if(only_qualvectors) {
    bool result=true;

    for(veciter=get_vecbegin(); veciter!=get_vecend(); ++veciter) {
      if((*veciter)->is_qualvector()) {
        ODINLOG(odinlog,normalDebug) << (*veciter)->get_label() << " alters" << STD_endl;
        result=false;
      } else ODINLOG(odinlog,normalDebug) << (*veciter)->get_label() << " NOT alters" << STD_endl;
    }

    ODINLOG(odinlog,normalDebug) << result << STD_endl;

    return result;

  } else return !(n_vectors());
}


bool SeqObjLoop::is_acq_repetition_loop() const {
  Log<Seq> odinlog(this,"is_acq_repetition_loop");

  if(is_repetition_loop()) {
    ODINLOG(odinlog,normalDebug) << "is_repetition_loop()==true" << STD_endl;
    return true;
  }

  bool result=true;
  for(veciter=get_vecbegin(); veciter!=get_vecend(); ++veciter) {
    if((*veciter)->is_acq_vector()) {
      ODINLOG(odinlog,normalDebug) << (*veciter)->get_label() << " is acq vector" << STD_endl;
      result=false;
      break;
    } else ODINLOG(odinlog,normalDebug) << (*veciter)->get_label() << " is NOT acq vector" << STD_endl;
  }

  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return result;
}


bool SeqObjLoop::is_obj_repetition_loop() const {
  for(veciter=get_vecbegin(); veciter!=get_vecend(); ++veciter) {
    if((*veciter)->is_obj_vector()) return false;
  }
  return true;
}

bool SeqObjLoop::unroll_program(programContext& context) const {
  return counterdriver->unroll_program(this,this,&vectors,context);
}



SeqObjLoop& SeqObjLoop::set_body(const SeqObjBase& so) {
  clear();
  (*this)+=(so);
  counterdriver->outdate_cache();
  return *this;
}

bool  SeqObjLoop::prep() {
  Log<Seq> odinlog(this,"prep");
  if(!SeqObjList::prep()) return false;
  if(!SeqCounter::prep()) return false;

  for(constinstiter it=get_const_inst_begin();it!=get_const_inst_begin();++it) {
    if((*it)->get_times()==0) {
      ODINLOG(odinlog,warningLog) << "loop " << (*it)->get_label() << " has 0 iterations" << STD_endl;
    }
  }

  // reset for next prep_acquisition
  numof_acq_cache=0;
  is_toplevel_reploop=false;
  return true;
}


void SeqObjLoop::clear_container() {
  SeqObjList::clear_container();
  SeqCounter::clear_container();
  Embed<SeqObjLoop,SeqObjBase>::clear_instances();
}


STD_string SeqObjLoop::get_properties() const {
  return "Times="+itos(get_times())+", NumOfVectors="+itos(n_vectors())+", "+SeqObjList::get_properties();
}


void SeqObjLoop::add_vector(const SeqVector& seqvector) {
  Log<Seq> odinlog(this,"add_vector");
  for(institer it=get_inst_begin();it!=get_inst_end();++it) {
    (*it)->add_vector(seqvector);
  }

  SeqCounter::add_vector(seqvector);
}

