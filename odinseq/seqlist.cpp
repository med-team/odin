#include "seqlist.h"
#include "seqloop.h"
#include "seqdec.h"
#include "seqparallel.h"
#include "seqgradobj.h"
#include "seqgradchanparallel.h"

#include <tjutils/tjlist_code.h>
#include <tjutils/tjhandler_code.h>


SeqObjList::SeqObjList(const STD_string& object_label) :
  SeqObjBase(object_label),
  listdriver(object_label) {
  Log<Seq> odinlog(this,"SeqObjList()");
}

SeqObjList::SeqObjList(const SeqObjList& so) {
  SeqObjList::operator = (so);
}

SeqObjList::~SeqObjList() {
  Log<Seq> odinlog(this,"~SeqObjList()");
}

SeqObjList& SeqObjList::operator = (const SeqObjList& so) {
  SeqObjBase::operator = (so);
  List<SeqObjBase, const SeqObjBase*, const SeqObjBase& >::operator = (so);
  listdriver=so.listdriver;
  return *this;
}

SeqObjList& SeqObjList::operator = (const SeqObjLoop& sl) {
  clear();
  (*this)+=sl;
  return *this;
}

SeqObjList& SeqObjList::operator = (const SeqDecoupling& sd) {
  clear();
  (*this)+=sd;
  return *this;
}

SeqObjList& SeqObjList::operator = (const SeqObjBase& soa) {
  clear();
  (*this)+=soa;
  return *this;
}



SeqObjList& SeqObjList::operator += (const SeqObjBase& soa) {
  Log<Seq> odinlog(this,"+=");
  if((&soa)->contains(this)) {
    ODINLOG(odinlog,errorLog) << "Refusing to append >" << soa.get_label() << "< to >" << get_label() << "< which would then contain itself" << STD_endl;
  }
  else append(soa);
  return *this;
}




SeqObjList& SeqObjList::operator += (SeqGradObjInterface& sgoa) {
  SeqParallel* par=new SeqParallel(STD_string("[")+sgoa.get_label()+"]");
  par->set_temporary();
  par->set_gradptr(&sgoa);
  (*this)+=(*par);
  return *this;
}

SeqObjList& SeqObjList::operator += (SeqGradChan& sgc) {
  SeqGradChanList* sgcl=new SeqGradChanList(STD_string("(")+sgc.get_label()+")");
  sgcl->set_temporary();
  (*sgcl)+=sgc;
  (*this)+=(*sgcl);
  return *this;
}


SeqObjList& SeqObjList::operator += (SeqGradChanList& sgcl) {
  SeqGradChanParallel* par=new SeqGradChanParallel(STD_string("{")+sgcl.get_label()+"}");
  par->set_temporary();
  (*par)+=sgcl;
  (*this)+=(*par);
  return *this;
}




unsigned int SeqObjList::event(eventContext& context) const {
  Log<Seq> odinlog(this,"event");

  unsigned int result=0;

  const RotMatrix* rotmatrix=0;
  if(gradrotmatrixvec.get_handled()) {
    current_gradrotmatrixvec.set_handled(gradrotmatrixvec.get_handled());
    rotmatrix=&(current_gradrotmatrixvec.get_handled()->get_current_matrix());
  }

  listdriver->pre_event(context,rotmatrix);
  if(context.abort) {
    ODINLOG(odinlog,errorLog) << "aborting" << STD_endl;
    return result; // return immediately
  }

  for(constiter it=get_const_begin();it!=get_const_end();++it) {

    listdriver->pre_itemevent(*it,context);
    if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately

    ODINLOG(odinlog,normalDebug) << (*it)->get_label() << "->event" << STD_endl;
    result+=(*it)->event(context);
    if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately

    listdriver->post_itemevent(*it,context);
    if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately

  }

  listdriver->post_event(context,rotmatrix);
  if(context.abort) {ODINLOG(odinlog,errorLog) << "aborting" << STD_endl; return result;} // return immediately

  current_gradrotmatrixvec.clear_handledobj();

  return result;
}



double SeqObjList::get_duration() const {
  Log<Seq> odinlog(this,"get_duration");
  double result=0.0;

  if(gradrotmatrixvec.get_handled()) current_gradrotmatrixvec.set_handled(gradrotmatrixvec.get_handled());

  for(constiter it=get_const_begin();it!=get_const_end();++it) {
     result+=(*it)->get_duration();
     ODINLOG(odinlog,normalDebug) << "duration(" << (*it)->get_label() << ")=" << (*it)->get_duration() << STD_endl;
  }

  current_gradrotmatrixvec.clear_handledobj();

  return result;
}


STD_string SeqObjList::get_properties() const {
  return "NumOfObjects="+itos(size());
}


STD_string SeqObjList::get_program(programContext& context) const {
  STD_string result;

  if(gradrotmatrixvec.get_handled()) current_gradrotmatrixvec.set_handled(gradrotmatrixvec.get_handled());

  result+=listdriver->pre_program(context, gradrotmatrixvec.get_handled());

  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    result+=listdriver->get_itemprogram(*it,context);
  }

  result+=listdriver->post_program(context, gradrotmatrixvec.get_handled());

  current_gradrotmatrixvec.clear_handledobj();

  return result;
}


void SeqObjList::query(queryContext& context) const {
  Log<Seq> odinlog(this,"query");
  SeqTreeObj::query(context); // defaults

  context.treelevel++;
  unsigned int acqresult=0;
  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    context.parentnode=this; // reset to this because it might changed by last query
    (*it)->query(context);
    acqresult+=context.numof_acqs;
  }
  context.treelevel--;
  if(context.action==count_acqs) context.numof_acqs=acqresult;
}



RecoValList SeqObjList::get_recovallist(unsigned int reptimes, LDRkSpaceCoords& coords) const {
  Log<Seq> odinlog(this,"get_recovallist");
  RecoValList result(get_label());
  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    ODINLOG(odinlog,normalDebug) << reptimes << "x from " << (*it)->get_label() << STD_endl;
    RecoValList oneiter=(*it)->get_recovallist(reptimes,coords);
    result.add_sublist(oneiter);
  }
  ODINLOG(odinlog,normalDebug) << "returning" << STD_endl;
  return result;
}


SeqValList SeqObjList::get_freqvallist(freqlistAction action) const {
  Log<Seq> odinlog(this,"get_freqvallist");
  SeqValList result(get_label());
  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    SeqValList oneiter=(*it)->get_freqvallist(action);
    result.add_sublist(oneiter);
  }
  return result;
}

SeqValList SeqObjList::get_delayvallist() const {
  Log<Seq> odinlog(this,"get_delayvallist");
  SeqValList result(get_label());
  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    SeqValList oneiter=(*it)->get_delayvallist();
    result.add_sublist(oneiter);
  }
  return result;
}

double SeqObjList::get_rf_energy() const {
  double result=0.0;
  for(constiter it=get_const_begin();it!=get_const_end();++it) {
    result+=(*it)->get_rf_energy();
  }
  return result;
}


bool SeqObjList::prep() {
  if(!SeqObjBase::prep()) return false;
  return listdriver->prep_driver();
}


Handler<const SeqRotMatrixVector* > SeqObjList::current_gradrotmatrixvec;


// Template instantiations
template class ListItem<SeqObjBase>;
template class List<SeqObjBase, const SeqObjBase*, const SeqObjBase& >;
template class Handler<const SeqRotMatrixVector*>;

