#include "seqphase.h"
#include "seqfreq.h"


///////////////////////////////////////////////////////////////////


SeqPhaseListVector::SeqPhaseListVector(const STD_string& object_label, const dvector& phase_list)
 : phasedriver(object_label) {
  set_label(object_label);
  set_phaselist(phase_list);
}


SeqPhaseListVector::SeqPhaseListVector(const SeqPhaseListVector& spl) : phasedriver(spl.get_label()) {
  SeqPhaseListVector::operator = (spl);
}


SeqPhaseListVector& SeqPhaseListVector::operator = (const SeqPhaseListVector& spl) {
  // do NOT copy user because it is set by SeqFreqChan and remains the same over the whole lifetime
  phasedriver=spl.phasedriver;
  phaselist=spl.phaselist;
  return *this;
};


bool SeqPhaseListVector::prep() {
  if(!SeqClass::prep()) return false;
  phasedriver->prep_driver(phaselist);
  return true;
}


bool SeqPhaseListVector::prep_iteration() const {
  Log<Seq> odinlog(this,"prep_iteration");
  ODINLOG(odinlog,normalDebug) << "user=" << user->get_label() << STD_endl;
  return user->prep_iteration();
}


SeqPhaseListVector& SeqPhaseListVector::set_phaselist(const dvector& pl) {
  Log<Seq> odinlog(this,"set_phaselist");
  phaselist=pl;

  // Limit to range 0-360, just as a precaution for flaky drivers
  for(unsigned int i=0; i<phaselist.size(); i++) {
    phaselist[i]-=floor(phaselist[i]/360.0)*360.0;
  }
  ODINLOG(odinlog,normalDebug) << "phaselist=" << phaselist << STD_endl;
  return *this;
}


unsigned int SeqPhaseListVector::get_phaselistindex() const {
  return phasedriver->get_phaselistindex(phaselist);
}



STD_string SeqPhaseListVector::get_loopcommand() const {
  return phasedriver->get_loopcommand(phaselist);
}


svector SeqPhaseListVector::get_vector_commands(const STD_string& iterator) const {
  return phasedriver->get_phasevec_commands(iterator,user->get_driver_instr_label());
}



double SeqPhaseListVector::get_phase() const {
  Log<Seq> odinlog(this,"get_phase");
  double result=0.0;
  unsigned int index=get_current_index();
  if(index<phaselist.length()) result=phaselist[index];
  return result;
}


