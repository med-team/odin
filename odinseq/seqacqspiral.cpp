#include "seqacqspiral.h"

void SeqAcqSpiral::common_init() {
  SeqAcqInterface::set_marshall(&acq);
  SeqFreqChanInterface::set_marshall(&acq);
}

SeqAcqSpiral::SeqAcqSpiral(const STD_string& object_label,double sweepwidth, float fov,
             unsigned int sizeRadial, unsigned int numofSegments, LDRtrajectory&  traj,
             bool inout, bool optimize, const STD_string& nucleus, const dvector& phaselist)
  : SeqObjList(object_label), par(object_label+"_par"),
    spirgrad_in(object_label+"_spirgrad_in",traj,secureInv(sweepwidth),secureDivision(fov,sizeRadial),sizeRadial/(1+int(inout)),numofSegments/(1+int(inout)),true,optimize,nucleus),
    spirgrad_out(object_label+"_spirgrad_out",traj,secureInv(sweepwidth),secureDivision(fov,sizeRadial),sizeRadial/(1+int(inout)),numofSegments/(1+int(inout)),false,optimize,nucleus),
    preacq(object_label+"_preacq"),
    acq(object_label+"_acq", int(inout)*spirgrad_in.spiral_size()+spirgrad_out.spiral_size(), sweepwidth, 1.0, nucleus, phaselist),
    inout_traj(inout) {

  Log<Seq> odinlog(this,"SeqAcqSpiral(...)");

  common_init();

  // set the rotation matrices for the spiral interleaves
  rotvec.set_label(STD_string(get_label())+"_rotvec");
  unsigned int nrot=numofSegments;
  if(inout) nrot/=2;
  if(!nrot) nrot=1;
  rotvec.create_inplane_rotation(nrot);

  acq.set_rel_center(0.0);
  if(inout) acq.set_rel_center(0.5);

  gbalance=SeqGradTrapezParallel(object_label+"_gbalance",
                                 -spirgrad_in.get_gradintegral()[readDirection], -spirgrad_in.get_gradintegral()[phaseDirection], 0.0,
                                 0.5*systemInfo->get_max_grad());


  build_seq();
}



SeqAcqSpiral::SeqAcqSpiral(const SeqAcqSpiral& sas) {
  common_init();
  SeqAcqSpiral::operator = (sas);
}


SeqAcqSpiral::SeqAcqSpiral(const STD_string& object_label)
   : SeqObjList(object_label) {
  common_init();
}


SeqAcqSpiral& SeqAcqSpiral::operator = (const SeqAcqSpiral& sas) {
  SeqObjList::operator = (sas);
  par=sas.par;
  spirgrad_in=sas.spirgrad_in;
  spirgrad_out=sas.spirgrad_out;
  preacq=sas.preacq;
  acq=sas.acq;
  gbalance=sas.gbalance;
  rotvec=sas.rotvec;
  inout_traj=sas.inout_traj;
  build_seq();
  return *this;
}


fvector SeqAcqSpiral::get_ktraj(unsigned int iseg, direction channel) const {
  Log<Seq> odinlog(this,"get_ktraj");

  const RotMatrix& rotmatrix=rotvec[iseg];
  ODINLOG(odinlog,normalDebug) << "rotmatrix=" << rotmatrix.print() << STD_endl;

  fvector kx_in(spirgrad_in.get_ktraj(readDirection));
  fvector ky_in(spirgrad_in.get_ktraj(phaseDirection));
  fvector kx_out(spirgrad_out.get_ktraj(readDirection));
  fvector ky_out(spirgrad_out.get_ktraj(phaseDirection));

  unsigned int size=kx_out.length();
  if(inout_traj) size+=kx_in.length();

  fvector result(size);

  dvector kvec(3);
  dvector kvec_rot(3);
  kvec=0;

  unsigned int offset=0;
  if(inout_traj) offset=kx_in.length();

  for(unsigned int i=0; i<size; i++) {
    if(i<offset) {
      kvec[0]=kx_in[i];
      kvec[1]=ky_in[i];
    } else {
      kvec[0]=kx_out[i-offset];
      kvec[1]=ky_out[i-offset];
    }
    kvec_rot=rotmatrix*kvec;

    result[i]=kvec_rot[channel];
  }

  return result;
}


fvector SeqAcqSpiral::get_denscomp() const {
  Log<Seq> odinlog(this,"get_denscomp");

  fvector denscomp_in(spirgrad_in.get_denscomp());
  fvector denscomp_out(spirgrad_out.get_denscomp());

  unsigned int size=denscomp_out.length();
  if(inout_traj) size+=denscomp_in.length();

  fvector result(size);

  unsigned int offset=0;
  if(inout_traj) offset=denscomp_in.length();

  for(unsigned int i=0; i<size; i++) {
    if(i<offset) result[i]=denscomp_in[i];
    else         result[i]=denscomp_out[i-offset];
  }

  return result;
}


SeqAcqInterface& SeqAcqSpiral::set_sweepwidth(double sw, float os_factor) {
  Log<Seq> odinlog(this,"set_sweepwidth");
  ODINLOG(odinlog,warningLog) << "Ignoring request to change sweepwidth after construction" << STD_endl;
  return *this;
}

bool SeqAcqSpiral::prep() {
  Log<Seq> odinlog(this,"prep");
  if(!SeqObjList::prep()) return false;

  unsigned int acqsize= get_ktraj(0,readDirection).length();
  unsigned int segsize=rotvec.get_vectorsize();
  unsigned int gradsize=3;
  ODINLOG(odinlog,normalDebug) << "acqsize/segsize=" << acqsize << "/" << segsize << STD_endl;

  farray ktrajs(segsize,acqsize,gradsize);

  for(unsigned int iseg=0; iseg<segsize; iseg++) {
    for(unsigned int igrad=0; igrad<gradsize; igrad++) {
      fvector ktraj(get_ktraj(iseg, direction(igrad)));
      for(unsigned int iacq=0; iacq<acqsize; iacq++) ktrajs(iseg,iacq,igrad)=ktraj[iacq];
    }
  }


  acq.set_kspace_traj(ktrajs);

  acq.set_weight_vec(real2complex(get_denscomp()));

  acq.set_reco_vector(cycle,rotvec);

  return true;
}

void SeqAcqSpiral::build_seq() {
  Log<Seq> odinlog(this,"build_seq");

  par.clear();
  SeqObjList::clear();

  double acqcent=par.get_pulprogduration()+acq.get_acquisition_start();

  double shift=systemInfo->get_grad_shift_delay()-acqcent;
  if(inout_traj) shift += gbalance.get_gradduration()+spirgrad_in.get_ramp_duration();

  ODINLOG(odinlog,normalDebug) << "acqcent/shift=" << acqcent << "/" << shift << STD_endl;

  if(shift>=systemInfo->get_min_duration(delayObj)) {
    ODINLOG(odinlog,normalDebug) << "acq is played out later, because gradient switching is delayed" << STD_endl;
    preacq.set_duration(shift);
    if(inout_traj) par /= (gbalance + spirgrad_in + spirgrad_out);
    else           par /= spirgrad_out;
    par /= (preacq + acq);

  } else {
    ODINLOG(odinlog,normalDebug) << "gradients are played out later, because acq is delayed" << STD_endl;
    if(inout_traj) {
      par /= (gbalance + spirgrad_in + spirgrad_out);
      spirgrad_in.set_predelay_duration(-shift);
    } else {
      par /= spirgrad_out;
      spirgrad_out.set_predelay_duration(-shift);
    }
    par /= acq;
  }

  (*this)+=par;

  SeqObjList::set_gradrotmatrixvector(rotvec);
}


