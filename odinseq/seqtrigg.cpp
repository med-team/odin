#include "seqtrigg.h"
#include "seqdelay.h"


#include <tjutils/tjfeedback.h>


SeqTrigger::SeqTrigger(const STD_string& object_label, double duration)
    : SeqObjBase(object_label), triggdriver(object_label),
  triggdur(duration)  {
}


SeqTrigger::SeqTrigger(const STD_string& object_label) : SeqObjBase(object_label), triggdriver(object_label),
  triggdur(0.0) {
}


SeqTrigger& SeqTrigger::operator = (const SeqTrigger& st) {
  SeqObjBase::operator = (st);
  triggdriver=st.triggdriver;
  triggdur=st.triggdur;
  return *this;
}


STD_string SeqTrigger::get_program(programContext& context) const {
  return triggdriver->get_program(context);
}


double SeqTrigger::get_duration() const {
  return triggdur+triggdriver->get_postduration();
}


unsigned int SeqTrigger::event(eventContext& context) const {
  Log<Seq> odinlog(this,"event");
  double startelapsed=context.elapsed;
  SeqObjBase::event(context);

  ODINLOG(odinlog,normalDebug) << "context.action/startelapsed=" << context.action << "/" << startelapsed << STD_endl;


  if(context.action==seqRun) {
    triggdriver->event(context,startelapsed);
  }
  context.increase_progmeter();
  return 1;
}


bool  SeqTrigger::prep() {
  if(!SeqObjBase::prep()) return false;
  return triggdriver->prep_exttrigger(triggdur);
}


/////////////////////////////////////////////////////////////////////////////


SeqHalt::SeqHalt(const STD_string& object_label) : SeqObjBase(object_label), triggdriver(object_label) {
}


SeqHalt& SeqHalt::operator = (const SeqHalt& sh) {
  SeqObjBase::operator = (sh);
  triggdriver=sh.triggdriver;
  return *this;
}


STD_string SeqHalt::get_program(programContext& context) const {
  return triggdriver->get_program(context);
}


double SeqHalt::get_duration() const {
  return triggdriver->get_postduration();
}


unsigned int SeqHalt::event(eventContext& context) const {
  Log<Seq> odinlog(this,"event");
  double startelapsed=context.elapsed;
  SeqObjBase::event(context);

  ODINLOG(odinlog,normalDebug) << "context.action/startelapsed=" << context.action << "/" << startelapsed << STD_endl;

  if(context.action==seqRun) {
    triggdriver->event(context,startelapsed);
  }
  context.increase_progmeter();
  return 1;
}


bool SeqHalt::prep() {
  if(!SeqObjBase::prep()) return false;
  return triggdriver->prep_halttrigger();
}


/////////////////////////////////////////////////////////////////////////////


SeqSnapshot::SeqSnapshot(const STD_string& object_label, const STD_string& snapshot_fname)
 : SeqObjBase(object_label), triggdriver(object_label)  {
  magn_fname=snapshot_fname;
}

SeqSnapshot::SeqSnapshot(const STD_string& object_label)
 : SeqObjBase(object_label), triggdriver(object_label)  {
}

SeqSnapshot& SeqSnapshot::operator = (const SeqSnapshot& ss) {
  SeqObjBase::operator = (ss);
  triggdriver=ss.triggdriver;
  magn_fname=ss.magn_fname;
  return *this;
}


unsigned int SeqSnapshot::event(eventContext& context) const {
  double startelapsed=context.elapsed;
  SeqObjBase::event(context);
  if(context.action==seqRun) {
    triggdriver->event(context,startelapsed);
  }
  context.increase_progmeter();
  return 1;
}

bool  SeqSnapshot::prep() {
  if(!SeqObjBase::prep()) return false;
  return triggdriver->prep_snaptrigger(magn_fname);
}

/////////////////////////////////////////////////////////////////////////////


SeqMagnReset::SeqMagnReset(const STD_string& object_label)
 : SeqObjBase(object_label), triggdriver(object_label)  {
}

SeqMagnReset& SeqMagnReset::operator = (const SeqMagnReset& smr) {
  SeqObjBase::operator = (smr);
  triggdriver=smr.triggdriver;
  return *this;
}


unsigned int SeqMagnReset::event(eventContext& context) const {
  double startelapsed=context.elapsed;
  SeqObjBase::event(context);
  if(context.action==seqRun) {
    triggdriver->event(context,startelapsed);
  }
  context.increase_progmeter();
  return 1;
}

bool  SeqMagnReset::prep() {
  if(!SeqObjBase::prep()) return false;
  return triggdriver->prep_resettrigger();
}

