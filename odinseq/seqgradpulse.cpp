#include "seqgradpulse.h"


SeqGradConstPulse::SeqGradConstPulse(const STD_string& object_label,direction gradchannel, float gradstrength,
                            float gradduration) : SeqGradChanList(object_label),
  constgrad(object_label+"_grad",gradchannel,gradstrength,gradduration),
  offgrad(object_label+"_off",gradchannel,0.0){

  SeqGradConstPulse::set_strength(gradstrength); // set duration of offgrad, do NOT call virtual functions in constructor
  (*this)+= constgrad + offgrad;
}


SeqGradConstPulse::SeqGradConstPulse(const SeqGradConstPulse& sgcp) {
  SeqGradConstPulse::operator = (sgcp);
}

SeqGradConstPulse::SeqGradConstPulse(const STD_string& object_label) : SeqGradChanList(object_label) {}


SeqGradConstPulse& SeqGradConstPulse::operator = (const SeqGradConstPulse& sgcp) {
  SeqGradChanList::operator = (sgcp);
  constgrad=sgcp.constgrad;
  offgrad=sgcp.offgrad;
  clear();
  (*this)+= constgrad + offgrad;
  return *this;
}

SeqGradInterface& SeqGradConstPulse::set_strength(float gradstrength) {
  constgrad.set_strength(gradstrength);
  offgrad.set_duration(systemInfo->get_grad_switch_time(constgrad.get_strength()));
  return *this;
}


/////////////////////////////////////////////////////////////////////////////////


SeqGradVectorPulse::SeqGradVectorPulse(const STD_string& object_label,
               direction gradchannel, float maxgradstrength,
       const fvector& trimarray, float gradduration) :
  SeqGradChanList(object_label),
  vectorgrad(object_label+"_grad",gradchannel,maxgradstrength,trimarray,gradduration),
  offgrad(object_label+"_off",gradchannel,0.0){

  SeqGradVectorPulse::set_strength(maxgradstrength); // set duration of offgrad, do NOT call virtual functions in constructor
  (*this)+= vectorgrad + offgrad;
}

SeqGradVectorPulse::SeqGradVectorPulse(const SeqGradVectorPulse& sgvp) {
  SeqGradVectorPulse::operator = (sgvp);
}

SeqGradVectorPulse::SeqGradVectorPulse(const STD_string& object_label) : SeqGradChanList(object_label) {
}

SeqGradVectorPulse& SeqGradVectorPulse::operator = (const SeqGradVectorPulse& sgvp) {
  SeqGradChanList::operator = (sgvp);
  vectorgrad=sgvp.vectorgrad;
  offgrad=sgvp.offgrad;
  clear();
  (*this)+= vectorgrad + offgrad;
  return *this;
}

SeqGradInterface& SeqGradVectorPulse::set_strength(float gradstrength) {
  vectorgrad.set_strength(gradstrength);
  offgrad.set_duration(systemInfo->get_grad_switch_time(vectorgrad.get_strength()));
  return *this;
}

