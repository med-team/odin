#include <odinseq/seqall.h>





class METHOD_CLASS : public SeqMethod {

 private:
  SeqPulsar exc;
  SeqPulsarReph exc_reph;
  SeqPulsarReph exc_reph_neg;


  SeqAcqRead acqread;
  SeqAcqRead negacqread; // need separate object since calling SeqAcqDeph on the same object with different polarity does not work
  SeqAcqDeph readdeph;
  SeqAcqDeph negreaddeph;

  SeqGradPhaseEncFlowComp pe;
  SeqGradPhaseEncFlowComp pe3d;

  SeqGradConstPulse crusher;

  SeqDelay exc2acq;
  SeqDelay relaxdelay;

  SeqObjList gradpart;
  SeqObjList scan;
  SeqObjList readout;

  SeqObjLoop peloop;
  SeqObjLoop peloop3d;
  SeqObjLoop reploop;

  SeqVecIter phaseiter;


  LDRfloat T1Ernst;


 public:
  METHOD_CLASS(const STD_string& label) : SeqMethod(label) {
    set_description("Fully flow-compensated FLASH sequence for Susceptibility Weighted Imaging. ");
  }

  void method_pars_init() {

    geometryInfo->set_Mode(voxel_3d);

    commonPars->set_MatrixSize(readDirection,256);
    commonPars->set_MatrixSize(phaseDirection,256,noedit);
    commonPars->set_RepetitionTime(50.0);
    commonPars->set_EchoTime(28.0);

    T1Ernst=1300.0;
    T1Ernst.set_minmaxval(0.0,5000.0).set_description("For optimum SNR, the flip angle will be set to the Ernst angle using this T1");
    append_parameter(T1Ernst,"T1Ernst");

//    fmapscan.init("fmapscan");
//    append_parameter(fmapscan.get_parblock(),"FieldMapPars");
  }


  void method_seq_init(){
    Log<Seq> odinlog(this,"method_seq_init");

    ///////////////// Pulses: /////////////////////

    // Excitation Pulse
    float spatres=3.0;
    float slicethick=geometryInfo->get_FOV(sliceDirection)-2.0*spatres;
    if(slicethick<spatres) slicethick=spatres;
    exc=SeqPulsarSinc("exc", slicethick, false, 4.0, commonPars->get_FlipAngle(), spatres, 512);
    exc.set_filter("Gauss");
    exc.set_freqoffset(systemInfo->get_gamma() * exc.get_strength() / (2.0*PII) *  geometryInfo->get_offset(sliceDirection) );
    exc.set_pulse_type(excitation);

//    ODINLOG(odinlog,significantDebug) << "exc.rel_magnetic_center/get_pulsduration" << exc.get_rel_magnetic_center() << "/" << exc.get_pulsduration() << STD_endl;
//    ODINLOG(odinlog,significantDebug) << "exc.get_Gz()" << exc.get_Gz() << STD_endl;

    // rephasing lobe for excitation pulse
    exc_reph=SeqPulsarReph("exc_reph",exc);

    exc_reph_neg=SeqPulsarReph("exc_reph",exc);
    exc_reph_neg.invert_strength();

    ////////////////// Geometry: /////////////////////////////////

    // calculate the resolution in the read Channel and set the number of phase encoding
    // steps so that we will obtain a uniform resolution in read and phase Channel:
    float resolution=secureDivision(geometryInfo->get_FOV(readDirection),commonPars->get_MatrixSize(readDirection));

    commonPars->set_MatrixSize(phaseDirection,int(secureDivision(geometryInfo->get_FOV(phaseDirection),resolution)+0.5),noedit);
    commonPars->set_MatrixSize(sliceDirection,int(secureDivision(geometryInfo->get_FOV(sliceDirection),resolution)+0.5),noedit);


    //////////////// Phase Encoding: //////////////////////////

    float t0=exc.get_duration()-exc.get_magnetic_center()+
                 exc_reph.get_duration()+
                 exc_reph.get_duration()+
                 exc_reph_neg.get_duration();

    pe=SeqGradPhaseEncFlowComp("pe", t0, commonPars->get_MatrixSize(phaseDirection), geometryInfo->get_FOV(phaseDirection),
                  phaseDirection, 0.25*systemInfo->get_max_grad(),
                  linearEncoding, noReorder, 1,
                  commonPars->get_ReductionFactor(), DEFAULT_ACL_BANDS, commonPars->get_PartialFourier());


    //////////////// Phase Encoding (3D): //////////////////////////

    pe3d=SeqGradPhaseEncFlowComp("pe3d", t0, commonPars->get_MatrixSize(sliceDirection),geometryInfo->get_FOV(sliceDirection),
                  sliceDirection, pe.get_strength(),
                  linearEncoding, noReorder, 1,
                  commonPars->get_ReductionFactor());


    //////////////// Readout: //////////////////////////////

    acqread=SeqAcqRead("acqread",commonPars->get_AcqSweepWidth(),commonPars->get_MatrixSize(readDirection),
                                 geometryInfo->get_FOV(readDirection),readDirection);

    negacqread=acqread;

    readdeph=SeqAcqDeph("readdeph", acqread);
    negreaddeph=SeqAcqDeph("negreaddeph", negacqread, spinEcho);

    //////////////// RF Spoiling: //////////////////////////////

    if(commonPars->get_RFSpoiling()) {

      exc.set_phasespoiling();
      acqread.set_phasespoiling();

      phaseiter=SeqVecIter("phaseiter");
      phaseiter.add_vector(exc.get_phaselist_vector());
      phaseiter.add_vector(acqread.get_phaselist_vector());
    }

    //////////////// Crusher: //////////////////////////////

    double crusher_strength=0.5*systemInfo->get_max_grad();
    double crusher_integral=4.0*fabs(readdeph.get_gradintegral().sum());
    double crusher_dur=secureDivision(crusher_integral, crusher_strength);
    crusher=SeqGradConstPulse("crusher",readDirection,crusher_strength,crusher_dur);


    //////////////// Field-map template: //////////////////////////////

//    fmapscan.build_seq(100.0, SeqObjList(), commonPars->get_RepetitionTime());


    //////////////// several padding delays ////////////////////

    exc2acq=SeqDelay("exc2acq");

    relaxdelay=SeqDelay("relaxdelay");


    //////////////// total sequence: //////////////////////////////

    gradpart=SeqObjList("gradpart");

    scan=SeqObjList("scan");

    readout=SeqObjList("readout");


    peloop=SeqObjLoop("peloop");

    peloop3d=SeqObjLoop("peloop3d");

    reploop=SeqObjLoop("reploop");

    gradpart = (negreaddeph+readdeph)/pe/pe3d;

    readout = exc + exc_reph + exc_reph + exc_reph_neg + gradpart + exc2acq + readdeph + acqread + crusher;

    if(commonPars->get_RFSpoiling()) readout += phaseiter;

//    scan += fmapscan + relaxdelay;

    scan+=  reploop(
              peloop3d(
                peloop(
                  readout + relaxdelay
                )[pe]
              )[pe3d]
            )[commonPars->get_NumOfRepetitions()];


    set_sequence( scan );
  }


  void method_rels(){

    // TE
    double minTE=exc.get_duration()-exc.get_magnetic_center()+
                 exc_reph.get_duration()+
                 exc_reph.get_duration()+
                 exc_reph_neg.get_duration()+
                 gradpart.get_duration()+
                 readdeph.get_duration()+
                 acqread.get_acquisition_center();

    if(commonPars->get_EchoTime()<minTE) commonPars->set_EchoTime(minTE);
    exc2acq=commonPars->get_EchoTime()-minTE;


    // TR
    // calculate relaxdelay to get the desired repetition time
    float readoutdur=readout.get_duration();
    if(readoutdur > commonPars->get_RepetitionTime()) commonPars->set_RepetitionTime(readoutdur);
    relaxdelay.set_duration( commonPars->get_RepetitionTime()-readoutdur );

    // calculate Ernst angle accordng to TR
    float flipangle=180.0/PII * acos( exp ( -secureDivision ( commonPars->get_RepetitionTime(), T1Ernst) ) );
    commonPars->set_FlipAngle( flipangle, noedit );
    exc.set_flipangle( flipangle );

  }


  void method_pars_set(){

    // inform the readout about the used phase encoding and slice vector (for automatic reconstruction)
    acqread.set_reco_vector(line,pe);
    acqread.set_reco_vector(line3d,pe3d);

    recoInfo->set_PreProc3D("swi");
    recoInfo->set_PostProc3D("mip");
//    recoInfo->set_CmdLineOpts("-ff CosSq -fp 0.5");
  }

};




// entry point for the sequence module
ODINMETHOD_ENTRY_POINT


