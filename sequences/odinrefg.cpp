#include <odinseq/seqall.h>


class METHOD_CLASS : public SeqMethod {

 public:
  METHOD_CLASS(const STD_string& label);
  ~METHOD_CLASS() {delete_objs();}

  void method_pars_init();
  void method_seq_init();
  void method_rels();
  void method_pars_set();
  unsigned int numof_testcases() const {return 2;}



 private:
  void delete_objs();

  LDRint    NumOfRefgPulses;
  LDRdouble RefgainStartingValue;
  LDRdouble MaxFlipangle;
  LDRdoubleArr AttenuationVals;
  LDRdouble PulseGain;
  LDRdouble TemplPulseDuration;
  LDRdouble PulseDuration;

  LDRint    NumOfDummyScans;
  LDRint    NumOfReadPoints;
  LDRbool   TestMode;

  SeqPulsar* exc_pulsar;

  SeqPulsNdim** exc;
  unsigned int numof_pulses;

  SeqPulsarReph reph;

  SeqAcqEPI* acqepi;
  SeqAcqEPI* acqtemplate;
  SeqAcq* acq;


  SeqAcqDeph deph;
  SeqAcqDeph dephtmpl;

  SeqObjLoop dummyloop;
  SeqDelay dummyacqdelay;

  SeqDelay relaxdelay;

  SeqObjVector exc_vector;

  SeqObjList slicepart;

  SeqObjLoop excloop;
  SeqObjLoop acculoop;
};


//////////////////////////////////////////////////////////////////////////////////////////

void METHOD_CLASS::delete_objs() {
  if(exc_pulsar) delete exc_pulsar;  exc_pulsar=0;


  // Use pointer-to-pointer because delete[] for objects does not work correctly on GCC2.9
  if(exc) {
    for(unsigned int i=0; i<numof_pulses; i++) delete exc[i];
    delete[] exc;
    exc=0;
  }

  if(acqepi) delete acqepi; acqepi=0;
  if(acqtemplate) delete acqtemplate; acqtemplate=0;
  if(acq) delete acq; acq=0;
}


METHOD_CLASS::METHOD_CLASS (const STD_string& label)
                                   : SeqMethod(label) {

  set_description("This method can be used to measure the spatial reference "
                  "gain for the pulse power, i.e. the spatial distribution of B1 "
                  "for a given setup. This is done by applying RF pulses with "
                  "different field strength followed by an EPI readout. "
                  "The oscilation of the image intensity due to the different "
                  "flip angles is then used to calculate the reference gain. ");

  exc_pulsar=0;
  exc=0;
  numof_pulses=0;
  acqepi=0;
  acqtemplate=0;
  acq=0;

}


void METHOD_CLASS::method_pars_init() {


  RefgainStartingValue=22.0;
  MaxFlipangle=500.0;
  NumOfRefgPulses=16;
  NumOfDummyScans=3;
  commonPars->set_MatrixSize(readDirection,48);
  commonPars->set_AcqSweepWidth(100.0);
  TestMode=false;

  TemplPulseDuration=4.0;

  // alternative default settings for sequence test
  if(get_current_testcase()==1) {
    TestMode=true;
  }

  if(systemInfo->get_platform()!=numaris_4) {
    append_parameter(RefgainStartingValue,"RefgainStartingValue");
    append_parameter(MaxFlipangle,"MaxFlipangle");
    append_parameter(NumOfRefgPulses,"NumOfRefgPulses");
    append_parameter(AttenuationVals,"AttenuationVals",noedit);
    append_parameter(PulseGain,"PulseGain",noedit);
    append_parameter(TemplPulseDuration,"TemplPulseDuration");
    append_parameter(PulseDuration,"PulseDuration",noedit);
    append_parameter(NumOfDummyScans,"NumOfDummyScans");
    append_parameter(NumOfReadPoints,"NumOfReadPoints",noedit);
  }
  append_parameter(TestMode,"TestMode");

}


void METHOD_CLASS::method_seq_init() {
  delete_objs();

  relaxdelay=SeqDelay("relaxdelay",commonPars->get_RepetitionTime());

  float resolution=secureDivision(geometryInfo->get_FOV(readDirection),commonPars->get_MatrixSize(readDirection));
  commonPars->set_MatrixSize(phaseDirection,int(secureDivision(geometryInfo->get_FOV(phaseDirection),resolution)));
  commonPars->set_MatrixSize(phaseDirection,(commonPars->get_MatrixSize(phaseDirection)/2)*2);

  acqepi=new SeqAcqEPI("acqepi",commonPars->get_AcqSweepWidth(),
                     commonPars->get_MatrixSize(readDirection),geometryInfo->get_FOV(readDirection),
                     commonPars->get_MatrixSize(phaseDirection),geometryInfo->get_FOV(phaseDirection),
                     1,1,1.0,systemInfo->get_main_nucleus());



  // use sagittal slice for reference gain
  Geometry sag; // dummy to calculate correct rotation matrix
  sag.set_orientation(sagittal);
  acqepi->set_gradrotmatrix(sag.get_gradrotmatrix());


  acqtemplate=new SeqAcqEPI(*acqepi);
  acqtemplate->set_label("acqtemplate");
  acqtemplate->set_template_type(phasecorr_template);
  acqtemplate->set_gradrotmatrix(sag.get_gradrotmatrix());


  deph=SeqAcqDeph("deph",*acqepi);
  deph.set_gradrotmatrix(sag.get_gradrotmatrix());
  dephtmpl=SeqAcqDeph("dephtmpl",*acqtemplate);
  dephtmpl.set_gradrotmatrix(sag.get_gradrotmatrix());

  NumOfReadPoints=acqepi->get_npts_read();


  numof_pulses=NumOfRefgPulses;

  if(!TestMode) exc=new SeqPulsNdim*[numof_pulses];

  if(!TestMode) systemInfo->set_reference_gain(RefgainStartingValue);

  AttenuationVals.resize(NumOfRefgPulses);

  // allocate template pulse
  exc_pulsar=new SeqPulsarSinc("exc",5.0,true,TemplPulseDuration);
  exc_pulsar->set_nucleus(systemInfo->get_main_nucleus());
  exc_pulsar->set_gradrotmatrix(sag.get_gradrotmatrix());


  PulseGain=exc_pulsar->get_pulse_gain();
  PulseDuration=exc_pulsar->get_pulsduration();

  reph=SeqPulsarReph("reph",*exc_pulsar);


  float angle;
  int i,index;


  if(TestMode) {
    fvector flips(NumOfRefgPulses);
    for (i=0;i<NumOfRefgPulses;i++) {
      angle=(float)(i+1)/(float)(NumOfRefgPulses)*180.0;
      flips[i]=angle;
      AttenuationVals(i)=angle;
    }
    exc_pulsar->set_flipangles(flips);

  } else {

    // linear flipangle
    for (i=0;i<NumOfRefgPulses;i++) {
      index=i;
      angle=float(i+1)/float(NumOfRefgPulses)*MaxFlipangle;
      exc_pulsar->set_flipangle(angle);
      exc[index]=new SeqPulsNdim(*exc_pulsar);
      exc[index]->set_gradrotmatrix(sag.get_gradrotmatrix());
      AttenuationVals(index)=exc[index]->get_power();
    }
  }


  slicepart=SeqObjList("slicepart");

  dummyloop=SeqObjLoop("dummyloop");
  dummyacqdelay=SeqDelay("dummyacqdelay",acqepi->get_duration());

  if(NumOfDummyScans>0) {
    if(TestMode) {
      slicepart += dummyloop( *exc_pulsar                      + dephtmpl   + dummyacqdelay +  relaxdelay + *exc_pulsar                      + deph  +  dummyacqdelay +  relaxdelay) [NumOfDummyScans];
    } else {
      slicepart += dummyloop( *(exc[NumOfRefgPulses-1]) + reph + dephtmpl   + dummyacqdelay +  relaxdelay + *(exc[NumOfRefgPulses-1]) + reph + deph  +  dummyacqdelay +  relaxdelay) [NumOfDummyScans];
    }
  }

  exc_vector=SeqObjVector("exc_vector");
  excloop=SeqObjLoop("excloop");
  acculoop=SeqObjLoop("acculoop");

  if(!TestMode) {
    for (i=0;i<NumOfRefgPulses;i++) {
      exc_vector += *(exc[i]);
    }
    delete exc_pulsar; exc_pulsar=0;
  }


  if(TestMode) {
    slicepart += excloop ( *exc_pulsar       + dephtmpl  + (*acqtemplate) + relaxdelay ) [exc_pulsar->get_flipangle_vector()]
               + excloop ( *exc_pulsar       + deph      + (*acqepi)      + relaxdelay ) [exc_pulsar->get_flipangle_vector()];
  } else {
    slicepart += excloop ( exc_vector + reph + dephtmpl  + (*acqtemplate) + relaxdelay ) [exc_vector]
               + excloop ( exc_vector + reph + deph      + (*acqepi)      + relaxdelay ) [exc_vector];
  }

  set_sequence(  acculoop(slicepart)[commonPars->get_NumOfRepetitions()]  );
}



void METHOD_CLASS::method_rels() {
}


void METHOD_CLASS::method_pars_set() {

  if(TestMode) {
    acqepi->set_reco_vector(userdef,exc_pulsar->get_flipangle_vector());
    acqtemplate->set_reco_vector(userdef,exc_pulsar->get_flipangle_vector());
  } else {
    acqepi->set_reco_vector(userdef,exc_vector,AttenuationVals);
    acqtemplate->set_reco_vector(userdef,exc_vector,AttenuationVals);
    recoInfo->set_PostProc3D("usercoll | refgain("+ftos(PulseDuration)+","+ftos(PulseGain)+")");
  }
}



// entry point for the sequence module
ODINMETHOD_ENTRY_POINT
