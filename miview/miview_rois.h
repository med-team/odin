/***************************************************************************
                          miview_rois.h  -  description
                             -------------------
    begin                : Mon Feb 15 10:00:00 CEST 2021
    copyright            : (C) 2000-2021 by Thies H. Jochimsen
    email                : thies@jochimsen.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MIVIEW_ROIS_H
#define MIVIEW_ROIS_H

#include <odinpara/ldrtypes.h>
#include <odinpara/ldrblock.h>

// forward declarations
class Protocol;

////////////////////////////////////////////////////////////

class MiViewRois : public LDRblock {

 public:
  MiViewRois();
  ~MiViewRois();

  bool init(int nx, int ny, int nz);

  bool is_valid() const {return valid;}

  const farray& get_overlay_map(int current_z=-1) const;
  bool write_overlay_map(const STD_string& filename, const Protocol& prot);

  void new_slice(const float *data, int slice);

  void update();

 private:
  LDRbool roismode;
  LDRaction clearall;
  LDRaction clearslice;

  mutable farray overlay_map;
  bool valid;

};



#endif


