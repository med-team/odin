
#include "geoeditlabel.h"
#include "geoeditview.h"


#include <tjutils/tjlog_code.h>

const char* GeoEditComp::get_compName() {return "GeoEdit";}
LOGGROUNDWORK(GeoEditComp)


////////////////////////////////////////////////////////////////////////////


GeoEditLabel::GeoEditLabel(const Image& background, unsigned int coarseFactor, QWidget *parent )
    : floatBox3D(background.get_magnitude().c_array(),0.0,0.0,
                 background.size(xAxis),background.size(yAxis),background.size(zAxis),true,
                 coarseFactor,parent,background.get_label().c_str()),
      backgr(background), coarse(coarseFactor), ia_cache(0) {
}


void GeoEditLabel::drawSliceProjection(const darray& cornersProj, GuiPainter& painter) {
  painter.setPen(PROJECTION_COLOR,1,true);
  painter.moveTo(xcoord2labelxpos(cornersProj(0,0)),ycoord2labelypos(cornersProj(0,1)));
  for(unsigned int icorner=1; icorner<4; icorner++) {
    painter.lineTo(xcoord2labelxpos(cornersProj(icorner,0)),ycoord2labelypos(cornersProj(icorner,1)));
  }
  painter.lineTo(xcoord2labelxpos(cornersProj(0,0)),ycoord2labelypos(cornersProj(0,1)));
}


void GeoEditLabel::drawSliceCrossSection(const darray& connectPoints, double slicethick, GuiPainter& painter, unsigned int slice) {
  Log<GeoEditComp> odinlog("GeoEditLabel","drawSliceCrossSection");

  int penwidth=(int)(slicethick/backgr.get_geometry().get_FOV(readDirection)*float(backgr.size(xAxis))*float(coarse));
  if(penwidth<1) penwidth=1;
  painter.setPen(CROSSSECT_COLOR,penwidth);
  int xstart=xcoord2labelxpos(connectPoints(0,0));
  int ystart=ycoord2labelypos(connectPoints(0,1));
  int xend=xcoord2labelxpos(connectPoints(1,0));
  int yend=ycoord2labelypos(connectPoints(1,1));

  ODINLOG(odinlog,normalDebug) << "xstart/ystart/xend/yend=" << xstart << "/" << ystart << "/" << xend << "/" << yend << STD_endl;

  painter.moveTo(xstart,ystart);
  painter.lineTo(xend,yend);

  painter.drawText((xend+xstart)/2, (yend+ystart)/2,itos(slice).c_str(),CROSSSECT_COLOR);
}


void GeoEditLabel::drawVoxelProjection(const darray& cornersProj, GuiPainter& painter) {
  unsigned int icorner;

  painter.setPen(PROJECTION_COLOR,1,true);

  for(unsigned int ilevel=0; ilevel<2; ilevel++) {
    painter.moveTo(xcoord2labelxpos(cornersProj(ilevel,0,0)),ycoord2labelypos(cornersProj(ilevel,0,1)));
    for(unsigned int icorner=1; icorner<4; icorner++) {
      painter.lineTo(xcoord2labelxpos(cornersProj(ilevel,icorner,0)),ycoord2labelypos(cornersProj(ilevel,icorner,1)));
    }
    painter.lineTo(xcoord2labelxpos(cornersProj(ilevel,0,0)),ycoord2labelypos(cornersProj(ilevel,0,1)));
  }

  for(icorner=0; icorner<4; icorner++) {
    painter.moveTo(xcoord2labelxpos(cornersProj(0,icorner,0)),ycoord2labelypos(cornersProj(0,icorner,1)));
    painter.lineTo(xcoord2labelxpos(cornersProj(1,icorner,0)),ycoord2labelypos(cornersProj(1,icorner,1)));
  }
}


void GeoEditLabel::drawReadVector(const dvector& readvecstart_proj, const dvector& readvec_proj, GuiPainter& painter) {
  painter.setPen(PROJECTION_COLOR);

  dvector readvec_end(2);
  readvec_end=readvecstart_proj+readvec_proj;

  dvector readvec_proj_perp(2);
  readvec_proj_perp[0]=-readvec_proj[1];
  readvec_proj_perp[1]= readvec_proj[0];
  
  dvector readvec_arrow_end(2);
  readvec_arrow_end=readvecstart_proj+(1.0-READVEC_ARROW_LENGTH)*readvec_proj+READVEC_ARROW_LENGTH*readvec_proj_perp;

  painter.moveTo(xcoord2labelxpos(readvecstart_proj[0]),ycoord2labelypos(readvecstart_proj[1]));
  painter.lineTo(xcoord2labelxpos(readvec_end[0]),ycoord2labelypos(readvec_end[1]));
  painter.lineTo(xcoord2labelxpos(readvec_arrow_end[0]),ycoord2labelypos(readvec_arrow_end[1]));
}


void GeoEditLabel::drawImagingArea(const Geometry& ia, bool drawProj, bool drawCross) {
  Log<GeoEditComp> odinlog("GeoEditLabel","drawImagingArea");


  ia_cache=&ia;
  drawProj_cache=drawProj;
  drawCross_cache=drawCross;

  // init painter
  label->init_pixmap();
  GuiPainter painter(label->pixmap);


  // first, get corner points in coordinate system of background image
  ODINLOG(odinlog,normalDebug) << "get_current_z()=" << get_current_z() << STD_endl;
  darray cornerPoints=ia.get_cornerPoints(backgr.get_geometry(),floatBox3D::get_current_z());


  // will be 1 for slicepack mode or 2 for 3D/Voxel mode
  int n_slice_bounds=cornerPoints.size(3);

  bool is3d=(n_slice_bounds>1);

  if(is3d) drawCross=false;

  darray corners(4,3);
  darray corners3d(2,4,3);
  dvector cornervec1(3);
  dvector cornervec2(3);


  darray crosspoints(2,2);

  darray allCornersTemp(4,2);
  darray allCornersTemp3d(2,4,2);


  // the coordinate system of the plot
  axis projAxis1=xAxis;
  axis projAxis2=yAxis;
  axis perpax=zAxis;


  unsigned int n_slices=cornerPoints.size(0);
  unsigned int midslice=n_slices/2;

  dvector readvec(3);
  dvector readvecstart(3);
  for(unsigned int idir=0;idir<3;idir++) {
    readvec[idir]=REL_READVEC_LENGTH*(cornerPoints(midslice,upperBound,0,0,idir)-cornerPoints(midslice,lowerBound,0,0,idir));
    readvecstart[idir]=cornerPoints(midslice,lowerBound,lowerBound,0,idir);
  }
  ODINLOG(odinlog,normalDebug) << "readvec=" << readvec.printbody() << STD_endl;

  dvector readvec_proj(2);
  readvec_proj[0]=readvec[projAxis1];
  readvec_proj[1]=readvec[projAxis2];

  dvector readvecstart_proj(2);
  readvecstart_proj[0]=readvecstart[projAxis1];
  readvecstart_proj[1]=readvecstart[projAxis2];

  if(drawProj) drawReadVector(readvecstart_proj,readvec_proj,painter);


  for(unsigned int j=0;j<n_slices;j++) {
    for(unsigned int idir=0;idir<3;idir++) {

      // corner points of slice
      corners(0,idir)=cornerPoints(j,lowerBound,lowerBound,lowerBound,idir);
      corners(1,idir)=cornerPoints(j,lowerBound,upperBound,lowerBound,idir);
      corners(2,idir)=cornerPoints(j,upperBound,upperBound,lowerBound,idir);
      corners(3,idir)=cornerPoints(j,upperBound,lowerBound,lowerBound,idir);


      // corner points of voxel
      corners3d(0,0,idir)=cornerPoints(j,lowerBound,lowerBound,lowerBound,idir);
      corners3d(0,1,idir)=cornerPoints(j,lowerBound,upperBound,lowerBound,idir);
      corners3d(0,2,idir)=cornerPoints(j,upperBound,upperBound,lowerBound,idir);
      corners3d(0,3,idir)=cornerPoints(j,upperBound,lowerBound,lowerBound,idir);
      corners3d(1,0,idir)=cornerPoints(j,lowerBound,lowerBound,upperBound,idir);
      corners3d(1,1,idir)=cornerPoints(j,lowerBound,upperBound,upperBound,idir);
      corners3d(1,2,idir)=cornerPoints(j,upperBound,upperBound,upperBound,idir);
      corners3d(1,3,idir)=cornerPoints(j,upperBound,lowerBound,upperBound,idir);

    }

    unsigned int nextcorner;
    unsigned int icross=0;
    for(unsigned int icorner=0;icorner<4;icorner++) {

      if(is3d) {
        allCornersTemp3d(0,icorner,0)=corners3d(0,icorner,projAxis1);
        allCornersTemp3d(0,icorner,1)=corners3d(0,icorner,projAxis2);
        allCornersTemp3d(1,icorner,0)=corners3d(1,icorner,projAxis1);
        allCornersTemp3d(1,icorner,1)=corners3d(1,icorner,projAxis2);
      } else {
        allCornersTemp(icorner,0)=corners(icorner,projAxis1);
        allCornersTemp(icorner,1)=corners(icorner,projAxis2);
      }

      if(drawCross) {
        nextcorner=icorner+1;
        if(nextcorner==4) nextcorner=0;
        unsigned int idir;
        for(idir=0;idir<3;idir++) cornervec1[idir]=corners(icorner,idir);
        for(idir=0;idir<3;idir++) cornervec2[idir]=corners(nextcorner,idir);
        double nenner=cornervec2[perpax]-cornervec1[perpax];
        if( ( (int)(nenner*1000.0)/1000 ) !=0.0 ) {
          double s=-cornervec1[perpax]/nenner;
          if( (s>0.0) && (s<1.0) && (icross<2) ) {
            crosspoints(icross,0)=cornervec1[projAxis1]+s*(cornervec2[projAxis1]-cornervec1[projAxis1]);
            crosspoints(icross,1)=cornervec1[projAxis2]+s*(cornervec2[projAxis2]-cornervec1[projAxis2]);
            icross++;
          }
        }
      }
    }

    if(drawProj) {
      if(is3d) drawVoxelProjection(allCornersTemp3d,painter);
      else     drawSliceProjection(allCornersTemp,painter);
    }

    if(drawCross && (icross==2)) {
      ODINLOG(odinlog,normalDebug) << "crosspoints(" << j << ")=" << crosspoints.printbody() << STD_endl;
      drawSliceCrossSection(crosspoints,ia.get_sliceThickness(),painter,j);
    }

  }

  painter.end();

  label->set_pixmap();
}


