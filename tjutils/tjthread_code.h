#include <tjutils/tjthread.h>
#include <tjutils/tjlog.h>


template<typename In, typename Out, typename Local>
void ThreadedLoop<In,Out,Local>::WorkThread::run() {
  Log<ThreadComponent> odinlog("WorkThread","run");
  while(1) {

    process.wait();
    process.reset(); // Reset for next event
    ODINLOG(odinlog,normalDebug) << "catched process signal, cont=" << tloop->cont << STD_endl;
    if(!tloop->cont) break;

    ODINLOG(odinlog,normalDebug) << "processing thread " << begin << "/" << end << STD_endl;

    status=tloop->kernel(*tloop->in_cache, *out_cache, local, begin, end);

    ODINLOG(odinlog,normalDebug) << "signaling finished=" << status << STD_endl;
    finished.signal();

    if(!status) break;
  }
}

//////////////////////////////////////////////////////////////////////////


template<typename In, typename Out, typename Local>
bool ThreadedLoop<In,Out,Local>::init(unsigned int numof_threads, unsigned int loopsize) {
  Log<ThreadComponent> odinlog("ThreadedLoop","init");
  mainbegin=0;
  mainend=loopsize;
#ifndef NO_THREADS
  destroy(); // stop old threads
  ODINLOG(odinlog,normalDebug) << "numof_threads=" << numof_threads << STD_endl;
  if(numof_threads>1) {
    threads.resize(numof_threads-1); // the main thread is also used
    unsigned int onesize=loopsize/numof_threads;
    unsigned int rest=loopsize%numof_threads;
    unsigned int count=0;
    for(unsigned int i=0; i<(numof_threads-1); i++) {
      threads[i]=new WorkThread(this);
      threads[i]->begin=count;
      count+=onesize;
      if(i<rest) count++;
      threads[i]->end=count;
      threads[i]->start();
    }
    mainbegin=count;
    count+=onesize;
    if((numof_threads-1)<rest) count++;
    mainend=count;
  }
#endif
  return true;
}

template<typename In, typename Out, typename Local>
void ThreadedLoop<In,Out,Local>::destroy() {
  Log<ThreadComponent> odinlog("ThreadedLoop","destroy");
#ifndef NO_THREADS
  cont=false; // Stop threads
  for(unsigned int i=0; i<threads.size(); i++) {
    threads[i]->process.signal();
    threads[i]->wait();
    delete threads[i];
  }
  threads.resize(0);
#endif
  }

template<typename In, typename Out, typename Local>
bool ThreadedLoop<In,Out,Local>::execute(const In& in, STD_vector<Out>& outvec) {
  Log<ThreadComponent> odinlog("ThreadedLoop","execute");
#ifdef NO_THREADS
  outvec.resize(1);
  return kernel(in, outvec[0], mainlocal, mainbegin, mainend);
#else

  unsigned int nthreads=threads.size();

  outvec.resize(nthreads+1);

  if(nthreads) {
    in_cache=&in;
    cont=true;

    for(unsigned int i=0; i<nthreads; i++) {
      threads[i]->out_cache=&(outvec[i]);
      threads[i]->status=true;
      threads[i]->process.signal();
    }
  }

  bool result=kernel(in, outvec[nthreads], mainlocal, mainbegin, mainend); // Use mainthread also for kernel

  if(nthreads) {
    for(unsigned int i=0; i<nthreads; i++) {
      threads[i]->finished.wait(); // Wait for result
      threads[i]->finished.reset();
      if(!threads[i]->status) result=false;
    }
    ODINLOG(odinlog,normalDebug) << "finished.wait() done" << STD_endl;
  }

  return result;
#endif
}


