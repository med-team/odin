#include "tjtypes.h"

unsigned int TypeTraits::typesize(const STD_string& typelabel) {
  if(typelabel==TypeTraits::type2label((u8bit)0))  return 1;
  if(typelabel==TypeTraits::type2label((s8bit)0))  return 1;
  if(typelabel==TypeTraits::type2label((u16bit)0)) return 2;
  if(typelabel==TypeTraits::type2label((s16bit)0)) return 2;
  if(typelabel==TypeTraits::type2label((u32bit)0)) return 4;
  if(typelabel==TypeTraits::type2label((s32bit)0)) return 4;
  if(typelabel==TypeTraits::type2label((float)0))  return sizeof(float);
  if(typelabel==TypeTraits::type2label((double)0)) return sizeof(double);
  if(typelabel==TypeTraits::type2label((STD_complex)0)) return sizeof(STD_complex);
//  if(typelabel==TypeTraits::type2label(STD_string(""))) return sizeof(STD_string); // string does not have fixed size
  return 0;
}

