//
// C++ Implementation: filter_nan
//
// Description: 
//
//
// Author:  <Enrico Reimer>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "filter_nan.h"

void FilterNaN::init(){
  replace=0;
  replace.set_description("Replacement value");
  append_arg(replace,"replace");
}

bool FilterNaN::process(Data<float,4>& data, Protocol& prot)const{
  //replace all elements by replace where element does NOT equal itself (which is only true for NaN)
  TinyVector<int,4> indexvec;
  for(unsigned int i=0; i<data.numElements(); i++) {
    indexvec=data.create_index(i);
    float val=data(indexvec);
    if(!(val == val)) data(indexvec)=replace;
  }
  return true;
}
