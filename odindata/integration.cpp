#include "integration.h"

#include <tjutils/tjtest.h>

#ifdef HAVE_LIBGSL

#include <gsl/gsl_integration.h>

double Integrand::get_integral(double xmin, double xmax, unsigned int max_subintervals, double error_limit) const {
  FunctionIntegral fi(*this,max_subintervals,error_limit);
  return fi.get_integral(xmin,xmax);
}

//////////////////////////////////////////////////////////////

struct GslData4Integr {
  gsl_integration_workspace* w;
};

//////////////////////////////////////////////////////////////

FunctionIntegral::FunctionIntegral(const Integrand& func, unsigned int max_subintervals, double error_limit)
 : f(func), n_intervals(max_subintervals), errlimit(error_limit)  {
  gsldata=new GslData4Integr;
  gsldata->w = gsl_integration_workspace_alloc(n_intervals);
}

FunctionIntegral::~FunctionIntegral() {
  gsl_integration_workspace_free(gsldata->w);
  delete gsldata;
}

double FunctionIntegral::get_integral(double xmin, double xmax) const {
  double result, error;
  gsl_function gsl_F;
  gsl_F.function = &integrand;
  gsl_F.params = (void*)&f;
  gsl_integration_qags (&gsl_F, xmin, xmax, 0, errlimit, n_intervals, gsldata->w, &result, &error);
  return result;
}

double FunctionIntegral::integrand(double x, void *params) {
  const Integrand* integr=(const Integrand*)params;
  return integr->evaluate(x);
}

//////////////////////////////////////////////////////////////
// Unit test


#ifndef NO_UNIT_TEST
class FunctionIntegralTest : public UnitTest {

  struct QuadrFunction : public Integrand {
    double evaluate(double x) const {return x*x;}
  };

  struct QuadrIntFunction : public Integrand {
    double evaluate(double x) const {return qf.get_integral(0.0,x);}
    QuadrFunction qf;
  };


 public:
  FunctionIntegralTest() : UnitTest("FunctionIntegral") {}

 private:
  bool check() const {
    Log<UnitTest> odinlog(this,"check");

    // testing quadratic integral function
    QuadrIntFunction func;
    STD_string expected_str=ftos(1.0/12.0);
    STD_string calculated_str=ftos(func.get_integral(0.0,1.0));

    if(calculated_str!=expected_str) {
      ODINLOG(odinlog,errorLog) << "integral=" << calculated_str << ", but expected integral=" << expected_str << STD_endl;
      return false;
    }

    return true;
  }

};

void alloc_FunctionIntegralTest() {new FunctionIntegralTest();} // create test instance
#endif




#else
#error "GNU Scientific library is missing!"
#endif




