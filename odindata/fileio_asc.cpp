#include "fileio.h"

//////////////////////////////////////////////////////////////

struct AsciiFormat : public FileFormat {
  STD_string description() const {return "ASCII";}
  svector suffix() const  {
    svector result; result.resize(1);
    result[0]="asc";
    return result;
  }
  svector dialects() const {
    svector result; result.resize(1);
    result[0]="tcourse";
    return result;
  }
  int read(Data<float,4>& data, const STD_string& filename, const FileReadOpts& opts, Protocol& prot) {
    STD_string ascfile;
    if(::load(ascfile,filename)<0) return -1;
    unsigned int nelements=tokens(ascfile).size();
    if(tolowerstr(opts.dialect)=="tcourse") data.resize(nelements,1,1,1);
    else                                    data.resize(1,nelements,1,1);
    if(data.read_asc_file(filename)<0) return -1;
    return nelements;
  }
  int write(const Data<float,4>& data, const STD_string& filename, const FileWriteOpts&, const Protocol& prot) {
    return data.write_asc_file(filename);
  }
};

//////////////////////////////////////////////////////////////
struct MatlabAsciiFormat : public FileFormat {
  STD_string description() const {return "Matlab ascii 2D data matrix";}
  svector suffix() const  {
    svector result; result.resize(1);
    result[0]="dat";
    return result;
  }
  svector dialects() const {return svector();}

  int read(Data<float,4>& data, const STD_string& filename, const FileReadOpts& opts, Protocol& prot) {
    STD_string str;
    if(::load(str, filename)<0) return -1;
    sarray table(parse_table(str));
    int nrows=table.size(0);
    int ncols=table.size(1);
    data.resize(1,1,nrows,ncols);
    for(int irow=0; irow<nrows; irow++) {
      for(int icol=0; icol<ncols; icol++) {
        data(0,0,irow,icol)=atof(table(irow,icol).c_str());
      }
    }
    return 1;
  }

  int write(const Data<float,4>& data, const STD_string& filename, const FileWriteOpts&, const Protocol& prot) {
    int nrows=data.extent(2);
    int ncols=data.extent(3);
    sarray table(nrows,ncols);
    for(int irow=0; irow<nrows; irow++) {
      for(int icol=0; icol<ncols; icol++) {
        table(irow,icol)=ftos(data(0,0,irow,icol));
      }
    }
    if(::write(print_table(table), filename)<0) return -1;
    return 1;
  }

};

//////////////////////////////////////////////////////////////

struct PosFormat : public FileFormat {
  STD_string description() const {return "x-y positions of non-zeroes in ASCII";}
  svector suffix() const  {
    svector result; result.resize(1);
    result[0]="pos";
    return result;
  }
  svector dialects() const {return svector();}

  int read(Data<float,4>& data, const STD_string& filename, const FileReadOpts& opts, Protocol& prot) {
    Log<FileIO> odinlog("PosFormat","read");
    ODINLOG(odinlog,errorLog) << "Implement me" << STD_endl;
    return -1;
  }
  int write(const Data<float,4>& data, const STD_string& filename, const FileWriteOpts&, const Protocol& prot) {
    int nx=data.extent(3);
    int ny=data.extent(2);
    STD_ofstream ofs(filename.c_str());
    if(ofs.bad()) return -1;
    for(unsigned int i=0; i<data.numElements(); i++) {
      TinyVector<int,4> index=data.create_index(i);
      if(data(index)>0.0) {
        int ix=index(3);
        int iy=index(2);
        ofs << ftos(float(ix)/float(nx)-0.5) << " " << ftos(float(iy)/float(ny)-0.5) << STD_endl;
      }
    }
    return 1;
  }
};

//////////////////////////////////////////////////////////////

struct IndexFormat : public FileFormat {
  STD_string description() const {return "3D-indices of non-zeroes in ASCII";}
  svector suffix() const  {
    svector result; result.resize(1);
    result[0]="idx";
    return result;
  }
  svector dialects() const {
    svector result; result.resize(1);
    result[0]="addval";
    return result;
  }

  int read(Data<float,4>& data, const STD_string& filename, const FileReadOpts& opts, Protocol& prot) {
    Log<FileIO> odinlog("IndexFormat","read");
    int nx=prot.seqpars.get_MatrixSize(readDirection);
    int ny=prot.seqpars.get_MatrixSize(phaseDirection);
    int nz=prot.seqpars.get_MatrixSize(sliceDirection);
    ODINLOG(odinlog,normalDebug) << "nz/ny/nx=" << nz << "/" << ny << "/" << nx << STD_endl;

    data.resize(1,nz,ny,nx);
    data=0.0;

    STD_ifstream ifs(filename.c_str());
    if(ifs.bad()) return -1;

    STD_string oneline;
    while (std::getline (ifs, oneline)) {
      svector toks=tokens(oneline);
      int ntoks=toks.size();
      ODINLOG(odinlog,normalDebug) << "ntoks=" << ntoks << STD_endl;
      if(ntoks>=3 && ntoks<=4) {
        int ix=atoi(toks[ntoks-1].c_str());
        int iy=atoi(toks[ntoks-2].c_str());
        int iz=atoi(toks[ntoks-3].c_str());
        if(ix>=0 && ix<nx && iy>=0 && iy<ny && iz>=0 && iz<nz) {
          if(ntoks==4) data(0,iz,iy,ix)=atof((toks[0].c_str()));
          else data(0,iz,iy,ix)=1.0;
        }
      }
    }

    ifs.close();
    return 1;
  }

  int write(const Data<float,4>& data, const STD_string& filename, const FileWriteOpts& opts, const Protocol& prot) {
    STD_ofstream ofs(filename.c_str());
    if(ofs.bad()) return -1;
    bool addval=(opts.dialect=="addval");
    for(unsigned int i=0; i<data.numElements(); i++) {
      TinyVector<int,4> index=data.create_index(i);
      if(data(index)) {
        if(addval) ofs << data(index) << " ";
        ofs << index(sliceDim) << " " << index(phaseDim) << " " << index(readDim) << STD_endl;
      }
    }
    return 1;
  }
};

//////////////////////////////////////////////////////////////

void register_asc_format() {
  static AsciiFormat af;
  static PosFormat pf;
  static IndexFormat idf;
  static MatlabAsciiFormat mf;
  af.register_format();
  pf.register_format();
  idf.register_format();
  mf.register_format();
}

