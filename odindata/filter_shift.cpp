#include "filter_shift.h"


void FilterShift::init(){

  for(int idir=0; idir<n_directions; idir++) {
    shift[idir].set_description(STD_string(directionLabel[idir])+" shift").set_unit("pixel");
    append_arg(shift[idir],"shift"+itos(idir));
  }
}

bool FilterShift::process(Data<float,4>& data, Protocol& prot) const {

  TinyVector<float,4> subpixel_shift(0.0,shift[sliceDirection],shift[phaseDirection],shift[readDirection]);

  data.congrid(data.shape(), &subpixel_shift);

  for(int idir=0; idir<n_directions; idir++) {
    prot.geometry.set_offset(direction(idir),prot.geometry.get_offset(direction(idir))-shift[idir]);
  }

  return true;
}


////////////////////////////////////////////////////

void FilterTimeShift::init(){
  shiftframes.set_description("time shift").set_unit("frames");
  append_arg(shiftframes,"shiftframes");
}

bool FilterTimeShift::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  TinyVector<float,4> subpixel_shift(shiftframes,0.0,0.0,0.0);

  ODINLOG(odinlog,normalDebug) << "subpixel_shift=" << subpixel_shift << STD_endl;
  ODINLOG(odinlog,normalDebug) << "data.shape()=" << data.shape() << STD_endl;

  data.congrid(data.shape(), &subpixel_shift);

  return true;
}
