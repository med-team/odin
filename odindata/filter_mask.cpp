#include "filter_mask.h"
#include "utils.h" // for TinyVector comparison
#include "fitting.h" // for exponential fit



struct ExponentialFunctionWithLinear : public ModelFunction {

  fitpar A;
  fitpar lambda;
  fitpar M;
  fitpar C;

  // implementing virtual functions of ModelFunction
  float evaluate_f(float x) const {
    return A.val * exp (lambda.val * x) + M.val*x + C.val;
  }

  fvector evaluate_df(float x) const {
    fvector result(numof_fitpars());
    result[0]=exp (lambda.val * x);
    result[1]=x * A.val * exp (lambda.val * x);
    result[2]=x;
    result[3]=1.0;
    return result;
  }

  unsigned int numof_fitpars() const {return 4;}

  fitpar& get_fitpar(unsigned int i) {
    if(i==0) return A;
    if(i==1) return lambda;
    if(i==2) return M;
    if(i==3) return C;
    return dummy_fitpar;
  }

};



///////////////////////////////////////////////////////////////////////////


void FilterGenMask::init(){
  min.set_description("lower threshold");
  append_arg(min,"min");
  max.set_description("upper threshold");
  append_arg(max,"max");
}

bool FilterGenMask::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");
  if(max<=min) {
    ODINLOG(odinlog,warningLog) << "max(" << max << ") <= min(" << min << ")" << STD_endl;
  }

  TinyVector<int,4> indexvec;
  for(unsigned int i=0; i<data.numElements(); i++) {
    indexvec=data.create_index(i);
    float val=data(indexvec);
    if(val>=min && val<=max) data(indexvec)=1.0;
    else data(indexvec)=0.0;
  }
  return true;
}

///////////////////////////////////////////////////////////////////////////


bool FilteNonZeroMask::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  TinyVector<int,4> indexvec;
  for(unsigned int i=0; i<data.numElements(); i++) {
    indexvec=data.create_index(i);
    if(data(indexvec)!=0.0) data(indexvec)=1.0;
    else data(indexvec)=0.0;
  }
  return true;
}


///////////////////////////////////////////////////////////////////////////

void FilterAutoMask::init() {

  skip=0;
  skip.set_description("skip leftmost slots");
  append_arg(skip,"skip");

  dump_histogram_fname.set_description("dump histogram");
  append_arg(dump_histogram_fname,"dump_histogram_fname");

  dump_histogram_fit_fname.set_description("dump histogram fit");
  append_arg(dump_histogram_fit_fname,"dump_histogram_fit_fname");



}

bool FilterAutoMask::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  Range all=Range::all();

  int nslots=100;

  float maxval=max(data);
  float step=secureDivision(maxval,nslots);

  ODINLOG(odinlog,normalDebug) << "maxval/nslots/step=" << maxval << "/" << nslots << "/" << step << STD_endl; 

  Data<float,1> hist(nslots); hist=0.0;

  // create histogram
  for(unsigned int i=0; i<data.size(); i++) {
    float val=data(data.create_index(i));
    if(val!=0.0) { // omit synthetic values with zero intensity
      int slot=int(secureDivision(val,step));
      ODINLOG(odinlog,normalDebug) << "val/slot=" << val << "/" << slot << STD_endl; 
      if(slot>=0 && slot<nslots) hist(slot)++;
    }
  }

  if(dump_histogram_fname!="") {
    ODINLOG(odinlog,infoLog) << "dumping histogram to file " << dump_histogram_fname << STD_endl; 
    hist.write_asc_file(dump_histogram_fname);
  }

  int slotstart=STD_max(maxIndex(hist)(0), int(skip)); // start at least at first maximum
  ODINLOG(odinlog,normalDebug) << "slotstart=" << slotstart << STD_endl; 



  // 1st method: find first minimum
  float thresh=0.0;
  int islot;
  for(islot=slotstart; islot<(nslots-1); islot++) {
    if(hist(islot+1)>hist(islot)) {
      thresh=(islot+1)*step; // use one step after min for threshold
      break;
    }
  }
  ODINLOG(odinlog,significantDebug) << "slot/thresh(min)=" << "/" << islot << "/" << thresh << STD_endl; 




  // 2nd method: use exponential+linear fit of histogram
  int nslots4fit=nslots-slotstart;
  Data<float,1> hist4fit(nslots4fit);
  hist4fit(all)=hist(Range(slotstart,nslots-1));

  ExponentialFunctionWithLinear expfunc;
  FunctionFitDerivative fitter;
  if(!fitter.init(expfunc,nslots4fit)) return false;

  Data<float,1> xvals(nslots4fit);
  Data<float,1> ysigma(nslots4fit);
  for(int islot=0; islot<nslots4fit; islot++) {
    xvals(islot)=islot;
    ysigma(islot)=1.0;
  }

  // starting values
  expfunc.A.val=max(hist);
  expfunc.lambda.val=-1;
  expfunc.M.val=-1;
  expfunc.C.val=0.0;

  if(fitter.fit(hist4fit, ysigma, xvals, 1000, 1e-3)) {

    ODINLOG(odinlog,normalDebug) << "A=" << expfunc.A << STD_endl; 
    ODINLOG(odinlog,normalDebug) << "lambda=" << expfunc.lambda << STD_endl; 
    ODINLOG(odinlog,normalDebug) << "M=" << expfunc.M << STD_endl; 
    ODINLOG(odinlog,normalDebug) << "C=" << expfunc.C << STD_endl; 


    if(dump_histogram_fit_fname!="") {
      ODINLOG(odinlog,infoLog) << "dumping histogram fit to file " << dump_histogram_fit_fname << STD_endl; 
      hist4fit=expfunc.get_function(xvals); // overwrite real histogram by fit
      Data<float,1> histfit(nslots); histfit=0.0;
      for(int islot=0; islot<nslots4fit; islot++) histfit(slotstart+islot)=hist4fit(islot);
      histfit.write_asc_file(dump_histogram_fit_fname);
    }


    for(islot=0; islot<nslots4fit; islot++) {
      float expval=expfunc.A.val * exp (expfunc.lambda.val * islot);
      float linval=expfunc.M.val*islot + expfunc.C.val;
      ODINLOG(odinlog,normalDebug) << "expval/linval["<< islot << "]=" << expval << "/" << linval << STD_endl;
      if(linval>=expval) break; // seth threshold where exponential falls below linear term
    }
    int threshslot_fit=slotstart+islot;
    float thresh_fit=step*threshslot_fit;
    ODINLOG(odinlog,significantDebug) << "slot/thresh(fit)=" << "/" << threshslot_fit << "/" << thresh_fit << STD_endl; 

    thresh=STD_min(thresh, thresh_fit); // combination of both methods
    ODINLOG(odinlog,significantDebug) << "thresh=" << "/" << thresh << STD_endl; 

  }


  TinyVector<int,4> indexvec;
  for(unsigned int i=0; i<data.numElements(); i++) {
    indexvec=data.create_index(i);
    if(data(indexvec)>thresh) data(indexvec)=1.0;
    else data(indexvec)=0.0;
  }

  return true;
}


///////////////////////////////////////////////////////////////////////////


typedef STD_list<TinyVector<int,4> > QuantilIndexList;
typedef STD_map<float, QuantilIndexList > QuantilIndexMap;


void FilterQuantilMask::init(){
  fraction.set_minmaxval(0.0,1.0).set_description("quantil");
  append_arg(fraction,"fraction");
}

bool FilterQuantilMask::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  int ntotal=data.size();

  float frac=fraction;
  check_range<float>(frac,0.0,1.0);
  int nmask=int((1.0-frac)*ntotal+0.5);

  ODINLOG(odinlog,normalDebug) << "ntotal/nmask=" << ntotal << "/" << nmask << STD_endl; 

  Data<float,4> mask(data.shape()); mask=0.0;

  QuantilIndexMap indexmap;

  for(int i=0; i<ntotal; i++) {
    TinyVector<int,4> index=data.create_index(i);
    indexmap[data(index)].push_back(index);
  }
  int nmap=indexmap.size();

  ODINLOG(odinlog,normalDebug) << "ntotal/nmap=" << ntotal << "/" << nmap << STD_endl; 

  QuantilIndexMap::const_iterator mapiter=indexmap.end();
  int j=0;
  while(j<nmask) {
    if(mapiter==indexmap.begin()) break;
    --mapiter;
    const QuantilIndexList& indexlist=mapiter->second;
    ODINLOG(odinlog,normalDebug) << "indexmap(" << mapiter->first << ")=" << indexlist.size() << STD_endl; 
    for(QuantilIndexList::const_iterator listiter=indexlist.begin(); listiter!=indexlist.end(); ++listiter) {
      mask(*listiter)=1.0;
      j++;
    }
  }

  data.reference(mask);

  return true;
}

///////////////////////////////////////////////////////////////////////////


void FilterSphereMask::init(){
  pos.set_description("Position string in the format (slicepos,phasepos,readpos)");
  append_arg(pos,"pos");
  radius.set_unit(ODIN_SPAT_UNIT).set_description("radius");
  append_arg(radius,"radius");
}

bool FilterSphereMask::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");


  svector toks=tokens(extract(pos, "(", ")", true),',');
  ODINLOG(odinlog,normalDebug) << "toks=" << toks.printbody() << STD_endl;

  if(toks.size()!=3) {
    ODINLOG(odinlog,errorLog) << "Wrong size (" << toks.size() << "!=3) of position string >" << pos << "<" << STD_endl;
    return false;
  }

  TinyVector<int,3> centerpos(atoi(toks[0].c_str()), atoi(toks[1].c_str()), atoi(toks[2].c_str()));

  TinyVector<int,4> shape=data.shape();
  TinyVector<int,4> maskshape(shape);
  maskshape(timeDim)=1;

  Data<float,4> mask(maskshape); mask=0.0;


  TinyVector<float,3> voxel_spacing;
  voxel_spacing(0)=FileFormat::voxel_extent(prot.geometry, sliceDirection, data.extent(sliceDim));
  voxel_spacing(1)=FileFormat::voxel_extent(prot.geometry, phaseDirection, data.extent(phaseDim));
  voxel_spacing(2)=FileFormat::voxel_extent(prot.geometry, readDirection,  data.extent(readDim));
  ODINLOG(odinlog,normalDebug) << "voxel_spacing=" << voxel_spacing << STD_endl;


  for(unsigned int i=0; i<mask.size(); i++) {

    TinyVector<int,4> index=mask.create_index(i);

    TinyVector<int,3> spatindex(index(1),index(2),index(3));

    TinyVector<float,3> dist=voxel_spacing*(spatindex-centerpos);

    float r=sqrt(double(sum(dist*dist)));

    if(r<=radius) mask(index)=1.0;

  }

  data.reference(mask);

  return true;
}



///////////////////////////////////////////////////////////////////////////

void FilterUseMask::init() {

  fname.set_description("filename");
  append_arg(fname,"fname");
}


bool FilterUseMask::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  Range all=Range::all();

  // Load external file
  Data<float,4> maskdata;
  if(maskdata.autoread(fname)<0) return false;
  TinyVector<int,4> maskshape=maskdata.shape();
  TinyVector<int,4> datashape=data.shape();

  maskshape(timeDim)=datashape(timeDim)=1;
  if(maskshape!=datashape) {
    ODINLOG(odinlog,errorLog) << "shape mismatch: " << maskshape << "!=" << datashape << STD_endl;
    return false;
  }

  fvector vals;
  for(unsigned int i=0; i<data.size(); i++) {
    TinyVector<int,4> index=data.create_index(i);
    float val=data(index);
    index(timeDim)=0;
    if(maskdata(index)) vals.push_back(val);
  }

  data.resize(1,vals.size(),1,1);
  data(0,all,0,0)=Data<float,1>(vals);

  return true;
}
