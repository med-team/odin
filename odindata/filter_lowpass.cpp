#include "filter_lowpass.h"
#include "complexdata.h"

#include <odinpara/ldrfilter.h>

void FilterLowPass::init(){
  freq=0.0;
  freq.set_unit("Hz").set_description("Cut-off frequency");
  append_arg(freq,"freq");
}

bool FilterLowPass::process(Data<float,4>& data, Protocol& prot) const {
  Log<Filter> odinlog(c_label(),"process");

  Range all=Range::all();

  TinyVector<int,4> shape=data.shape();
  int nrep=shape(timeDim);

  ComplexData<4> cdata(float2real(data));

  cdata.partial_fft( TinyVector<bool,4>(true,false,false,false), true);

  int centindex=nrep/2;

  float maxfreq_Hz=1000.0*secureDivision(1.0, prot.seqpars.get_RepetitionTime());

  int cutoffindex=int(secureDivision(freq, maxfreq_Hz)*centindex+0.5);

  int plateauindex=int(0.8*cutoffindex+0.5);

  ODINLOG(odinlog,normalDebug) << "plateauindex/cutoffindex=" << plateauindex << "/" << cutoffindex << STD_endl;

  LDRfilter transition;
  transition.set_function("Gauss");

  for(int i=0; i<(centindex+1); i++) {
    if(i>=plateauindex && i<=cutoffindex) {
      float relpos=secureDivision(i-plateauindex, cutoffindex-plateauindex);
      STD_complex factor(transition.calculate(relpos));
      if((centindex-i)>=0)   cdata(centindex-i, all, all, all)*=factor;
      if((centindex+i)<nrep) cdata(centindex+i, all, all, all)*=factor;

    }
    if(i>cutoffindex) {
      if((centindex-i)>=0)   cdata(centindex-i, all, all, all)=STD_complex(0.0);
      if((centindex+i)<nrep) cdata(centindex+i, all, all, all)=STD_complex(0.0);
    }
  }

  cdata.partial_fft( TinyVector<bool,4>(true,false,false,false), false);

  data=creal(cdata);

  return true;
}
