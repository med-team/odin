#include "fileio.h"
#include "complexdata.h"

//////////////////////////////////////////////////////////////

template<typename T>
struct RawFormat : public FileFormat {
  STD_string description() const {
    STD_string descr=TypeTraits::type2label((T)0);
    if(descr.find("bit")!=STD_string::npos) {
      descr=replaceStr(descr, "s", "signed ");
      descr=replaceStr(descr, "u", "unsigned ");
      descr=replaceStr(descr, "bit", " bit");
    }
    descr+=" raw data";
    return descr;
  }
  svector suffix() const  {
    svector result; result.resize(1);
    result[0]=TypeTraits::type2label((T)0);
    return result;
  }
  svector dialects() const {return svector();}

  int read(Data<float,4>& data, const STD_string& filename, const FileReadOpts& opts, Protocol& prot) {
    Log<FileIO> odinlog("RawFormat","read");
    TinyVector<int,4> newshape;
    newshape=1;

    unsigned long typesize=sizeof(T);
    if( int(opts.cplx)>0 ) typesize*=2;

    int fsize=filesize(filename.c_str())-opts.skip;

    newshape(0)=prot.seqpars.get_NumOfRepetitions();
    newshape(3)=prot.seqpars.get_MatrixSize(readDirection);
    newshape(2)=prot.seqpars.get_MatrixSize(phaseDirection);
    newshape(1)=(unsigned int)secureDivision(fsize,typesize*product(newshape));

    ODINLOG(odinlog,normalDebug) << "fsize/typesize/newshape(1)=" << fsize << "/" << typesize  << "/" << newshape(1) << STD_endl;

    if(!product(newshape)) {
      ODINLOG(odinlog,errorLog) << "wrong size: " << newshape << STD_endl;
      return -1;
    }
    data.resize(newshape);


    if(int(opts.cplx)>0 ) {
      ComplexData<4> data_cplx(newshape);
      if(data_cplx.Data<STD_complex,4>::read<T>(filename,opts.skip)<0) return -1; // extra scop for MinGW
      if(opts.cplx=="abs")  data=cabs(data_cplx);
      if(opts.cplx=="pha")  data=phase(data_cplx);
      if(opts.cplx=="real") data=creal(data_cplx);
      if(opts.cplx=="imag") data=cimag(data_cplx);
    } else {
      prot.system.set_data_type(TypeTraits::type2label((T)0));
      if(data.Data<float,4>::read<T>(filename,opts.skip)<0) return -1; // extra scop for MinGW
    }

    return data.extent(0)*data.extent(1);
  }

  int write(const Data<float,4>& data, const STD_string& filename, const FileWriteOpts& opts, const Protocol& prot) {
    Log<FileIO> odinlog("RawFormat","write");

    if(opts.append) {
      Data<T,4> data_copy;
      data.Data<float,4>::convert_to(data_copy,!opts.noscale);
      return data_copy.write(filename,appendMode);
    } else return data.Data<float,4>::write<T>(filename,!opts.noscale);
  }

};


//////////////////////////////////////////////////////////////

void register_raw_format() {
  static RawFormat<s8bit>  s8f;
  static RawFormat<u8bit>  u8f;
  static RawFormat<s16bit> s16f;
  static RawFormat<u16bit> u16f;
  static RawFormat<s32bit> s32f;
  static RawFormat<u32bit> u32f;
  static RawFormat<float>  floatf;
  static RawFormat<double> doublef;

  s8f.register_format();
  u8f.register_format();
  s16f.register_format();
  u16f.register_format();
  s32f.register_format();
  u32f.register_format();
  floatf.register_format();
  doublef.register_format();
}

