#include "odinmethod.h"

#include "odindialog_process.h"


OdinMethod::OdinMethod(const OdinConf& conf, OdinMethodCallback* callback, QWidget* parent4dialog) : StateMachine<OdinMethod>(&clean),

  clean   (this,"Clean",   0,        &OdinMethod::make_clean),
  compiled(this,"Compiled",&clean,   &OdinMethod::clean2compiled),
  linked  (this,"Linked"  ,&compiled,&OdinMethod::compiled2linked),
  settings(conf), cback(callback), parent(parent4dialog) {

  Log<OdinComp> odinlog("OdinMethod","OdinMethod");

  register_direct_trans(&linked,&compiled,&OdinMethod::linked2compiled);
}



bool OdinMethod::make_clean() {
  Log<OdinComp> odinlog("OdinMethod","make_clean");
  if(!check_srcdir()) return false;
  bool result=unlink();
  cback->report(result, "make_clean", method.get_status_string());
  return result;
}

bool OdinMethod::clean2compiled() {
  Log<OdinComp> odinlog("OdinMethod","clean2compiled");
  if(!check_srcdir()) return false;
  return make();
}


bool OdinMethod::compiled2linked() {
  Log<OdinComp> odinlog("OdinMethod","compiled2linked");
  if(!check_srcdir()) return false;

  STD_string so_name(find_so());
  ODINLOG(odinlog,normalDebug) << "so_name=" << so_name << STD_endl;

  if(!method.load_method_so(so_name)) {
    cback->report(false, "compiled2linked", "Failed linking shared object "+so_name);
    return false;
  } else ODINLOG(odinlog,normalDebug) << "loaded sequence plugin"  << STD_endl;

  int nmeth=method.get_numof_methods();
  ODINLOG(odinlog,normalDebug) << "nmeth=" << nmeth  << STD_endl;

  if(nmeth) {

    if(!method->init()) {
      cback->report(false, "compiled2linked", method.get_status_string());
      return false;
    }
    ODINLOG(odinlog,normalDebug) << "init method done"  << STD_endl;

    method->load_sequencePars(seqParsFile());
    ODINLOG(odinlog,normalDebug) << "loaded sequencePars"  << STD_endl;

    if(!method->build()) {
      cback->report(false, "compiled2linked", method.get_status_string());
      return false;
    }
    ODINLOG(odinlog,normalDebug) << "build method done"  << STD_endl;

    cback->create_widgets();
  }

  cback->report(true, "compiled2linked", method.get_status_string());

  return true;
}

bool OdinMethod::linked2compiled() {
  Log<OdinComp> odinlog("OdinMethod","linked2compiled");
  if(!check_srcdir()) return false;
  return unlink();
}

bool OdinMethod::relink() {
  Log<OdinComp> odinlog("OdinMethod","relink");
  if(compiled.obtain_state()) return linked.obtain_state();
  return false;
}


STD_string OdinMethod::seqParsFile() {
  return settings.sourcecode.get_dirname()+SEPARATOR_STR+settings.sourcecode.get_basename_nosuffix()+"_sequencePars";
}


bool OdinMethod::unlink() {
  Log<OdinComp> odinlog("OdinMethod","unlink");

  if(method.get_numof_methods()) method->write_sequencePars(seqParsFile());
  ODINLOG(odinlog,normalDebug) << "sequencePars stored" << STD_endl;

  ODINLOG(odinlog,normalDebug) << "deleting widgets" << STD_endl;
  cback->delete_widgets();

  ODINLOG(odinlog,normalDebug) << "deleting old methods" << STD_endl;
  method.delete_methods();

  return true;
}


bool OdinMethod::check_srcdir() {
  Log<OdinComp> odinlog("OdinMethod","check_srcdir");

  if(settings.sourcecode=="") {
    cback->report(false, "check_srcdir", "No method specified");
    return false;
  }

  STD_string dirname(settings.sourcecode.get_dirname());
  ODINLOG(odinlog,normalDebug) << "sourcecode / dirname=" << settings.sourcecode << " / " <<  dirname << STD_endl;

  if(!browse_dir(dirname).size()) {
    ODINLOG(odinlog,normalDebug) << "length(dirname)=0" << STD_endl;
    cback->report(false, "check_srcdir", "Directory " + dirname + " is invalid");
    return false;
  }

  if(chpwd(dirname.c_str())) {
    ODINLOG(odinlog,normalDebug) << "chpwd failed" << STD_endl;
    cback->report(false, "check_srcdir", "Cannot change directory to " + dirname);
    return false;
  }

  cback->report(true, "check_srcdir", "Ready");
  return true;
}


bool OdinMethod::make() {
  Log<OdinComp> odinlog("OdinMethod","make");

  ProcessDialog makedlg(parent);

  int waitstate=makedlg.execute_cmds(settings.get_method_compile_chain());

  bool status=!waitstate;

  if(waitstate==-2) {
    cback->report(true,"make","Compilation interrupted");
  }
  if(waitstate==-1) {
    cback->report(false,"make","Cannot start compiler");
  }
  if(waitstate>0) {
    cback->report(false,"make","Compilation failed");
  }

  ODINLOG(odinlog,normalDebug) << "status / waitstate = " << status << " / " << waitstate << STD_endl;

  if(status) cback->report(true, "make_clean", "Compiled");

  return status;
}


STD_string OdinMethod::find_so() {
  Log<OdinComp> odinlog("OdinMethod","find_so");
  STD_string result;

  STD_string methlabel(settings.sourcecode.get_basename_nosuffix());

  svector filelist(browse_dir(settings.sourcecode.get_dirname(),false,true));

  for(unsigned int i=0; i<filelist.size(); i++) {
    STD_string curr_file(filelist[i]);
    STD_string::size_type findpos=curr_file.find(methlabel);

    if(curr_file[(unsigned int)0]=='_' && findpos!=STD_string::npos && findpos>0) {
      result=curr_file;
      break;
    }
  }

  ODINLOG(odinlog,normalDebug) << "result = " << result << STD_endl;

  return settings.sourcecode.get_dirname()+SEPARATOR_STR+result;
}
