#include "odinconf.h"
#include "odincomp.h"

#include <tjutils/tjprocess.h>

#include <odinqt/odinqt.h> // for message_question

#include <odinseq/seqmakefile.h>

#ifdef USING_WIN32
#include <process.h>
#include <windows.h>
#include <errno.h>
#endif


OdinConf::OdinConf(const STD_string& label, bool ignore_environment) : LDRblock(label),
  sourcecode("","SourceCode"),
  editor(DEFAULT_EDITOR,"SourceCodeEditor"),
  browser(DEFAULT_BROWSER,"WebBrowser"),
  methroot(get_homedir()+SEPARATOR_STR+DEFAULT_METH_ROOT+SEPARATOR_STR,"MethodsRootDir"),
  smpfile(get_samplesdir(),"SampleFile"),
  protfile(get_homedir(),"ProtocolFile"),
  compiler(CXX,"Compiler"),
  linker(CXX,"Linker"),
  compiler_flags(CXXFLAGS,"CompilerOptions"),
  extra_includes("","ExtraIncludes"),
  extra_libs("","ExtraLibraries"),
  selectedMethods(sarray(0),"SelectedMethods"),
  attachDebugger(false,"AttachDebugger") {

  Log<OdinComp> odinlog("OdinConf","OdinConf");

  sourcecode.set_parmode(hidden);

  ignore_env=ignore_environment;

  sourcecode.set_defaultdir(get_homedir());
  sourcecode.set_suffix("cpp");

  methroot.set_dir(true);

#ifdef USING_WIN32
//  compiler=WIN_CXX;
//  linker=WIN_LD;
#else
  editor.set_defaultdir("/usr/bin");
  browser.set_defaultdir("/usr/bin");
#endif

  protfile.set_parmode(hidden);

  LDRblock::append(sourcecode);

  LDRblock::append(methroot);
  LDRblock::append(smpfile);
  LDRblock::append(protfile);

  if(!ignore_env) {
    LDRblock::append(compiler_flags);
    LDRblock::append(extra_includes);
    LDRblock::append(extra_libs);
  }

#ifdef HAVE_GDB_XTERM
  LDRblock::append(attachDebugger);
#endif
  LDRblock::append(editor);
#ifndef USING_WIN32
  LDRblock::append(browser); // In windows, HTML will be handled by open_file_in_win32
#endif

  LDRblock::append(selectedMethods);
}

bool OdinConf::init(QWidget* parent) {
  Log<OdinComp> odinlog("OdinConf","init");

#ifdef USING_WIN32
#ifdef USING_GCC // Assume MinGW

  STD_string instprefix_bin=get_installdir()+SEPARATOR_STR+"bin";

  // hack for MinGW-gcc440_1.zip, otherwise libgmp-3.dll is not found
  STD_string oldpath=getenv_nonnull("PATH");
  if(oldpath.find(instprefix_bin) != 0) { // add only if cygnus_bin is not already 1st entry
    STD_string path="PATH="+instprefix_bin+";"+oldpath;
    ODINLOG(odinlog,normalDebug) << "oldpath/path=" << oldpath << "/" << path << STD_endl;
    _putenv(path.c_str());
  }

  compiler=instprefix_bin+SEPARATOR_STR+"g++";
  linker=STD_string(compiler); // the same for GCC

  // Try to find and use notepad++
  STD_string notepadpp_fname=instprefix_bin+SEPARATOR_STR+"notepad++.exe";
  if(filesize(notepadpp_fname.c_str())>0) editor=notepadpp_fname;

#else // Assume Visual C++ 6

  STD_string keyname("SOFTWARE\\Microsoft\\VisualStudio\\6.0\\Setup\\Microsoft Visual C++");
  STD_string valname("ProductDir");
  STD_string vcreg(get_registryvalue(keyname,valname)); // retrieve VC++ installation path from registry
  ODINLOG(odinlog,normalDebug) << "vcreg=" << vcreg << STD_endl;
  STD_string msg;
  if(vcreg=="") {
    msg=justificate("Visual C++ 6.0 is required for compiling ODIN sequences. I cannot find the path of the Visual C++ installation under the registry key-value pair "+keyname+"-"+valname+". Please install Visual C++ 6.0 and re-install ODIN.");
    message_question(msg.c_str(), "Cannot find Visual C++", parent, false, true);
    return false;
  } else {
    STD_string compiler_fname=vcreg+SEPARATOR_STR+"bin"+SEPARATOR_STR+"cl.exe";
    if(filesize(compiler_fname.c_str())<=0) {
      msg=justificate("Visual C++ 6.0 is installed but I cannot find the compiler cl.exe under the path "+compiler_fname+". Please make sure that it is installed together with Visual C++ 6.0 and re-install ODIN.");
      message_question(msg.c_str(), "Cannot find Visual C++ Compiler", parent, false, true);
      return false;
    } else {
      compiler=compiler_fname;  // and set it as the initial value, if found
      ODINLOG(odinlog,normalDebug) << "compiler=" << compiler << STD_endl;
    }
    STD_string linker_fname=vcreg+SEPARATOR_STR+"bin"+SEPARATOR_STR+"link.exe";
    if(filesize(linker_fname.c_str())<=0) {
      msg=justificate("Visual C++ 6.0 is installed but I cannot find the linker link.exe under the path "+linker_fname+". Please make sure that it is installed together with Visual C++ 6.0 and re-install ODIN.");
      message_question(msg.c_str(), "Cannot find Visual C++ Linker", parent, false, true);
      return false;
    } else {
      linker=linker_fname;  // and set it as the initial value, if found
      ODINLOG(odinlog,normalDebug) << "linker=" << linker << STD_endl;
    }
    extra_includes="-I\""+vcreg+SEPARATOR_STR+"Include\"";  // and set it as the initial value, if found
    extra_libs="/LIBPATH:\""+vcreg+SEPARATOR_STR+"lib\" ";
  }

  vcreg=get_registryvalue("SOFTWARE\\Microsoft\\VisualStudio\\6.0\\Setup","VsCommonDir");
  if(vcreg!="") {
    editor=vcreg+SEPARATOR_STR+"MSDev98"+SEPARATOR_STR+"Bin"+SEPARATOR_STR+"msdev.exe";
  }

#endif
#endif

  return true;
}


svector OdinConf::get_method_compile_chain() const {
  Log<OdinComp> odinlog("OdinConf","get_method_compile_chain");
  STD_string methlabel(sourcecode.get_basename_nosuffix());
  ODINLOG(odinlog,normalDebug) << "methlabel=" << methlabel << STD_endl;
  if(methlabel=="") return svector();
  SeqMakefile mf(methlabel,get_installdir(),compiler,compiler_flags,linker,extra_includes,extra_libs);
  return mf.get_method_compile_chain(false,true); // Only shared object
}


STD_string OdinConf::get_registryvalue(const STD_string& keyname, const STD_string& valuename) {
  Log<OdinComp> odinlog("OdinConf","get_registryvalue");
  STD_string result;
#ifdef USING_WIN32
  HKEY reghandle=0;
  LONG openresult=RegOpenKeyEx(HKEY_LOCAL_MACHINE,keyname.c_str(),0,KEY_READ,&reghandle);
  ODINLOG(odinlog,normalDebug) << "reghandle/openresult=" << reghandle << "/" << openresult << STD_endl;

  if(reghandle) {
    if(openresult==ERROR_SUCCESS) {
      unsigned char buff[ODIN_MAXCHAR];
      DWORD buffsize=ODIN_MAXCHAR-1; buff[0]='\0';
      LONG queryresult=RegQueryValueEx(reghandle,valuename.c_str(),NULL,NULL,buff,&buffsize);
      ODINLOG(odinlog,normalDebug) << "buffsize/queryresult=" << buffsize << "/" << queryresult << STD_endl;
      if(queryresult==ERROR_SUCCESS && buffsize<(ODIN_MAXCHAR-1)) {
        buff[buffsize]='\0';
        result=(const char *)buff;
      } else ODINLOG(odinlog,normalDebug) << "unable to get registry value " << valuename << STD_endl;
    } else ODINLOG(odinlog,normalDebug) << "unable to open registry key " << keyname << STD_endl;
    LONG closeresult=RegCloseKey(reghandle);
    ODINLOG(odinlog,normalDebug) << "closeresult=" << closeresult << STD_endl;
  } else ODINLOG(odinlog,normalDebug) << "unable to get handle for registry key " << keyname << ", openresult=" << openresult << STD_endl;
#endif
  ODINLOG(odinlog,normalDebug) << "result(" << keyname << "\\" << valuename  << ")=" << result << STD_endl;
  return result;
}


STD_string OdinConf::get_installdir() {
  Log<OdinComp> odinlog("OdinConf","get_installdir");
  LDRfileName result; // create normalized file name
  STD_string reg;
  if(!ignore_env) reg=get_registryvalue("SOFTWARE\\Odin","Install_Dir");
  if(reg!="") result=reg;
  else result=INSTALL_PREFIX;
  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return result;
}


STD_string OdinConf::get_binprefix() {
  Log<OdinComp> odinlog("OdinConf","get_binprefix");
  STD_string result; // default: empty string -> use PATH
#ifdef USING_WIN32
  result=get_installdir()+SEPARATOR_STR+"bin"+SEPARATOR_STR; // Append separator (won't work with LDRfileName) so that prefix can either be directory or empty
#endif
  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return result;
}


STD_string OdinConf::get_homedir() {
  Log<OdinComp> odinlog("OdinConf","get_homedir");
  LDRfileName result(getenv_nonnull("HOME"));
#ifdef USING_WIN32
  result=getenv_nonnull("USERPROFILE"); // the windows 'home' directory
#endif
  if(result=="") {
    ODINLOG(odinlog,warningLog) << "Cannot determine homedir" << STD_endl;
  }
  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return result;
}


STD_string  OdinConf::get_datadir() {
  Log<OdinComp> odinlog("OdinConf","get_datadir");
  LDRfileName result=get_installdir()+SEPARATOR_STR+"share"+SEPARATOR_STR+"odin";
  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return result;
}

STD_string OdinConf::get_seqexamplesdir() {
  Log<OdinComp> odinlog("OdinConf","get_seqexamplesdir");
  LDRfileName result=get_datadir()+SEPARATOR_STR+"sequences";
  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return result;
}

STD_string OdinConf::get_samplesdir() {
  Log<OdinComp> odinlog("OdinConf","get_samplesdir");
  LDRfileName result=get_datadir()+SEPARATOR_STR+"samples";
  return result;
}

STD_string OdinConf::get_coilsdir() {
  Log<OdinComp> odinlog("OdinConf","get_coilsdir");
  LDRfileName result=get_datadir()+SEPARATOR_STR+"coils";
  return result;
}

STD_string OdinConf::get_confdir() {
  Log<OdinComp> odinlog("OdinConf","get_confdir");
  STD_string confdir(get_homedir()+SEPARATOR_STR+".odin");
  ODINLOG(odinlog,normalDebug) << "confdir=" << confdir << STD_endl;
  createdir(confdir.c_str());
  return confdir;
}

STD_string OdinConf::get_tmpdir() {
  Log<OdinComp> odinlog("OdinConf","get_tmpdir");
  LDRfileName tmpdir(get_confdir()+SEPARATOR_STR+"tmp");
  createdir(tmpdir.c_str());
  return tmpdir;
}

STD_string OdinConf::get_manual_location() {
  Log<OdinComp> odinlog("OdinConf","get_manual_location");
  STD_string wwwloc("http://od1n.sourceforge.net/manual/html/");
  STD_string instloc(get_installdir()+SEPARATOR_STR+"share"+SEPARATOR_STR+"doc"+SEPARATOR_STR+"odin"+SEPARATOR_STR+"manual"+SEPARATOR_STR+"html"+SEPARATOR_STR);

  STD_string result=wwwloc;
  if(filesize((instloc+"index.html").c_str())>0) result="file://"+instloc;

  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return result;
}

#ifdef USING_WIN32
bool open_file_in_win32(const STD_string& location, const STD_string& prog) {
  Log<OdinComp> odinlog("OdinConf","open_file_in_win32");
  const char* args[4];
  STD_string exe;
  if(prog=="") {
    args[0]="rundll32.exe";
    args[1]="url.dll,FileProtocolHandler";
    args[2]=location.c_str();
    args[3]=0;
  } else {
    args[0]=prog.c_str();
    args[1]=location.c_str();
    args[2]=0;
    args[3]=0;
  }

  int spawnpid=_spawnvp( _P_NOWAIT, args[0], args);
  if (spawnpid == -1) {
    if(errno==E2BIG)   ODINLOG(odinlog,errorLog) << "exe to big" << STD_endl;
    if(errno==EINVAL)  ODINLOG(odinlog,errorLog) << "mode invalid" << STD_endl;
    if(errno==ENOENT)  ODINLOG(odinlog,errorLog) << "file not found" << STD_endl;
    if(errno==ENOEXEC) ODINLOG(odinlog,errorLog) << "exe not found" << STD_endl;
    if(errno==ENOMEM)  ODINLOG(odinlog,errorLog) << "no mem" << STD_endl;
    return false;
  }
  return true;
}
#endif

#ifdef MACOS
bool open_file_in_macos(const STD_string& location, STD_list<Process>& subprocs) {
  Log<OdinComp> odinlog("OdinConf","open_file_in_macos");
  return OdinConf::start_proc("open", location, subprocs);
}
#endif



bool OdinConf::display_html(const STD_string& location, STD_list<Process>& subprocs) {
  Log<OdinComp> odinlog("OdinConf","display_html");
  bool result=true;
  STD_string errmsg;
#ifdef USING_WIN32
  result=open_file_in_win32(location,"");
  if(!result) {
    errmsg="Cannot open location "+location+" using built-in mechanism of Windows.";
  }
#else
#ifdef MACOS
  result=open_file_in_macos(location, subprocs);
  if(!result) {
    errmsg="Cannot open location "+location+" using built-in mechanism of MacOS.";
  }
#else
  result=OdinConf::start_proc(browser, location, subprocs);
  if(!result) {
    errmsg="Cannot open URL "+location+" using "+browser+". Please change the browser under Preferences->Settings.";
  }
#endif
#endif
  if(errmsg!="") {
    message_question(justificate(errmsg).c_str(), "Browser not found", 0, false, true);
  }
  return result;
}


bool OdinConf::open_ascfile(const STD_string& filename, STD_list<Process>& subprocs) {
  Log<OdinComp> odinlog("OdinConf","open_ascfile");
  bool result=true;
#ifdef USING_WIN32
  result=open_file_in_win32(filename,editor);
#else
#ifdef MACOS
  result=open_file_in_macos(filename, subprocs);
#else
  result=OdinConf::start_proc(editor, filename, subprocs);
#endif
#endif
  if(!result) {
    STD_string msg=justificate("Cannot open ASCII file "+filename+" using "+editor+". Please change the editor under Preferences->Settings.");
    message_question(msg.c_str(), "Editor not found", 0, false, true);
  }
  return result;
}


#ifndef USING_WIN32
bool OdinConf::start_proc(const STD_string& program, const STD_string& filename, STD_list<Process>& subprocs) {
  Log<OdinComp> odinlog("OdinConf","start_proc");
  bool result=true;
  STD_string cmd(program+" \""+STD_string(filename)+"\"");
  Process proc;
  result=proc.start(cmd);
  sleep_ms(1000); // wait for error
  int retval=0;
  bool finished=proc.finished(retval);
  if(finished && retval) result=false;
  if(!finished) subprocs.push_back(proc);
  return result;
}
#endif

bool OdinConf::ignore_env;
