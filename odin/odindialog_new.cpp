#include "odindialog_new.h"

#include "odincomp.h"

#include <odinpara/ldrblock.h>

NewMethDialog::NewMethDialog(QWidget *parent, LDRblock& files)
 : GuiDialog(parent,"Please select a template and a name for the new method",true) {
  Log<OdinComp> odinlog("NewMethDialog","NewMethDialog");

  grid=new GuiGridLayout(GuiDialog::get_widget(), 2, 2);

  fileswidget=new LDRwidget(files,1,GuiDialog::get_widget());
  connect(fileswidget,SIGNAL(valueChanged()),this,SLOT(emit_valueChanged()));
  grid->add_widget( fileswidget, 0, 0, GuiGridLayout::Default, 1, 2 );

  pb_cancel = new GuiButton( GuiDialog::get_widget(), this, SLOT(cancel()), "Cancel" );
  grid->add_widget( pb_cancel->get_widget(), 1, 0, GuiGridLayout::Center );

  pb_done = new GuiButton( GuiDialog::get_widget(), this, SLOT(done()), "Done" );
  grid->add_widget( pb_done->get_widget(), 1, 1, GuiGridLayout::Center );

//  GuiDialog::show();
}

void NewMethDialog::updateWidget() {
  fileswidget->updateWidget();
}


void NewMethDialog::done() {
  GuiDialog::done();
}

void NewMethDialog::cancel() {
  GuiDialog::cancel();
}


void NewMethDialog::emit_valueChanged() {
  emit valueChanged();
}



NewMethDialog::~NewMethDialog() {
  delete pb_done;
  delete pb_cancel;
  delete fileswidget;
  delete grid;
}

