#include <qlabel.h>
#include <qpixmap.h>

#include "odindialog_kspace.h"

#include "odincomp.h"



KspaceDialog::KspaceDialog(QWidget *parent, float kmax, unsigned int nadc)
 : GuiDialog(parent,"k-Space",false),
   k0(kmax), progcount(0), canceled(false) {
  Log<OdinComp> odinlog("KspaceDialog","KspaceDialog");

  ODINLOG(odinlog,normalDebug) << "kmax/nadc=" << kmax << "/" << nadc << STD_endl;

  grid=new GuiGridLayout(GuiDialog::get_widget(), 3, 1);

  int pixsize=KSPACE_INPLANE_SIZE+2*KSPACE_BORDER_SIZE+2*KSPACE_THROUGHPLANE_SIZE;
  kspace_pixmap_buff=new QPixmap(pixsize, pixsize);
  kspace_pixmap_buff->fill(_ARRAY_BACKGROUND_COLOR_);

  painter=new GuiPainter(kspace_pixmap_buff);

  painter->setPen(_ARRAY_GRID_COLOR_);

  int rectoffset=KSPACE_BORDER_SIZE+KSPACE_THROUGHPLANE_SIZE;
  painter->drawRect(rectoffset, rectoffset, KSPACE_INPLANE_SIZE, KSPACE_INPLANE_SIZE);

  painter->setPen(_ARRAY_GRID_COLOR_, 1, true);

  painter->moveTo(rectoffset,rectoffset+KSPACE_INPLANE_SIZE/2);
  painter->lineTo(rectoffset+KSPACE_INPLANE_SIZE,rectoffset+KSPACE_INPLANE_SIZE/2);
  painter->moveTo(rectoffset+KSPACE_INPLANE_SIZE/2,rectoffset);
  painter->lineTo(rectoffset+KSPACE_INPLANE_SIZE/2,rectoffset+KSPACE_INPLANE_SIZE);

  painter->end();


  kspace_label=new QLabel(GuiDialog::get_widget());
  kspace_label->setPixmap(*kspace_pixmap_buff);
  grid->add_widget( kspace_label, 0, 0, GuiGridLayout::Center );

  progbar=new GuiProgressBar (GuiDialog::get_widget(), nadc);
  progbar->set_min_width(pixsize);
  grid->add_widget( progbar->get_widget(), 1, 0, GuiGridLayout::Center );

  pb_cancel = new GuiButton( GuiDialog::get_widget(), this, SLOT(cancel()), "Cancel/Close" );
  grid->add_widget( pb_cancel->get_widget(), 2, 0, GuiGridLayout::Center );

  GuiDialog::show();
}


KspaceDialog::~KspaceDialog() {
  delete pb_cancel;
  delete kspace_label;
  delete kspace_pixmap_buff;
  delete progbar;
  delete grid;
  delete painter;
}


void KspaceDialog::draw_point(float kx, float ky, float kz) {
  Log<OdinComp> odinlog("KspaceDialog","draw_point");

  ODINLOG(odinlog,normalDebug) << "kx/ky/kz=" << kx << "/" << ky << "/" << kz << STD_endl;


  painter->begin(kspace_pixmap_buff);
  int xpos=kpos2index(kx);
  int ypos=kpos2index(ky);

  float relz=secureDivision(-kz,k0); // draw high kz in the foreground
  xpos+=int(relz*KSPACE_THROUGHPLANE_SIZE+0.5);
  ypos-=int(relz*KSPACE_THROUGHPLANE_SIZE+0.5); // Start in lower left corner

  painter->setPen(_ARRAY_FOREGROUND_COLOR1_, 1, false, -relz); // Make foreground brighter

  painter->drawRect(xpos-KSPACE_POINT_SIZE/2, ypos-KSPACE_POINT_SIZE/2, KSPACE_POINT_SIZE, KSPACE_POINT_SIZE);
  painter->end();
  painter->repaint(kspace_label);
  progress();
}


void KspaceDialog::repaint() {
  painter->repaint(kspace_label);
}


void KspaceDialog::progress() {
  progcount++; progbar->set_progress(progcount);
  GuiApplication::process_events();
  if(canceled) throw OdinCancel();
}


void KspaceDialog::cancel() {
  canceled=true;
  hide();
}


int KspaceDialog::kpos2index(float kpos) const {
  float relpos=secureDivision(kpos,k0);
  return KSPACE_BORDER_SIZE+KSPACE_THROUGHPLANE_SIZE+int(0.5*(relpos+1.0)*KSPACE_INPLANE_SIZE);
}
