#include "odindialog_tree.h"

#include "odincomp.h"

TreeDialog::TreeDialog(QWidget *parent, const char* caption, const svector& column_labels)
 : GuiDialog(parent,caption,false) {

  grid=new GuiGridLayout(GuiDialog::get_widget(), 1, 1);

  root=new GuiListView(GuiDialog::get_widget(), column_labels, 400, 400, 0, true);

  grid->add_widget( root->get_widget(), 0, 0 );
}

TreeDialog::~TreeDialog() {
  delete root;
  delete grid;
}


void TreeDialog::display_node(const SeqClass* thisnode, const SeqClass* parentnode, int treelevel, const svector& columntext) {
  Log<OdinComp> odinlog("TreeDialog","display_node");
  GuiListItem* item=0;

  if(lastitemmap.size()) { // sub-node
    GuiListItem* parentitem=nodemap[parentnode];
    GuiListItem* lastitem=lastitemmap[parentitem];
    ODINLOG(odinlog,normalDebug) << "columntext=" << columntext.printbody() << STD_endl;
    ODINLOG(odinlog,normalDebug) << "parentnode/parentitem/lastitem=" << (void*)parentnode << "/" << parentitem << "/" << lastitem << STD_endl;
    item=new GuiListItem(parentitem, lastitem, columntext);
    ODINLOG(odinlog,normalDebug) << "item=" << item << STD_endl;
    lastitemmap[parentitem]=item;
  } else { // top-level node
    item=new GuiListItem(root,columntext);
    ODINLOG(odinlog,normalDebug) << "(root)item=" << item << STD_endl;
  }

  lastitemmap[item]=0; // insert dummy element at beginning
  nodemap[thisnode]=item;
}
