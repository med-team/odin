#include <qlabel.h>
#include <qcombobox.h> // for connecting signals

#include "odindialog_debug.h"


DebugDialog::DebugDialog(QWidget *parent)
 : GuiDialog(parent, "Debugging/Tracing Options",false) {
#ifdef ODIN_DEBUG
  int mapsize=LogBase::global->components.size();

  grid = new GuiGridLayout( GuiDialog::get_widget(), mapsize+1, 2 );


  STD_map<STD_string,log_component_fptr>::const_iterator it;

  svector debuglevels; debuglevels.resize(numof_log_priorities);
  for(int i=0; i<numof_log_priorities; i++) debuglevels[i]=logPriorityLabel[i];


  int row=0;
  for(it=LogBase::global->components.begin();it!=LogBase::global->components.end();++it) {

    QLabel* ql=new QLabel(GuiDialog::get_widget());
    ql->setText(it->first.c_str());
    grid->add_widget( ql, row, 0, GuiGridLayout::Center );

    GuiComboBox* cb = new GuiComboBox( GuiDialog::get_widget(), debuglevels );
    log_component_fptr fp=it->second;
    cb->set_current_item(fp(ignoreArgument));
    grid->add_widget( cb->get_widget(), row, 1, GuiGridLayout::Center );
    connect( cb->get_widget(), SIGNAL(activated(int)),this, SLOT(levelChanged(int)) );


    debugenums.push_back(cb);

    row++;
  }


  pb_done = new GuiButton( GuiDialog::get_widget(), this, SLOT(emitDone()), "Done" );
  grid->add_widget( pb_done->get_widget(), mapsize, 1, GuiGridLayout::Center );
//  connect( pb_done->get_widget(), SIGNAL(clicked()), this,SLOT(emitDone()) );

  GuiDialog::show();

#endif
}

DebugDialog::~DebugDialog() {
#ifdef ODIN_DEBUG
  for(STD_list<GuiComboBox*>::const_iterator dbgit=debugenums.begin();dbgit!=debugenums.end();++dbgit) delete (*dbgit);
  delete pb_done;
  delete grid;
#endif
}


void DebugDialog::levelChanged(int) {
#ifdef ODIN_DEBUG

  STD_map<STD_string,log_component_fptr>::const_iterator it=LogBase::global->components.begin();

  for(STD_list<GuiComboBox*>::const_iterator dbgit=debugenums.begin();dbgit!=debugenums.end();++dbgit) {
    LogBase::set_log_level(it->first.c_str(),logPriority((*dbgit)->get_current_item()));
    ++it;
  }
#endif
}

void DebugDialog::emitDone() {
#ifdef ODIN_DEBUG
  GuiDialog::done();
  emit finished();
#endif
}

