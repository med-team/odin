#include "odindebugger.h"
#include "odincomp.h"
#include "odinconf.h"

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif


///////////////////////////////////////////////////////////////////////////////


bool OdinDebugger::attach() {
  Log<OdinComp> odinlog("OdinDebugger","attach");
  int mypid=-1;
#ifdef HAVE_UNISTD_H
  mypid=getpid();
#endif
  if(mypid<=0) return false;

#ifdef HAVE_GDB_XTERM
  STD_string batfile("c"); // continue cmd for gdb
  STD_string batfname(OdinConf::get_tmpdir()+SEPARATOR_STR+"gdb.bat."+itos(mypid));
  ::write(batfile,batfname);

  STD_string cmd("xterm -geometry 80x10+0+0 -e 'gdb -x "+batfname+" odin "+itos(mypid)+"'");
  ODINLOG(odinlog,normalDebug) << "Running command " << cmd << STD_endl;

  return debugger_proc.start(cmd);
#else
  ODINLOG(odinlog,errorLog) << "gdb or xterm is missing" << STD_endl;
  return false;
#endif
}


bool OdinDebugger::detach(){
  return debugger_proc.kill();
}


