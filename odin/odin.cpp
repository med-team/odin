#include "odin.h"

#include "icons.h"

#include "odincomp.h"


Odin::Odin() : old_status(true), first_status(true) {
  Log<OdinComp> odinlog("Odin","Odin");

  set_status_xpm(greenIcon);  // init once


  view=new OdinView(GuiMainWindow::get_widget());
  connect(view,SIGNAL(newCaption(const char*)),this,SLOT(changeCaption(const char*)));
  connect(view,SIGNAL(newStatus(bool,const char*)),this,SLOT(changeStatus(bool,const char*)));


  fileMenu=new GuiPopupMenu(GuiMainWindow::get_widget());
  fileMenu->insert_item("&New", view, SLOT(new_method()), Qt::CTRL+Qt::Key_N);
  fileMenu->insert_item("&Open", view, SLOT(open_method()), Qt::CTRL+Qt::Key_O);
  fileMenu->insert_item("&Close", view, SLOT(close_method()), Qt::CTRL+Qt::Key_W);
  fileMenu->insert_item("&Save Settings", view, SLOT(save_prefs()), Qt::CTRL+Qt::Key_S);
  fileMenu->insert_item("S&tore Protocol", view, SLOT(save_protocol()), Qt::CTRL+Qt::Key_T);
  fileMenu->insert_item("&Load Protocol", view, SLOT(load_protocol()), Qt::CTRL+Qt::Key_L);
  fileMenu->insert_item("&Quit", view, SLOT(exit_odin()), Qt::CTRL+Qt::Key_Q);



  toolbar = new GuiToolBar(this, "Action");
  actionMenu=new GuiPopupMenu(GuiMainWindow::get_widget());

  new GuiToolButton(toolbar, editIcon, "Edit", view, SLOT(edit()));
  actionMenu->insert_item("Edit", view, SLOT(edit()), Qt::Key_F1);

  new GuiToolButton(toolbar, recompIcon, "Compile & Link", view, SLOT(recompile()));
  actionMenu->insert_item("Compile and &Link", view, SLOT(recompile()), Qt::Key_F2);

  new GuiToolButton(toolbar, geoIcon, "Geometry", view, SLOT(geo()));
  actionMenu->insert_item("Geometry", view, SLOT(geo()), Qt::Key_F3);

#ifdef STANDALONE_PLUGIN
  new GuiToolButton(toolbar, plotIcon, "Plot Sequence", view, SLOT(plot()));
  actionMenu->insert_item("Plot Sequence", view, SLOT(plot()), Qt::Key_F4);

  new GuiToolButton(toolbar, kspaceIcon, "k-Space", view, SLOT(kspace()));
  actionMenu->insert_item("Acquisition k-Space", view, SLOT(kspace()), Qt::Key_F5);

  new GuiToolButton(toolbar, simIcon, "Simulate", view, SLOT(sim()));
  actionMenu->insert_item("Simulate", view, SLOT(sim()), Qt::Key_F6);
#endif


  infoMenu=new GuiPopupMenu(GuiMainWindow::get_widget());
  infoMenu->insert_item("Sequence Properties", view, SLOT(seqprops()));
  infoMenu->insert_item("Sequence &Tree", view, SLOT(seqtree()));
  infoMenu->insert_item("Reconstruction Parameters", view, SLOT(recoinfo()));
  infoMenu->insert_item("P&ulsar Pulses", view, SLOT(pulsars()));


  brukerMenu=0;
#ifdef PARAVISION_PLUGIN
  brukerMenu=new GuiPopupMenu(GuiMainWindow::get_widget());
  brukerMenu->insert_item("&Pulse Program", view, SLOT(pulsprog()), Qt::CTRL+Qt::Key_P);
  brukerMenu->insert_item("&Gradient Program", view, SLOT(gradprog()), Qt::CTRL+Qt::Key_G);
  brukerMenu->insert_item("P&ARX parameters", view, SLOT(parx()), Qt::CTRL+Qt::Key_A);
  brukerMenu->insert_item("Pilot", view, SLOT(pv_pilot()));
  brukerMenu->insert_item("Scan", view, SLOT(pv_scan()));
  brukerMenu->insert_item("AutoRG and Scan", view, SLOT(pv_rgscan()));
  brukerMenu->insert_item("&Show HOWTO", view, SLOT(pv_howto()));
#endif


  siemensMenu=0;
#ifdef IDEA_PLUGIN
  siemensMenu=new GuiPopupMenu(GuiMainWindow::get_widget());
  siemensMenu->insert_item("&Event Table", view, SLOT(ideaevents()), Qt::CTRL+Qt::Key_E);
#ifdef USING_WIN32
  siemensMenu->insert_item("&Compile ODIN for IDEA", view, SLOT(compile_idea()));
  siemensMenu->insert_item("E&xport DLLs", view, SLOT(export_dlls()), Qt::CTRL+Qt::Key_X);
#endif
  siemensMenu->insert_item("&Show HOWTO", view, SLOT(ideahowto()));
#endif

  geMenu=0;
#ifdef EPIC_PLUGIN
  geMenu=new GuiPopupMenu(GuiMainWindow::get_widget());
  geMenu->insert_item("EPIC Code", view, SLOT(epiccode()));
#endif

  prefMenu=new GuiPopupMenu(GuiMainWindow::get_widget());
  prefMenu->insert_item("Settings", view, SLOT(edit_prefs()));
  prefMenu->insert_item("System", view, SLOT(edit_system()));
  prefMenu->insert_item("Load systemInfo", view, SLOT(load_system()));
  prefMenu->insert_item("Study", view, SLOT(edit_study()));
#ifdef ODIN_DEBUG
  prefMenu->insert_item("Debugging/Tracing", view, SLOT(debug_opts()));
#endif



  helpMenu=new GuiPopupMenu(GuiMainWindow::get_widget());
  helpMenu->insert_item("User Manual", view, SLOT(show_manual()));
  helpMenu->insert_item("Programmers Manual", view, SLOT(show_apidoc()));
  helpMenu->insert_item("Documentation for Seq... Classes", view, SLOT(show_seqdoc()));
  helpMenu->insert_item("About...", this, SLOT(slotHelpAbout()));


  GuiMainWindow::insert_menu("File", fileMenu);
  GuiMainWindow::insert_menu("Action", actionMenu);
  GuiMainWindow::insert_menu("Info", infoMenu);
  if(brukerMenu) GuiMainWindow::insert_menu("Bruker", brukerMenu);
  if(siemensMenu) GuiMainWindow::insert_menu("Siemens", siemensMenu);
  if(geMenu) GuiMainWindow::insert_menu("GE", geMenu);
  GuiMainWindow::insert_menu("Preferences", prefMenu);

  GuiMainWindow::insert_menu_separator();
  GuiMainWindow::insert_menu("Help", helpMenu);

  GuiMainWindow::show(view);
}

void Odin::initOdin(GuiApplication* a, bool has_debug_cmdline) {
  Log<OdinComp> odinlog("Odin","initOdin");
  view->initOdinView(toolbar,has_debug_cmdline);
  connect( a->get_object(),SIGNAL(aboutToQuit()), view,SLOT(exit_odin()) );
}

void Odin::changeCaption(const char* text) {
  GuiMainWindow::set_caption((STD_string("Odin " VERSION) + " - " + text).c_str());
}

void Odin::changeStatus(bool status, const char* text) {
  Log<OdinComp> odinlog("OdinView","changeStatus");

  old_status=status;
  first_status=false;

  if(status) {
    GuiMainWindow::set_status_xpm(greenIcon);
    if(text) GuiMainWindow::set_status_message(text);
  } else {
    STD_string msg="Error";
    if(text && STD_string(text)!="") msg+=STD_string(" - ")+text;
    GuiMainWindow::set_status_xpm(redIcon);
    GuiMainWindow::set_status_message(msg.c_str());
  }

}

void Odin::slotHelpAbout() {
  message_question(IDS_ODIN_ABOUT, "About...", GuiMainWindow::get_widget());
}
