#include <qslider.h>

#include "floatedit.h"

#include <tjutils/tjstring.h> // for ftos
#include <tjutils/tjcstd.h>


floatLineEdit::floatLineEdit(float minValue, float maxValue, float value,int digits, QWidget *parent, const char *name, int width, int height ) {

  gle = new GuiLineEdit(parent, this, SLOT(emitSignal()), width, height);

  // minValue & maxValue unused so far:
  minValue=maxValue=0.0;

  digits_cache=digits;
  set_value(value);
}

floatLineEdit::~floatLineEdit() {
  delete gle;
}

void floatLineEdit::setfloatLineEditValue( float newValue ) {
  set_value(newValue);
}


void floatLineEdit::emitSignal() {
  if(gle->is_modified()) {
    value_cache=atof(gle->get_text());
    set_value(value_cache);
    emit floatLineEditValueChanged(value_cache);
  }
}


void floatLineEdit::set_value(float value) {
  value_cache=value;
  gle->set_text( ftos(value_cache,digits_cache).c_str() );
}


//////////////////////////////////////////////////////////////////////////


floatSlider::floatSlider(float minValue, float maxValue, float step,
                  float value, QWidget *parent, const char *name ) {

  int nsteps=int((maxValue-minValue)/step+0.5);
  int ival=int((value-minValue)/step+0.5);

  gs=new GuiSlider( parent, 0, nsteps , 1, ival, nsteps/20);

  minValue_cache=minValue;
  step_cache=step;

  connect(gs->get_widget(),SIGNAL(valueChanged( int )),this,SLOT(emitSignal( int )));
}

void floatSlider::setfloatSliderValue( float newValue ) {
  oldPosition=int((newValue-minValue_cache)/step_cache+0.5);
  gs->set_value(oldPosition);
}

void floatSlider::emitSignal( int newintvalue ) {
  Log<OdinQt> odinlog("floatSlider","emitSignal");
  ODINLOG(odinlog,normalDebug) << "newintvalue/oldPosition=" << newintvalue << "/" << oldPosition << STD_endl;
  if (!(newintvalue==oldPosition)) {
    float newfloatval=minValue_cache+newintvalue*step_cache;
    emit floatSliderValueChanged(newfloatval);
  }
}

QWidget* floatSlider::get_widget() {return gs->get_widget();}

floatSlider::~floatSlider() {
  delete gs;
}

//////////////////////////////////////////////////////////////////////////

floatLineBox::floatLineBox(float value, int digits,QWidget *parent, const char *name )
 : QGroupBox(name,parent) {

  grid=new GuiGridLayout( this, 1, 1);

 // Create connected floatSlider and floatLineEdit

  le = new floatLineEdit( 0.0, 0.0, value,digits, this, "LineEdit", TEXTEDIT_WIDTH, TEXTEDIT_HEIGHT);

  grid->add_widget(le->get_widget(), 0, 0);

  connect(le,SIGNAL(floatLineEditValueChanged( float )), this,SLOT(emitSignal( float )));

}

void floatLineBox::setfloatLineBoxValue( float newvalue ) {
  le->setfloatLineEditValue(newvalue);
}

void floatLineBox::emitSignal( float newvalue ) {
  emit floatLineBoxValueChanged(newvalue);
}



floatLineBox::~floatLineBox(){
  delete le;
  delete grid;
}

//////////////////////////////////////////////////////////////////////////

floatScientSlider::floatScientSlider(float minValue, float maxValue, float Step,
                  float value, int digits, QWidget *parent, const char *name )
    : QGroupBox(name, parent ) {

  grid=new GuiGridLayout(this, 1, 4);

  slider = new floatSlider( minValue, maxValue, Step, value, this, "Slider" );

  le = new floatLineEdit( minValue, maxValue, value,digits, this, "LineEdit", SLIDER_CELL_WIDTH, SLIDER_CELL_HEIGHT );

  grid->add_widget(slider->get_widget(), 0, 0, GuiGridLayout::Default, 1, 3 );
  grid->add_widget(le->get_widget(), 0, 3);

  // connect slider and lineedit
  connect(slider, SIGNAL(floatSliderValueChanged( float )),le, SLOT(setfloatLineEditValue( float)));
  connect(le, SIGNAL(floatLineEditValueChanged( float )),slider, SLOT(setfloatSliderValue( float)));


  // Emit signal 'floatScientSliderValueChanged' to outer world
  connect(slider,SIGNAL(floatSliderValueChanged( float )),this,SLOT(emitSignal( float )));
  connect(le,SIGNAL(floatLineEditValueChanged( float )),this,SLOT(emitSignal( float )));

}

void floatScientSlider::setfloatScientSliderValue( float newvalue ) {
  slider->setfloatSliderValue(newvalue);
  le->setfloatLineEditValue(newvalue);
}

void floatScientSlider::emitSignal( float newvalue ) {
  emit floatScientSliderValueChanged(newvalue);
}


floatScientSlider::~floatScientSlider(){
  delete le;
  delete slider;
  delete grid;
}



////////////////////////////////////////////////////////////////////



floatLineBox3D::floatLineBox3D(float xval, float yval, float zval, int digits,QWidget *parent, const char *name )
              : QGroupBox(name,parent) {

  grid=new GuiGridLayout(this, 1, 3);

  xcache=xval;
  ycache=yval;
  zcache=zval;

  lex = new floatLineEdit( 0.0, 0.0, xval,digits,this, "lex", TEXTEDIT_WIDTH, TEXTEDIT_HEIGHT );
  ley = new floatLineEdit( 0.0, 0.0, yval,digits,this, "ley", TEXTEDIT_WIDTH, TEXTEDIT_HEIGHT );
  lez = new floatLineEdit( 0.0, 0.0, zval,digits,this, "lez", TEXTEDIT_WIDTH, TEXTEDIT_HEIGHT );

  grid->add_widget( lex->get_widget(), 0, 0 );
  grid->add_widget( ley->get_widget(), 0, 1 );
  grid->add_widget( lez->get_widget(), 0, 2 );

  connect(lex,SIGNAL(floatLineEditValueChanged( float )),this,SLOT(emitSignal_x( float )));
  connect(ley,SIGNAL(floatLineEditValueChanged( float )),this,SLOT(emitSignal_y( float )));
  connect(lez,SIGNAL(floatLineEditValueChanged( float )),this,SLOT(emitSignal_z( float )));

  connect(this,SIGNAL(SignalToChild_x( float )),lex, SLOT(setfloatLineEditValue( float)));
  connect(this,SIGNAL(SignalToChild_y( float )),ley, SLOT(setfloatLineEditValue( float)));
  connect(this,SIGNAL(SignalToChild_z( float )),lez, SLOT(setfloatLineEditValue( float)));
}

void floatLineBox3D::setfloatLineBox3DValue( float xval, float yval, float zval ) {
   xcache=xval;
   ycache=yval;
   zcache=zval;
   emit SignalToChild_x( xval );
   emit SignalToChild_y( yval );
   emit SignalToChild_z( zval );
}

void floatLineBox3D::emitSignal_x( float newvalue ) {xcache=newvalue; emit floatLineBox3DValueChanged(newvalue,ycache,zcache);}
void floatLineBox3D::emitSignal_y( float newvalue ) {ycache=newvalue; emit floatLineBox3DValueChanged(xcache,newvalue,zcache);}
void floatLineBox3D::emitSignal_z( float newvalue ) {zcache=newvalue; emit floatLineBox3DValueChanged(xcache,ycache,newvalue);}


floatLineBox3D::~floatLineBox3D(){
  delete lex;
  delete ley;
  delete lez;
  delete grid;
}

