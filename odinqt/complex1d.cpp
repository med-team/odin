#include <qevent.h> // for mouse events


#include "complex1d.h"
#include "plot.h"

complexfloatBox1D::complexfloatBox1D(const float *data1,const float *data2,int n,QWidget *parent, const char *name, bool fixed_size, const char *xAxisLabel, const char *yAxisLabelLeft, const char *yAxisLabelRight, float min_x, float max_x, bool detachable)
 : QGroupBox(name, parent ) {
  Log<OdinQt> odinlog("complexfloatBox1D","complexfloatBox1D(const float* ...)");
  common_init(name,fixed_size,data1,data2,xAxisLabel,yAxisLabelLeft,yAxisLabelRight,detachable);
  refresh(data1,data2,n,min_x,max_x);
}

complexfloatBox1D::complexfloatBox1D(const double *data1,const double *data2,int n,QWidget *parent, const char *name, bool fixed_size, const char *xAxisLabel, const char *yAxisLabelLeft, const char *yAxisLabelRight, float min_x, float max_x, bool detachable)
 : QGroupBox(name, parent ) {
  Log<OdinQt> odinlog("complexfloatBox1D","complexfloatBox1D(const double* ...)");
  common_init(name,fixed_size,data1,data2,xAxisLabel,yAxisLabelLeft,yAxisLabelRight,detachable);
  refresh(data1,data2,n,min_x,max_x);
}


void complexfloatBox1D::common_init(const char *name, bool fixed_size, bool data1, bool data2, const char *xAxisLabel, const char *yAxisLabelLeft, const char *yAxisLabelRight, bool detachable) {
  Log<OdinQt> odinlog("complexfloatBox1D","common_init()");

  detached=0;

  data1_ptr=0;
  data2_ptr=0;

  if(name) name_cache=name;
  if(xAxisLabel) xAxisLabel_cache=xAxisLabel;
  if(yAxisLabelLeft) yAxisLabelLeft_cache=yAxisLabelLeft;
  if(yAxisLabelRight) yAxisLabelRight_cache=yAxisLabelRight;
  detachable_cache=detachable;


  if(fixed_size) setFixedSize(_ARRAY_WIDGET_WIDTH_,_ARRAY_WIDGET_HEIGHT_);
  else setMinimumSize(_ARRAY_WIDGET_WIDTH_,_ARRAY_WIDGET_HEIGHT_);
  
  grid=new GuiGridLayout( this, 1, 1);

  plotter = new GuiPlot(this,fixed_size);

  ODINLOG(odinlog,normalDebug) << "xAxisLabel/yAxisLabelLeft/yAxisLabelRight=" << xAxisLabel << "/" << yAxisLabelLeft << "/" << yAxisLabelRight << STD_endl;
  plotter->set_x_axis_label(xAxisLabel);

  const char* data1_label=0; if(data1) data1_label=yAxisLabelLeft;
  const char* data2_label=0; if(data2) data1_label=yAxisLabelRight;
  plotter->set_y_axis_label(data1_label,data2_label);

  curveid1=0;
  curveid2=0;

  if(data1) curveid1 = plotter->insert_curve(false);
  if(data2) curveid2 = plotter->insert_curve(true);

  connect( plotter, SIGNAL(plotMousePressed(const QMouseEvent&)),  SLOT(mousePressedInPlot(const QMouseEvent&)) );
  connect( plotter, SIGNAL(plotMouseReleased(const QMouseEvent&)), SLOT(mouseReleasedInPlot(const QMouseEvent&)) );

  grid->add_widget( plotter->get_widget(), 0, 0 );
}


void complexfloatBox1D::create_x_cache(float min_x, float max_x, int n) {
  min_x_cache=min_x;
  max_x_cache=max_x;
  n_cache=n;

  int i;
  x_cache.resize(n);
  if(min_x<max_x) for(i=0; i<n; i++) x_cache[i]=min_x+float(i)/float(n-1)*(max_x-min_x);
  else            for(i=0; i<n; i++) x_cache[i]=i;
}



void complexfloatBox1D::refresh(const float *data1,const float *data2, int n, float min_x, float max_x) {
  Log<OdinQt> odinlog("complexfloatBox1D","refresh(const float* ...)");
  int i;

  ODINLOG(odinlog,normalDebug) << "data1/data2/n/min_x/max_x=" << data1 << "/" << data2 << "/" << n << "/" << min_x << "/" << max_x << STD_endl;

  create_x_cache(min_x, max_x, n);
  const double* x_arr=x_cache.c_array(); // call c_array only once

  data1_ptr=data2_ptr=0;

  if(data1) {
    data1_cache.resize(n);
    for(i=0; i<n; i++) data1_cache[i]=data1[i];
    data1_ptr=data1_cache.c_array();
    plotter->set_curve_data(curveid1, x_arr, data1_ptr, n, n<SYMBOL_MAX_NUMOF_POINTS);
  }
  if(data2) {
    data2_cache.resize(n);
    for(i=0; i<n; i++) data2_cache[i]=data2[i];
    ODINLOG(odinlog,normalDebug) << "data2_cache=" << data2_cache << STD_endl;
    data2_ptr=data2_cache.c_array();
    plotter->set_curve_data(curveid2, x_arr, data2_ptr, n, n<SYMBOL_MAX_NUMOF_POINTS);
  }

  plotter->replot();
  if(detached) detached->refresh(data1_ptr,data2_ptr,n,min_x,max_x);
}


void complexfloatBox1D::refresh(const double *data1,const double *data2, int n, float min_x, float max_x) {
  Log<OdinQt> odinlog("complexfloatBox1D","refresh(const double* ...)");

  create_x_cache(min_x, max_x, n);
  const double* x_arr=x_cache.c_array(); // call c_array only once

  data1_ptr=data2_ptr=0;

  if(data1) {
    data1_cache.resize(n);
    data1_cache.set_c_array((unsigned char*)data1,n);
    data1_ptr=data1_cache.c_array();
    plotter->set_curve_data(curveid1, x_arr, data1_ptr, n, n<SYMBOL_MAX_NUMOF_POINTS);
  }
  if(data2) {
    data2_cache.resize(n);
    data2_cache.set_c_array((unsigned char*)data2,n);
    data2_ptr=data2_cache.c_array();
    plotter->set_curve_data(curveid2, x_arr, data2_ptr, n, n<SYMBOL_MAX_NUMOF_POINTS);
  }

  plotter->replot();
  if(detached) detached->refresh(data1_ptr,data2_ptr,n,min_x,max_x);
}


void complexfloatBox1D::mousePressedInPlot(const QMouseEvent& qme) {
  Log<OdinQt> odinlog("complexfloatBox1D","mouseReleasedInPlot");

  if(left_button(&qme,false)) {
    x_pressed=qme.x();
    y_pressed=qme.y();
    ODINLOG(odinlog,normalDebug) << "x_pressed/y_pressed=" << x_pressed << "/" << y_pressed << STD_endl;
  }

  if(right_button(&qme,false)) {
    GuiPopupMenu pm(this);
    pm.insert_item("Autoscale", this, SLOT(autoscale()), Qt::Key_F1);
    if(detachable_cache) pm.insert_item("Detach", this, SLOT(detach()), Qt::Key_F2);
    pm.popup(plotter->get_widget()->mapToGlobal(qme.pos()));
  }  


}


void complexfloatBox1D::autoscale() {
  plotter->autoscale();
}


void complexfloatBox1D::detach() {
  if(!detached) delete detached;
  detached=new DetachedComplexfloatBox1D(data1_ptr,data2_ptr,n_cache,this, name_cache.c_str(), false,
                   xAxisLabel_cache.c_str(), yAxisLabelLeft_cache.c_str(), yAxisLabelRight_cache.c_str(), min_x_cache, max_x_cache);
}


void complexfloatBox1D::mouseReleasedInPlot(const QMouseEvent& qme) {
  Log<OdinQt> odinlog("complexfloatBox1D","mouseReleasedInPlot");

//  plotter->enable_outline(false);

  if(left_button(&qme,false)) {
    int x_released=qme.x();
    int y_released=qme.y();
    ODINLOG(odinlog,normalDebug) << "x_released/y_released=" << x_released << "/" << y_released << STD_endl;

    int low_x=x_pressed;
    if(x_released<low_x) low_x=x_released;
    int low_y=y_pressed;
    if(y_released<low_y) low_y=y_released;
    int upp_x=x_pressed;
    if(x_released>upp_x) upp_x=x_released;
    int upp_y=y_pressed;
    if(y_released>upp_y) upp_y=y_released;

    double x_axis_low=plotter->get_x(low_x);
    double x_axis_upp=plotter->get_x(upp_x);
    if(x_axis_low<x_axis_upp) plotter->set_x_axis_scale(x_axis_low,x_axis_upp);


    double y1_axis_low=plotter->get_y(upp_y); // reverse y-direction
    double y1_axis_upp=plotter->get_y(low_y);
    if(y1_axis_low<y1_axis_upp) plotter->set_y_axis_scale(y1_axis_low,y1_axis_upp);
    ODINLOG(odinlog,normalDebug) << "y1_axis_low/y1_axis_upp=" << y1_axis_low << "/" << y1_axis_upp << STD_endl;


    double y2_axis_low=plotter->get_y(upp_y,true); // reverse y-direction
    double y2_axis_upp=plotter->get_y(low_y,true);
    if(y2_axis_low<y2_axis_upp) plotter->set_y_axis_scale(y2_axis_low,y2_axis_upp,true);


    plotter->replot();

  }
}


complexfloatBox1D::~complexfloatBox1D(){
  delete plotter;
  delete grid;
  if(detached) delete detached;
}


//////////////////////////////////////////////////////////////////////////////////

void DetachedComplexfloatBox1D::create_grid() {
  grid = new GuiGridLayout( GuiDialog::get_widget(), 1, 1 );
  grid->add_widget( cfb, 0, 0, GuiGridLayout::Center );
  GuiDialog::show();
}


DetachedComplexfloatBox1D::DetachedComplexfloatBox1D(const double *data1, const double *data2, int n,complexfloatBox1D *parent, const char *name, bool fixed_size, const char *xAxisLabel, const char *yAxisLabelLeft, const char *yAxisLabelRight, float min_x, float max_x)
 : GuiDialog(parent,name,false) {
  Log<OdinQt> odinlog("DetachedComplexfloatBox1D","DetachedComplexfloatBox1D");
  ODINLOG(odinlog,normalDebug) << "yAxisLabelLeft/xAxisLabel=" << yAxisLabelLeft << "/" << xAxisLabel << STD_endl;
  cfb=new complexfloatBox1D(data1, data2, n, GuiDialog::get_widget(), name, fixed_size, xAxisLabel, yAxisLabelLeft, yAxisLabelRight, min_x, max_x);
  create_grid();
}

void DetachedComplexfloatBox1D::refresh(const double *data1,const double *data2,int n, float min_x, float max_x) {
  cfb->refresh(data1,data2,n,min_x,max_x);
}

DetachedComplexfloatBox1D::~DetachedComplexfloatBox1D() {
  delete grid;
  delete cfb;
}
