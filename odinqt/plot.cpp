#include <qevent.h> // for mouse events

#include "plot.h"


#include <qwt_plot.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_layout.h>
#include <qwt_wheel.h>
#if QWT_VERSION < 0x060000
#include <qwt_double_rect.h>
#else
#include <qwt_picker_machine.h>
#include <qwt_plot_renderer.h>
#endif

#if QWT_VERSION > 0x04FFFF
#include <qwt_plot_curve.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_grid.h>
#include <qwt_symbol.h>
#include <qwt_scale_div.h>
#include <qwt_scale_draw.h>
#else
#include <qwt_scldraw.h>
#endif



#if QWT_VERSION < 0x060000

class GuiPlotPrintFilter : public QwtPlotPrintFilter {

 public:
  GuiPlotPrintFilter(long baseline_id) : baseline_id_cache(baseline_id) {}

 private:
  long baseline_id_cache;

  // overloading virtual functions from QwtPlotPrintFilter
#if QWT_VERSION > 0x04FFFF
  QColor color(const QColor &c, Item item) const {
    QColor result=QwtPlotPrintFilter::color(c,item); // default
#else
  QColor color(const QColor &c, Item item, int id = -1) const {
    QColor result=QwtPlotPrintFilter::color(c,item,id); // default
#endif
    if(item==QwtPlotPrintFilter::Curve)            result=QColor("Black");
    if(item==QwtPlotPrintFilter::CurveSymbol)      result=QColor("Black");
    if(item==QwtPlotPrintFilter::Marker)           result=QColor("Black");
    if(item==QwtPlotPrintFilter::MajorGrid)        result=QColor("Gray").light(130);
    if(item==QwtPlotPrintFilter::MinorGrid)        result=QColor("Gray").light(150);
#if QWT_VERSION > 041
    if(item==QwtPlotPrintFilter::CanvasBackground)       result=QColor("White");
#else
    if(item==QwtPlotPrintFilter::Background)             result=QColor("White");
#endif
#if QWT_VERSION < 0x04FFFF
    if(item==QwtPlotPrintFilter::Curve && id==baseline_id_cache) result=QColor("Gray").dark(150);
#endif
    return result;
  }


#if QWT_VERSION > 0x04FFFF
  QFont font(const QFont &, Item item) const {
#else
  QFont font(const QFont &, Item item, int id = -1) const {
#endif
    return QFont(_FONT_TYPE_, 7);
  }

};

#endif // QWT_VERSION < 0x060000



////////////////////////////////////////////////////////////////////////////

struct GuiScaleDraw : QwtScaleDraw {

  // implementing virtual functions of QwtScaleDraw
#if QWT_VERSION < 0x04FFFF
  QString label (double val) const {return get_str(val);}
#else
  QwtText label (double val) const {return QString(get_str(val));}
#endif

 private:
  const char* get_str(double val) const {
    bool vertical=false;

#if QWT_VERSION < 0x04FFFF
    QwtScaleDraw::Orientation ort=orientation();
    if(ort==QwtScaleDraw::Left || ort==QwtScaleDraw::Right) vertical=true;
#else
    QwtScaleDraw::Alignment ort=alignment();
    if(ort==QwtScaleDraw::LeftScale || ort==QwtScaleDraw::RightScale) vertical=true;
#endif

    if(vertical) result=ftos(val,2,alwaysExp);
    else         result=ftos(val,2,neverExp);

    return result.c_str();
  }

  mutable STD_string result;
};

////////////////////////////////////////////////////////////////////////////

class GuiPlotPicker : public QwtPlotPicker {

 public:
  GuiPlotPicker(QwtPlotCanvas* canvas, GuiPlot* plot4feedback) : QwtPlotPicker(canvas), plot(plot4feedback) {}


 protected:
  void widgetMousePressEvent(QMouseEvent* qme) {
    Log<OdinQt> odinlog("GuiPlotPicker","widgetMousePressEvent");
    QwtPlotPicker::widgetMousePressEvent(qme);
    plot->emit_plotMousePressed(*qme);
    ODINLOG(odinlog,normalDebug) << "widgetMousePressEvent" << STD_endl;
  }

  void widgetMouseReleaseEvent(QMouseEvent* qme) {
    Log<OdinQt> odinlog("GuiPlotPicker","widgetMouseReleaseEvent");
    QwtPlotPicker::widgetMouseReleaseEvent(qme);
    plot->emit_plotMouseReleased(*qme);
    ODINLOG(odinlog,normalDebug) << "widgetMouseReleaseEvent" << STD_endl;
  }

  void widgetMouseMoveEvent(QMouseEvent* qme) {
    QwtPlotPicker::widgetMouseMoveEvent(qme);
    plot->emit_plotMouseMoved(*qme);
  }

 private:
  GuiPlot* plot;
};


////////////////////////////////////////////////////////////////////////////


GuiPlot::GuiPlot(QWidget *parent, bool fixed_size, int width, int height) {
  Log<OdinQt> odinlog("GuiPlot","GuiPlot(...)");

  baseline_id_cache=0;

  qwtplotter = new QwtPlot(parent);
  if(fixed_size) qwtplotter->setFixedSize  (width,height);
  else           qwtplotter->setMinimumSize(width,height);
  qwtplotter->resize(width,height);

  qwtplotter->plotLayout()->setAlignCanvasToScales(true);

  // to get mouse-move events
  qwtplotter->canvas()->setMouseTracking(true);

  // set default axis titles
  set_x_axis_label(0);
  set_y_axis_label(0,0);

/*
// still necessary?
#if QWT_VERSION < 0x04FFFF
  qwtplotter->setAxisTitleFont(QwtPlot::xBottom,QFont(_FONT_TYPE_, _FONT_SIZE_));
  qwtplotter->setAxisTitleFont(QwtPlot::yLeft,  QFont(_FONT_TYPE_, _FONT_SIZE_));
  qwtplotter->setAxisTitleFont(QwtPlot::yRight, QFont(_FONT_TYPE_, _FONT_SIZE_));
#endif
*/

  qwtplotter->enableAxis(QwtPlot::xBottom,true);

/*
  qwtplotter->setAxisLabelFormat(QwtPlot::yLeft,'e',1,10);
  qwtplotter->setAxisLabelFormat(QwtPlot::xBottom,'f',2);
*/
  qwtplotter->setAxisScaleDraw(QwtPlot::yLeft, new GuiScaleDraw());
  qwtplotter->setAxisScaleDraw(QwtPlot::xBottom, new GuiScaleDraw());


#if QWT_VERSION < 0x060000
  qwtplotter->setCanvasBackground(_ARRAY_BACKGROUND_COLOR_);
#else
  qwtplotter->setCanvasBackground(QBrush(QColor(_ARRAY_BACKGROUND_COLOR_)));
#endif


#if QWT_VERSION > 0x04FFFF
  plotgrid=new QwtPlotGrid();
  ODINLOG(odinlog,normalDebug) << "plotgrid=" << plotgrid << STD_endl;
#endif
  QPen gridpen(QColor(_ARRAY_GRID_COLOR_).dark(_ARRAY_GRID_DARK_FACTOR_));
#if QWT_VERSION > 0x04FFFF
  ODINLOG(odinlog,normalDebug) << "plotgrid=" << plotgrid << STD_endl;
  plotgrid->setPen(gridpen);

#if QWT_VERSION < 0x060100
  plotgrid->setMajPen(gridpen);
  plotgrid->setMinPen(gridpen);
#else
  plotgrid->setMajorPen(gridpen);
  plotgrid->setMinorPen(gridpen);
#endif

#else
  qwtplotter->setGridPen(gridpen);
  qwtplotter->setGridMajPen(gridpen);
  qwtplotter->setGridMinPen(gridpen);
#endif
  enable_grid(true);
#if QWT_VERSION > 0x04FFFF
  plotgrid->attach(qwtplotter);
#endif


#if QWT_VERSION < 0x04FFFF
  qwtplotter->enableLegend(false);
  qwtplotter->enableOutline(false);
#endif

  QwtPlotCanvas* canv=(QwtPlotCanvas*)qwtplotter->canvas(); // cast required for qwt 6.1 (and higher?)

  canvas_framewidth=canv->lineWidth();

  picker=new GuiPlotPicker(canv,this);
  picker->setRubberBandPen(QColor(_ARRAY_SELECTION_COLOR_));
  set_rect_outline_style(); // do this after picker was initialized

}


void GuiPlot::set_axis_label(int axisId, const char* label, bool omit, int alignment) {
  Log<OdinQt> odinlog("GuiPlot","set_axis_label");
  ODINLOG(odinlog,normalDebug) << "axisId/label/omit/alignment=" << axisId << "/" << label << "/" << omit << "/" << alignment << STD_endl;
  if(!label || STD_string(label)=="") omit=true;
  if(omit) {
#if QWT_VERSION > 0x04FFFF
    QwtText txt("");
    txt.setFont(QFont(_FONT_TYPE_, 1));
    qwtplotter->setAxisTitle(axisId,txt);
#else
    qwtplotter->setAxisTitle(axisId,"");
    qwtplotter->setAxisTitleFont(axisId,QFont(_FONT_TYPE_, 1)); // make font zero size to save space in widget
#endif
  } else {
#if QWT_VERSION > 0x04FFFF
    QwtText txt(label);
    txt.setFont(QFont(_FONT_TYPE_, _FONT_SIZE_));
    txt.setRenderFlags(alignment);
    qwtplotter->setAxisTitle(axisId,txt);
#else
    qwtplotter->setAxisTitle(axisId,label);
    qwtplotter->setAxisTitleFont(axisId,QFont(_FONT_TYPE_, _FONT_SIZE_));
    qwtplotter->setAxisTitleAlignment(axisId,alignment);
#endif
  }
}


void GuiPlot::set_x_axis_label(const char *xAxisLabel, bool omit) {
  set_axis_label(QwtPlot::xBottom,xAxisLabel,omit,Qt::AlignRight);
}


void GuiPlot::set_y_axis_label(const char *yAxisLabelLeft, const char *yAxisLabelRight) {
  if(yAxisLabelLeft)  set_axis_label(QwtPlot::yLeft, yAxisLabelLeft, false,Qt::AlignCenter);
  if(yAxisLabelRight) set_axis_label(QwtPlot::yRight,yAxisLabelRight,false,Qt::AlignCenter);

  if(yAxisLabelLeft)  qwtplotter->enableAxis(QwtPlot::yLeft,true);
  if(yAxisLabelRight) qwtplotter->enableAxis(QwtPlot::yRight,true);
}


long GuiPlot::insert_curve(bool use_right_y_axis, bool draw_spikes, bool baseline) {
  Log<OdinQt> odinlog("GuiPlot","insert_curve");
  long result=0;

  int yaxis=QwtPlot::yLeft;
  if(use_right_y_axis) yaxis=QwtPlot::yRight;

  QPen curvepen(QColor(_ARRAY_FOREGROUND_COLOR1_));
  if(use_right_y_axis || baseline) {
    curvepen=QColor(_ARRAY_FOREGROUND_COLOR2_);
  }

#if QWT_VERSION > 0x04FFFF
  QwtPlotCurve* curve=new QwtPlotCurve();
#if QWT_VERSION < 0x060000
  curve->setAxis(QwtPlot::xBottom, yaxis);
#else
  curve->setAxes(QwtPlot::xBottom, yaxis);
#endif
  curve->setPen(curvepen);
  curve->attach(qwtplotter);
  result=curve_map.size()+1;
  curve_map[result]=curve;
#else
  result=qwtplotter->insertCurve("",QwtPlot::xBottom,yaxis);
  qwtplotter->setCurvePen(result,curvepen);
#endif


  if(draw_spikes) {
#if QWT_VERSION > 0x04FFFF
    curve->setBaseline(0.0);
    curve->setStyle(QwtPlotCurve::Sticks);
#else
    qwtplotter->setCurveBaseline(result,0.0);
    qwtplotter->setCurveStyle(result,QwtCurve::Sticks);
#endif
  }


  if(baseline) baseline_id_cache=result;

  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return result;
}


long GuiPlot::insert_marker(const char* label, double x, bool outline, bool horizontal, bool animate) {
  Log<OdinQt> odinlog("GuiPlot","insert_marker");
  long result=0;

  ODINLOG(odinlog,normalDebug) << "label/x/outline/horizontal/animate=" << label << "/" << x << "/" << outline << "/" << horizontal << "/" << animate << STD_endl;

  QColor markcolor(QColor(_ARRAY_MARKER_COLOR_).light(_ARRAY_MARKER_BRIGHT_FACTOR_));
  if(animate) markcolor=QColor("red");
  if(outline) markcolor=QColor(_ARRAY_SELECTION_COLOR_);

#if QWT_VERSION > 0x04FFFF
  QwtPlotMarker* marker=new QwtPlotMarker();
  if(horizontal) {
    marker->setLineStyle(QwtPlotMarker::HLine);
    marker->setYValue(x);
  } else {
    marker->setLineStyle(QwtPlotMarker::VLine);
    marker->setXValue(x);
  }
  marker->setLinePen(QPen(markcolor));
#if QWT_VERSION > 0x04FFFF
  QwtText txt(label);
  txt.setColor(markcolor);
  txt.setRenderFlags(Qt::AlignTop);
  marker->setLabel(txt);
#else
  marker->setLabelPen(QPen(markcolor));
  marker->setLabelAlignment(Qt::AlignTop);
  marker->setLabel(QString(label));
#endif
  marker->attach(qwtplotter);
  result=marker_map.size()+1;
  marker_map[result]=marker;
#else
  int axis=QwtPlot::xBottom;
  if(horizontal) axis=QwtPlot::yLeft;
  result=qwtplotter->insertLineMarker(label, axis);
  qwtplotter->setMarkerPen(result,QPen(markcolor));
  if(horizontal) qwtplotter->setMarkerYPos(result,x);
  else           qwtplotter->setMarkerXPos(result,x);
#endif

  ODINLOG(odinlog,normalDebug) << "result=" << result << STD_endl;
  return result;
}



void GuiPlot::remove_marker(long id) {
  Log<OdinQt> odinlog("GuiPlot","remove_marker");
  ODINLOG(odinlog,normalDebug) << "id=" << id << STD_endl;
#if QWT_VERSION > 0x04FFFF
  QwtPlotMarker* qpm=get_marker(id);
  if(qpm) qpm->detach();
#else
  qwtplotter->removeMarker(id);
#endif
}

void GuiPlot::set_marker_pos(long id, double x) {
  Log<OdinQt> odinlog("GuiPlot","remove_marker");
  ODINLOG(odinlog,normalDebug) << "id/x=" << id << "/" << x << STD_endl;
#if QWT_VERSION > 0x04FFFF
  QwtPlotMarker* qpm=get_marker(id);
  if(qpm) qpm->setXValue(x);
#else
  qwtplotter->setMarkerXPos(id,x);
#endif
}


void GuiPlot::set_curve_data(long curveid, const double* x, const double* y, int n, bool symbol) {
  Log<OdinQt> odinlog("GuiPlot","set_curve_data");
  ODINLOG(odinlog,normalDebug) << "curveid/x/y/symbol=" << curveid << "/" << x << "/" << y << "/" << symbol << STD_endl;

#if QWT_VERSION > 0x04FFFF
  QwtSymbol::Style symbstyle=QwtSymbol::NoSymbol;
#else
  QwtSymbol::Style symbstyle=QwtSymbol::None;
#endif
  if(symbol) {
    symbstyle=QwtSymbol::Ellipse;
  }

  QwtSymbol* symb=new QwtSymbol(symbstyle,QBrush(),QColor(_ARRAY_FOREGROUND_COLOR1_),QSize(PLOT_SYMBOLS_SIZE,PLOT_SYMBOLS_SIZE));

  if(n) {
    ODINLOG(odinlog,normalDebug) << "y[" << n/2 << "]=" << y[n/2] << STD_endl;
  }
#if QWT_VERSION > 0x04FFFF
  QwtPlotCurve* qpc=get_curve(curveid);
  if(qpc) {
#if QWT_VERSION < 0x060000
    qpc->setSymbol(*symb);
    qpc->setRawData(x, y, n);
#else
    qpc->setSymbol(symb);
    qpc->setRawSamples(x, y, n);
#endif
  }
#else
  qwtplotter->setCurveSymbol(curveid,*symb);
  qwtplotter->setCurveRawData(curveid, x, y, n);
#endif
#if QWT_VERSION < 0x060000
  delete symb;  // qwt6 deletes symbol itself
#endif
}


void GuiPlot::replot() {
  Log<OdinQt> odinlog("GuiPlot","replot");
  ODINLOG(odinlog,normalDebug) << "replotting " << curve_map.size() << " curves and " << marker_map.size() << " markers" << STD_endl;
  qwtplotter->replot();
//  qwtzoomer->setZoomBase();
}


void GuiPlot::autoscale() {
  qwtplotter->setAxisAutoScale(QwtPlot::xBottom);
  qwtplotter->setAxisAutoScale(QwtPlot::yLeft);
  qwtplotter->setAxisAutoScale(QwtPlot::yRight);
  replot();
}


void GuiPlot::autoscale_y(double& maxBound) {
  qwtplotter->setAxisAutoScale(QwtPlot::yLeft);
  qwtplotter->replot(); // seems to be neccessary for correct autoscaling, even if the curve is already added

#if QWT_VERSION > 0x04FFFF
#if QWT_VERSION > 0x0501FF

#if QWT_VERSION < 0x060100
   double new_lbound=qwtplotter->axisScaleDiv(QwtPlot::yLeft)->lowerBound();
   double new_hbound=qwtplotter->axisScaleDiv(QwtPlot::yLeft)->upperBound();
#else
   double new_lbound=qwtplotter->axisScaleDiv(QwtPlot::yLeft).lowerBound();
   double new_hbound=qwtplotter->axisScaleDiv(QwtPlot::yLeft).upperBound();
#endif

#else
   double new_lbound=qwtplotter->axisScaleDiv(QwtPlot::yLeft)->lBound();
   double new_hbound=qwtplotter->axisScaleDiv(QwtPlot::yLeft)->hBound();
#endif
#else
  double new_lbound=qwtplotter->axisScale(QwtPlot::yLeft)->lBound();
  double new_hbound=qwtplotter->axisScale(QwtPlot::yLeft)->hBound();
#endif

  // symmetrical adjustment
  maxBound=STD_max(fabs(new_lbound),fabs(new_hbound));
  new_lbound=-maxBound;
  new_hbound= maxBound;

  qwtplotter->setAxisScale(QwtPlot::yLeft, new_lbound, new_hbound); // switch back to manual scaling for scrolling
  replot();
}

void GuiPlot::rescale_y(double maxBound) {
  qwtplotter->setAxisScale(QwtPlot::yLeft, -maxBound, maxBound);
  replot();
}


void GuiPlot::clear() {
  Log<OdinQt> odinlog("GuiPlot","clear()");
#if QWT_VERSION > 0x04FFFF
  for(STD_map<long,QwtPlotCurve*>::const_iterator curvit=curve_map.begin(); curvit!=curve_map.end(); ++curvit) {
    ODINLOG(odinlog,normalDebug) << "Detaching curve #" << curvit->first << STD_endl;
    curvit->second->detach();
    ODINLOG(odinlog,normalDebug) << "Deleting curve #" << curvit->first << STD_endl;
    delete curvit->second;
  }
  ODINLOG(odinlog,normalDebug) << "Clearing curve cache" << STD_endl;
  curve_map.clear();
  ODINLOG(odinlog,normalDebug) << "Removing markers" << STD_endl;
  remove_markers();
#endif
#if QWT_VERSION < 0x060000
  qwtplotter->clear();  // do this after detaching curves
#else
//  qwtplotter->detachItems(); // does not work with QwtPlotGrid
#endif
}

void GuiPlot::remove_markers() {
#if QWT_VERSION > 0x04FFFF
  for(STD_map<long,QwtPlotMarker*>::const_iterator markit=marker_map.begin(); markit!=marker_map.end(); ++markit) {
    markit->second->detach();
    delete markit->second;
  }
  marker_map.clear();
#else
  qwtplotter->removeMarkers();
#endif
}

double GuiPlot::get_x(int x_pixel) const {
  Log<OdinQt> odinlog("GuiPlot","get_x");
  double result=qwtplotter->invTransform(QwtPlot::xBottom,x_pixel+canvas_framewidth);
  ODINLOG(odinlog,normalDebug) << "result/x_pixel=" << result << "/" << x_pixel << STD_endl;
  return result;;
}

double GuiPlot::get_y(int y_pixel, bool right_axes) const {
  if(right_axes) return qwtplotter->invTransform(QwtPlot::yRight, y_pixel+canvas_framewidth);
  else           return qwtplotter->invTransform(QwtPlot::yLeft,  y_pixel+canvas_framewidth);
}

long GuiPlot::closest_curve(int x, int y, int& dist) const {
#if QWT_VERSION > 0x04FFFF
  Log<OdinQt> odinlog("GuiPlot","closest_curve");
  long result=-1;
  double dmin = 1.0e10;
  double curvedist;
  QPoint pos(x,y);
  for(STD_map<long,QwtPlotCurve*>::const_iterator curvit=curve_map.begin(); curvit!=curve_map.end(); ++curvit) {
    curvit->second->closestPoint(pos,&curvedist);
    if(curvedist<dmin) {
      result=curvit->first;
      dist=int(curvedist);
      dmin=curvedist;
    }
  }
  ODINLOG(odinlog,normalDebug) << "result/dist" << result << "/" << dist << STD_endl;
  return result;
#else
  return qwtplotter->closestCurve(x,y,dist);
#endif
}

void GuiPlot::highlight_curve(long id, bool flag) {
  const char* color=_ARRAY_FOREGROUND_COLOR1_;
  if(flag) color=_ARRAY_HIGHLIGHT_COLOR_;
  set_curve_pen(id,color);
}

void GuiPlot::set_x_axis_scale(double lbound, double ubound) {
  Log<OdinQt> odinlog("GuiPlot","set_x_axis_scale");
  ODINLOG(odinlog,normalDebug) << "lbound/ubound=" << lbound << "/" << ubound << STD_endl;
  qwtplotter->setAxisScale(QwtPlot::xBottom,lbound,ubound);
}


void GuiPlot::set_y_axis_scale(double lbound, double ubound, bool right_axes) {
  if(right_axes) qwtplotter->setAxisScale(QwtPlot::yRight,lbound,ubound);
  else           qwtplotter->setAxisScale(QwtPlot::yLeft, lbound,ubound);
}


void GuiPlot::set_curve_pen(long id, const char* color, int width) {
  QPen qp;
  qp.setColor(color);
  qp.setWidth(width);
#if QWT_VERSION > 0x04FFFF
  QwtPlotCurve* qpc=get_curve(id);
  if(qpc) qpc->setPen(qp);
#else
  qwtplotter->setCurvePen(id,qp);
#endif
}

void GuiPlot::set_rect_outline_style() {
#if QWT_VERSION < 0x060000
  picker->setSelectionFlags(QwtPicker::RectSelection | QwtPicker::CornerToCorner | QwtPicker::DragSelection);
#else
  picker->setStateMachine(new QwtPickerDragRectMachine);
#endif
  picker->setRubberBand(QwtPicker::RectRubberBand);
}

void GuiPlot::set_line_outline_style(bool horizontal) {
#if QWT_VERSION < 0x060000
  picker->setSelectionFlags(QwtPicker::PointSelection | QwtPicker::DragSelection);
#else
  picker->setStateMachine(new QwtPickerDragPointMachine);
#endif
  if(horizontal) picker->setRubberBand(QwtPicker::HLineRubberBand);
  else           picker->setRubberBand(QwtPicker::VLineRubberBand);
}

void GuiPlot::enable_axes(bool flag) {
  qwtplotter->enableAxis(QwtPlot::xBottom,flag);
  qwtplotter->enableAxis(QwtPlot::yLeft,flag);
}


void GuiPlot::enable_grid(bool flag) {
  Log<OdinQt> odinlog("GuiPlot","enable_grid");
#if QWT_VERSION > 0x04FFFF
  plotgrid->enableX(flag);
  plotgrid->enableY(flag);
#else
  qwtplotter->enableGridX(flag);
  qwtplotter->enableGridY(flag);
#endif
}


void GuiPlot::print(QPainter* painter, const QRect& rect) const {
  Log<OdinQt> odinlog("GuiPlot","print");
#if QWT_VERSION < 0x060000
  GuiPlotPrintFilter pfilter(baseline_id_cache);
  qwtplotter->print(painter,rect,pfilter);
#else
  QwtPlotRenderer renderer;
  renderer.render(qwtplotter, painter, QRectF(rect));
#endif
}


QWidget* GuiPlot::get_widget() {return qwtplotter;}


void GuiPlot::emit_plotMousePressed(const QMouseEvent& qme) {
  emit plotMousePressed(qme);
}

void GuiPlot::emit_plotMouseReleased(const QMouseEvent& qme) {
  emit plotMouseReleased(qme);
}

void GuiPlot::emit_plotMouseMoved(const QMouseEvent& qme) {
  emit plotMouseMoved(qme);
}


QwtPlotCurve* GuiPlot::get_curve(long id) {
  STD_map<long,QwtPlotCurve*>::iterator it=curve_map.find(id);
  if(it==curve_map.end()) return 0;
  return it->second;
}


QwtPlotMarker* GuiPlot::get_marker(long id) {
  STD_map<long,QwtPlotMarker*>::iterator it=marker_map.find(id);
  if(it==marker_map.end()) return 0;
  return it->second;
}



GuiPlot::~GuiPlot() {
  Log<OdinQt> odinlog("GuiPlot","~GuiPlot()");
  clear();
#if QWT_VERSION > 0x04FFFF
  delete plotgrid;
#endif
  delete picker;
  delete qwtplotter;
}


////////////////////////////////////////////////////////////////////////////


GuiWheel::GuiWheel(QWidget *parent) {
  wheel=new QwtWheel(parent);
  wheel->setOrientation(Qt::Vertical);

  connect( wheel, SIGNAL(valueChanged(double)),  SLOT(emit_valueChanged(double)) );
}

void GuiWheel::set_range(double min, double max) {
  wheel->setRange(min,max);
}

void GuiWheel::set_value(double newval) {
//  wheel->fitValue(newval); // ?
  wheel->setValue(newval);
}


QWidget* GuiWheel::get_widget() {return wheel;}

void GuiWheel::emit_valueChanged(double newval) {emit valueChanged(newval);}

GuiWheel::~GuiWheel() {
  delete wheel;
}


