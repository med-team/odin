#include "ldrwidget.h"
#include "ldrblockwidget.h"



/////////////////////////////////////////////////////////////////////////////

LDRblockGrid::LDRblockGrid(LDRblock& block,unsigned int columns,QWidget *parent,const char* omittext)
   : QWidget(parent), val(block) {
  Log<OdinQt> odinlog(&block,"LDRblockGrid(...)");

  grid=0;

  STD_list<LDRwidget*> subwidgets;
  STD_list<LDRwidget*>::iterator it;


  unsigned int numof_pars=block.numof_pars();
  LDRbase* ldrptr;


  // fill list with subwidgets
  for(unsigned int i=0;i<numof_pars;i++) {
    ldrptr=&(block[i]);
    if(ldrptr && ldrptr->get_jdx_props().userdef_parameter && (ldrptr->get_parmode()!=hidden)) {
      LDRwidget* ldrwidget=0;
      LDRblock* blockdummy=0;
      blockdummy=ldrptr->cast(blockdummy);
      if(blockdummy) {
        unsigned int cols4block=1;
        if(blockdummy->numof_pars()>5) cols4block=2;
        ldrwidget=new LDRwidget(*ldrptr,cols4block,this,false,omittext);

      } else ldrwidget=new LDRwidget(*ldrptr,1,this,false,omittext);
      subwidgets.push_back(ldrwidget);
    }
  }

  unsigned int n_widget_rows=0;
  unsigned int rowheight=0;
  unsigned int colstart=0;

  // calculate n_widget_rows layout
  for(it=subwidgets.begin(); it!=subwidgets.end(); ++it) {
    unsigned int colwidth=(*it)->get_cols();

    if( (colstart+colwidth) > 2 ) {
      // goto new row
      n_widget_rows+=rowheight;
      colstart=0;
      rowheight=0;
    }

    ODINLOG(odinlog,normalDebug) << "rows(" << (*it)->get_label() << ")=" << (*it)->get_rows() << STD_endl;
    if ( (*it)->get_rows() > rowheight ) rowheight=(*it)->get_rows();
    colstart+=colwidth;
  }
  n_widget_rows+=rowheight;

  ODINLOG(odinlog,normalDebug) << "n_widget_rows/columns" << n_widget_rows << "/" << columns << STD_endl;
  unsigned int rows=(n_widget_rows/columns);
//  if(n_widget_rows%columns) rows++;
  rows++;


  unsigned int irow=0;
  unsigned int icolumn=0;
  rowheight=0;
  colstart=0;

  grid = new GuiGridLayout( this, rows , columns*2);
  ODINLOG(odinlog,normalDebug) << "creating " << rows << "x" << columns*2 << " grid" << STD_endl;

  for(it=subwidgets.begin(); it!=subwidgets.end(); ++it) {

    unsigned int colwidth=(*it)->get_cols();

    if( (colstart+colwidth) > 2 ) {
      // goto new row
      irow+=rowheight;
      colstart=0;
      rowheight=0;
    }

//    rowheight=(*it)->get_rows();
    if ( (*it)->get_rows() > rowheight ) rowheight=(*it)->get_rows();
    if( (irow+rowheight) > rows ) {
      icolumn++;
      irow=0;
    }

    unsigned int coloffset=2*icolumn+colstart;
    unsigned int rowoffset=irow;

    colstart+=colwidth;

    int rowstart=rowoffset;
//    int rowend=rowoffset+rowheight-1;
//    int colstart=coloffset;
//    int colend=coloffset+colwidth-1;
    ODINLOG(odinlog,normalDebug) << (*it)->get_label() << " at " << "(" << rowstart << "-" << coloffset << "," << rowheight << "-" << colwidth << ")" << STD_endl;

    grid->add_widget( (*it), rowoffset, coloffset, GuiGridLayout::VCenter, rowheight, colwidth );

    connect((*it),SIGNAL(valueChanged()),this,SLOT(emitValueChanged()));
    connect(this,SIGNAL(updateSubWidget()),(*it),SLOT(updateWidget()));
    connect(this,SIGNAL(deleteSubDialogs()),(*it),SLOT(deleteDialogs()));
  }
}

void LDRblockGrid::updateWidget() {
  for(STD_list<LDRwidgetDialog*>::iterator it=subdialogs.begin(); it!=subdialogs.end(); ++it) {
    (*it)->updateWidget();
  }
  emit updateSubWidget();
}


void LDRblockGrid::deleteDialogs() {
  emit deleteSubDialogs();
}



void LDRblockGrid::createDialog() {
  Log<OdinQt> odinlog(&val,"createDialog");
  LDRwidgetDialog* dlg=new LDRwidgetDialog(val,1,this);
  subdialogs.push_back(dlg);
  connect(dlg,SIGNAL(valueChanged()), this,SLOT(emitValueChanged()));
  emit valueChanged();
}


/////////////////////////////////////////////////////////////////////////////

LDRblockScrollView::LDRblockScrollView(LDRblock& block,unsigned int columns,QWidget *parent,const char* omittext) {
  Log<OdinQt> odinlog(&block,"LDRblockScrollView(...)");

  ldrgrid=new LDRblockGrid(block,columns,parent,omittext);
  connect(ldrgrid,SIGNAL(valueChanged()),this,SLOT(emitValueChanged()));

  scroll=new GuiScroll(ldrgrid,parent);
}


LDRblockScrollView::~LDRblockScrollView() {
  delete scroll;
}

/////////////////////////////////////////////////////////////////////////////


LDRblockWidget::LDRblockWidget(LDRblock& ldrblock,unsigned int columns,QWidget *parent,bool doneButton,bool is_dialog,const char* omittext, bool storeLoadButtons, bool readonly)
   : QGroupBox(ldrblock.get_label().c_str(), parent ), parblock(ldrblock) {
  Log<OdinQt> odinlog(&ldrblock,"LDRblockWidget(...)");

  pb_done=0;
  pb_edit=0;
  pb_store=0;
  pb_load=0;
  grid=0;
  ldrscroll=0;
  noeditlist=0;


  if(ldrblock.is_embedded() || is_dialog) {

    int height=1;
    if(doneButton || storeLoadButtons) height=2;

    grid=new GuiGridLayout(this, height, 3);


//    if(ldrblock.get_parmode()==noedit) {
    if(readonly) {
      ODINLOG(odinlog,normalDebug) << "noedit mode" << STD_endl;

      svector columns; columns.resize(4);
      columns[0]="Name";
      columns[1]="Value";
      columns[2]="Unit";
      columns[3]="Description";

      noeditlist=new GuiListView(this, columns);
      grid->add_widget( noeditlist->get_widget(), 0, 0, GuiGridLayout::Default, 1, 3);

      unsigned int n=ldrblock.numof_pars();
      noedititems.resize(n);
      for(unsigned int i=0; i<n; i++) {
        LDRbase& par=ldrblock[i];
        noedititems[i]=0;
        LDRaction* dummy=0;
        if(!par.cast(dummy)) { // do not display action flags
          columns[0]=par.get_label();
          columns[1]=par.printvalstring();
          columns[2]=par.get_unit();
          columns[3]=par.get_description();
          ODINLOG(odinlog,normalDebug) << "columns=" << columns.printbody() << STD_endl;
          noedititems[i]=new GuiListItem(noeditlist, columns);
        }
      }

    } else {

      ldrscroll=new LDRblockScrollView(ldrblock,columns,this,omittext);
      grid->add_widget( ldrscroll->get_widget(), 0, 0, GuiGridLayout::Default, 1, 3);
      connect(ldrscroll,SIGNAL(valueChanged()),this,SLOT(emitValueChanged()));
    }

    // dummy button which gets the focus
    if(doneButton || storeLoadButtons) {
      GuiButton* pb_dummy = new GuiButton( this, 0, "", "Dummy" );
      pb_dummy->set_default(true);
      pb_dummy->get_widget()->hide();
    }
   
    if(doneButton) {
      pb_done = new GuiButton( this, this, SLOT(emitDone()), "Done" );
      pb_done->set_default(false);
      grid->add_widget( pb_done->get_widget(), 1, 2, GuiGridLayout::Center );
//      connect( pb_done->get_widget(), SIGNAL(clicked()), this,SLOT(emitDone()) );
    }

    if(storeLoadButtons) {
      pb_store = new GuiButton( this, this, SLOT(storeBlock()), "Store ..." );
      pb_load =  new GuiButton( this, this, SLOT(loadBlock()),  "Load ..." );
      pb_store->set_default(false);
      pb_load->set_default(false);
      grid->add_widget( pb_store->get_widget(), 1, 0, GuiGridLayout::Center );
      grid->add_widget( pb_load->get_widget(), 1, 1, GuiGridLayout::Center );
//      connect( pb_store->get_widget(), SIGNAL(clicked()), this,SLOT(storeBlock()) );
//      connect( pb_load->get_widget(), SIGNAL(clicked()), this,SLOT(loadBlock()) );
    }
    
  } else {  // if(block.is_embedded())

    ldrscroll=new LDRblockScrollView(ldrblock,columns,0,omittext);
    connect(ldrscroll,SIGNAL(valueChanged()),this,SLOT(emitValueChanged()));

    grid=new GuiGridLayout( this, 1 , 1);

    pb_edit = new GuiButton( this, this, SLOT(createDialog()), "Edit" );
    grid->add_widget( pb_edit->get_widget(), 0, 0, GuiGridLayout::Center );
//    connect( pb_edit->get_widget(), SIGNAL(clicked()), this,SLOT(createDialog()) );
  }

}

void LDRblockWidget::createDialog() {
  Log<OdinQt> odinlog("LDRblockWidget","createDialog");
  ODINLOG(odinlog,normalDebug) << "ldrscroll=" << ldrscroll << STD_endl;
  if(ldrscroll) ldrscroll->createDialog();
}

LDRblockWidget::~LDRblockWidget() {
  if(pb_done) delete pb_done;
  if(pb_store) delete pb_store;
  if(pb_load) delete pb_load;
  if(grid) delete grid;
  if(ldrscroll) delete ldrscroll;
  if(noeditlist) delete noeditlist;
  for(unsigned int i=0; i<noedititems.size(); i++) if(noedititems[i]) delete noedititems[i];
}



void LDRblockWidget::emitDone() {
  emit doneButtonPressed();
}

void LDRblockWidget::storeBlock() {
  STD_string fname=get_save_filename(("Storing "+STD_string(parblock.get_label())).c_str(),"","",this);
  if(fname=="") return;
  parblock.write(fname);
}

void LDRblockWidget::loadBlock() {
  STD_string fname=get_open_filename(("Loading "+STD_string(parblock.get_label())).c_str(),"","",this);
  if(fname=="") return;
  parblock.load(fname);
  updateWidget();
}


/////////////////////////////////////////////////////////////////////////////////


LDRwidgetDialog::LDRwidgetDialog(LDRblock& ldr,unsigned int columns,QWidget *parent, bool modal, bool readonly)
 : GuiDialog(parent, ldr.get_label().c_str(), modal) {
  Log<OdinQt> odinlog(&ldr,"LDRwidgetDialog(...)");

  grid=new GuiGridLayout(GuiDialog::get_widget(), 2, 1);

  ldrwidget=new LDRblockWidget(ldr,columns,GuiDialog::get_widget(),true,true, "",  false, readonly);

  grid->add_widget( ldrwidget, 0, 0 );
  connect(ldrwidget,SIGNAL(valueChanged()),this,SLOT(emitChanged()));
  connect(ldrwidget,SIGNAL(doneButtonPressed()), this,SLOT(callDone()) );


  GuiDialog::show();

#if QT_VERSION > 299
  if(modal) GuiDialog::exec();
#endif
}


LDRwidgetDialog::~LDRwidgetDialog() {
  delete ldrwidget;
  delete grid;
}

void LDRwidgetDialog::updateWidget() {
  ldrwidget->updateWidget();
}

void LDRwidgetDialog::callDone() {
  emit finished();
  GuiDialog::done();
}

void LDRwidgetDialog::emitChanged() {
  emit valueChanged();
}
