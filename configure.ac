dnl Boilerplate


AC_PREREQ([2.69])

AC_INIT([odin],[2.0.5])
AC_CONFIG_SRCDIR(tjutils/tjutils.h)
AM_INIT_AUTOMAKE

AM_CONFIG_HEADER(tjutils/config.h)

AC_PREFIX_DEFAULT(/usr/local)
if test "x$prefix" = "xNONE"; then
  prefix=$ac_default_prefix
  ac_configure_args="$ac_configure_args --prefix $prefix"
fi


dnl Options

AC_ARG_ENABLE([debug],
 [AS_HELP_STRING([--enable-debug],[Include code for debugging/tracing])],
 enable_debug=yes
)

AC_ARG_ENABLE([stdcpp-replacement],
 [AS_HELP_STRING([--enable-stdcpp-replacement],[Use built-in replacement for C++ standard library (STL, strings, streams)])],
 enable_stdcpp_replacement=yes
)

AC_ARG_ENABLE([blitz-replacement],
 [AS_HELP_STRING([--enable-blitz-replacement],[Use built-in replacement for Blitz++ library])],
 enable_blitz_replacement=yes
)

AC_ARG_ENABLE([custom-heap],
 [AS_HELP_STRING([--enable-custom-heap=SIZE],[Use a heap which is a static block of memory (SIZE MB large), useful for real-time systems which do not have a safe new/delete])],
 enable_custom_heap=yes
 custom_heap_size=$enableval
 if test "$enableval" = "yes"; then custom_heap_size=2; fi
)

AC_ARG_ENABLE([cmdline],
 [AS_HELP_STRING([--disable-cmdline],[Disable code for the command line, useful for real-time systems])],
 enable_cmdline=no, enable_cmdline=yes
)

AC_ARG_ENABLE([filehandling],
 [AS_HELP_STRING([--disable-filehandling],[Disable code for file handling, useful if ODIN is used on a platform without file system])],
 enable_filehandling=no, enable_filehandling=yes
)

AC_ARG_ENABLE([processhandling],
 [AS_HELP_STRING([--disable-processhandling],[Disable code for process handling, useful if ODIN is used on a platform without useful process handling])],
 enable_processhandling=no, enable_processhandling=yes
)

AC_ARG_ENABLE([threads],
 [AS_HELP_STRING([--disable-threads],[Disable multithreading])],
 enable_threads=no, enable_threads=yes
)

AC_ARG_ENABLE([unit-test],
 [AS_HELP_STRING([--disable-unit-test],[Disable unit test])],
 enable_unittest=no, enable_unittest=yes
)

AC_ARG_ENABLE([gui],
 [AS_HELP_STRING([--disable-gui],[Disable graphical user interface])],
 enable_gui=no, enable_gui=yes
)

AC_ARG_ENABLE([only-corelibs],
 [AS_HELP_STRING([--enable-only-corelibs],[Compile only the essential libraries for sequence execution (tjutils/odinpara/odinseq)])],
 enable_onlylibs=yes
)


srcdir_abs=$(cd "$srcdir" && pwd)

have_ideaplugin=no
have_paravisionplugin=no
have_standaloneplugin=no
have_epicplugin=no

if test -d $srcdir_abs/platforms/IDEA_n4 ;    then have_ideaplugin=yes ; fi
if test -d $srcdir_abs/platforms/Paravision ; then have_paravisionplugin=yes ; fi
if test -d $srcdir_abs/platforms/StandAlone ; then have_standaloneplugin=yes ; fi
if test -d $srcdir_abs/platforms/EPIC ;       then have_epicplugin=yes ; fi

enable_ideaplugin=$have_ideaplugin
enable_paravisionplugin=$have_paravisionplugin
enable_standaloneplugin=$have_standaloneplugin
enable_epicplugin=$have_epicplugin


AC_ARG_ENABLE([only-epic-plugin],
 [AS_HELP_STRING([--enable-only-epic-plugin],[Compile only the plugin for Epic])],
 enable_ideaplugin=no
 enable_paravisionplugin=no
 enable_standaloneplugin=no
)

AC_ARG_ENABLE([only-idea-plugin],
 [AS_HELP_STRING([--enable-only-idea-plugin],[Compile only the plugin for IDEA(Siemens)])],
 enable_paravisionplugin=no
 enable_standaloneplugin=no
 enable_epicplugin=no
)

AC_ARG_ENABLE([only-paravision-plugin],
 [AS_HELP_STRING([--enable-only-paravision-plugin],[Compile only the plugin for Paravision(Bruker)])],
 enable_ideaplugin=no
 enable_standaloneplugin=no
 enable_epicplugin=no
)

AC_ARG_ENABLE([only-standalone-plugin],
 [AS_HELP_STRING([--enable-only-standalone-plugin],[Compile only the plugin for stand-alone mode])],
 enable_ideaplugin=no
 enable_paravisionplugin=no
 enable_epicplugin=no
)

dnl disable unavailable plugins
if test "x$have_ideaplugin" = "xno" ;       then enable_ideaplugin=no ; fi
if test "x$have_paravisionplugin" = "xno" ; then enable_paravisionplugin=no ; fi
if test "x$have_standaloneplugin" = "xno" ; then enable_standaloneplugin=no ; fi
if test "x$have_epicplugin" = "xno" ;       then enable_epicplugin=no ; fi




AC_ARG_ENABLE([ideasupport],
 [AS_HELP_STRING([--enable-ideasupport],[Compile against IDEA(Siemens) libraries, implies --enable-only-corelibs and --enable-stdcpp-replacement])],
 enable_ideasupport=yes
 enable_onlylibs=yes
 enable_stdcpp_replacement=yes
)



AC_ARG_ENABLE([niftisupport],
 [AS_HELP_STRING([--enable-niftisupport],[Use libniftiio to read/write NIFTI files])],
 enable_niftisupport="$enableval"
)

AC_ARG_ENABLE([vtksupport],
 [AS_HELP_STRING([--enable-vtksupport],[Use VTK for input/output and visualization])],
 enable_vtksupport="$enableval"
)

AC_ARG_ENABLE([dcmtksupport],
 [AS_HELP_STRING([--enable-dcmtksupport],[Use DICOMTK to read/write DICOM files])],
 enable_dcmtksupport="$enableval"
)

AC_ARG_ENABLE([pngsupport],
 [AS_HELP_STRING([--enable-pngsupport],[Use libpng to write PNG files])],
 enable_pngsupport="$enableval"
)

dnl AC_ARG_ENABLE([hdf5support],
dnl  [AS_HELP_STRING([--enable-hdf5support],[Use libhdf5 to read HDF files])],
dnl  enable_hdf5support="$enableval"
dnl )

AC_ARG_ENABLE([ismrmrdsupport],
 [AS_HELP_STRING([--enable-ismrmrdsupport],[Use libismrmrd to read ISMRMRD raw data files])],
 enable_ismrmrdsupport="$enableval"
)


AC_ARG_WITH([qt-dir],
  [AS_HELP_STRING([--with-qt-dir=DIR],[Base directory of the Qt library])],
  with_qtdir="$withval"
)

AC_ARG_WITH([extra-include-base],
  [AS_HELP_STRING([--with-extra-include-base=DIR],[Extra base to find includes (default=/usr/include)])],
  extra_include_base="$withval",
  extra_include_base=""
)

AC_ARG_WITH([extra-build-cxxflags],
  [AS_HELP_STRING([--with-extra-build-cxxflags=FLAGS],[Extra compiler flags during ODIN build (but not during sequence compilation)])],
  extra_cxxflags="$withval"
)

AC_ARG_WITH([extra-build-ldflags],
  [AS_HELP_STRING([--with-extra-build-ldflags=FLAGS],[Extra linker flags during ODIN build (but not during sequence compilation)])],
  extra_ldflags="$withval"
)

AC_ARG_WITH([extra-odinseq-include-path],
  [AS_HELP_STRING([--with-extra-odinseq-include-path=PATH],[Extra include path for odinseq during ODIN build (but not during sequence compilation)])],
  extra_odinseq_includes="$withval"
)

AC_ARG_WITH([lapack-libname],
  [AS_HELP_STRING([--with-lapack-libname=LIBNAME],[Library name of LAPACK (default=lapack)])],
  lapack_libname="$withval",
  lapack_libname="lapack"
)

AC_ARG_WITH([editor],
  [AS_HELP_STRING([--with-editor=EDITOR],[Initial settings for editor (default=/usr/bin/sensible-editor)])],
  initial_editor="$withval",
  initial_editor="/usr/bin/sensible-editor"
)

AC_ARG_WITH([browser],
  [AS_HELP_STRING([--with-browser=BROWSER],[Initial settings for browser (default=/usr/bin/sensible-browser)])],
  initial_browser="$withval",
  initial_browser="/usr/bin/sensible-browser"
)



dnl Cache user-supplied compiler flags, but ignore compiler flags of AC_PROG_CXX
CXXFLAGS_CACHE="$CXXFLAGS"

dnl Set C++ as the programming language and check for C++ compiler
AC_LANG(C++)
AC_PROG_CXX



dnl Add compiler flags in debug/release mode
if test "x$enable_debug" = "xyes" ; then
  dnl Disable debugging symbols and warnings when compiling for VxWorks
  if test "x$enable_ideasupport" = "xyes" ; then
    CXXFLAGS="-O2"
  else
    CXXFLAGS="-O2 -g -Wall"
  fi
  AC_MSG_CHECKING(whether compiler accepts debug flag(s) $CXXFLAGS)
  AC_COMPILE_IFELSE([AC_LANG_SOURCE([int main() {return 0;}])], AC_MSG_RESULT(yes); ADD_CXXFLAGS="$CXXFLAGS", AC_MSG_RESULT(no))
else
  dnl work around GCC bug
  CXXFLAGS="-O3 -fno-tree-vectorize"
  AC_MSG_CHECKING(whether compiler accepts release flag(s) $CXXFLAGS)
  AC_COMPILE_IFELSE([AC_LANG_SOURCE([int main() {return 0;}])], AC_MSG_RESULT(yes); ADD_CXXFLAGS="$CXXFLAGS", AC_MSG_RESULT(no))
fi
CXXFLAGS="$ADD_CXXFLAGS $CXXFLAGS_CACHE"




dnl Checks for debugger and xterm to attach debugger
AC_CHECK_PROGS(GDB, gdb)
AC_CHECK_PROGS(XTERM, xterm)


dnl libtool stuff
AC_ENABLE_SHARED(yes)
AC_ENABLE_STATIC(no)
LT_INIT



dnl cache CXXFLAGS/CPPFLAGS/LDFLAGS for sequence compilation before adding extra_ldflags to exclude absolut pathes and Qt libs on windows
CXXFLAGS_EXPORT="$CXXFLAGS"
CPPFLAGS_EXPORT="$CPPFLAGS"
LDFLAGS_EXPORT="$LDFLAGS"


dnl add compiler/linker flags for ODIN build
CXXFLAGS="$extra_cxxflags $CXXFLAGS"
CPPFLAGS="$extra_cxxflags $CPPFLAGS"
LDFLAGS="$extra_ldflags $LDFLAGS"


case $host in
*apple*)
  macos=yes
  ;;
esac


PIC_OPTS="$lt_cv_prog_compiler_pic"



dnl Checks for C/C++ standard headers
AC_CHECK_HEADERS(dirent.h unistd.h time.h)
AC_CHECK_HEADERS(sys/stat.h sys/types.h sys/wait.h sys/time.h sys/mman.h)
AC_CHECK_HEADERS(locale.h ctype.h)
AC_CHECK_HEADERS(signal.h setjmp.h)
AC_CHECK_HEADERS(typeinfo)


dnl Checks for Windows headers
have_win32_api=yes
AC_CHECK_HEADERS(windows.h direct.h io.h,,have_win32_api=no)



dnl Checks for libraries.

if test "x$enable_onlylibs" = "xyes" ; then
  AC_MSG_NOTICE([Skipping library linking due to --enable-only-corelibs])
else

  LIBS_RETAIN="$LIBS"

  if test "${extra_include_base}" != "" ; then
    all_includes="-I${extra_include_base} $all_includes"
    AC_MSG_NOTICE([Adding $extra_include_base as additional root directory for includes])
  fi

  dnl to iterate over to find includes in subdirs, e.g. for Debian/Ubuntu
  include_base_list="${extra_include_base} /usr/include"


  AC_CHECK_LIB(m,sin,LIBS="-lm $LIBS"; BASELIBS="-lm $BASELIBS")

  valid_dl=yes
  AC_CHECK_LIB(dl,dlopen,LIBS="-ldl $LIBS"; BASELIBS="-ldl $BASELIBS",valid_dl=no)
  AC_CHECK_HEADER(dlfcn.h,,valid_dl=no)

  if test "x$have_win32_api" = "xno" ; then
    valid_pthread=yes
    AC_CHECK_LIB(pthread,pthread_create,LIBS="-lpthread $LIBS"; BASELIBS="-lpthread $BASELIBS",valid_pthread=no)
    AC_CHECK_HEADER(pthread.h,,valid_pthread=no)
  fi

  valid_libz=yes
  AC_CHECK_LIB(z,main,LIBS="-lz $LIBS"; DATALIBS="-lz $DATALIBS", valid_libz=no)
  AC_CHECK_HEADER(zlib.h,,valid_libz=no)

  dnl GSL is used in tjutils and odindata so we add it to the global LIBS
  valid_gsl=yes
  AC_CHECK_LIB(gslcblas,main,LIBS="-lgslcblas $LIBS"; BASELIBS="-lgslcblas $BASELIBS")
  AC_CHECK_LIB(gsl,gsl_multifit_fdfsolver_iterate,LIBS="-lgsl $LIBS"; BASELIBS="-lgsl $BASELIBS",valid_gsl=no)
  CPPFLAGS_CACHE="$CPPFLAGS"
  CPPFLAGS="-DHAVE_CONFIG_H $all_includes $CPPFLAGS"
  AC_CHECK_HEADER(gsl/gsl_multifit_nlin.h,,valid_gsl=no)
  AC_CHECK_HEADER(gsl/gsl_multimin.h,,valid_gsl=no)
  CPPFLAGS="$CPPFLAGS_CACHE"
  if test "x$valid_gsl" = "xno" ; then
    AC_MSG_ERROR([Please install the GNU Scientific Library (There is probably a precompiled package for your UNIX/Linux distribution, otherwise see http://www.gnu.org/software/gsl)])
  fi
  AC_CHECK_DECL(gsl_multimin_fminimizer_nmsimplex2,have_gsl_multimin_fminimizer_nmsimplex2=yes,,[#include <gsl/gsl_multimin.h>])
  AC_CHECK_FUNCS(gsl_multifit_fdfsolver_jac)

  if test "x$macos" = "xyes" ; then
    AC_MSG_NOTICE([Using vecLib for lapack support])
    valid_lapack=yes
    lapack_libname="vecLib"
  else

    dnl Prerequisites for different lapack implementations
    if test "$lapack_libname" = "lapack" ; then
      AC_CHECK_LIB(g2c,i_indx,DATALIBS="-lg2c $DATALIBS"; LIBS="-lg2c $LIBS")
      dnl netlibs LAPACK claims to thread-safe since version 3.1.0
      thread_safe_lapack=yes
    fi
    if test "$lapack_libname" = "acml" ; then
      for acmlprereq in quadmath gfortran acml_mv ; do
        AC_CHECK_LIB(${acmlprereq},main,DATALIBS="-l${acmlprereq} $DATALIBS"; LIBS="-l${acmlprereq} $LIBS")
      done
      thread_safe_lapack=yes
    fi
    if test "$lapack_libname" = "flame" ; then
      for acmlprereq in blis blis-mt ; do
        AC_CHECK_LIB(${acmlprereq},main,DATALIBS="-l${acmlprereq} $DATALIBS"; LIBS="-l${acmlprereq} $LIBS")
      done
      thread_safe_lapack=yes
    fi

    valid_lapack=no
    AC_CHECK_LIB($lapack_libname,cgelss_,DATALIBS="-l${lapack_libname} $DATALIBS"; valid_lapack=yes)
    if test "x$valid_lapack" = "xno" ; then
      AC_MSG_WARN([LAPACK not found (using slower GSL linear algebra instead)])
    fi
  fi


  valid_blitz=yes
  AC_CHECK_LIB(blitz,main,DATALIBS="-lblitz $DATALIBS",valid_blitz=no)
  CPPFLAGS="-DHAVE_CONFIG_H $all_includes $CPPFLAGS"
  AC_CHECK_HEADER(blitz/array.h,,valid_blitz=no)
  AC_CHECK_HEADER(blitz/tinyvec-et.h,have_blitz_tinyvec_et=yes)
  CPPFLAGS="$CPPFLAGS_CACHE"
  if test "x$valid_blitz" = "xno" ; then
    AC_MSG_ERROR([Please install Blitz++ (Version 0.8 or higher) in order to use the reconstruction library (There is probably a precompiled package for your UNIX/Linux distribution, otherwise see http://blitz.sourceforge.net)])
  fi


  dnl optional libraries


  if test ! "x$enable_niftisupport" = "xno" ; then
    lib_nifti=yes
    AC_CHECK_LIB(znz,main,LIBS="-lznz $LIBS"; DATALIBS="-lznz $DATALIBS")
    AC_CHECK_LIB(niftiio,nifti_image_read,DATALIBS="-lniftiio $DATALIBS",lib_nifti=no)
    for basedir in $include_base_list ; do
      if test -d ${basedir}/nifti ; then
        AC_MSG_NOTICE([Adding existing ${basedir}/nifti to include path])
        all_includes="-I${basedir}/nifti $all_includes"
      fi
    done
    CPPFLAGS_CACHE="$CPPFLAGS"
    CPPFLAGS="-DHAVE_CONFIG_H $all_includes $CPPFLAGS"
    AC_CHECK_HEADER(nifti1_io.h,,lib_nifti=no)
    CPPFLAGS="$CPPFLAGS_CACHE"
    if test "x$lib_nifti" = "xno" ; then
      if test "x$enable_niftisupport" = "xyes" ; then
        AC_MSG_ERROR([libniftiio missing])
      else
        AC_MSG_WARN([NIFTI I/O library not found, NIFTI support will be disabled])
      fi
    fi
  fi

  if test ! "x$enable_vtksupport" = "xno" ; then
    lib_vtk=yes

    dnl Tests for vtk6/7 on Debian, list of modules obtained by script Utilities/Maintenance/WhatModulesVTK.py in VTK source

    dnl mandatory data libs
    for vtklib in vtkIOLegacy ; do
      dnl Continue only if previous lib was found
      if test "x$lib_vtk" = "xyes" ; then
        vtkver_match=no
        for vtkver in 7.1 7.0 6.3 6.2 6.1 6.0  ; do
          if test "x$vtkver_match" = "xno" ; then
            AC_CHECK_LIB(${vtklib}-${vtkver},main,DATALIBS="-l${vtklib}-${vtkver} $DATALIBS"; vtkver_match=yes; vtkver_selection=-${vtkver})
          fi
        done
        if test "x$vtkver_match" = "xno" ; then
          lib_vtk=no
        fi
      fi
    done

    
    dnl mandatory GUI libs
    for vtklib in vtkCommonColor vtkCommonComputationalGeometry vtkCommonCore vtkCommonDataModel vtkCommonExecutionModel vtkCommonMath vtkCommonMisc vtkCommonSystem vtkCommonTransforms vtkFiltersCore vtkFiltersGeneral vtkFiltersSources vtkInteractionStyle vtkRenderingCore ; do
      dnl Continue only if previous lib was found
      if test "x$lib_vtk" = "xyes" ; then
        vtkver_match=no
        AC_CHECK_LIB(${vtklib}${vtkver_selection},main,VTKLIBS="-l${vtklib}${vtkver_selection} $VTKLIBS"; vtkver_match=yes)
        if test "x$vtkver_match" = "xno" ; then
          lib_vtk=no
        fi
      fi
    done

    dnl optional GUI libs, depending on version of vtk
    if test "x$lib_vtk" = "xyes" ; then
      for vtklib in vtkRenderingOpenGL vtkRenderingOpenGL2 ; do
        AC_CHECK_LIB(${vtklib}${vtkver_selection},main,VTKLIBS="-l${vtklib}${vtkver_selection} $VTKLIBS"; VTKOPENGLMODULE=${vtklib})
      done
    fi

    dnl test for pre vtk6 libs
    if test "x$lib_vtk" = "xno" ; then
      lib_vtk=yes
      dnl vtk will also be used by Odin GUI so we add an extra variable VTKLIBS
      AC_CHECK_LIB(vtkIO,main,DATALIBS="-lvtkIO $DATALIBS",lib_vtk=no)
      AC_CHECK_LIB(vtkRendering,main,VTKLIBS="-lvtkRendering $VTKLIBS",lib_vtk=no)
      dnl additional required libs in vtk-5.8 and later
      AC_CHECK_LIB(vtkGraphics,main,VTKLIBS="-lvtkGraphics $VTKLIBS")
      AC_CHECK_LIB(vtkFiltering,main,VTKLIBS="-lvtkFiltering $VTKLIBS")
      AC_CHECK_LIB(vtkCommon,main,VTKLIBS="$VTKLIBS -lvtkCommon")
    fi



    for basedir in $include_base_list ; do
      for vtk_header_dir in vtk-5.8 vtk${vtkver_selection} vtk ; do
        if test -d ${basedir}/${vtk_header_dir} ; then
          AC_MSG_NOTICE([Adding existing ${basedir}/${vtk_header_dir} to include path])
          all_includes="-I${basedir}/${vtk_header_dir} $all_includes"
        fi
      done
    done

    CPPFLAGS_CACHE="$CPPFLAGS"
    CPPFLAGS="-DHAVE_CONFIG_H $all_includes $CPPFLAGS"
    AC_CHECK_HEADERS(vtkStructuredPoints.h vtkArrowSource.h,,lib_vtk=no)
    CPPFLAGS="$CPPFLAGS_CACHE"

    if test "x$lib_vtk" = "xno" ; then
      if test "x$enable_vtksupport" = "xyes" ; then
        AC_MSG_ERROR([VTK library missing])
      else
        AC_MSG_WARN([VTK library (Version 4.4 or higher) not found, VTK support will be disabled])
      fi
    fi
  fi


  if test ! "x$enable_dcmtksupport" = "xno" ; then
    lib_dcmtk=yes
    for basedir in $include_base_list ; do
      if test -d ${basedir}/dcmtk ; then
        AC_MSG_NOTICE([Adding existing ${basedir}/dcmtk to include path])
        all_includes="-I${basedir}/dcmtk -I${basedir}/dcmtk/dcmdata -I${basedir}/dcmtk/ofstd $all_includes"
      fi
    done
    CPPFLAGS_CACHE="$CPPFLAGS"
    CPPFLAGS="-DHAVE_CONFIG_H $all_includes $CPPFLAGS"
    AC_CHECK_HEADER(dcmtk/dcmdata/dcdatset.h,,lib_dcmtk=no)
    AC_CHECK_HEADER(dcmtk/dcmimgle/dcmimage.h,,lib_dcmtk=no)
    CPPFLAGS="$CPPFLAGS_CACHE"
    AC_CHECK_LIB(wsock32,main,LIBS="-lwsock32 $LIBS"; DATALIBS="-lwsock32 $DATALIBS")
    AC_CHECK_LIB(ofstd,main,LIBS="-lofstd $LIBS"; DATALIBS="-lofstd $DATALIBS",lib_dcmtk=no)
    AC_CHECK_LIB(oflog,main,LIBS="-loflog $LIBS"; DATALIBS="-loflog $DATALIBS")
    AC_CHECK_LIB(dcmdata,main,LIBS="-ldcmdata $LIBS"; DATALIBS="-ldcmdata $DATALIBS",lib_dcmtk=no)
    AC_CHECK_LIB(dcmimgle,main,LIBS="-ldcmimgle $LIBS"; DATALIBS="-ldcmimgle $DATALIBS",lib_dcmtk=no)
    if test "x$lib_dcmtk" = "xno" ; then
      if test "x$enable_dcmtksupport" = "xyes" ; then
        AC_MSG_ERROR([DICOMTK library missing])
      else
        AC_MSG_WARN([DICOMTK library not found, DICOM support will be disabled])
      fi
    fi
  fi


  if test ! "x$enable_pngsupport" = "xno" ; then
    lib_png=yes
    CPPFLAGS_CACHE="$CPPFLAGS"
    CPPFLAGS="-DHAVE_CONFIG_H $all_includes $CPPFLAGS"
    AC_CHECK_HEADER(png.h,,lib_png=no)
    CPPFLAGS="$CPPFLAGS_CACHE"
    if test "x$lib_png" = "xyes" ; then
      AC_CHECK_LIB(png, png_create_write_struct, DATALIBS="-lpng $DATALIBS",lib_png=no)
      if test "x$lib_png" = "xno" ; then
        if test "x$enable_pngsupport" = "xyes" ; then
          AC_MSG_ERROR([libpng missing])
        else
          AC_MSG_WARN([PNG library not found, PNG support will be disabled])
        fi
      fi
    fi
  fi



  if test ! "x$enable_ismrmrdsupport" = "xno" ; then
    lib_ismrmrd=yes
    for basedir in $include_base_list ; do
      if test -d ${basedir}/hdf5/serial ; then
        AC_MSG_NOTICE([Adding existing ${basedir}/hdf5/serial to include path])
        all_includes="-I${basedir}/hdf5/serial $all_includes"
      fi
    done
    CPPFLAGS_CACHE="$CPPFLAGS"
    CPPFLAGS="-DHAVE_CONFIG_H $all_includes $CPPFLAGS"
    AC_CHECK_HEADER(ismrmrd/dataset.h,,lib_ismrmrd=no)
    CPPFLAGS="$CPPFLAGS_CACHE"
    if test "x$lib_ismrmrd" = "xyes" ; then
      AC_CHECK_LIB(hdf5_serial, main, LIBS="-lhdf5_serial $LIBS"; DATALIBS="-lhdf5_serial $DATALIBS",lib_ismrmrd=no)
      AC_CHECK_LIB(ismrmrd, ismrmrd_read_header, DATALIBS="-lismrmrd $DATALIBS",lib_ismrmrd=no)
    fi
    if test "x$lib_ismrmrd" = "xno" ; then
      AC_MSG_WARN([ISMRMRD library not found, ISMRMRD support will be disabled])
    fi
  fi


  LIBS="$LIBS_RETAIN"
fi





dnl get size of integral types
AC_DEFUN([AC_COMPILE_CHECK_SIZEOF],
[changequote(<<, >>)dnl
dnl The name to #define.
define(<<AC_TYPE_NAME>>, translit(sizeof_$1, [a-z *], [A-Z_P]))dnl
dnl The cache variable name.
define(<<AC_CV_NAME>>, translit(ac_cv_sizeof_$1, [ *], [_p]))dnl
changequote([, ])dnl
AC_MSG_CHECKING(size of $1)
AC_CACHE_VAL(AC_CV_NAME,
[for ac_size in 4 8 1 2 16 $2 ; do # List sizes in rough order of prevalence.
  AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[#include "confdefs.h"
#include <sys/types.h>
$2
]], [[switch (0) case 0: case (sizeof ($1) == $ac_size):;]])],[AC_CV_NAME=$ac_size],[])
  if test x$AC_CV_NAME != x ; then break; fi
done
])
if test x$AC_CV_NAME = x ; then
  AC_MSG_ERROR([cannot determine a size for $1])
fi
AC_MSG_RESULT($AC_CV_NAME)
AC_DEFINE_UNQUOTED(AC_TYPE_NAME, $AC_CV_NAME, [The number of bytes in type $1])
undefine([AC_TYPE_NAME])dnl
undefine([AC_CV_NAME])dnl
])
AC_COMPILE_CHECK_SIZEOF(char)
AC_COMPILE_CHECK_SIZEOF(short)
AC_COMPILE_CHECK_SIZEOF(int)
AC_COMPILE_CHECK_SIZEOF(long)



AC_DEFUN([AC_C_LONG_LONG],
[AC_CACHE_CHECK(for long long int, ac_cv_c_long_long,
[if test "$GCC" = yes; then
  ac_cv_c_long_long=yes
  else
        AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[]], [[long long int i;]])],[ac_cv_c_long_long=yes],[ac_cv_c_long_long=no])
   fi])
   if test $ac_cv_c_long_long = yes; then
     AC_DEFINE(HAVE_LONG_LONG, 1, [compiler understands long long])
   fi
])
AC_C_LONG_LONG


dnl Checks for library functions.
AC_CHECK_FUNCS([j1 snprintf erfc acosh gettimeofday nanosleep srand read stat sysconf stat64 fopen64 open64 fseeko fseeko64 ftello ftello64 mmap64 strftime strptime])



dnl Add configured LIBS and all_includes to exported compiler/linker flags
CXXFLAGS_EXPORT="$CXXFLAGS_EXPORT $all_includes"
CPPFLAGS_EXPORT="$CPPFLAGS_EXPORT $all_includes"
LDFLAGS_EXPORT="$LDFLAGS_EXPORT $LIBS"



dnl Find location of GUI libraries, they will NOT get exported to sequence compilation

if test "x$enable_onlylibs" = "xyes" ; then
  AC_MSG_NOTICE([Skipping Qt/Qwt linking due to --enable-only-corelibs])
  enable_gui=no
fi

if test "x$enable_gui" = "xyes" ; then

  if test "$with_qtdir" = "" ; then
    CONF_QTDIR="$QTDIR"
  else
    CONF_QTDIR="$with_qtdir"
  fi


  AC_PATH_X

  if test  "$x_includes" = "" ; then
    x_includes="."
  fi

  if test "$x_libraries" = "" ; then
    x_libraries="."
  fi

  if ! test  "$x_includes" = "NONE" ; then
    all_includes="-I$x_includes $all_includes"
  fi

  dnl Add X11 libraries which might be necessary for Qt
dnl  if ! test  "$x_libraries" = "NONE" ; then
dnl    LDFLAGS="-L$x_libraries $LDFLAGS"
dnl    for xlib in X11 Xext ICE SM Xi Xrender Xfixes Xinerama Xrandr Xcursor Xft ; do
dnl      AC_CHECK_LIB($xlib,main,GUILIBS="-l${xlib} $GUILIBS")
dnl    done
dnl  fi

  dnl default for moc
  CONF_QTDIR_BIN=/usr/bin

  dnl Trying to detect QTDIR if not provided
  if test "$CONF_QTDIR" = "" ; then
    AC_MSG_NOTICE([Environment variable QTDIR not specified, searching for Qt:])
    qtincludes_found=no


    for qtver in 5 4 3 ; do
      for subdir in "" "QtCore/" ; do
        dnl hack for Debian multiarch (Qt5 and Debian 9/10)
        for triplet in "" "$($CXX -print-multiarch)" ; do
          if test "x$qtincludes_found" = "xno" ; then
            qtincdir=/usr/include/${triplet}/qt${qtver}
            AC_CHECK_FILE($qtincdir/${subdir}qglobal.h,  qtincludes_found=yes; CONF_QTDIR_INCLUDE=$qtincdir)
          fi
        done

        dnl without multiarch (Qt3 and Qt4, up to Debian 8)
        if test "x$qtincludes_found" = "xno" ; then
          qtdir=/usr/share/qt${qtver}
          AC_CHECK_FILE($qtdir/include/${subdir}qglobal.h,  qtincludes_found=yes; CONF_QTDIR=$qtdir; CONF_QTDIR_INCLUDE=$qtdir/include)
        fi

      done
    done

    if test "x$qtincludes_found" = "xno" ; then
      AC_MSG_ERROR([Qt headers not found]); exit -1
    fi

  else

    dnl simply set subdirs if QTDIR was specified
    CONF_QTDIR_BIN=$CONF_QTDIR/bin
    CONF_QTDIR_INCLUDE=$CONF_QTDIR/include

    dnl Add QTDIR to lib path
    for extra_qt_libdir in lib lib64 ; do
      if test -d $CONF_QTDIR/$extra_qt_libdir ; then
        LDFLAGS="-L$CONF_QTDIR/$extra_qt_libdir $LDFLAGS"
      fi
    done

  fi


  dnl Detect Qt version, start with Qt3
  dnl TODO: Use qtver of check above
  AC_CHECK_FILE($CONF_QTDIR_INCLUDE/qglobal.h, QTVERSION="3")
  AC_CHECK_FILE($CONF_QTDIR_INCLUDE/QtCore/qglobal.h, QTVERSION="4")
  AC_CHECK_FILE($CONF_QTDIR_INCLUDE/QtPrintSupport/qprinter.h, QTVERSION="5")
  if test "$QTVERSION" = "" ; then
    AC_MSG_ERROR([Cannot detect Qt version]); exit -1
  fi

  dnl Add QTDIR to include path
  all_includes="-I$CONF_QTDIR_INCLUDE $all_includes"


  dnl Qt5/GCC5 on Debian requires PIC options
  if test "$QTVERSION" -ge "4" ; then
    CXXFLAGS_CACHE="$CXXFLAGS"
    CPPFLAGS_CACHE="$CPPFLAGS"
    PIC_CXXFLAGS=""
    PIC_REQUIRED=no
    CXXFLAGS="$all_includes"
    CPPFLAGS="$all_includes"
    AC_CHECK_HEADERS($CONF_QTDIR_INCLUDE/QtCore/qglobal.h,,PIC_REQUIRED=yes)
    if test "$PIC_REQUIRED" = "yes" ; then
      AC_MSG_NOTICE([PIC option might be required for this GCC/Qt configuration:])
      CXXFLAGS="$PIC_OPTS"
      AC_MSG_CHECKING(whether compiler accepts PIC flag(s) $CXXFLAGS)
      AC_COMPILE_IFELSE([AC_LANG_SOURCE([int main() {return 0;}])], AC_MSG_RESULT(yes); PIC_CXXFLAGS="$PIC_OPTS", AC_MSG_RESULT(no))
    fi
    CXXFLAGS="$PIC_CXXFLAGS $CXXFLAGS_CACHE"
    CPPFLAGS="$PIC_CXXFLAGS $CPPFLAGS_CACHE"
  fi


  lib_qt=no

  if test "$QTVERSION" = "3" ; then
    if test "x$lib_qt" = "xno" ; then
      AC_CHECK_LIB(qt-mt,main,GUILIBS="-lqt-mt $GUILIBS"; lib_qt=yes; CXXFLAGS="-DQT_THREAD_SUPPORT $CXXFLAGS")
    fi
    if test "x$lib_qt" = "xno" ; then
      AC_CHECK_LIB(qt,main,GUILIBS="-lqt $GUILIBS"; lib_qt=yes)
    fi
  else
    AC_MSG_NOTICE([Searching for Qt4/5 subdirs:])
    AC_CHECK_FILE($CONF_QTDIR_INCLUDE/QtCore/QPointF,  all_includes="-I$CONF_QTDIR_INCLUDE/QtCore $all_includes")
    AC_CHECK_FILE($CONF_QTDIR_INCLUDE/QtGui/QPolygonF, all_includes="-I$CONF_QTDIR_INCLUDE/QtGui $all_includes")
    AC_CHECK_FILE($CONF_QTDIR_INCLUDE/QtWidgets/qwidget.h, all_includes="-I$CONF_QTDIR_INCLUDE/QtWidgets $all_includes")
    AC_CHECK_FILE($CONF_QTDIR_INCLUDE/QtPrintSupport/qprinter.h, all_includes="-I$CONF_QTDIR_INCLUDE/QtPrintSupport $all_includes")
    for qt_extension in "$QTVERSION" "" ; do
      post_extension=""
      pre_extension=""
      if test "x$qt_extension" = "x4" ; then
        post_extension="4"
      fi
      if test "x$qt_extension" = "x5" ; then
        pre_extension="5"
      fi
      if test "x$lib_qt" = "xno" ; then
        cont="yes"
        for module in Core Gui Widgets PrintSupport ; do
          if test "x$cont" = "xyes" ; then
            AC_CHECK_LIB(Qt${pre_extension}${module}${post_extension},main,GUILIBS="-lQt${pre_extension}${module}${post_extension} $GUILIBS"; lib_qt=yes; cont=yes, cont=no)
          fi
        done
      fi
    done
  fi

  if test "x$lib_qt" = "xno" ; then
    if test "$extra_ldflags" = ""; then
      AC_MSG_ERROR([Qt library not found]); exit -1
    else
      AC_MSG_NOTICE([Assuming that Qt library was specified by --extra-build-ldflags=$extra_ldflags])
    fi
  fi


  AC_CHECK_FILE($CONF_QTDIR_BIN/moc, QTMOC=$CONF_QTDIR_BIN/moc)
  if test "$QTMOC" = "" ; then
    AC_MSG_ERROR([Qt moc not found]); exit -1
  fi



  QWT_ERROR_STR="Qwt Widget Library not found. There is probably a precompiled package for your UNIX/Linux distribution, otherwise see http://qwt.sourceforge.net"

  qwt_lib=no

  if test "$QTVERSION" = "3" ; then
    AC_CHECK_LIB(qwt,main,GUILIBS="-lqwt $GUILIBS"; qwt_lib=yes)
  else
    for qwt_extension in "-qt${QTVERSION}" "" ; do     dnl e.g. -qt4 for Debian
      if test "x$qwt_lib" = "xno" ; then
        AC_CHECK_LIB(qwt${qwt_extension},main,GUILIBS="-lqwt${qwt_extension} $GUILIBS"; qwt_lib=yes)
      fi
    done
  fi

  if test "x$qwt_lib" = "xno" ; then
    AC_MSG_ERROR($QWT_ERROR_STR); exit -1
  fi


  dnl Check for Qwt headers
  for qwt_extension in "-qt${QTVERSION}" "" ; do     dnl e.g. -qt4 for Debian
    for basedir in $include_base_list ; do
      if test -d ${basedir}/qwt${qwt_extension} ; then
        AC_MSG_NOTICE([Adding existing ${basedir}/qwt${qwt_extension} to include path])
        all_includes="-I${basedir}/qwt${qwt_extension} $all_includes"
      fi
    done
  done
  CPPFLAGS_CACHE="$CPPFLAGS"
  CPPFLAGS="-DHAVE_CONFIG_H $all_includes $CPPFLAGS"
  qwt_headers=no
  AC_CHECK_HEADERS(qwt_global.h,qwt_headers=yes)
  CPPFLAGS="$CPPFLAGS_CACHE"
  if test "x$qwt_headers" = "xno" ; then
    AC_MSG_ERROR($QWT_ERROR_STR); exit -1
  fi

fi



dnl Checks for typedefs, structures, and compiler characteristics.

AC_MSG_CHECKING(whether compiler accepts empty template list)
AC_COMPILE_IFELSE([AC_LANG_SOURCE([template<class T> class TC{static int s;}; template<> int TC<int>::s; int main() {return 0;}])],
                  AC_MSG_RESULT(yes); empty_templ_list=yes, AC_MSG_RESULT(no); empty_templ_list=no)

AC_MSG_CHECKING(whether compiler accepts/needs self-friend classes)
AC_COMPILE_IFELSE([AC_LANG_SOURCE([class C{friend class C;}; int main() {return 0;}])],
                  AC_MSG_RESULT(yes); self_friend_class=yes, AC_MSG_RESULT(no); self_friend_class=no)


dnl Create Defines

CONFIGSTR=""

AC_MSG_NOTICE([------------ ODIN Configuration ------------])

AC_MSG_NOTICE([host=$host])

if test "x$macos" = "xyes" ; then
  AC_DEFINE(MACOS,,"MacOS support")
  CONFIGSTR="$CONFIGSTR MacOS support"
fi

if test "x$GXX" = "xyes" ; then
  AC_MSG_NOTICE([Using the GNU compiler collection])
  AC_DEFINE(USING_GCC,,"Using the GNU compiler collection")
  CONFIGSTR="$CONFIGSTR using_gcc"
fi


if test "x$enable_debug" = "xyes" ; then
  AC_MSG_NOTICE([Debug compilation])
  AC_DEFINE(ODIN_DEBUG,,"Debug compilation")
  CONFIGSTR="$CONFIGSTR debug"
fi

if test "x$enable_stdcpp_replacement" = "xyes" ; then
  AC_MSG_NOTICE([Using built-in C++ standard library])
  AC_DEFINE(STL_REPLACEMENT,,"Using STL replacement")
  AC_DEFINE(STRING_REPLACEMENT,,"Using string replacement")
  AC_DEFINE(STREAM_REPLACEMENT,,"Using stream replacement")
  CONFIGSTR="$CONFIGSTR string_replacement stream_replacement"
fi

if test "x$enable_blitz_replacement" = "xyes" ; then
  AC_MSG_NOTICE([Using built-in Blitz++ library])
  AC_DEFINE(BLITZ_REPLACEMENT,,"Using Blitz++ replacement")
  CONFIGSTR="$CONFIGSTR blitz_replacement"
fi

if test "x$enable_custom_heap" = "xyes" ; then
  AC_MSG_NOTICE([Using custom heap of size $custom_heap_size MB])
  AC_DEFINE(CUSTOM_HEAP,,"Using custom heap")
  AC_DEFINE_UNQUOTED(CUSTOM_HEAP_SIZE,$custom_heap_size,[Size of Custom Heap in MB])
  CONFIGSTR="$CONFIGSTR custom_heap(${CUSTOM_HEAP_SIZE}MB)"
fi

if test "x$enable_cmdline" = "xno" ; then
  AC_MSG_NOTICE([Not using command line])
  AC_DEFINE(NO_CMDLINE,,"Not using command line")
  CONFIGSTR="$CONFIGSTR no_cmdline"
fi

if test "x$enable_filehandling" = "xno" ; then
  AC_MSG_NOTICE([No file handling])
  AC_DEFINE(NO_FILEHANDLING,,"No file handling")
  CONFIGSTR="$CONFIGSTR no_filehandling"
fi

if test "x$enable_processhandling" = "xno" ; then
  AC_MSG_NOTICE([No process handling])
  AC_DEFINE(NO_PROCESSHANDLING,,"No process handling")
  CONFIGSTR="$CONFIGSTR no_processhandling"
fi

if test "x$enable_ideasupport" = "xyes" ; then
  AC_MSG_NOTICE([IDEA support])
  AC_DEFINE(ODIN4IDEA,,"IDEA support")
  CONFIGSTR="$CONFIGSTR odin4idea"
fi

if test "x$have_win32_api" = "xyes" ; then
  AC_MSG_NOTICE([Using Win32-API])
  AC_DEFINE(USING_WIN32,,"Using Win32-API")
  CONFIGSTR="$CONFIGSTR win32_api"
fi

if test "x$enable_onlylibs" = "xyes" ; then
  AC_MSG_NOTICE([Only essential libraries])
  CONFIGSTR="$CONFIGSTR onlylibs"
fi
AM_CONDITIONAL(ONLY_LIBS, test "x$enable_onlylibs" = "xyes")

if test "x$enable_gui" = "xyes" ; then
  AC_DEFINE(GUISUPPORT,,"Graphical User Interface enabled")
  AC_MSG_NOTICE([Graphical User Interface enabled])
  AC_MSG_NOTICE([QTDIR:QTVERSION=${CONF_QTDIR}:${QTVERSION}])
  CONFIGSTR="$CONFIGSTR gui(${CONF_QTDIR}:${QTVERSION})"
fi
AM_CONDITIONAL(GUI_ENABLED, test "x$enable_gui" = "xyes")


if test "x$enable_ideaplugin" = "xyes" ; then
  AC_MSG_NOTICE([Compiling IDEA drivers])
  PLUGIN_LIBS="$PLUGIN_LIBS odinseq_idea"
  AC_DEFINE(IDEA_PLUGIN,,"Compile IDEA drivers")
  PLATFORMS="$PLATFORMS IDEA_n4"
  CONFIGSTR="$CONFIGSTR idea"
fi
AM_CONDITIONAL(IDEA_PLUGIN, test "x$enable_ideaplugin" = "xyes")

if test "x$enable_paravisionplugin" = "xyes" ; then
  AC_MSG_NOTICE([Compiling Paravision drivers])
  PLUGIN_LIBS="$PLUGIN_LIBS odinseq_paravision"
  AC_DEFINE(PARAVISION_PLUGIN,,"Compile Paravision drivers")
  PLATFORMS="$PLATFORMS Paravision"
  CONFIGSTR="$CONFIGSTR paravision"
fi
AM_CONDITIONAL(PARAVISION_PLUGIN, test "x$enable_paravisionplugin" = "xyes")

if test "x$enable_standaloneplugin" = "xyes" ; then
  AC_MSG_NOTICE([Compiling Standalone drivers])
  PLUGIN_LIBS="$PLUGIN_LIBS odinseq_standalone"
  AC_DEFINE(STANDALONE_PLUGIN,,"Compile Standalone drivers")
  PLATFORMS="$PLATFORMS StandAlone"
  CONFIGSTR="$CONFIGSTR standalone"
fi
AM_CONDITIONAL(STANDALONE_PLUGIN, test "x$enable_standaloneplugin" = "xyes")

if test "x$enable_epicplugin" = "xyes" ; then
  AC_MSG_NOTICE([Compiling EPIC drivers])
  PLUGIN_LIBS="$PLUGIN_LIBS odinseq_epic"
  AC_DEFINE(EPIC_PLUGIN,,"Compile EPIC drivers")
  PLATFORMS="$PLATFORMS EPIC"
  CONFIGSTR="$CONFIGSTR epic"
fi
AM_CONDITIONAL(EPIC_PLUGIN, test "x$enable_epicplugin" = "xyes")


if test "x$enable_unittest" = "xno" ; then
  AC_MSG_NOTICE([Unit test disabled])
  AC_DEFINE(NO_UNIT_TEST,,"Unit test disabled")
  CONFIGSTR="$CONFIGSTR no_unittest"
fi

if test "x$enable_threads" = "xno" ; then
  AC_MSG_NOTICE([Multithreading disabled])
  AC_DEFINE(NO_THREADS,,"Multithreading disabled")
  CONFIGSTR="$CONFIGSTR multithreading"
fi

if test "x$valid_dl" = "xyes" ; then
  AC_MSG_NOTICE([Using dynamic linking loader])
  AC_DEFINE(HAVE_DL,,"Using dynamic linking loader")
  CONFIGSTR="$CONFIGSTR dl"
fi

if test "x$valid_pthread" = "xyes" ; then
  AC_MSG_NOTICE([Using POSIX threads])
  AC_DEFINE(HAVE_PTHREAD,,"Using POSIX threads")
  CONFIGSTR="$CONFIGSTR posix_threads"
fi

if test "x$valid_lapack" = "xyes" ; then
  if test "x$thread_safe_lapack" = "xyes" ; then
    AC_DEFINE(HAVE_THREADSAFE_LAPACK,,"Thread-safe LAPACK")
    tsmessage=" thread-safe"
  fi
  AC_MSG_NOTICE([Using${tsmessage} LAPACK($lapack_libname)])
  AC_DEFINE(HAVE_LAPACK,,"Using LAPACK")
  AC_DEFINE_UNQUOTED(LAPACK_LIBNAME,"$lapack_libname","LAPACK name")
  CONFIGSTR="$CONFIGSTR LAPACK($lapack_libname${tsmessage})"
fi

if test "x$have_blitz_tinyvec_et" = "xyes" ; then
  AC_DEFINE(HAVE_BLITZ_TINYVEC_ET,,"existence of blitz/tinyvec-et.h")
fi

if test "x$valid_libz" = "xyes" ; then
  AC_DEFINE(HAVE_LIBZ,,"libz support")
  CONFIGSTR="$CONFIGSTR libz"
fi

if test "x$valid_gsl" = "xyes" ; then
  AC_DEFINE(HAVE_LIBGSL,,"GSL support")
  CONFIGSTR="$CONFIGSTR gsl"
fi

if test "x$have_gsl_multimin_fminimizer_nmsimplex2" = "xyes" ; then
  AC_DEFINE(HAVE_GSL_MULTIMIN_FMINIMIZER_NMSIMPLEX2,,"existence of newer gsl_multimin_fminimizer_nmsimplex2 algorithm")
fi

if test "x$lib_vista" = "xyes" ; then
  AC_MSG_NOTICE([Vista support])
  AC_DEFINE(VISTASUPPORT,,"Vista support")
  CONFIGSTR="$CONFIGSTR vista"
fi

if test "x$lib_nifti" = "xyes" ; then
  AC_MSG_NOTICE([NIFTI support])
  AC_DEFINE(NIFTISUPPORT,,"NIFTI support")
  CONFIGSTR="$CONFIGSTR nifti"
fi

if test "x$lib_vtk" = "xyes" ; then
  AC_MSG_NOTICE([VTK support])
  AC_DEFINE(VTKSUPPORT,,"VTK support")
  if test "x$VTKOPENGLMODULE" != "x" ; then
    AC_DEFINE_UNQUOTED(VTKOPENGLMODULE,$VTKOPENGLMODULE,[OpenGL module of VTK])
  fi
  CONFIGSTR="$CONFIGSTR vtk"
fi

if test "x$lib_dcmtk" = "xyes" ; then
  AC_MSG_NOTICE([DICOM support])
  AC_DEFINE(DICOMSUPPORT,,"DICOM support")
  CONFIGSTR="$CONFIGSTR dicom"
fi

if test "x$lib_png" = "xyes" ; then
  AC_MSG_NOTICE([PNG support])
  AC_DEFINE(PNGSUPPORT,,"PNG support")
  CONFIGSTR="$CONFIGSTR png"
fi

dnl if test "x$lib_hdf5" = "xyes" ; then
dnl   AC_MSG_NOTICE([HDF support])
dnl   AC_DEFINE(HDFSUPPORT,,"HDF support")
dnl fi

if test "x$lib_ismrmrd" = "xyes" ; then
  AC_MSG_NOTICE([ISMRMRD support])
  AC_DEFINE(ISMRMRDSUPPORT,,"ISMRMRD support")
  CONFIGSTR="$CONFIGSTR ismrmrd"
fi


if test "x$empty_templ_list" = "xyes" ; then
  AC_DEFINE(EMPTY_TEMPL_LIST,template<>,"empty template list")
else
  AC_DEFINE(EMPTY_TEMPL_LIST,,define for empty template list)
fi

if test "x$self_friend_class" = "xyes" ; then
  AC_DEFINE(SELF_FRIEND_CLASS,,"self-friend class possible/required")
fi



if test ! -z "$GDB" -a ! -z "$XTERM" ; then
  AC_MSG_NOTICE([Using gdb/xterm to attach debugger])
  AC_DEFINE(HAVE_GDB_XTERM,,"Using gdb/xterm")
  CONFIGSTR="$CONFIGSTR gdb"
fi


AC_MSG_NOTICE([INSTALL_PREFIX=$prefix])


AC_MSG_NOTICE([---------------------------------------------------])

AC_DEFINE_UNQUOTED(ODIN_CONFIGSTR,"$CONFIGSTR","ODIN Configuration")

AC_SUBST(PLATFORMS,$PLATFORMS)

dnl Common generation command for manpages
AC_SUBST(HELP2MAN,"help2man -N")

AC_SUBST(MOC,$QTMOC)
AC_SUBST(BASELIBS)
AC_SUBST(GUILIBS)
AC_SUBST(DATALIBS)
AC_SUBST(VTKLIBS)

AC_SUBST(ODINSEQ_INCLUDES,$extra_odinseq_includes)


dnl Make sure top-level source and build dir are in the include path
all_includes="-I.. -I$srcdir_abs $all_includes"
AC_SUBST(all_includes)

AC_DEFINE_UNQUOTED(BASELIBS,"$BASELIBS",[Third-party libraries used by all ODIN components])


AC_DEFINE_UNQUOTED(CXXFLAGS,"$CXXFLAGS_EXPORT",[Compiler flags])
AC_DEFINE_UNQUOTED(CPPFLAGS,"$CPPFLAGS_EXPORT",[Preprocessor flags])
AC_DEFINE_UNQUOTED(LDFLAGS,"$LDFLAGS_EXPORT",[Linker flags])

AC_DEFINE_UNQUOTED(PIC_FLAGS,"$PIC_OPTS",[PIC Flags])

AC_DEFINE_UNQUOTED(CXX,"$CXX",[Compiler])
AC_DEFINE_UNQUOTED(LD,"$LD",[Linker])
AC_DEFINE_UNQUOTED(PLUGIN_LIBS,"$PLUGIN_LIBS",[Plugin-Libraries])

AC_DEFINE_UNQUOTED(DEFAULT_EDITOR,"$initial_editor",[Initial default editor])
AC_DEFINE_UNQUOTED(DEFAULT_BROWSER,"$initial_browser",[Initial default browser])


AC_DEFINE_UNQUOTED(INSTALL_PREFIX,"$prefix",[Installation prefix])


if test ! "$libdir" = '${exec_prefix}/lib' ; then
  libdir_export="$libdir"
fi
AC_DEFINE_UNQUOTED(LIBDIR_CONFIGURE,"$libdir_export",[libdir of configure])



dnl Output

AC_CONFIG_FILES([Makefile tjutils/Makefile odinpara/Makefile odinseq/Makefile odinqt/Makefile odindata/Makefile \
odin/Makefile pulsar/Makefile geoedit/Makefile miview/Makefile \
odinreco/Makefile cmdline-utils/Makefile sequences/Makefile samples/Makefile coils/Makefile \
docs/Makefile docs/homepage/Makefile docs/tutorials/Makefile icons/Makefile replacements/Makefile])
AC_OUTPUT
