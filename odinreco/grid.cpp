#include "grid.h"
#include "grid_code.h"

// Instantiations used in step.cpp
template class RecoGrid<1>;
template class RecoGrid<2>;
template class RecoDeapodize<1>;
template class RecoDeapodize<2>;

