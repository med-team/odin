#include "sum.h"
#include "data.h"


template<unsigned int Nunmod, class Unmod, unsigned int Nsum, class Sum, bool Magn>
bool RecoSum<Nunmod,Unmod,Nsum,Sum,Magn>::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  // create new view of input data
  ComplexData<Nsum+Nunmod>& indata=rd.data(Rank<Nsum+Nunmod>());

  TinyVector<int,Nsum+Nunmod> inshape(indata.shape());

  TinyVector<int,Nsum> sumshape;
  for(int i=0; i<int(Nsum); i++) sumshape(i)=inshape(i);

  TinyVector<int,Nunmod> outshape;
  for(int i=0; i<int(Nunmod); i++) outshape(i)=inshape(Nsum+i);

  ODINLOG(odinlog,normalDebug) << "indata.shape()/outshape/sumshape=" << indata.shape() << "/" << outshape << "/" << sumshape << STD_endl;

  // summed output data with same (padded) shape as input data
  TinyVector<int,Nsum+Nunmod> outshape_padded=1;
  for(unsigned int i=0; i<Nunmod; i++) outshape_padded(Nsum+i)=outshape(i);


  // create domains for retrieving input data from the correct position
  // initialize unmodified part with outshape
  TinyVector<int,Nsum+Nunmod> lowin=0;
  TinyVector<int,Nsum+Nunmod> uppin=outshape_padded-1;

  TinyVector<int,Nsum+Nunmod> lowout=0;
  TinyVector<int,Nsum+Nunmod> uppout=outshape_padded-1;

   // output domain, the same for each dataset
  RectDomain<Nsum+Nunmod> outdomain(lowout,uppout);

  ComplexData<Nsum+Nunmod> outdata_padded(outshape_padded); outdata_padded=STD_complex(0.0);

  for(int isum=0; isum<product(sumshape); isum++) {

    // create Nsum-dim index from linear index
    TinyVector<int,Nsum> sumindex=index2extent(sumshape, isum);

    // set input inidices
    for(unsigned int i=0; i<Nsum; i++) lowin(i)=uppin(i)=sumindex(i);

    // sum data
    RectDomain<Nsum+Nunmod> indomain(lowin,uppin);

    if(Magn) outdata_padded(outdomain)+=float2real(cabs(indata(indomain)));
    else     outdata_padded(outdomain)+=indata(indomain);
  }

  ComplexData<Nunmod> outdata(outshape);
  outdata_padded.convert_to(outdata); // bring to output shape
  rd.data(Rank<Nunmod>()).reference(outdata);

  ODINLOG(odinlog,normalDebug) << " outshape=" << outshape << STD_endl;

  modify_coord(rd.coord());

  if(Magn) rd.mode=RecoData::real_data;

  if(!execute_next_step(rd,controller)) return false;

  return true;
}
