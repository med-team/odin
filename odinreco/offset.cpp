#include "offset.h"
#include "controller.h"
#include "data.h"


bool RecoOffset::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  ComplexData<3>& data=rd.data(Rank<3>());

  float os_factor=rd.coord().overSamplingFactor; // should be set to 1.0 by prior functors if oversampling has already been removed
  float grid_factor=rd.coord().gridScaleFactor;

  // spatial shifting
  TinyVector<float,3> reloffset=controller.reloffset();
  reloffset(2)/=os_factor * grid_factor;
  reloffset(1)/=grid_factor;
  bool do_shift=false;
  if(max(abs(reloffset))>0.0) do_shift=true;
  if(do_shift) {
//    data.fft(false); // backward FFT
    ODINLOG(odinlog,normalDebug) << "shifting with reloffset=" << reloffset << STD_endl;
    data.modulate_offset(reloffset);
//    data.fft(true); // forward FFT for correct sign of modulated offset
  }

  return execute_next_step(rd,controller);
}
