#include "grappa.h"
#include "grappa_code.h"

// Instantiations used in step.cpp
template class RecoGrappaWeights<line,line3d,1,0>;
template class RecoGrappaWeights<line,line3d,1,0, RecoDim<2, repetition, freq> >;
template class RecoGrappaWeights<line3d,line,0,1>;
template class RecoGrappaWeights<line3d,line,0,1, RecoDim<2, repetition, freq> >;
template class RecoGrappa<line,line3d,1,0>;
template class RecoGrappa<line3d,line,0,1>;
