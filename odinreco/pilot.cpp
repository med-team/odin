#include "pilot.h"
#include "data.h"
#include "controller.h"

///////////////////////////////////////////////////////////////////////////

void  RecoPilot::init() {
  slicedist=0.0;
  slicedist.set_description("Inter-slice distance");
  append_arg(slicedist,"slicedist");

}


///////////////////////////////////////////////////////////////////////////

void RecoPilot::transpose_inplane(ComplexData<5>& data, Geometry& geo, bool reverse_read, bool reverse_phase) {
  geo.transpose_inplane(reverse_read,reverse_phase);

  if(reverse_read) data.reverseSelf(4);
  if(reverse_phase) data.reverseSelf(3);
  data.transposeSelf(0,1,2,4,3);

  data.reference(data.copy());
}

///////////////////////////////////////////////////////////////////////////

bool RecoPilot::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<4>& indata=rd.data(Rank<4>());
  TinyVector<int,4> inshape=indata.shape();
  TinyVector<int,5> outshape(1, 1, inshape(1), inshape(2), inshape(3)); // slice size will be adjusted later
  ODINLOG(odinlog,normalDebug) << "inshape/outshape=" << inshape << "/" << outshape << STD_endl;

  dvector orientations=controller.dim_values(userdef);
  int nimages=orientations.size();
  if(nimages!=inshape(0)) {
    ODINLOG(odinlog,errorLog) << "size mismatch" << STD_endl;
    return false;
  }

  STD_map<sliceOrientation,UInt> slicecount;
  for(int i=0; i<nimages; i++) {
    slicecount[sliceOrientation(orientations[i])]++;
  }


  RecoData data[n_orientations]; // local copies for different orientations
  Protocol prot[n_orientations];

  for(unsigned int ior=0; ior<n_orientations; ior++) {

    // Resize data
    ComplexData<5>& dataref=data[ior].data(Rank<5>());
    outshape(1)=slicecount[sliceOrientation(ior)];
    dataref.resize(outshape);

    // Modify protocol
    prot[ior]=controller.protocol();
    Geometry& geo=prot[ior].geometry;
    geo.set_orientation(sliceOrientation(ior));
    geo.set_nSlices(outshape(1));
    geo.set_sliceDistance(slicedist);
    data[ior].override_protocol=&(prot[ior]);

    // modify coordinate
    data[ior].coord().index[userdef].set_numof(n_orientations);
    data[ior].coord().index[userdef]=ior;
  }


  // fill data
  UInt index[n_orientations];
  for(int i=0; i<nimages; i++) {
    sliceOrientation ior=sliceOrientation(orientations[i]);
    ComplexData<5>& outdata=data[ior].data(Rank<5>());
    outdata(0,int(index[ior]),all,all,all)=indata(i,all,all,all);
    index[ior]++;
  }


  STD_string series;
  int number;
  controller.protocol().study.get_Series(series, number);

  // transpose and feed rest of pipeline
  for(unsigned int ior=0; ior<n_orientations; ior++) {
    STD_string seriesname;
    if(ior==sagittal) {seriesname=series+"_Sagittal"; transpose_inplane(data[ior].data(Rank<5>()), prot[ior].geometry, true,false);}
    if(ior==coronal) {seriesname=series+"_Coronal"; transpose_inplane(data[ior].data(Rank<5>()), prot[ior].geometry, true,true);}
    if(ior==axial)   {seriesname=series+"_Axial";}

    prot[ior].study.set_Series(seriesname,number);

    if(!execute_next_step(data[ior],controller)) return false;
  }

  return true;
}
