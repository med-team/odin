#include "circmask.h"
#include "data.h"

bool RecoCircMask::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  ComplexData<3>& data=rd.data(Rank<3>());
  TinyVector<int,3> shape=data.shape();

  if(shape(1)!=shape(2)) {
    ODINLOG(odinlog,errorLog) << "non-quadratic image shape=" << shape << STD_endl;
    return false;
  }

  float radius=0.5*float(shape(1)-1);

  for(int iphase3d=0; iphase3d<shape(0); iphase3d++) {
    for(int iphase=0; iphase<shape(1); iphase++) {
      for(int iread=0; iread<shape(2); iread++) {
        if(norm(iphase-radius,iread-radius)>radius) data(iphase3d,iphase,iread)=STD_complex(0.0);
      }
    }
  }

  return execute_next_step(rd,controller);
}
