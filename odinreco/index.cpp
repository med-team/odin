#include "index.h"


STD_string RecoIndex::print(recoDim dim, printMode printmode) const {

  bool do_print=false;
  if(printmode==brief && (numof>1 || mode==single)) do_print=true;
  if(printmode==all) do_print=true;
  if(printmode==filename &&  numof>1 && mode==separate) do_print=true;

  if(do_print) {
    STD_string result=kSpaceCoord::index2string(val, dim, numof);
    switch(mode) {
      case ignore:    result+="i"; break;
      case single:    result+="s"; break;
      case collected: result="all"; break;
      case any:       result="any"; break;
    }
    return result;
  }
  return "";
}



STD_string RecoCoord::print(RecoIndex::printMode printmode) const {
  STD_string result;

  const char* separator=",";
  const char* equals="=";
  if(printmode==RecoIndex::filename) {
    separator="_";
    equals="";
  }

  bool first=true;
  for(int i=0; i<n_recoDims; i++) {
    STD_string istr=index[i].print(recoDim(i),printmode);
    if(istr!="") {
      if(!first) result+=separator;
      result+=STD_string(recoDimLabel[i])+equals+istr;
      first=false;
    }
  }
  return result;
}
