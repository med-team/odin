#include "fft.h"
#include "data.h"

void RecoFFT::init() {

  forward=true;
  forward.set_description("Forward (true) or backward (false) FFT");
  append_arg(forward,"forward");
}

bool RecoFFT::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");
  rd.data(Rank<3>()).fft(forward);
  return execute_next_step(rd,controller);
}
