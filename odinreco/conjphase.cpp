#include "conjphase.h"
#include "data.h"
#include "controller.h"


bool RecoConjPhaseFT::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  ComplexData<1>& indata=rd.data(Rank<1>());
  ComplexData<3>& outdata=rd.data(Rank<3>());

  int nsample=indata.size();
  int ncycle=rd.coord().numof(cycle);
  ODINLOG(odinlog,normalDebug) << "nsample/ncycle=" << nsample << "/" << ncycle << STD_endl;

  TinyVector<int,3> outsize=controller.image_size();
  outdata.resize(outsize);
  outdata=STD_complex(0.0);
  ODINLOG(odinlog,normalDebug) << "outsize=" << outsize << STD_endl;

  // local fast cache of trajectories and weight
  TinyVector<float,3> ktraj[nsample];
  float kweight[nsample];
  trajmutex.lock();
  const KspaceTraj* traj=rd.coord().kspaceTraj; // access is protected by mutex
  if(!traj) {
    ODINLOG(odinlog,errorLog) << "Trajectory missing" << STD_endl;
    trajmutex.unlock();
    return false;
  }
  int nseg=traj->extent(0);
  int npts=traj->extent(1);
  if(nseg!=ncycle) {
    ODINLOG(odinlog,errorLog) << "Size mismatch: nseg(" << nseg << ")!=ncycle(" << ncycle << ")" << STD_endl;
    trajmutex.unlock();
    return false;
  }
  if(npts!=nsample) {
    ODINLOG(odinlog,errorLog) << "Size mismatch: npts(" << npts << ")!=nsample(" << nsample << ")" << STD_endl;
    trajmutex.unlock();
    return false;
  }

  int icycle=rd.coord().index[cycle];
  for(int isample=0; isample<nsample; isample++) {
    const GriddingPoint<3>& gp=(*traj)(icycle,isample);
    ktraj[isample]=gp.coord;
    kweight[isample]=gp.weight;
  }
  trajmutex.unlock();

  // local fast cache of indata
  STD_complex signal[nsample];
  for(int isample=0; isample<nsample; isample++) signal[isample]=STD_complex(kweight[isample])*indata(isample);


  // get fieldmap
  Data<float,3> fieldmap;
  bool has_fieldmap=false;
  if(controller.data_announced(posted_fmap_str)) {
    RecoCoord fmapcoord=rd.coord();
    RecoFieldMapUser::modify4fieldmap(fmapcoord);
    if(!get_fmap(fieldmap, outsize, fmapcoord, controller)) return false;
    has_fieldmap=true;
  }

  double dt=rd.coord().dwellTime;
  ODINLOG(odinlog,normalDebug) << "dt=" << dt << STD_endl;

  Protocol prot(controller.protocol());

  // get FOV
  TinyVector<float,3> fov;
  fov(0)=0.0; //prot.geometry.get_FOV(sliceDirection); only 2D FT yet
  fov(1)=prot.geometry.get_FOV(phaseDirection);
  fov(2)=prot.geometry.get_FOV(readDirection);
  ODINLOG(odinlog,normalDebug) << "fov=" << fov << STD_endl;

  TinyVector<float,3> extent;
  extent=outsize;
  ODINLOG(odinlog,normalDebug) << "extent=" << extent << STD_endl;

  TinyVector<float,3> reloffset=controller.reloffset();

  for(unsigned int i=0; i<outdata.size(); i++) {
    TinyVector<int,3> index=outdata.create_index(i);

    bool reconstruct_voxel=true;
    float deltaOmega_dt=0.0;
    if(has_fieldmap) {
      deltaOmega_dt=dt*fieldmap(index);
      if(!deltaOmega_dt) reconstruct_voxel=false;
    }

    TinyVector<float,3> spatcoord= fov * (index/extent - 0.5 + reloffset);

    STD_complex voxel(0.0);
    if(reconstruct_voxel) {
      for(int isample=0; isample<nsample; isample++) {

        float phase = sum(ktraj[isample]*spatcoord);

        // Conjugate phase reco
        if(has_fieldmap) {
          phase += float(isample) * deltaOmega_dt;
        }

        voxel += signal[isample] * exp(STD_complex(0.0, -phase));
      }
    }
    outdata(index)=voxel;
  }

  return execute_next_step(rd,controller);
}
