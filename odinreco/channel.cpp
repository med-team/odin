#include "channel.h"
#include "data.h"

void RecoChannelSum::init() {
  Log<Reco> odinlog(c_label(),"init");

  phasecomb=false;
  phasecomb.set_cmdline_option("pc").set_description("Combine phase of channels using a phase correction");
  append_arg(phasecomb,"phasecomb");

  magnexp=0.0;
  magnexp.set_cmdline_option("pe").set_description("Exponent for magnitude weighting when calculating combined phase of all channels");
  append_arg(magnexp,"magnexp");
}


bool RecoChannelSum::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  TinyVector<int,4> inshape(rd.data(Rank<4>()).shape());
  TinyVector<int,3> outshape(inshape(1), inshape(2), inshape(3));
  ODINLOG(odinlog,normalDebug) << "inshape/outshape=" << inshape << "/" << outshape << STD_endl;

  ComplexData<4>& indata=rd.data(Rank<4>());

  int nchan=indata.extent(0);
  ODINLOG(odinlog,normalDebug) << "nchan=" << nchan << STD_endl;

  ComplexData<3>& outdata=rd.data(Rank<3>());
  outdata.resize(outshape);

  if(rd.mode==RecoData::real_data || rd.mode==RecoData::weighted_real_data) {

    outdata=STD_complex(0.0);

    for(unsigned int i=0; i<outdata.size(); i++) {
      TinyVector<int,3> index=outdata.create_index(i);

      float valsum=0.0;
      float relsum=0.0;
      for(int ichan=0; ichan<nchan; ichan++) {
        float val=indata(ichan,index(0),index(1),index(2)).real();
        float rel=indata(ichan,index(0),index(1),index(2)).imag();
        valsum+=val*rel;
        relsum+=rel;
      }
      if(rd.mode==RecoData::weighted_real_data) outdata(index)=STD_complex(secureDivision(valsum, relsum),relsum);
      else                                      outdata(index)=STD_complex(secureDivision(valsum, nchan) ,0.0);
    }

  } else {

    Data<float,3> mag(outshape); mag=0.0;
    Data<float,4> magall(cabs(indata));
    for(int i=0; i<nchan; i++) {
      mag+=magall(i,all,all,all)*magall(i,all,all,all); // take sum of squares for magnitude
    }
    mag=sqrt(mag);

    ODINLOG(odinlog,normalDebug) << "phasecomb=" << bool(phasecomb) << STD_endl;

    if(phasecomb) {

      // Find reference voxel for phase correction
      float meanmag=mean(magall);
      if(meanmag>0.0) magall/=meanmag; // avoid signal under/overflow when building product
      Data<float,3> magprod(outshape); magprod=1.0;
      for(int i=0; i<nchan; i++) {
        magprod*=magall(i,all,all,all);
      }
      TinyVector<int,3> refindex=maxIndex(magprod);
      ODINLOG(odinlog,normalDebug) << "refindex=" << refindex << STD_endl;


      // phase correct each channel and sum up complex data
      ComplexData<3> cplxsum(outshape); cplxsum=STD_complex(0.0);
      for(int i=0; i<nchan; i++) {
        float phaseval=STD_arg(indata(i, refindex(0), refindex(1), refindex(2)));
        STD_complex phasecorr=expc(float2imag(-phaseval));
        if(magnexp>0.0) cplxsum+=phasecorr*indata(i,all,all,all)*pow(magall(i,all,all,all),magnexp);
        else            cplxsum+=phasecorr*indata(i,all,all,all);
      }

      outdata=float2real(mag)*expc(float2imag(phase(cplxsum)));


    } else {
      outdata=float2real(mag);
    }

  }


  return execute_next_step(rd,controller);
}
