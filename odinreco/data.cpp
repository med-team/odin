#include "data.h"

#include <tjutils/tjprofiler.h>

RecoData::~RecoData() {
  if (prof) delete prof;
}

RecoData& RecoData::copy_meta(const RecoData& rd) {
  kcoord=rd.kcoord;
  override_protocol=rd.override_protocol;
  mode=rd.mode;
  interpolated=rd.interpolated;
  return *this;
}

RecoData& RecoData::free(int dim) {
  if(dim==1) data1.free();
  if(dim==2) data2.free();
  if(dim==3) data3.free();
  if(dim==4) data4.free();
  if(dim==5) data5.free();
  if(dim==6) data6.free();
  if(dim==7) data7.free();
  if(dim==8) data8.free();
  return *this;
}

RecoData& RecoData::operator = (const RecoData& rd) {
  data1.reference(rd.data1.copy());
  data2.reference(rd.data2.copy());
  data3.reference(rd.data3.copy());
  data4.reference(rd.data4.copy());
  data5.reference(rd.data5.copy());
  data6.reference(rd.data6.copy());
  data7.reference(rd.data7.copy());
  data8.reference(rd.data8.copy());
  return copy_meta(rd);
}


void RecoData::new_profiler(const STD_string& steplabel) {
#ifdef ODIN_DEBUG
  if(prof) delete prof;
  prof=new Profiler("RecoStep->"+steplabel);
#endif
}
