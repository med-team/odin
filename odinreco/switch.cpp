#include "switch.h"
#include "controller.h"
#include "data.h"

void RecoSwitch::init() {
  brancharg.set_description("A semicolon-separated list of entries in the form 'dim=value {sub-pipeline}' where 'dim' can \
                             be any of the dimensions in recoDim, 'value' is an integer number (or character from templateTypeChar) \
                             and 'sub-pipeline' is a pipeline to be executed when the index of the input in dimension 'dim' matches 'value'. \
                             If the 'dim=value' is omitted, the pipeline will be executed for each incoming data.");
  append_arg(brancharg,"brancharg");
}


bool RecoSwitch::pipeline_init(const RecoController& controller, const RecoCoord& input_coord) {
  Log<Reco> odinlog(c_label(),"pipeline_init");

  svector branchstr(tokens(brancharg,';','{','}'));

  for(unsigned int ibranch=0; ibranch<branchstr.size(); ibranch++) {
    ODINLOG(odinlog,normalDebug) << "branchstr[" << ibranch << "]=" << branchstr[ibranch] << STD_endl;

    STD_string sublinestr=extract(branchstr[ibranch], "{", "}", true);
    STD_string selectstr=shrink(rmblock(branchstr[ibranch], "{", "}", true, true, false, true));

    ODINLOG(odinlog,normalDebug) << "sublinestr=" << sublinestr << STD_endl;
    ODINLOG(odinlog,normalDebug) << "selectstr=" << selectstr << STD_endl;

    if(sublinestr!="") {

      if(selectstr=="") {

        RecoStep* pipeline=controller.create_pipeline(sublinestr,&input_coord);
        if(!pipeline) return false;
        unconditional.push_back(pipeline);

      } else {

        svector selecttoks(tokens(selectstr,'='));
        if(selecttoks.size()==2) {
          STD_string dimstr=shrink(selecttoks[0]);
          int dim=-1;
          for(int idim=0; idim<n_recoDims; idim++) {
            if(dimstr==STD_string(recoDimLabel[idim])) dim=idim;
          }
          if(dim>=0) {

            BranchKey sel;
            BranchValue val;
            sel.dim=recoDim(dim);
            sel.val=kSpaceCoord::string2index(selecttoks[1], recoDim(dim));

            RecoCoord coord=input_coord;
            coord.index[sel.dim].set_mode(RecoIndex::single);
            coord.index[sel.dim]=sel.val; // for RecoIndex::compatible
            val.step=controller.create_pipeline(sublinestr,&coord);
            ODINLOG(odinlog,normalDebug) << "create_pipeline(" << sublinestr << " , " << coord.print() << ")=" << (void*)val.step << STD_endl;
            if(!val.step) return false;

            ODINLOG(odinlog,normalDebug) << "subline/sublinestr=" << (void*)val.step << "/" << sublinestr << STD_endl;
            branches[sel]=val;

          } else {
            ODINLOG(odinlog,errorLog) << "Dimension with label >" << dimstr << "< not found" << STD_endl;
            return false;
          }
        } else {
          ODINLOG(odinlog,errorLog) << "No comparison in selector >" << selectstr << "<" << STD_endl;
          return false;
        }
      }

    }
  }

  return true;
}



bool RecoSwitch::query(RecoQueryContext& context) {
  Log<Reco> odinlog(c_label(),"query");

  if(context.mode==RecoQueryContext::print) {
    ODINLOG(odinlog,infoLog) << "switch (" << STD_endl;
  }

  int iuncond=0;
  for(STD_list<RecoStep*>::iterator it=unconditional.begin(); it!=unconditional.end(); ++it) {
    // separate context objects for sub-branches to have different printprefixes
    RecoQueryContext context_copy(context);
    if(context_copy.printprefix!="") context_copy.printprefix+=",";
    context_copy.printprefix+=itos(iuncond);
    if(!(*it)->query(context_copy)) return false;
    iuncond++;
  }

  for(BranchMap::iterator mapit=branches.begin(); mapit!=branches.end(); ++mapit) {

    recoDim dim=mapit->first.dim;
    unsigned short selection=mapit->first.val;

    // separate context objects for sub-branches to have separate ranges according to selection and different printprefixes
    RecoQueryContext context_copy(context);
    context_copy.coord.index[dim].set_mode(RecoIndex::single);
    context_copy.coord.index[dim]=selection; // for RecoIndex::prep_count
    if(context_copy.printprefix!="") context_copy.printprefix+=",";
    context_copy.printprefix+=STD_string(recoDimLabel[dim])+"="+kSpaceCoord::index2string(selection, recoDim(dim));

    // creating numof in this branch
    STD_string printed_numof;
    if(!context.controller.create_numof(context_copy.coord, mapit->second.numof, printed_numof)) return false;
    context_copy.printpostfix=printed_numof;

    ODINLOG(odinlog,normalDebug) << "Querying branch " << context_copy.printprefix << STD_endl;
    if(!mapit->second.step->query(context_copy)) return false;
  }

  if(context.mode==RecoQueryContext::print) {
    ODINLOG(odinlog,infoLog) << ")" << STD_endl;
  }

//  return true; // pipeline ends here
  return RecoStep::query(context);
}



bool RecoSwitch::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  for(STD_list<RecoStep*>::iterator it=unconditional.begin(); it!=unconditional.end(); ++it) {
    RecoData rdcopy(rd); // use copy since original rd will be re-used here or for branches
    if(!(*it)->process(rdcopy,controller)) return false;
  }

  // go through available branches and check for selection match
  unsigned int match=0;
  for(BranchMap::iterator mapit=branches.begin(); mapit!=branches.end(); ++mapit) {

    recoDim dim=mapit->first.dim;
    unsigned short selection=mapit->first.val;

    bool single_selected = (rd.coord().index[dim]==selection);
    ODINLOG(odinlog,normalDebug) << "single_selected=" << single_selected << STD_endl;
    if(single_selected) {
      RecoData rdcopy(rd); // use copy since original rd will be re-used in remaining part of pipeline
      rdcopy.coord().index[dim].set_mode(RecoIndex::single);
      rdcopy.coord().index[dim]=selection; // for RecoIndex::prep_count
      for(int idim=0; idim<n_recoDims; idim++) rdcopy.coord().index[idim].set_numof(mapit->second.numof[idim]); // updating numof in this branch
      if(!mapit->second.step->process(rdcopy,controller)) return false;
      match++;
    }
  }

  if(match>1) {
    ODINLOG(odinlog,errorLog) << "More than one branch matches" << STD_endl;
    return false;
  }

//  return true; // pipeline ends here
  return execute_next_step(rd, controller);
}


