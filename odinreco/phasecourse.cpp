#include "phasecourse.h"
#include "data.h"

#include <odindata/fitting.h>


bool RecoPhaseCourse::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<5>& indata=rd.data(Rank<5>());

  TinyVector<int,5> inshape=indata.shape();
  int nchan=inshape(0);
  int nrep=inshape(1);
  int nline3d=inshape(2);
  int nphase=inshape(3);
  int nread =inshape(4);


  TinyVector<int,4> outshape(nrep, nline3d, nphase, nread);
  ODINLOG(odinlog,normalDebug) << "outshape=" << outshape << STD_endl;

  Data<float,4> outdata(outshape);


  Data<float,1> unwrapped(nrep);
  ComplexData<1> cplx1d(nrep);
  ComplexData<1> cplxsum(nrep);

  LinearFunction linf;

  for(int iline3d=0; iline3d<nline3d; iline3d++) {
    for(int iphase=0; iphase<nphase; iphase++) {
      for(int iread=0; iread<nread; iread++) {

        cplxsum=STD_complex(0.0);
        for(int ichan=0; ichan<nchan; ichan++) {
          cplx1d=indata(ichan,all,iline3d,iphase,iread);

          unwrapped=cplx1d.phasemap();

          // subtract linear drift and offset
          linf.fit(unwrapped);
          for(int irep=0; irep<nrep; irep++) unwrapped(irep)-=irep*linf.m.val+linf.c.val;

          cplxsum=cplxsum+float2real(cabs(cplx1d))*expc(float2imag(unwrapped)); // sum up complex signal

        }

        outdata(all,iline3d,iphase,iread)=cplxsum.phasemap();

      }
    }
  }

  indata.free();

  rd.mode=RecoData::real_data; // Results can be negative
  rd.data(Rank<4>()).reference(float2real(outdata));

  return execute_next_step(rd,controller);
}
