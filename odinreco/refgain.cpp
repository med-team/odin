#include "refgain.h"
#include "data.h"
#include "controller.h"

#include <odindata/fitting.h>


#define _BRIM_POW_ 10

struct RefgainFunction : public ModelFunction {

  fitpar gain0;
  double A,C;
  double start_gain,brim_gain_delta;
  
  float evaluate_f(float x) const {
    return A * ( fabs( sin( C * pow( 10.0, -(x-gain0.val)/20.0 ) ) ) + pow((gain0.val-start_gain)/brim_gain_delta,_BRIM_POW_) );
  }

  fvector evaluate_df(float x) const {
    fvector result(numof_fitpars());
    double power=pow( 10.0, -(x-gain0.val)/20.0 );
    double sinus=sin( C * power );
    double cosinus=cos( C * power );
    result[0] = A *( C*log(10.0)/(20.0) * power * cosinus * sinus  +
                                      1.0/(_BRIM_POW_*brim_gain_delta) * pow((gain0.val-start_gain)/brim_gain_delta,_BRIM_POW_-1)  )/fabs(sinus);
    return result;
  }

  unsigned int numof_fitpars() const {return 1;}

  fitpar& get_fitpar(unsigned int i) {
    if(i==0) return gain0;
    return dummy_fitpar;
  }

};


///////////////////////////////////////////////////////////////////////////


int first_max_index(const Array<float,1>& vec, float thresh) {
  int n=vec.numElements();
  if(n==0) return 0;
  float max=vec(0);
  int maxindex=n-1;
  bool maxfound=false;
  int i;
  for(i=1;i<(n-1);i++) {
    if(vec(i)>max && vec(i+1)<vec(i) && vec(i)>thresh) {
      maxfound=true;
      break;
    }
  }
  if(maxfound) maxindex=i;
  else maxindex=n-1;
  return maxindex;
}

///////////////////////////////////////////////////////////////////////////

int first_min_index(const Array<float,1>& vec, float thresh) {
  int n=vec.numElements();
  if(n==0) return 0;
  float min=vec(0);
  int minindex=n-1;
  bool minfound=false;
  int i;
  for(i=1;i<(n-1);i++) {
    if(vec(i)<min && vec(i+1)>vec(i) && vec(i)>thresh) {
      minfound=true;
      break;
    }
  }
  if(minfound) minindex=i;
  else minindex=n-1;
  return minindex;
}

///////////////////////////////////////////////////////////////////////////

void RecoRefGain::init() {

  pulsedur=0.0;
  pulsedur.set_description("Pulse duration");
  append_arg(pulsedur,"pulsedur");

  pulsegain=0.0;
  pulsegain.set_description("Pulse gain");
  append_arg(pulsegain,"pulsegain");
}



#define NOISELEVEL_FIRSTMAX 0.2
#define BRIM_REFGAIN 3.0
#define MIN_REFGAIN -1000.0
#define MAX_REFGAIN 1000.0
#define _REFERENCE_GAIN_BP_PARAVISION_  0.9


bool RecoRefGain::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  Range all=Range::all();

  ComplexData<4>& indata=rd.data(Rank<4>());
  Data<float,4> magn(cabs(indata));
  TinyVector<int,4> inshape=indata.shape();

  float noiselevel=0.2*max(magn);

  int nrefg=inshape(0);
  int nline3d=inshape(1);
  int nline=inshape(2);
  int nread=inshape(3);
  ODINLOG(odinlog,normalDebug) << "nrefg/nline3d/nline/nread=" << nrefg << "/" << nline3d << "/" << nline << "/" << nread << STD_endl;

  RefgainFunction refgf;
  FunctionFitDerivative refgfit;
  if(!refgfit.init(refgf,nrefg)) return false;

  refgf.C=pulsedur*0.5*PII;

  int anglecounter;
  double refgain;
  double absval;

  double weightsum=0.0;
  double refgsum=0.0;


  dvector attenuations=controller.dim_values(userdef);
  if(int(attenuations.size())!=nrefg) {
    ODINLOG(odinlog,errorLog) << "attenuations.size()=" << attenuations.size() << " does not match nrefg=" << nrefg << STD_endl;
    return false;
  }

  Data<float,1> pixel(nrefg);
  Data<float,1> yvals(nrefg);
  Data<float,1> xvals(nrefg);
  for(int i=0; i<nrefg; i++) xvals(i)=attenuations[i];

  Data<float,3> refgmap(nline3d,nline,nread);

  for(int iline3d=0; iline3d<nline3d; iline3d++) {
    for (int j=0;j<nline;j++) {
      for (int i=0;i<nread;i++) {
        absval=max(magn(all,iline3d,j,i));
        if( absval > noiselevel ) {

          pixel=magn(all,iline3d,j,i);

          int minmaxindex=0;
          int firstmaxindex=0;
          anglecounter=0;

          // find the first maximum above the noise threshhold
          float threshhold=NOISELEVEL_FIRSTMAX*max(pixel);
          minmaxindex=first_max_index(pixel,threshhold);
          xvals(anglecounter)=pow(10.0,-attenuations[minmaxindex]/20.0);
          yvals(anglecounter)=(double)(anglecounter+1);
          anglecounter++;
          firstmaxindex=minmaxindex;


          // if only one max is found, calculate refgain according to a 90deg pulse
          refgain=0.0;
          if(anglecounter==1) {
            refgain=attenuations[firstmaxindex]+20.0*log10(secureInv(pulsedur))-pulsegain;
          }


          // non-linear fit
          refgf.A=pixel(firstmaxindex);
          refgf.start_gain=refgain+pulsegain;
          refgf.brim_gain_delta=BRIM_REFGAIN;
          refgf.gain0.val=refgain+pulsegain;
          refgfit.fit(pixel,Array<float,1>(0),xvals,10000,0.001);
          refgain=refgf.gain0.val-pulsegain;

          if(refgain<MIN_REFGAIN) refgain=0.0;
          if(refgain>MAX_REFGAIN) refgain=0.0;
          if(refgain!=0.0) {
            double fitweight=secureInv(refgf.gain0.err);
            refgsum+=refgain*fitweight;
            weightsum+=fitweight;
          }
          refgmap(iline3d,j,i)=refgain+_REFERENCE_GAIN_BP_PARAVISION_;
        } else {
          refgmap(iline3d,j,i)=0.0;
        }
      }
    }
  }

  double avrefgain=secureDivision(refgsum,weightsum);
  ODINLOG(odinlog,infoLog) << "average refgain=" << avrefgain << STD_endl;

  STD_string valfile=STD_string(getenv_nonnull("HOME"))+"/.odin/opdir/odin_refgain";
  ::write(ftos(avrefgain),valfile);
  system(("chmod 666 "+valfile).c_str());

  ComplexData<3>& outdata=rd.data(Rank<3>());
  outdata.resize(nline3d,nline,nread);
  outdata=float2real(refgmap);

  return execute_next_step(rd,controller);
}
