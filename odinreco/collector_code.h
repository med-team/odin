#include "collector.h"


template<unsigned int Nin, class In, unsigned int Ncoll, class Coll, bool use_numof>
bool RecoCollector<Nin,In,Ncoll,Coll,use_numof>::process(RecoData& rd, RecoController& controller) {
  Log<Reco> odinlog(c_label(),"process");

  TinyVector<int,Ncoll> index;
  Coll::create_index(rd.coord(), index);

  TinyVector<int,Ncoll> shape;
  Coll::create_shape(rd.coord(), shape);

  // create shape of collected data
  TinyVector<int,Ncoll+Nin> collshape;
  for(int i=0; i<int(Ncoll); i++) collshape(i)=shape(i);
  for(int i=0; i<int(Nin); i++) collshape(Ncoll+i)=rd.data(Rank<Nin>()).extent(i);
  ODINLOG(odinlog,normalDebug) << "index/shape/collshape=" << index << "/" << shape << "/"  << collshape << STD_endl;
  if(!product(collshape)) {
    ODINLOG(odinlog,errorLog) << "Zero-size shape " << collshape << STD_endl;
    return false;
  }

  // adjust coordinate to output dims for correct indexing of todo map
  modify_coord(rd.coord());

  // create new view of input data
  ComplexData<Ncoll+Nin> indata;
  rd.data(Rank<Nin>()).convert_to(indata); // bring to same rank

  // create domains for copying input data to the correct position
  TinyVector<int,Ncoll+Nin> lowin=0;
  TinyVector<int,Ncoll+Nin> uppin=0;

  TinyVector<int,Ncoll+Nin> lowdst=0;
  TinyVector<int,Ncoll+Nin> uppdst=0;

  for(int i=0; i<int(Ncoll); i++) lowdst(i)=uppdst(i)=index(i);
  for(int i=0; i<int(Nin); i++) uppin(Ncoll+i)=uppdst(Ncoll+i)=rd.data(Rank<Nin>()).extent(i)-1;
  ODINLOG(odinlog,normalDebug) << "lowin/uppin=" << lowin << "/" << uppin << STD_endl;
  ODINLOG(odinlog,normalDebug) << "lowdst/uppdst=" << lowdst << "/" << uppdst << STD_endl;

  RectDomain<Ncoll+Nin> indomain(lowin,uppin);
  RectDomain<Ncoll+Nin> dstdomain(lowdst,uppdst);

  // fill data
  mutex.lock();
  ComplexData<Ncoll+Nin>& dataref=datamap[rd.coord()];
  if(dataref.shape()!=collshape) {
    dataref.resize(collshape);
    dataref=STD_complex(0.0); // Initialize to zero for partial k-space acquisition
  }
  dataref(dstdomain)=dataref(dstdomain)+indata(indomain);
  mutex.unlock();

  unsigned int numof_count=0;
  if(use_numof) numof_count=product(shape);

  if( completed(rd.coord(),numof_count) ) {
    ODINLOG(odinlog,normalDebug) << " data " << collshape << " complete" << STD_endl;

    // fill rd with collected data
    mutex.lock();
    typename DataMap::iterator it=datamap.find(rd.coord()); // typename is necessary here for GCC4
    rd.data(Rank<Nin>()).free(); // deallocate input data
    rd.data(Rank<Ncoll+Nin>()).reference(it->second);
    datamap.erase(it); // delete entry from map to save memory and to trigger initialization if data is needed again
    mutex.unlock();

    if(!execute_next_step(rd,controller)) return false;
  }

  return true;
}



template<unsigned int Nin, class In, unsigned int Ncoll, class Coll, bool use_numof>
bool RecoCollector<Nin,In,Ncoll,Coll,use_numof>::query(RecoQueryContext& context) {
  Log<Reco> odinlog(c_label(),"query");

  if(context.mode==RecoQueryContext::prep) {
    if(!use_numof) {
      RecoCoord outmask=context.coord;
      modify_coord(outmask);
      if(!calculate_count(context, context.coord, outmask)) return false;
    }
  }

  return RecoStep::query(context);
}
